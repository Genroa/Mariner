"use strict";

import fs from "fs";
import path from "path";
import { readdir } from "fs/promises";
import ExcelJS from "exceljs";

const getDirectories = async (source) =>
  (await readdir(source, { withFileTypes: true })).filter((dirent) => dirent.isDirectory()).map((dirent) => dirent.name);

const getFiles = async (source) =>
  (await readdir(source, { withFileTypes: true })).filter((dirent) => !dirent.isDirectory()).map((dirent) => dirent.name);

const isAspectFile = (fileName, fileObj) => fileName.startsWith("aspects.");
const isElementFile = (fileName, fileObj) => !fileName.startsWith("aspects.") && fileObj["elements"] != undefined;
const isRecipeFile = (fileName, fileObj) => fileObj["recipes"] != undefined;

const wb = new ExcelJS.Workbook();

const aspectSheet = wb.addWorksheet("Aspects", {
  views: [{ state: "frozen", xSplit: 2, ySplit: 1 }],
});
aspectSheet.columns = [
  { header: "Id", key: "id", width: 0 },
  { header: "Label", key: "label", width: 20 },
  {
    header: "Description",
    key: "description",
    width: 50,
    style: { alignment: { wrapText: true } },
  },
];

const elementSheet = wb.addWorksheet("Cards", {
  views: [{ state: "frozen", xSplit: 2, ySplit: 1 }],
});
elementSheet.columns = [
  { header: "Id", key: "id", width: 0 },
  { header: "Label", key: "label", width: 50 },
  {
    header: "Description",
    key: "description",
    width: 50,
    style: { alignment: { wrapText: true } },
  },
];

const recipeSheet = wb.addWorksheet("Recipes", {
  views: [{ state: "frozen", xSplit: 2, ySplit: 1 }],
});
recipeSheet.columns = [
  { header: "Id", key: "id", width: 0 },
  { header: "Label", key: "label", width: 50 },
  {
    header: "Description during warmup",
    key: "startdescription",
    width: 50,
    style: { alignment: { wrapText: true } },
  },
  {
    header: "Description when the action-chain ends",
    key: "description",
    width: 50,
    style: { alignment: { wrapText: true } },
  },
];

function logAspectFile(aspects) {
  for (const aspect of aspects) {
    if (!aspect.label && !aspect.description) continue;
    aspectSheet.addRow(aspect);
  }
}
function logElementFile(elements) {
  for (const element of elements) {
    if (!element.label && !element.description) continue;
    elementSheet.addRow(element);
  }
}
function logRecipeFile(recipes) {
  for (const recipe of recipes) {
    if (!recipe.label && !recipe.startdescription && !recipe.description) continue;
    recipeSheet.addRow(recipe);
  }
}

function logFile(directories, file) {
  const fileObj = JSON.parse(fs.readFileSync(directories.join(path.sep) + path.sep + file));

  // console.log(fileContent)
  if (isAspectFile(file, fileObj)) {
    logAspectFile(fileObj.elements.filter((el) => !el.isHidden));
  } else if (isElementFile(file, fileObj)) {
    logElementFile(fileObj.elements);
  } else if (isRecipeFile(file, fileObj)) {
    logRecipeFile(fileObj.recipes);
  }
}

async function readAndLogDirectory(directoryPath) {
  const files = await getFiles(directoryPath.join(path.sep));
  const directories = await getDirectories(directoryPath.join(path.sep));
  console.log("READING DIRECTORY", directoryPath);
  console.log("Found directories:", directories);

  for (const file of files) {
    console.log("READING FILE", file);
    logFile(directoryPath, file);
  }

  for (const directory of directories) {
    await readAndLogDirectory([...directoryPath, directory]);
  }
}

await readAndLogDirectory(["content"]);

await wb.csv.writeFile("strings_aspects.csv", { sheetName: "Aspects" });
await wb.csv.writeFile("strings_cards.csv", { sheetName: "Cards" });
await wb.csv.writeFile("strings_recipes.csv", { sheetName: "Recipes" });
await wb.xlsx.writeFile("strings.xlsx");
