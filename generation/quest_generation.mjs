import { ME_RECIPE } from "./generation_helpers.mjs";

function computeIconName(index, update) {
  // if (typeof update.icon === "string") return update.icon;
  return `quests/mariner.questupdates.${update.icon ?? index + 1}`;
}

function computeXtriggers(id, updates) {
  mod.initializeRecipeFile(`recipes.updatequest.${id}`, ["quests"]);
  const xtriggers = {};

  for (const update of updates) {
    const questCardId = `mariner.quests.${id}`;
    const signalId = `mariner.questsignal.${id}.applyupdate.${update.id}`;
    const updateAspectId = `mariner.globalflags.quest.${id}.${update.id}`;
    mod.setHiddenAspect(`aspects.quests.${id}`, { id: signalId });

    mod.setRecipe(`recipes.updatequest.${id}`, {
      id: `mariner.updatequest.${id}.applyupdate.${update.id}`,
      furthermore: [
        // Set the flag at the root
        // Add the aspect to the quest card
        {
          target: "~/visible",
          rootAdd: {
            [updateAspectId]: 1,
          },
          mutations: [
            {
              filter: questCardId,
              mutate: updateAspectId,
              level: 1,
              additive: true,
            },
          ],
        },
      ],
    });
    const recipe = mod.getRecipe(`recipes.updatequest.${id}`, `mariner.updatequest.${id}.applyupdate.${update.id}`);

    // Add the potential effects defined on that update
    if (update.effects) {
      if (Array.isArray(update.effects)) recipe.furthermore.push(...update.effects);
      else recipe.furthermore.push(update.effects);
    }
    xtriggers[signalId] = ME_RECIPE(`mariner.updatequest.${id}.applyupdate.${update.id}`);
  }
  return xtriggers;
}

function computeSlots(id, updates) {
  return updates
    .filter((s) => s.slots)
    .map((update) => {
      return update.slots.map((s) => ({
        ifAspectsPresent: { [`mariner.globalflags.quest.${id}.${update.id}`]: 1 },
        actionId: s.actionId ?? "talk",
        fromSlot: "noninitial",
        ...s,
      }));
    })
    .flat();
}

export function generateQuest({ id, label, description, updates }) {
  if (!mod.elements[`quests`]) mod.initializeElementFile("quests", ["quests"]);

  //0. generate generic quest aspect(s)
  if (!mod.elements[`aspects.quests`]) {
    mod.initializeAspectFile(`aspects.quests`, ["quests"]);
    mod.setAspect("aspects.quests", {
      id: `mariner.quest`,
      label: "A Thread",
      description: "A thread to pursue in the tapestry of lives and stories that together weave the world.",
    });
  }

  mod.initializeAspectFile(`aspects.quests.${id}`, ["quests"]);
  //1. Generate the aspects for each update
  for (const [index, update] of updates.entries()) {
    mod.setAspect(`aspects.quests.${id}`, {
      id: `mariner.globalflags.quest.${id}.${update.id}`,
      isHidden: update.isHidden,
      label: update.label,
      description: update.description,
      icon: computeIconName(index, update),
      sort: `_${String(update.sort ?? index).padStart(2, "0")}`,
    });
  }

  //2. Generate the card element with all its xtriggers and related recipes
  mod.setElement("quests", {
    id: QUEST_ID(id),
    label,
    description,
    unique: true,
    aspects: { "mariner.topic": 1, "mariner.quest": 1 },
    xtriggers: computeXtriggers(id, updates),
    // Generate conversation slots for each update, if they exist
    slots: computeSlots(id, updates),
  });
}

export function UPDATE_QUEST(questId, updateIdToAdd) {
  return {
    target: "~/visible",
    aspects: {
      [`mariner.questsignal.${questId}.applyupdate.${updateIdToAdd}`]: 1,
    },
  };
}

export function QUEST_ID(questId) {
  return `mariner.quests.${questId}`;
}
