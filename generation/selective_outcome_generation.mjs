
/*
Generation:
- generate a card for each outcome
- generate the deck that will be used
- generate the recipes
*/

/*
Order of execution:
1. draw all outcome cards
// 2. discard all outcome cards that cannot be fulfilled
3. discard all outcome cards that point to a recipe whose requirements are not met
4. If you have no cards left, stop. Otherwise, discard all cards but one. One card remains
5. route to the recipe for that card.
*/

export class SelectiveOutcomeGenerator {
    mod;
    prefix;
    filePath = [];
    ELEMENTS_FILE = null;
    ASPECTS_FILE = null;
    RECIPES_FILE = null;
    DECKS_FILE = null;

    outcomes = [];
    generalFilters = [];
    DECK_ID = null;
    CARD_ASPECT = null;
    ROUTER_ID = null;

    noOutcomeLinkedRecipe = null;

    constructor({ filePath, prefix, aspectsFile, recipesFile, elementsFile, decksFile, noOutcomeLinkedRecipe }) {
        if (!prefix || !mod) throw new Error("Prefix and mod are mandatory fields");
        this.mod = mod;
        this.prefix = prefix;
        this.ELEMENTS_FILE = elementsFile ?? `${prefix}`;
        this.RECIPES_FILE = recipesFile ?? `recipes.${prefix}`;
        this.ASPECTS_FILE = aspectsFile ?? `aspects.${prefix}`;
        this.DECKS_FILE = decksFile ?? `decks.${prefix}`;

        this.filePath = filePath ?? [];
        if (!elementsFile) mod.initializeElementFile(this.ELEMENTS_FILE, this.filePath)
        if (!aspectsFile) mod.initializeAspectFile(this.ASPECTS_FILE, this.filePath)
        if (!recipesFile) mod.initializeRecipeFile(this.RECIPES_FILE, this.filePath)
        if (!decksFile) mod.initializeDeckFile(this.DECKS_FILE, this.filePath)

        this.DECK_ID = `mariner.${prefix}.outcomesdeck`;
        this.CARD_ASPECT = `mariner.${prefix}.outcomecardaspect`;
        this.ROUTER_ID = `mariner.${prefix}.outcomeselection.filterrouter`;

        this.noOutcomeLinkedRecipe = noOutcomeLinkedRecipe ?? null;
    }

    addGeneralFilter(filter) {
        this.generalFilters.push(filter);
    }

    addOutcome({ id, filterRecipe, recipeId, aspects }) {
        if (!id) throw new Error("'id' is a mandatory property when adding an outcome")
        if (!recipeId) throw new Error("'recipeId' is a mandatory property when adding an outcome")
        if (!aspects) throw new Error("'aspects' is a mandatory property when adding an outcome")
        if (filterRecipe && filterRecipe.id) throw new Error("outcome-specific filters should not define an id")
        this.outcomes.push({ id, filterRecipe, recipeId, aspects })
    }

    getFilters() {
        const outcomeFilters = this.outcomes.filter(o => o.filterRecipe !== undefined).map(o => ({ id: o.id, ...o.filterRecipe }));
        return [...(this.generalFilters ?? []), ...outcomeFilters]
    }

    generateOutcomeElement(outcome) {
        this.mod.setElement(this.ELEMENTS_FILE, {
            id: this.outcomeCardId(outcome.id),
            aspects: {
                [this.CARD_ASPECT]: 1,
                ...outcome.aspects
            }
        })
    }

    outcomeCardId(id) {
        return `mariner.${this.prefix}.${id}.outcomecard`;
    }

    generateTechnicalAspects() {
        this.mod.setHiddenAspect(this.ASPECTS_FILE, {
            id: this.CARD_ASPECT
        })
    }

    generateOutcomesDeckAndElements() {
        this.generateTechnicalAspects();

        for (const outcome of this.outcomes) {
            this.generateOutcomeElement(outcome);
        }

        this.mod.setDeck(this.DECKS_FILE, {
            id: this.DECK_ID,
            spec: this.outcomes.map(({ id }) => this.outcomeCardId(id)),
            resetonexhaustion: true,
            shuffleAfterDraw: true
        })
    }

    generatePostFilteringExecution() {
        this.mod.setRecipe(this.RECIPES_FILE, {
            id: `mariner.${this.prefix}.outcomeselection.finishedfiltering`,
            requirements: { [this.CARD_ASPECT]: -2 },
            linked: [
                ...this.outcomes.map(({ id, recipeId }) => ({
                    id: `mariner.${this.prefix}.outcomeselection.selectandroute.${id}`,
                    requirements: { [this.outcomeCardId(id)]: 1 },
                    effects: { [this.CARD_ASPECT]: -10 },
                    linked: [{ id: recipeId }, { id: `mariner.${this.prefix}.outcomeselection.novalidoutcome` }]
                })),
                { id: `mariner.${this.prefix}.outcomeselection.novalidoutcome` }
            ]
        })
    }

    generateExecutionRecipes() {
        // 1. draw all outcome cards
        this.mod.setRecipe(this.RECIPES_FILE, {
            id: `mariner.${this.prefix}.outcomeselection.start`,
            deckeffects: {
                [this.DECK_ID]: this.outcomes.length
            },
            linked: [{ id: this.ROUTER_ID }]
        })

        // 2. discard all outcome cards that cannot be fulfilled (looping back router pattern)
        this.mod.setRecipe(this.RECIPES_FILE, {
            id: this.ROUTER_ID,
            linked: [
                ...this.getFilters().map(({ id }) => ({ id: `mariner.${this.prefix}.outcomefilters.${id}` })),
                { id: `mariner.${this.prefix}.outcomeselection.filterremainingoutcomes` },
                { id: `mariner.${this.prefix}.outcomeselection.finishedfiltering` }
            ]
        })

        // 3. discard all outcome cards that point to a recipe whose requirements are not met
        if (this.generalFilters) {
            for (const filter of this.generalFilters) {
                this.mod.setRecipe(this.RECIPES_FILE, {
                    ...filter,
                    id: `mariner.${this.prefix}.outcomefilters.${filter.id}`,
                    linked: [{ id: this.ROUTER_ID }]
                })
            }
        }
        for (const outcome of this.outcomes) {
            if (!outcome.filterRecipe) continue;
            this.mod.setRecipe(this.RECIPES_FILE, {
                id: `mariner.${this.prefix}.outcomefilters.${outcome.id}`,
                linked: [{ id: this.ROUTER_ID }],
                ...outcome.filterRecipe,
                requirements: { [this.outcomeCardId(outcome.id)]: 1, ...(outcome.filterRecipe.requirements ?? {}) }, //this line stops the endless looping filter bug. 
                effects: {
                    [this.outcomeCardId(outcome.id)]: -100,
                    ...outcome.filterRecipe.effects
                }
            })
        }

        // 4. If you have no cards left, stop. Otherwise, discard all cards but one. One card remains
        this.mod.setRecipe(this.RECIPES_FILE, {
            id: `mariner.${this.prefix}.outcomeselection.filterremainingoutcomes`,
            requirements: { [this.CARD_ASPECT]: 2 },
            effects: { [this.CARD_ASPECT]: `-([${this.CARD_ASPECT}]-1)` },
            linked: [
                { id: `mariner.${this.prefix}.outcomeselection.finishedfiltering` },
                { id: `mariner.${this.prefix}.outcomeselection.novalidoutcome` } // shouldn't be there if noOutcomeLinkedRecipe isn't defined
            ]
        })

        // 5. route to the recipe for that card. Intermediary recipe to also clean the card just before we route
        this.generatePostFilteringExecution();

        if (this.noOutcomeLinkedRecipe) {
            this.mod.setRecipe(this.RECIPES_FILE, {
                id: `mariner.${this.prefix}.outcomeselection.novalidoutcome`,
                requirements: { [this.CARD_ASPECT]: -1 },
                linked: [{ id: this.noOutcomeLinkedRecipe }]
            })
        }
    }

    generate() {
        this.generateOutcomesDeckAndElements();
        this.generateExecutionRecipes();
    }
}