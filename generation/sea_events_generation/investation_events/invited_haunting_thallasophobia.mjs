import { generateScaffholding } from "../sea_events/sea_events_helpers.mjs";
export function generateExampleSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("example");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "example label",
    startdescription: "start description",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome` }, { id: `${PREFIX}.outcome.failure` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    tablereqs: {},
    label: "first outcome label",
    startdescription: "first outcome description",
    effects: {},
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "second outcome label",
    startdescription: "second outcome description",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
