import { INJURE_CREWMEMBER } from "../../generation_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateFireEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("fire");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Fire in the Hull",
    startdescription:
      "A lantern spilled, and a pool of burning oil in the depths of the ship. If we do not act quickly, the fire might continue to spread until nothing of the Kite is left. I could order to dampen the fire, or drown it out with buckets from the sea. [Use Winter or Grail to approach this event, or throw able-bodied men it's way to contain it.]",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.weathersuccess` },
      {
        id: `${PREFIX}.highwindcheck.winter`,
        chance: minChance("winter", 10),
      },
      {
        id: `${PREFIX}.highwindcheck.grail`,
        chance: minChance("grail", 10),
      },
      { id: `${PREFIX}.outcome.success`, chance: minChance("winter") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("grail") },
      {
        id: `${PREFIX}.outcome.success`,
        chance: "10 + 5*[mariner.crew]",
      },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Critical situation check
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.highwindcheck.winter`,
    grandReqs: { "[~/visible : mariner.weathermeta.windy]": 1 },
    linked: [{ id: `${PREFIX}.outcome.success` }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.highwindcheck.grail`,
    tablereqs: { "[~/visible : mariner.weathermeta.windy]": 1 },
    linked: [{ id: `${PREFIX}.outcome.success` }],
  });

  // Success check
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.weathersuccess`,
    tablereqs: { "[~/visible : mariner.weathermeta.rainy]": 1 },
    linked: [{ id: `${PREFIX}.outcome.success` }],
  });

  // Outcomes
  // Success
  generateSimpleSuccessOutcome({
    label: "The Flames Last Whimper",
    startdescription:
      "We have managed to decrease the flames until they are nothing but some glowing timber, and even that soon ceases to spark.",
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    linked: [
      { id: `${PREFIX}.outcome.failure.cargoburnt`, chance: 33 },
      { id: `${PREFIX}.outcome.failure.killedcrew`, chance: 50 },
      { id: `${PREFIX}.outcome.failure.shipdamage` },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure.cargoburnt`,
    tablereqs: { "mariner.cargo": 1 },
    warmup: 20,
    label: "Cargo Damaged",
    startdescription: "Some cargo was lost to the flames.",
    purge: { "mariner.cargo": 1 },
    linked: [{ id: EVENT_START }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure.killedcrew`,
    tablereqs: { "mariner.crew": 1 },
    warmup: 20,
    label: "Lost Someone",
    startdescription: "No horror as visceral as the smell of burning flesh.",
    inductions: [INJURE_CREWMEMBER],
    linked: [{ id: EVENT_START }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure.shipdamage`,
    warmup: 20,
    label: "Ship Damaged",
    startdescription: "Ship sustained a wound.",
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
    linked: [{ id: "mariner.sailing.sinking" }, { id: EVENT_START }],
  });
}
