import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { DISTANCE_SCALED } from "../../sea_generation/generate_sailing_routes.mjs";
import { SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";
import { INJURE_CREWMEMBER } from "../../generation_helpers.mjs";

export function generateSquallEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("squall");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "A Sudden Squall",
    startdescription:
      "the sky is a battlefield, and we will be crushed in the roil of war. The sky is a churning factory, and she drops her munitions upon us readily. The sky is a beast, and bats us about with it's claws. We must brave this storm with Edge, or survive it with Heart.",
    slots: SHIP_MANOEUVERING_SLOTS(),
    linked: [
      {
        id: `${PREFIX}.highwindcheck.edge`,
        chance: minChance("edge", 10),
      },
      {
        id: `${PREFIX}.highwindcheck.heart`,
        chance: minChance("heart", 10),
      },
      { id: `${PREFIX}.outcome.criticalfailure` },
      { id: `${PREFIX}.outcome.crewfailure` },
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.cargofailure` },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Critical situation check
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.highwindcheck.edge`,
    tablereqs: { "mariner.weathermeta.windy": 1 },
    linked: [{ id: `${PREFIX}.outcome.success` }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.highwindcheck.heart`,
    tablereqs: { "mariner.weathermeta.windy": 1 },
    linked: [{ id: `${PREFIX}.outcome.success` }],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "Control the Vessel",
    startdescription:
      "The wind plays with us, but we manoeuvre, turn, dance and fight against it. It will not cost us any time on our travel.",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.cargofailure`,
    warmup: 20,
    label: "one of my crew gets crushed by what we carry.",
    startdescription:
      "The stowed ",
    tablereqs: { "mariner.cargo": 1 },
    effects: { "mariner.sailing.distance": DISTANCE_SCALED(0.4) },
    inductions: [INJURE_CREWMEMBER],
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.cargo": -1 },
      }
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.criticalfailure`,
    warmup: 20,
    tablereqs: { "mariner.weathermeta.stormy": 1 },
    label: "Snapping Sails, Tearing Mast",
    startdescription:
      "The Ship is damaged by the storm, and that violence hurts one of our own.",
    effects: { "mariner.sailing.distance": DISTANCE_SCALED(0.8) },
    inductions: [INJURE_CREWMEMBER],
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.crewfailure`,
    warmup: 20,
    label: "Swept Overboard",
    tablereqs: { "mariner.weathermeta.windy": 1 },
    effects: { "mariner.sailing.distance": DISTANCE_SCALED(0.4) },
    tablereqs: { "mariner.crew": 6 },
    inductions: [INJURE_CREWMEMBER],
    startdescription: "One of ours is swept overboard, and disappears beneath the roiling waves",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Hurt in the Press of the Storm",
    effects: { "mariner.sailing.distance": DISTANCE_SCALED(0.4) },
    inductions: [INJURE_CREWMEMBER],
    startdescription: "We have been slowed, significantly, and one of ours has got his hand mangled in between the lines.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
