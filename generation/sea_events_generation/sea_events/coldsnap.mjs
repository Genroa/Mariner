import { INJURE_CREWMEMBER } from "../../generation_helpers.mjs";
import { CHALLENGE_SLOTS, SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { minChance, generateScaffholding } from "./sea_events_helpers.mjs";

export function generateColdsnapSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("coldsnap");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Cold Spell",
    startdescription:
      "A chill can rise from the waters at sea. The wet cold creeps even through the corridors, and suffuces the Kite to her bones. We should take precautions with Heart's Perseverance or Grails Comfort so it doesn't claim us too.",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.coldsuccess`, chance: minChance("heart", 10) },
      { id: `${PREFIX}.outcome.coldsuccess`, chance: minChance("grail", 10) },
      { id: `${PREFIX}.outcome.coldfailure` },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("grail") },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Bundled Up",
    startdescription:
      "Wrapped in overcoats or sweaters, with strong bitter coffee and strong, cheery companionship, we get through these watches. ",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.coldsuccess`,
    tablereqs: { "mariner.weathermeta.cold": 1 },
    warmup: 20,
    label: "Bundled Up",
    startdescription: "Wrapped in Overcoats or sweaters, with strong bitter coffee and strong, cheery companionship, we get through these watches.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.coldfailure`,
    tablereqs: { "mariner.weathermeta.cold": 1 },
    warmup: 20,
    label: "Hypothermia",
    startdescription: "In the current coldsnap, our preparations were not just insufficient, they were were fatal for one of us.",
    inductions: [INJURE_CREWMEMBER],
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.frail] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 30,
          },
        },
      },
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "The Sniffles",
    startdescription: "The chill entered our noses and mouths last night, and now nestles in our gut.",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.frail] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 30,
          },
        },
      },
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
  });
}
