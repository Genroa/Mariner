import { CALL_FUNCTION } from "../../generate_function_calls.mjs";
import { SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { minChance, generateScaffholding } from "./sea_events_helpers.mjs";
export function generateShipwreckSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("shipwreck");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Remains of Another Vessel",
    slots: SHIP_MANOEUVERING_SLOTS(),
    startdescription:
      "We reach an area where the sea is cluttered like a workshop floor. Ships collided here, and what was once their hull and insides is spilled out to open air and sea. I can approach this challenge with Knock or Forge.",
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("knock") },
      { id: `${PREFIX}.outcome.nothing`, chance: 66 },
      { id: `${PREFIX}.outcome.pain` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Salvage from the Wreck",
    startdescription:
      "The sea carries many gifts, it is said. With net and hook and cable, we fish what can still be of use out of the water.",
    furthermore: [
      ...CALL_FUNCTION("drawreward.precious"),
      { deckeffects: { "mariner.decks.cargo": 2 } },
      { movements: { "~/tabletop": ["mariner.cargo", "mariner.trapping"] } },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.nothing`,
    warmup: 20,
    label: "Nothing of Note",
    startdescription:
      "The Sea is a fickle provider, it is said. We spend some time salvaging, but find nothing worth anything except as a soggy souvenir. We continue.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.pain`,
    warmup: 20,
    label: "Scrape on the Hull",
    startdescription:
      "Moving through this field of debris is a delicate manoeuver, and the constelation of scrap was ill-omened today. A screeching sound and horrible tremble indicate that an unseen danger has breached the hull.",
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: 1,
        additive: true,
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
