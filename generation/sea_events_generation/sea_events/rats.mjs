import { SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { minChance, generateScaffholding } from "./sea_events_helpers.mjs";

export function generateRatsSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("rats");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Rats on the Beams",
    startdescription:
      "Any ship has the odd pest, but the infestation on the kite has grown out of proportion. We need to manage it, and now. [Approach this challenge with Moth or Knock.]",
    linked: [
      //{ id: `${PREFIX}.outcome.instrumentsucces` },
      { id: `${PREFIX}.outcome.success`, chance: minChance("knock") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("moth") },
      { id: `${PREFIX}.outcome.failure` },
    ],
    slots: SEA_EVENT_SLOTS(),
    //slots: [
    //  {
    //    id: "instrument",
    //    label: "An Instrument",
    //    description: "If I believe the old stories...",
    //    required: "mariner.instrument",
    //  },]
  });

  // Outcomes
  //mod.setRecipe(RECIPES_FILE, {
  //  id: `${PREFIX}.outcome.instrumentsucces`,
  //  warmup: 20,
  //  requirements: {"mariner.instrument":1},
  //  label: "That Old Tune",
  //  startdescription:
  //    "first outcome description",
  //  effects: { },
  //  linked: [{ id: "mariner.sailing.postevent" }],
  //});

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Extermination",
    startdescription:
      "With venom, with traps, with tricks and with ingenuity, we make the tight spaces inhospitable to them. We quell their numbers until they can no longer act so bold.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Tide of Vermin",
    effects: { "mariner.cargo": -1 },
    startdescription: "We cannot stem the tide, and some of our cargo is lost to the onslaught.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
