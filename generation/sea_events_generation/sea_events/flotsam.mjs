import { generateScaffholding } from "./sea_events_helpers.mjs";

export function generateFlotsamEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("flotsam");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Flotsam",
    startdescription:
      "Our look out spots something bobbing on the waves, too angular to be a seal. There seems to be some floating debris, potentially the cargo of another ship.",
    linked: [{ id: `${PREFIX}.outcome.success`, chance: 50 }, { id: `${PREFIX}.outcome.failure` }],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "Treasures amids Trash",
    startdescription: "Something of value could be salvaged. We take on board what we can.",
    //TODO: this needs a check if the ship is full
    furthermore: [{ deckeffects: { "mariner.decks.cargo": 2 } }, { movements: { "~/tabletop": ["mariner.cargo"] } }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Half-digested Refuse",
    startdescription: "We cannot find anything of worth amids the broken planks and debris. We leave the sea to her meal.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
