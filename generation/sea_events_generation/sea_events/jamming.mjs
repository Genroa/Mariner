import { generateScaffholding } from "./sea_events_helpers.mjs";

export function generateJammingSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("jamming");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    tablereqs: { "mariner.instrument": 1 },
    warmup: 20,
    label: "Practicing with the Crew",
    startdescription:
      "Usually, I keep my instruments in their room, and practice there alone. But tonight was a night that needed entertainment, and I brought them out to play with the crew.",
    linked: [
      { id: `${PREFIX}.outcome.firstoutcome`, chance: 33 },
      { id: `${PREFIX}.outcome.cleardread`, chance: 33 },
      { id: `${PREFIX}.outcome.clearfasc` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    tablereqs: { "mariner.crew.exhausted": 1 },
    label: "A Joyous Chorus",
    startdescription:
      "Nothing so revitalizing to body and spirit as a night of singing, nothing as restorative to the harmony of a crew as as a collectively sung shanty.",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/tabletop",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]": {
            mutate: "mariner.crew.exhausted",
            level: 0,
            limit: 3,
          },
        },
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.cleardread`,
    warmup: 20,
    label: "Clearing the Air",
    startdescription:
      "As my fingers move and me throat thrums, a knot inside me unfirls. I leave the evening feeling lighter than I did before.",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.experiences.dreadful.standby": -3 },
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.clearfasc`,
    warmup: 20,
    label: "Grounding in my Body",
    startdescription:
      "At the end of the evening, my lips tingle and my body is delightfully soar from dancing. As I lay on my cot, I feel myself sinking into self, settle back into my wrapping of flesh and bones.",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.experiences.vivid.standby": -3 },
      },
    ],
  });
}
