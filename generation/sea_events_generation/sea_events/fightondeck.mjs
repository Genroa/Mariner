import { INJURE_CREWMEMBER } from "../../generation_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateFightingEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("fight");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "A Fight on the Kite",
    startdescription:
      "Raised fists and raised voices. I leave my quarters to find my men squared off on the deck, solving their grievances in a manner that is older than human speech. I could still their actions with Winters authority, or break them apart with the force of Edge. ",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("winter") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.murderrelay`, chance: 30 },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "Authority Restored",
    startdescription:
      "Their fighting spirits have withered before my gaze. Whatever petty complaint, it has been put aside. The sailors are moved to different shifts, and they will not continue their argument until they touch shore again. ",
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.murderrelay`,
    inductions: [INJURE_CREWMEMBER],
    linked: [{ id: `${PREFIX}.outcome.specialfailure` }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.specialfailure`,
    warmup: 20,
    label: "United in the End",
    startdescription:
      "It ended with a strange sort of harmony, as the blades of quarrelsome crewmembers both found purchase, and they shared together their last breaths.",
    effects: { "mariner.crew": -1 },
    inductions: [INJURE_CREWMEMBER],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "A Roused Air",
    startdescription:
      "We managed to break the fight apart, but the argument lingers. Everyone is taking sides, and my crew has lost its sense of cohesion.",
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.boisterous] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 30,
          },
        },
      },
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: "max(0, 3 - [mariner.crew.traits.harmonious])",
          },
        },
      },
    ],
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
