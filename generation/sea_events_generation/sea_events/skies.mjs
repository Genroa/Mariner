import { CALL_FUNCTION } from "../../generate_function_calls.mjs";
import { generateScaffholding } from "./sea_events_helpers.mjs";

export function generateSkySeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("skies");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "A Sky Clear as Diamond",
    tablereqs: { "mariner.weathermeta.clear": 1 },
    startdescription:
      "Certain sights are only reserved for hermits and explorers. Tonight the sky is black velvet overlayed with an affluance of stars. There is nothing to gain or lose here, just a chance to marvel in delight.",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome`, chance: 50 }, { id: `${PREFIX}.outcome.secondoutcome` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "Quiet Stargazing",
    startdescription:
      "I sit on the back of the deck, and trust the running of the ship completely to the crew. I simply enjoy a peaceful moment with the cold far twinkling of the stars.",
    furthermore: CALL_FUNCTION("addgroundedness.30"),
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.secondoutcome`,
    warmup: 20,
    label: "A Sparkle Retained",
    startdescription:
      "Something of that candlelight glimmer has gotten caught in my eyes. The stars still dance in echoes on my retina, and I am sure those sensitive to this will be able to see them glimmer.",
    linked: [{ id: "mariner.sailing.postevent" }],
    effects: { "mariner.influences.lantern": 2 },
  });
}
