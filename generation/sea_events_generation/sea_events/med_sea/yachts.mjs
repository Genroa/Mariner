import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";

export function generateYachtsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("yachts");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "A Ship Alone",
    tablereqs: { "mariner.weathermeta.sunny": 1 },
    grandReqs: { "[~/extant: mariner.locations.medsea.sailingthere]": 1 },
    startdescription:
      'We come up to a vessel, nearly as large as the kite, but as white as snow, shining in the sun. No crew is seen on deck, only men and women languishing in the shade or baking in the sun. In the water, in a small sloops, a man drifts equipped with neither paddle nor fear raises. He raises a glass, sparkling diamonds in the sun. "A toast to the bounty of the seas."',
    linked: [{ id: `${PREFIX}.outcome.talk.router` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.router`,
    warmup: 20,
    linked: [
      { id: `${PREFIX}.outcome.talk.reward.rumour`, chance: 33 },
      { id: `${PREFIX}.outcome.talk.reward.scrap`, chance: 50 },
      { id: `${PREFIX}.outcome.talk.reward.gifts` },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.reward.rumour`,
    warmup: 20,
    effects: { "mariner.rumour": 1 },
    linked: [{ id: `${PREFIX}.outcome.wrapup` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.reward.scrap`,
    warmup: 20,
    effects: { "mariner.scrapofinformation": 1 },
    linked: [{ id: `${PREFIX}.outcome.wrapup` }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.talk.reward.gifts`,
    warmup: 20,
    effects: { "mariner.trappings.moth": 1 },
    linked: [{ id: `${PREFIX}.outcome.wrapup` }],
  });
  // Outcomes // TODO different outcomes based on notoriety level

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.wrapup`,
    warmup: 20,
    label: "Passing like Ships in the Day",
    startdescription:
      "We do not linger long, nor would we have held their attention for long. But some goods where exchanged. some pleasantries. some smiles.",
    effects: { "mariner.influences.grail": 4 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
