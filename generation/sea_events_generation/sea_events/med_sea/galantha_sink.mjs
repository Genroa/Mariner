import { generateSpecialScaffholding } from "./../sea_events_helpers.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
export function generateGalanthaSunkSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateSpecialScaffholding("galanthasunk");

  mod.setRecipe(RECIPES_FILE, {
    id: `${EVENT_START}`,
    warmup: 20,
    tablereqs: {
      "mariner.galantha.captured": 1,
    },
    grandReqs: { "[~/extant: mariner.locations.medsea.sailingthere]": 1 },
    label: "Another Parting",
    startdescription:
      "<Sereno adviced that the deepest abysses may contain the Ragged Ones the longest, so we sail as far of coast as a day gets us, and on the rceeding tide, drop a bound and blindfolded Galantha into the ocean. The sea, which never never sleeps, which is always hungry, accepts our offering.>",
    purge: { "mariner.galantha.captured": -1 },
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: {
      [QUEST_FLAG_ID("galantha", "sunk")]: 1,
    },
  });
}
