import { INJURE_CREWMEMBER } from "../../../generation_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./../sea_events_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateSightlessFightEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateScaffholding("sightlessfight");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "[~/extant: mariner.locations.medsea.sailingthere]": 1 },
    label: "A Sightless Fight",
    startdescription:
      "A figure stalks the quarters, seemingly unhindered by the new moon night. Several crew members have already found their way to the intruder, and we now find them on the floor, shivering and moaning. Figure advances, slowly, but inexorably, through the halls. What can one do to halt a glacier's path? We can throw ourselves at it with sustained effort of Forge, or forceful tactics of Edge",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge", "8+([root/mariner.galantha.growing]*2)") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("edge", "8+([root/mariner.galantha.growing]*2)") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  mod.setAspect("aspects.quests.bewitch", { id: `mariner.galantha.growing` });
  // Outcomes
  generateSimpleSuccessOutcome({
    label: "Send Them Flying",
    startdescription:
      "Beaten, the entity slinks to the bow of the ship. We pursue, slipping in the darkness on a deck suddenly slick as ice. We can just hear a few words muttered in latin, followed by a splash. As we rush to spark a light, we can see the frost blooming in the path of its escape, and two handprints on the railing, already crisping with ice.. We are left to puzzle on this, and the meaning of the words. A parting shot, a thread, a curse? No one returns to their racks until the sunrise paints the sky a pale glory.",
    rootAdd: {
      "mariner.galantha.growing": 1,
    },
    linked: [
      {
        id: "mariner.sailing.postevent",
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Noon Water Pilfered",
    startdescription:
      "Victorious, the entity glides domineeringly past our wretched forms after all we experienced, it is hard to accept that its footsteps still sound so human.. It heads to the captains quarters, and reemergers, now with the clinking phial. It steps over us once more, but I crawl and stumble after it to the edge of the ship. When I reached the spot it was heading to, it had  already vanished. We cannot deduce where it went. Is some shape slinking away beneath the waves? Is that its shadow passing across the moon, or just a midnight seabird?",
    rootAdd: {
      [QUEST_FLAG_ID("noonwater", "stolen")]: 1,
    },
    linked: [
      {
        id: "mariner.sailing.postevent",
      },
    ],
    furthermore: [
      {
        target: "~/extant",
        effects: {
          "mariner.noonwater": -1,
        },
      },
    ],
  });
}
