import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateBoraEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("bora");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        grandReqs: {
            "[~/exterior : mariner.weathermeta.seasmoke]": -1,
            "[~/extant: mariner.locations.medsea.sailingthere]": 1
        },
        warmup: 20,
        label: "Bora",
        startdescription:
            "On the Mediterranian, it is only the fools and the dead who think the cold is married to quiet and mists to peace. When the Bora rushes in from the Dalmatian Mountains, the vapor is whipped from the sea, and the air is choked in storm that steals all visibility.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.BORA_SET_SEASMOKE]: 1 },
            },
        ],
    });

    // Outcomes
    mod.setRecipe(RECIPES_FILE, {
        id: `${PREFIX}.outcome.success`,
        label: "Asphodel At Sea",
        startdescription:
            "We feel a kinship with the dead. Our world now is a roaring empty, churning cold, and a sightless misery",
        linked: [{ id: "mariner.sailing.seaevents.start.squall" }],
    });
}
