import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateSiroccoEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("sirocco");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        warmup: 20,
        label: "Sirocco",
        grandReqs: {
            "[~/exterior : mariner.weathermeta.sirocco]": -1,
            "[~/extant: mariner.locations.medsea.sailingthere]": 1
        },
        startdescription:
            "The wind blows from the motherland. Miah's lament and the Malachite's bounty. Desert sand clouds the eyes and dries the tongue. We must sail carefully, as the weather changes.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.SIROCCO_SET_SIROCCO]: 1 },
            },
        ],
    });

    // Outcomes
    mod.setRecipe(RECIPES_FILE, {
        id: `${PREFIX}.outcome.success`,
        label: "Decay and Sustain",
        startdescription:
            "The Sirocco blows hot and fickle. stone crumbles to sand crumbles to a fine haze, softenin the sun and scattering the stars.",
        linked: [{ id: "mariner.sailing.seaevents.start.windrises" }],
    });
}
