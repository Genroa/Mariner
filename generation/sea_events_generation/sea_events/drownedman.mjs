import { CALL_FUNCTION } from "../../generate_function_calls.mjs";
import { generateScaffholding } from "./sea_events_helpers.mjs";

export function generateDrownedeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("drownedman");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    jumpstart: true,
    warmup: 20,
    label: "Drowned Drifter",
    startdescription:
      "We spot a shape, floating in the water. At first we think it might be a seal, then later we hope it is. But proximity reveals it the remains of a man, languishing as a rest for gulls.",
    slots: [
      {
        id: "crew",
        label: "Crew",
        description: "Poor soul I send to haul the body out of the water. ofcourse, I could just let the drifter be.",
        required: { "mariner.crew": 1 },
      },
    ],
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: 50 },
      { id: `${PREFIX}.outcome.failure` },
      { id: `${PREFIX}.outcome.nothing` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Clutched Treasures",
    startdescription:
      "We lift the body out of the water, and lay it on the deck. Carefully, we feel in the pockets of the overcoat, and pry what it holds clutched in its hand. We find no identification, but something peculiar does fall bare.",
    requirements: { "mariner.crew": 1 },
    furthermore: CALL_FUNCTION("drawreward.jumble"),
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Grim Portent",
    requirements: { "mariner.crew": 1 },
    startdescription:
      "We see a face, both sallow and bloated. We try to lift, but the matter disintegrates around our rope. Soon we are left with inhuman chunks and floating fabric.",
    effects: { "mariner.experiences.sobering.standby": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        mutations: {
          "[mariner.crew]": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 1,
          },
        },
      },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.nothing`,
    warmup: 20,
    label: "Passing in the Night",
    startdescription: "We let the shape drift by, unhelped or unhindered. We do not say our goodbyes, and turn back to our work.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
