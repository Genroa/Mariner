import { DISTANCE_SCALED } from "../../sea_generation/generate_sailing_routes.mjs";
import { SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { BASE_SUCCESS_CHANCE } from "../generate_seaevents.mjs";
import { generateScaffholding, minChance } from "./sea_events_helpers.mjs";

export function generateFairWeatherEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("fairweather");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Fair Weather",
    startdescription:
      "The sea is a mercurial mistress, but rarely, she grants opportunity. The weathervane turns in our favor today. If we seize this opportunity, we may reap the benefits. Forge for the will to act or let our Heart beat in tune with the world.",
    requirements: {
      "mariner.weathermeta.clear": 1,
    },
    slots: SHIP_MANOEUVERING_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge") },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart") },
      { id: `${PREFIX}.outcome.success`, chance: BASE_SUCCESS_CHANCE },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Flawless Sailing",
    startdescription:
      "The weather is crisp, the wind swells our sails. We travel twice as fast this day, and enjoy the journey fourfold.",
    effects: { "mariner.sailing.distancetravelled": DISTANCE_SCALED(1) },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Merely Good",
    startdescription:
      "Opportunity slips away, but the weather stays true. We travel at the expected rate, and whistle while we work. A journey to enjoy.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
