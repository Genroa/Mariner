import { generateSupernaturalScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateOpeningWavesSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateSupernaturalScaffholding("example");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    jumpstart: true,
    slots: SEA_EVENT_SLOTS(),
    label: "An Opening in the Waves",
    startdescription:
      "The hot breeze toys with the clouds and the brine, teasing it one way, then hammering it into the next shape. It catches my tatoo'd sails, and its roiling patterns clarify into road the sun travels, and above it, a haze herds the clouds. If I can ride that gap in the waves, I can pursue that shimmer. This will require a High amount of Knock.",
    grandReqs: {
      "[~/exterior : mariner.weathermeta.sirocco]": 1,
    },
    linked: [
      {
        id: `${PREFIX}.outcome.firstoutcome`,
        chance: minChance("knock", 10),
      },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    tablereqs: {},
    label: "Slip Beyond",
    startdescription:
      "We slink through the gap, and we can now see the Blazing Galleon sailing the trembling airs between the clouds.The Imago now unfurls sails where once her she soared on her wings. We can spot her scars, bright as a twinned venus, but hotter still we feel the embers of her eyes.",
    linked: [{ id: "mariner.minigames.scoresequence.imago.start" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "This Too Passes",
    startdescription: "A moment of power was here, and now that moment is history. we sail on.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
