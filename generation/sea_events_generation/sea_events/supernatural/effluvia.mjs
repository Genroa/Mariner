import { generateSupernaturalScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";
import { INJURE_CREWMEMBER } from "../../../generation_helpers.mjs";


export function generateEffluviaSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateSupernaturalScaffholding("effluvia");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Effluvia",
    jumpstart: { effects: { "mariner.influences.grail": 10, "mariner.influences.winter": 10 } },
    tablereqs: { "mariner.weathermeta.sickly": 1 },
    startdescription: "A foulness drifts in the air, as black as the billows of Machester or the pit-clouds above Athens. But such sites are not near now, and what clouds we encounter here, not even the wind can stir. Perhaps the smog is bellowing directly from behind the sky. Perhaps there is a power we can distill from here, with enough Forge or Winter.",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome`, chance: 10 },
    { id: `${PREFIX}.outcome.secondoutcome`, chance: minChance("winter", 7) },
    { id: `${PREFIX}.outcome.secondoutcome`, chance: minChance("forge", 7) },
    { id: `${PREFIX}.outcome.failure` }],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "Lethargic Vapor; Unrepentant Ash",
    startdescription: "Within the billowing clouds, something stirs of a wretched intelligence. We believe it may be toying with us; Having us jump at swirls and spying for sparking eyes. We can perhaps convince it to stay, if we barter with bright Lantern or Grail and the Trappings of Moth",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("grail", 7) },
      { id: `${PREFIX}.outcome.success`, chance: minChance("lantern", 7) },
      { id: `${PREFIX}.outcome.nosuccess` }
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.secondoutcome`,
    warmup: 20,
    label: "Memory of Cinder",
    startdescription: "Fuel is consumed, but power lingers. We collect some of the power that sears memory but settles easily within muscle and bone.",
    effects: {
      "mariner.influences.forge": 6
    },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Soot-Kiss",
    startdescription: "The coughs that ring now are more known among miners of the Whelsh lands and smelteries along the Rhine. When this rings, all the doctors would still prescribe is gin and honey and enough time to say good bye.",
    linked: [{ id: "mariner.sailing.postevent" }],
    inductions: [INJURE_CREWMEMBER],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Tempted",
    startdescription: "They have settled in the wind shadow of our sails, sometimes peeping out as a guttering black flag in the wind. we have reached an understanding, and it assures me it is now mine to command. it assures me profusely, and through my own borrowed throat.",
    effects: { "mariner.crew.summon.caligine": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.nosuccess`,
    warmup: 20,
    label: "Evaporated",
    startdescription: "I could not compell them to stay, and they faded with the changing winds",
    effects: {
      "mariner.influences.forge": 2
    },
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
