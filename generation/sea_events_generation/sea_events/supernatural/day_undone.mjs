import { generateSupernaturalScaffholding } from "../sea_events_helpers.mjs";

export function generateDayUndoneSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateSupernaturalScaffholding("example");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "The Day Undone",
    startdescription:
      "The sun is blotted out, and a ring stands in its place as a rememberance. Around it, dim stars flicker uncertain, as the ink of night disperses around the horizon, never quite settling comfortably. any moment it may be ripped away, but this is a moment of great power. However, others regard may also be pointed to this moment, and I may be hurt in trying to claim want was not for me.",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome` }],
  });

  // Outcomes //TODO NEEDS EFFECT
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "Midnight at Noon",
    startdescription:
      "Was this a thread, a promise, an attempted coup? the mechinations of the celestials have resolved themselves, and the normal operations of the heavens resume, but we feel a little less then we were before.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
