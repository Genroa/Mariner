import { DISTANCE_SCALED } from "../../../sea_generation/generate_sailing_routes.mjs";
import { generateSupernaturalScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateOperationMercuryEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateSupernaturalScaffholding("operation_mercury");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    jumpstart: true,
    grandReqs: {
      "[root/mariner.flag.operation.gold.set]": 1,
    },
    label: "The Operations of the Sky: Mercurial Airs",
    startdescription:
      "The air thrums with potential. On days like these, painters become visionaries and alchemists bottle distilled truth. This is not the season called numa, but rather the clear air that remains after thie ringing of certain bella. With the applicaiton of Forge in her highest form, we can tame not only wind and wave but the flame of time herself.",
    linked: [{ id: `${PREFIX}.outcome.success`, chance: minChance("forge", 10) }, { id: `${PREFIX}.outcome.failure` }],
    lots: SEA_EVENT_SLOTS(),
  });


  // Outcomes
  generateSimpleSuccessOutcome({
    label: "In Perpetuum Potestas, in Potentia Perpetua",
    startdescription:
      "There is a power who cannot act, not yet. but in the space between potential and reality, they are known nudge things in the right direction. and now, so are we. We will arrive at our location imminently.",
    effects: { "mariner.sailing.distancetravelled": DISTANCE_SCALED(10) },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Baser Weathers",
    startdescription:
      "We could not meet the demands to wield the airs of potential. The pressure shifts, the moment fades, and we drift back into less leaden weather patterns.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
