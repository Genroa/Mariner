import { generateSupernaturalScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";
export function generateWhiteWestSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateSupernaturalScaffholding("whitewest");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "White in the West",
    jumpstart: true,
    tablereqs: { "mariner.weathermeta.cold": 1 },
    startdescription: "Frost spins lace upon the sail, as the snow-veiled sun only allows for the black and the white. It sinks into the west, a pale spotlight highighting the bleak clouds around. It is a reminder of the transience of now, and pallor of Eternity. We must remain stalward, not to let that invitation sway our course to a more final destination.",
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("winter", 7) },
      { id: `${PREFIX}.outcome.success`, chance: minChance("lantern", 7) },
      { id: `${PREFIX}.outcome.failure` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    tablereqs: {},
    label: "After Elegabalus",
    startdescription: "There was another who performed the path before, and we can follow in their footsteps, crisp frost upon the waves.",
    effects: {},
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Winter does not Wait Forever",
    startdescription: "I have been beset by moods often in my life; but this time the shadow that falls is a white one, bleaching my mood into fatalistic haze.",
    linked: [{ id: "mariner.sailing.postevent" }],
    effects: { "mariner.experiences.dreadful": 1 },
  });
}
