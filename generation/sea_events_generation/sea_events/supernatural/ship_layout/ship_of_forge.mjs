import { generateSupernaturalScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfForgeEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateSupernaturalScaffholding("ShipOfForge");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : forge] + [ ~/local: { [mariner.ship.kite] } : forge]": 10,
    },
    label: "<Ship of Theseus>",
    startdescription: "The ship's life is a crucible...",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "<Ship of Theseus: Eternity Assured in Reinvention>",
    startdescription:
      "<The Life on a ship is a crucible, and sailors do not pass it unaltered. life on the hsip is also an alembic,  bringing together disperate parts come together to combine into a stronger alloyed hwole. Today is a day of clanging harmonies. Each is assigned a task, and together we mend what is broken, and reach eternity through reinvention>",
    linked: [{ id: "mariner.sailing.postevent" }],
    mutations: [
      {
        filter: "mariner.ship",
        mutate: "mariner.ship.damage",
        level: -3,
        additive: true,
      },]
  });
}
