import { generateSupernaturalScaffholding } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfHeartEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateSupernaturalScaffholding("ShipOfHeart");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : heart] + [ ~/local: { [mariner.ship.kite] } : heart]": 10,
    },
    label: "<Swaddled Ship>",
    startdescription: "A good crew is woven together to be more then the sum of its parts...",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "<Swaddled Ship: Those on Watch do not Sleep>",
    startdescription:
      "<A good crew is woven together to be more then the sum of its parts. When the operation of the ship is orchestrated perfectly to the wind and the waves. When the routine and rigours of the ship become like a dance and the crews heart beats as one, there is no longer a need for the watch-schedule, for the cabins to rest in, no exhaustion, no isolation, no seperation, no breaks, no more. not tonight.>",
    linked: [{ id: "mariner.sailing.postevent" }],
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.longing",
            limit: 20,
            level: -1,
            additive: true,
          },
        ],
      },
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.exhausted",
            limit: 20,
            level: 0,
            additive: false,
          },
        ],
      },
    ],
  });
}
