import { generateSupernaturalScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfGrailEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateSupernaturalScaffholding("shipofgrail");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : grail] + [ ~/local: { [mariner.ship.kite] } : grail]": 10,
    },
    label: "<Chalice Ship>",
    startdescription:
      "A ship is a vessel, and ours is filled with a brimming delight.",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "Chalice Ship: The Well Runneth Over",
    startdescription:
      "<A ship is a vessel, and ours is filled with a brimming delight. Passions and hungers rise up from depths unfathomed. And like within a well, echoes reverberate and reform and become more then they were. [My Heart, Grail, Moth and Knock Songs will be refreshed.]>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
