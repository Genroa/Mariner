import { generateSupernaturalScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateShipOfLanternEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
    generateSupernaturalScaffholding("ShipOfLantern");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/exterior : { [mariner.crew] } : lantern] + [ ~/local: { [mariner.ship.kite] } : lantern]": 10,
    },
    label: "<The Ship of Light>",
    startdescription:
      "Like lights bounding and rebounding in a room of polished mirrors...",
    linked: [{ id: `${PREFIX}.outcome.end` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.end`,
    warmup: 20,
    label: "The Ship of Light: Each Hour its' Color",
    startdescription:
      "<Like lights bounding and rebounding in a room of polished mirrors so too does my crew refract and amplify my briliance. They might each be candles flame, but together we sail as a constellation. They might be prismes and one light is refrected into all of the colours of the Higher House. They migth be mirrors, and in our collection we amplify. [My Edge, Lantern, Winter and Forge Songs will be refreshed.]>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
