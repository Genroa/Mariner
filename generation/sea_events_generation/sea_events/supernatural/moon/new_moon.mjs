import { generateSupernaturalScaffholding, minChance } from "../../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { DISTANCE_SCALED } from "../../../../sea_generation/generate_sailing_routes.mjs";

export function generateNewMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateSupernaturalScaffholding("new_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "[~/exterior : mariner.moon.basic.1]": 1 },
    jumpstart: { effects: { "mariner.influneces.moth": 10 } },
    label: "<A New Moon Rising>",
    startdescription:
      "These nights the moon has been devoured until there isn't a sliver left and we sail in complete darkness. With no celestial observer and no land in sight we sway in crucim between anuwhere and Nowhere. We could cling to the skin of the world we know with sufficient heart, or catch the currents the sea conjures when the moon is absent.",
    linked: [
      { id: `${PREFIX}.outcome.safe`, chance: minChance("heart", 10) },
      { id: `${PREFIX}.outcome.chance_change`, chance: minChance("moth", 10) },
      { id: `${PREFIX}.outcome.chance_delay` }
    ],
    slots: SEA_EVENT_SLOTS()
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.safe`,
    warmup: 20,
    label: "<The World Still Turns>",
    startdescription:
      "<Those who watch do not sleep, not even when the moon is blind. so in their eyeswe will remain bound and back and here. >",
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.chance_change`,
    warmup: 20,
    label: "<Into the Unseen>",
    startdescription:
      "<We sail onto stranger tides, and out of sight of the omnipresent sun of vigilant moon. Here location can lapse from certainty and we swaying in potentia, in waves. When the sun rises, on that collapsing wave the sun first lights, we will be somewhere again, and must once again find our bearing.>",
    effects: { "mariner.sailing.distancetravelled": DISTANCE_SCALED(10) },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.chance_delay`,
    warmup: 20,
    label: "<Into the Unseen>",
    startdescription:
      "<We sail onto stranger tides, and out of sight of the omnipresent sun of vigilant moon. Here location can lapse from certainty and we swaying in potentia, in waves. When the sun rises, on that collapsing wave the sun first lights, we will be somewhere again, and must once again find our bearing.>",
    effects: { "mariner.sailing.distance": DISTANCE_SCALED(2) },
  });
}
