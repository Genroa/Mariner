import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { generateSupernaturalScaffholding, minChance } from "../../sea_events_helpers.mjs";

export function generateThievesMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateSupernaturalScaffholding("thieves_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "[~/exterior : mariner.moon.basic.8]": 1 },
    jumpstart: { effects: { "mariner.influences.knock": 10 } },
    label: "<Thieves' Moon Rising>",
    startdescription:
      "Tonight the night is the near dark of the inside of a jacketpocket. Under the Thieves' Moon, the night becomes crowded with ill intent. In the light of that waning sliver of a sickle, the shadows of pursuers cannot be cleft from the shadows of the night, not until you can catch the glint of their eyes.... Do I wish to slip through this darkenss unseen, or will I pry open the darkness of the night and witness the prey or predator besides.",
    linked: [{ id: `${PREFIX}.outcome.encounter`, chance: minChance("knock", 10) }, { id: `${PREFIX}.outcome.solitude` }],
    slots: SEA_EVENT_SLOTS(),
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.encounter`,
    warmup: 20,
    label: "<An Encounter>",
    startdescription: "<An endless night intterrupted by another who dwells at sea. Onward.>",
    linked: [
      { id: `${PREFIX}.outcome.tourists`, chance: 33 },
      { id: `${PREFIX}.outcome.yacht`, chance: 50 },
      { id: `${PREFIX}.outcome.pirates` },
      { id: `${PREFIX}.outcome.solitude` },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.tourists`,
    warmup: 20,
    linked: [{ id: "mariner.sailing.seaevents.start.tourists" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.yacht`,
    warmup: 20,
    linked: [{ id: "mariner.sailing.seaevents.start.yachts" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.pirates`,
    warmup: 20,
    linked: [{ id: "mariner.sailing.seaevents.start.pirates" }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.solitude`,
    warmup: 20,
    label: "Mantle of Solitude",
    startdescription:
      "<We take the nights blessings, an carry it with us for the rest of the trip. we travel under the velvets skin now.>",
    linked: [{ id: "mariner.sailing.postevent" }],
    effects: { "mariner.influences.moth": 4 },
  });
}
