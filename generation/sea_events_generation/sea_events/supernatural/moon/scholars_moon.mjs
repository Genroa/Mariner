import { SEA_EVENT_SLOTS } from "../../../../slots_helpers.mjs";
import { generateSupernaturalScaffholding, minChance } from "../../sea_events_helpers.mjs";

export function generateScholarMoonsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateSupernaturalScaffholding("scholar_moon");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: { "[~/exterior : mariner.moon.basic.6]": 1 },
    jumpstart: { effects: { "mariner.influences.lantern": 10 } },
    label: "<Scholar's Moon Rising>",
    startdescription:
      "There is a anticipation in the waning moon, a dimming of ligh, so slow one might hardly notice it. But as beings cursed with glories gifts, we know. This night I will engage in a staring contest with the long slow blink of the moon. Then when morning bleaches the moon's shadows, in a dialogue with the rushing waves I will write down a secret story... if I have the Lantern to guide my pen.",
    linked: [{ id: `${PREFIX}.outcome.success`, chance: minChance("lantern", 10) }, { id: `${PREFIX}.outcome.failure` }],
    slots: SEA_EVENT_SLOTS()
  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "<Candles Grace>",
    startdescription: "<Before the fading light is fully gone, I have penned down my tale>", //TODO: need to write the story for this
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Dim",
    startdescription:
      "<I am insufficiently immulinated, what usually brings me inspiration, now wanes alongside the moon in morning>",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
