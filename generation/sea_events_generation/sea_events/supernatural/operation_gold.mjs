import { generateSupernaturalScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateOperationGoldEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } = generateSupernaturalScaffholding("operation_gold");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[root/mariner.flag.operation.silver.set]": 1,
      "[root/mariner.flag.operation.gold.set]": -1,
      "[~/exterior : mariner.weathermeta.sunny]": 1,
    },
    label: "The Operations of the Sky: A Golden Haze",
    startdescription:
      "Homer called the heavens bronze, but today she is filled with a purer colour. The sunset paints the cloud in hazy streaks of gold. The wind is a lover's sigh, and every person on board finds an excuse to linger in this perfect light. But I knows know the potential leaden is this moment is almost to the crux of a culmination. If I alloy it further with a winter trapping and the operation of Forge or Heart I might complete the Operations of the Sky.",
    linked: [
      { id: `${PREFIX}.outcome.success`, chance: minChance("forge", 8) },
      { id: `${PREFIX}.outcome.success`, chance: minChance("heart", 8) },
      { id: `${PREFIX}.outcome.failure` },
    ],
    slots: SEA_EVENT_SLOTS(),

  });

  // Outcomes
  generateSimpleSuccessOutcome({
    label: "The Refinement of Potential",
    startdescription: "Gold yields to white, as pure light contains all, and eternity permits no flaws or seperations.",
    furthermore: [
      {
        rootAdd: {
          "mariner.flag.operation.gold.set": 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Baser Weathers",
    startdescription:
      "We could not meet the demands to wield the airs of potential. The pressure shifts, the moment fades, and we drift back into less leaden weather patterns.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
