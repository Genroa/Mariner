import { generateScaffholding } from "./sea_events_helpers.mjs";

export function generateShapesSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("shapes");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Shapes in the Water",
    startdescription: "The Kite has found Companionship. Something is trailing in our ships wake.",
    linked: [{ id: `${PREFIX}.outcome.success`, chance: 66 }, { id: `${PREFIX}.outcome.failure` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.success`,
    warmup: 20,
    label: "Companionship",
    startdescription:
      "Curious dolphins, playful seals, watchful sharks. Those creatures that call the waters home follow us for a while, and break up the monotomy of water and air.",
    effects: { contentment: 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Things From Below",
    effects: { "mariner.experiences.rattling.standby": 1 },
    startdescription:
      "These creatures following us, they do not move in the way earthly things should. We watch, but we never catch more than a glance. They stay below the waterline, hide in the ship's wake, before sinking back to deeper places. ",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
