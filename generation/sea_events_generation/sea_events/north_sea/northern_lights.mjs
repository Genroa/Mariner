import { generateScaffholding } from "../sea_events_helpers.mjs";
import { CALL_FUNCTION } from "../../../generate_function_calls.mjs";

export function generateNorthernLightsSeaEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("northernlights");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    label: "Solar Aurora",
    grandReqs: {
      "[~/exterior : mariner.weathermeta.clear]": 1,
      "[~/extant: mariner.locations.northsea.sailingthere]": 1
    },
    startdescription: "The sky is painted in beauty, the electric representation of an arctic song.",
    linked: [{ id: `${PREFIX}.outcome.firstoutcome`, chance: 50 }, { id: `${PREFIX}.outcome.secondoutcome` }],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.firstoutcome`,
    warmup: 20,
    label: "A True Miracle",
    startdescription:
      "We marvel at the weave of the universe, and the magic that can wrought even in world as grey and listless as ours.",
    furthermore: CALL_FUNCTION("addgroundedness.30"),
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.secondoutcome`,
    warmup: 20,
    label: "The Midnight Dawn",
    startdescription:
      "These are the last echoed colours of the Sun's true radiance. But even so far south and in this decayed history, we can still bask in it. Way may wish to travel to experience it's radiance more fully, in a colder, brighter place.",
    linked: [{ id: "mariner.sailing.postevent" }],
    effects: { "mariner.influences.lantern": 6 },
  });
}
