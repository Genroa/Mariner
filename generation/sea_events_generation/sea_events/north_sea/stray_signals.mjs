import { generateScaffholding, minChance } from "../sea_events_helpers.mjs";
import { SEA_EVENT_SLOTS } from "../../../slots_helpers.mjs";

export function generateStraySingalsEvent() {
  const { RECIPES_FILE, PREFIX, EVENT_START } = generateScaffholding("stray_signals");

  mod.setRecipe(RECIPES_FILE, {
    id: EVENT_START,
    warmup: 20,
    grandReqs: {
      "[~/extant: mariner.locations.northsea.sailingthere]": 1
    },
    label: "Stray Signals",
    startdescription:
      "Mortals have spun a web in the aether around the North Sea. Civilisation has nestled on its shores on all sides, and between shipping lanes and fishing boats, there is rarely a quiet moment on this crawling sea When one is sentivite, or equipped wit the right tools, One might even steal stray messages from the airwaves. [approach this with Moth or Knock or the Lantern Instrument]",
    slots: SEA_EVENT_SLOTS(),
    linked: [
      { id: `${PREFIX}.outcome.moth`, chance: minChance("moth") },
      { id: `${PREFIX}.outcome.knock`, chance: minChance("knock") },
      { id: `${PREFIX}.outcome.failure` },
    ],
  });

  // Outcomes
  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.moth`,
    warmup: 20,
    label: "Errant Message",
    startdescription:
      "Snippets from other conversations, flashes of music. Are we simply getting flashes from other peoples lives, or from other histories? We might distill something pleasurable from these flashes, or even something useful.",
    effects: { "mariner.rumour.northsea": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.knock`,
    warmup: 20,
    label: "Errant Message",
    startdescription:
      "Snippets from other conversations, flashes of music. Are we simply getting flashes from other peoples lives, or from other histories? We might distill something pleasurable from these flashes, or even something useful.",
    effects: { "mariner.rumour.northsea": 1 },
    linked: [{ id: "mariner.sailing.postevent" }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${PREFIX}.outcome.failure`,
    warmup: 20,
    label: "Over our Head",
    startdescription: "whatever there was to hear, we do not. whatever we might have learned, we do not.",
    linked: [{ id: "mariner.sailing.postevent" }],
  });
}
