import { generateScaffholding } from "../sea_events_helpers.mjs";
import { SIGNALS } from "../../../generate_signal_aspects.mjs";

export function generateBlizzardEvent() {
    const { RECIPES_FILE, PREFIX, EVENT_START, generateSimpleSuccessOutcome } =
        generateScaffholding("blizzard");

    mod.setRecipe(RECIPES_FILE, {
        id: EVENT_START,
        warmup: 20,
        label: "Blizzard",
        grandReqs: {
            "[~/exterior : mariner.weathermeta.cold]": -1,
            "[~/exterior : mariner.weathermeta.stormy]": -1,
            "[~/extant: mariner.locations.northsea.sailingthere]": 1
        },
        startdescription:
            "'The coldest winds are not always North winds, but they always Howl' And today, a Howl is rising up in the Arctic, and it's hurrying down to this soupbowl of a sea.",
        linked: [
            { id: `${PREFIX}.outcome.success` },
        ],
        furthermore: [
            {
                target: "~/exterior",
                aspects: { [SIGNALS.BLIZZARD_SET_BLIZZARD]: 1 },
            },
        ],
    });

    // Outcomes  link to mariner.sailing.seaevents.start.coldsnap or mariner.sailing.seaevents.start.windriseshi
    mod.setRecipe(RECIPES_FILE, {
        id: `${PREFIX}.outcome.success`,
        label: "Fury At Sea",
        startdescription:
            "When Winter stirs in Rage, the sailors job becomes the hardest. When the blizzard rages, the fisherman stays home and the trawler remains in harbor.",
        linked: [{ id: "mariner.sailing.seaevents.start.squall" }],
    });
}
