import { SIGNALS } from "./generate_signal_aspects.mjs";

export function generateSavingSystem() {
  /*
    - take moon influences into account everywhere
  */
  mod.initializeRecipeFile("recipes.saving", ["saving"]);
  mod.setRecipes(
    "recipes.saving",
    {
      id: "mariner.saving",
      actionId: "mariner.sing",
      hintOnly: true,
      grandReqs: {
        "mariner.atsea": -1,
        "[~/exterior : {[mariner.vault] = 0} : mariner.docked]": 1,
        "mariner.moon": 1,
        "[mariner.story] < (1 + [~/visible : aspectmax/desire])": 1,
      },
      label: "<Saving: More Stories Needed>",
      startdescription: "<I need to tell more stories to the moon to listen>",
    },
    {
      id: "mariner.saving",
      actionId: "mariner.sing",
      hintOnly: true,
      grandReqs: {
        "mariner.atsea": -1,
        "[~/exterior : {[mariner.vault] = 0} : mariner.docked]": 1,
        "mariner.moon": 1,
        "mariner.story": -1,
      },
      label: "<Regale the Moon with my Story?>",
      startdescription: "<If I entrust my story to the Moon, she will keep it safe for me, whatever face she now bears. [This will create a Savefile, so you progress is not lost, and can be returned too.]>",
    },
    {
      id: "mariner.saving",
      actionId: "mariner.sing",
      craftable: true,
      grandReqs: {
        "mariner.atsea": -1,
        "[~/exterior : {[mariner.vault] = 0} : mariner.docked]": 1,
        "mariner.moon": 1,
        "mariner.story": "1 + [~/visible : aspectmax/desire]",
      },
      warmup: 10,
      label: "<A Dialogue of One>",
      startdescription: "<The moon is a good audience. the moon is a steady companion. the moon is a faithful guide. I can feel secure in my story being kept safe there.>",
      description: "<My future is uncertain, but my past is secure.>",
      furthermore: [
        { aspects: { [SIGNALS.CLEAR_MOON_BONUSES]: 1 } },
        { aspects: { [SIGNALS.APPLY_MOON_BONUS_ASPECTS]: 1 } },
        { aspects: { [SIGNALS.INSTALL_MOON_BONUSES]: 1, [SIGNALS.EXHAUST_STORY]: 1 } },
      ],
      saveCheckpoint: true,
    }
  );
}
