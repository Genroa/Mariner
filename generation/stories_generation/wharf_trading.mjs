import { generateStoryTrading } from "./trading_helper.mjs";

export function generateWharfStoryTrading() {
  generateStoryTrading({
    id: "generic.wharf",
    story: {
      label: "Dockworkers' Stories",
      icon: "kitlingripe",
      description:
        "The tall tales of working men are of the great accomplishments of the everyday, where what-if's and could-haves gets exalted into epics of skill and accomplishment.",
      aspects: {
        forge: 1,
        heart: 1,
        "mariner.storyaspect.mortal": 1,
        "mariner.storyaspect.rousing": 1,
        "mariner.storyaspect.witty": 1,
      },
    },
    requirements: { "mariner.wharf": 1 },
    label: "Trade My Story With Others at the Wharf",
    startdescription:
      "If I settle at the Wharf, I could listen to the stories that grow larger each time they are retold.",
    description:
      "Stories rebound and revolve here, it is a back and forth between participants, and each story takes a step above the last. As they collaboratively climb to a baudy creak of incredulity, and I cannot help but smile.",
  });
}
