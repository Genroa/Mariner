const ASPECT_FILE = "aspects.signals";

export function signalId(id) {
  if (!mod.aspects[ASPECT_FILE]) mod.initializeAspectFile(ASPECT_FILE, ["signals"]);

  if (!mod.getAspect(ASPECT_FILE, `mariner.signal.${id}`)) mod.setHiddenAspect(ASPECT_FILE, { id: `mariner.signal.${id}` });

  return `mariner.signal.${id}`;
}

export let SIGNALS = {
  // TOOL SIGNALS
  USE_TOOL: signalId("usetool"),
  MISUSE_TOOL: signalId("misusetool"),
  UPGRADE_TOOL: signalId("upgradetool"),
  SPECIALISE_TOOL_WITH_LANTERN: signalId("specialisetool.with.lantern"),
  SPECIALISE_TOOL_WITH_FORGE: signalId("specialisetool.with.forge"),
  SPECIALISE_TOOL_WITH_EDGE: signalId("specialisetool.with.lantern"),
  SPECIALISE_TOOL_WITH_WINTER: signalId("specialisetool.with.winter"),
  SPECIALISE_TOOL_WITH_HEART: signalId("specialisetool.with.heart"),
  SPECIALISE_TOOL_WITH_GRAIL: signalId("specialisetool.with.grail"),
  SPECIALISE_TOOL_WITH_MOTH: signalId("specialisetool.with.moth"),
  SPECIALISE_TOOL_WITH_KNOCK: signalId("specialisetool.with.knock"),

  // PET SIGNALS
  GIVE_PET: signalId("givepet"),
  RETRIEVE_PET: signalId("retrievepet"),
  UPDATE_PET: signalId("updatepet"),

  // NOTORIETY SIGNALS
  GENERATE_REPUTATION_HEAT: signalId("generatereputationheat"),
  UPDATE_DISPLAYED_REPUTATION: signalId("updatedisplayedreputation"),
  // Only reacted to by docked to destination
  DESTROY_LOCAL_REPUTATION: signalId("destroylocalreputation"),
  // Reacted to by all destinations, so use that signal inside a verb after a grab or something
  DESTROY_REPUTATION: signalId("destroyreputation"),

  FORCE_WEATHER_DECAY: signalId("forceweatherdecay"),
  PERFORMANCE_CLEAR_WEATHER: signalId("performanceclearweather"),
  PERFORMANCE_ROUSE_WEATHER: signalId("performancerouseweather"),

  CHANNELRATS_SET_STORM: signalId("channelratssetstorm"),
  BLIZZARD_SET_BLIZZARD: signalId("blizzardsetblizzard"),
  BORA_SET_SEASMOKE: signalId("borasetseasmoke"),
  MISTRAL_SET_WIND: signalId("mistralsetwind"),
  HARMATTAN_SET_SANDSTORM: signalId("harmattansetsandstorm"),
  SIROCCO_SET_SIROCCO: signalId("siroccosetsirocco"),
  BRUME_SET_NUMA: signalId("brumesetnuma"),

  // SAILING SIGNALS
  LEAVE_PORT: signalId("leaveport"),
  ARRIVE_TO_DESTINATION: signalId("arrivetodestination"),
  LOCK_PORT: signalId("lockport"),
  ENABLE_EXPERIENCE_DECAY: signalId("allowexperiences"),
  DUPLICATE_EXPERIENCES: signalId("duplicateexperiences"),

  MARK_COMING_FROM_WAYPOINT: signalId("markcomingfromwaypoint"),
  RESET_WAYPOINTS: signalId("resetwaypoints"),

  // PERFORMANCE SIGNALS
  PERFORMING: signalId("performing"),

  PERFORMANCE_SUMMON_CALIGINE: signalId("summmoncaligine"),
  PERFORMANCE_SUMMON_DIDUMOS: signalId("summmondidumos"),
  PERFORMANCE_SUMMON_MAID: signalId("summmonmaid"),
  PERFORMANCE_SUMMON_STRIXLING: signalId("summmonstrixling"),
  PERFORMANCE_SUMMON_PUPPET: signalId("summmonpuppet"),
  PERFORMANCE_SUMMON_SHATTERED_RISEN: signalId("summmonshattered"),
  PERFORMANCE_SUMMON_VOICELESS_RISEN: signalId("summmonvoiceless"),
  PERFORMANCE_SUMMON_BURGEONING_RISEN: signalId("summmonburgeoning"),
  PERFORMANCE_SUMMON_COUSIN: signalId("summmoncousin"),
  PERFORMANCE_SUMMON_SKAPTODON: signalId("summmonskaptodon"),
  PERFORMANCE_SUMMON_IARELLA: signalId("summmoniarella"),
  PERFORMANCE_SUMMON_INVITED: signalId("summmoninvited"),

  // SONG SIGNALS
  UPGRADE_SONG: signalId("upgradesong"),
  DOWNGRADE_SONG: signalId("downgradesong"),
  REFRESH_SONG: signalId("refreshsong"),
  USE_SONG: signalId("usesong"),

  // LOCAL ELEMENTS SIGNALS
  APPLY_DEFAULT_ROUTINE: signalId("applydefaultroutine"),
  START_ROUTINE: signalId("startroutine"),
  STOP_ROUTINE: signalId("stoproutine"),

  // CREW SIGNALS
  TRIGGER_PHYSICAL_ATTACK_RESULT: signalId("triggerphysicalattackresult"),
  TRIGGER_UNRAVEL_ATTACK_RESULT: signalId("triggeunravelattackresult"),
  WITNESS_SUPERNATURAL_TRAUMA: signalId("witnesssupernaturaltrauma"),
  WITNESS_HORROR_TRAUMA: signalId("witnesshorrortrauma"),
  TRIGGER_REBELLION_BEHAVIOUR: signalId("triggerrebellionbehaviour"),
  FREAK_OUT: signalId("freakout"),
  WITNESS_CANNIBALISM: signalId("witnesscannibalism"),
  FORGET_LEFT_OPERAND: signalId("forgetleftoperand"),
  FORGET_RIGHT_OPERAND: signalId("forgetrightoperand"),
  TURN_INTO_LUNATIC: signalId("turnintolunatic"),
  WIPE_ALL_TRAITS: signalId("wipealltraits"),

  // HEART SIGNALS
  USE_HEART: signalId("useheart"),
  EXHAUST_LH: signalId("exhaustlackheart"),
  EXHAUST_HH: signalId("exhausthalfheart"),

  WOUND_LH: signalId("lackheart.applywound"),
  WOUND_HH: signalId("halfheart.applywound"),

  ALLEVIATE_WOUND_LH: signalId("lackheart.alleviatewound"),
  ALLEVIATE_WOUND_HH: signalId("halfheart.alleviatewound"),

  // SHIP SIGNALS
  COMPUTE_SHIP_STATS: signalId("computeshipstats"),

  // SHIP CONFIGURATIONS AND UPGRADE SIGNALS
  INSTALL_RIGGING_CONFIGURATION: signalId("installriggingconfiguration"),
  CLEAR_RIGGING_CONFIGURATION: signalId("clearrrigingconfiguration"),

  INSTALL_HOLD_CONFIGURATION: signalId("installholdconfiguration"),
  CLEAR_HOLD_CONFIGURATION: signalId("clearholdconfiguration"),

  INSTALL_HULL_CONFIGURATION: signalId("installhullconfiguration"),
  CLEAR_HULL_CONFIGURATION: signalId("clearhullconfiguration"),

  // OPTION SELECTION SIGNALS
  PICK_OPTION_1: signalId("pickoption1"),
  PICK_OPTION_2: signalId("pickoption2"),
  PICK_OPTION_3: signalId("pickoption3"),

  // LOCAL ACTIONS
  UPGRADE_CARGO: signalId("upgradecargo"),
  BARGAIN_CARGO: signalId("bargaincargo"),
  SELL_CARGO: signalId("sellcargo"),
  DISCOVER_LOCAL_ELEMENT: signalId("discoverlocalelement"),

  // SEASON SIGNALS
  TURN_TO_HEARTACHE: signalId("turntoheartache"),
  TURN_TO_WANDERLUST: signalId("turntowanderlust"),

  // STORY SIGNALS
  EXHAUST_STORY: signalId("exhauststory"),
  UNEXHAUST_STORY: signalId("unexhauststory"),

  // melody
  EXHAUST_MELODY: signalId("exhaustmelody"),
  UNEXHAUST_MELODY: signalId("unexhaustmelody"),

  // AI Agent Controller methods
  // port specific methods, on the aspect
  AGENT_UNDOCK: signalId("agent.undock"),
  AGENT_DOCK: signalId("agent.dock"),
  AGENT_PRESSURE_PORT: signalId("agent.pressureport"),

  // Sea specific methods, on the aspect
  AGENT_PICK_DESTINATION: signalId("agent.pickdestination"),
  AGENT_LEAVE_SEA: signalId("agent.leavesea"),
  AGENT_ARRIVE_IN_SEA: signalId("agent.arriveinsea"),

  // Agent specific methods, on the agent card
  AGENT_INVESTIGATE: signalId("agent.investigate"),

  //Undress
  UNDRESS: signalId("undress"),

  // Moon bonuses
  // Clear the moon
  CLEAR_MOON_BONUSES: signalId("clearmoonbonuses"),
  // Ask story aspects to install, if the quantities are right, bonus aspects onto the moon
  APPLY_MOON_BONUS_ASPECTS: signalId("applymoonbonusaspects"),
  // Ask the moon bonus aspects to install the aspects they add as a bonus
  INSTALL_MOON_BONUSES: signalId("installmoonbonuses"),
};
