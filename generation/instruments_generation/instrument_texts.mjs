export const PLAIN_INSTRUMENT_TEXTS = {
  knock: {
    id: "mariner.instrument.plain.knock",
    label: "Sitar",
    description: "Through these strings, give your audience a taste of your experiences, an understanding of your view.",
  },
  moth: {
    label: "Violin",
    description:
      "An instrument made for lilting tunes, to describe changing moods or changing seasons.",
  },
  lantern: {
    label: "Theremin",
    description:
      "Its tunes live on the ether alone; Are these the tones that angels sing?",
  },
  forge: {
    label: "Organ",
    description:
      "More an apparatus than a tool. Brass pipes and polished copper, it contorts the air into sound.",
  },
  edge: {
    label: "Brass",
    description:
      "For Valor, Victory of Funeral, its song carries echoes of battle.",
  },
  winter: {
    label: "Bell",
    description: "Its call marks the silence.",
  },
  heart: {
    label: "Drum",
    description: "The oldest instrument, the most familiar rhythms.",
  },
  grail: {
    label: "Guitar",
    description:
      "Cradled on your lap, this instrument can be your perfect accompaniment",
  },
};
