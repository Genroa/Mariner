import { buildRefinementBlock } from "../generation_helpers.mjs";

const shopId = (seaId, portId, elementId) =>
  `mariner.locations.${seaId}.${portId}.${elementId ?? "instrumentshop"}`;

export const BUY_INSTRUMENT_TEXTS = {
  knock: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Certainly. The method is imported, but the instrument will come from our very own shop."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Hawthorn wood, sanded with snake scales. The strings are spun from feathers, But do not ask me the method, it is quite well protected.',
    }),
  },
  moth: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Ah... yes, This will interest you, I am sure. In a way, you are our collaborator here."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"The strings have been treated with the Spiteful Resin. Be careful with it, it aches to change. But that tension is what colors the sounds most extraordinarily."',
    }),
  },
  lantern: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"I used to have said no, but recently, our technology has advanced. Let me show you."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Voice was the preferred method for Lantern performances for centuries. But now we can sing without using throat or touch."',
    }),
  },
  forge: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Ah... I will call the boys... You\'ll see why."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"My sons will come and help you assemble. The alloys are intricate, and some connections must be made with the right application of heat."',
    }),
  },
  edge: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Ah yes... The instruments have changed over the years, but the method of preparation has not. Wait one moment."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Not easy to acquire these. To be properly Attuned, they must have seen 66 battles at least."',
    }),
  },
  winter: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"The eternal conundrum... Which instrument can be used to enhance the principle of Silence."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"We must smelt these ourselfs. Coffin nails and sanctified silver, among other things."',
    }),
  },
  heart: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Unsurprisingly, our most requisted item. For a London client alone, we have sold a companies worth. "',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Boar skin, of course. Pine wood is traditional, but we have replaced the frame with something more durable. "',
    }),
  },
  grail: {
    startdescription: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"Like with my children, I probably should not have favorites among the instruments. But on these alone our craftmanship is truely appreciated."',
    }),
    description: buildRefinementBlock({
      [shopId("northsea", "copenhagen")]:
        '"We once displayed these in our windows, but it ran the risk of our crowding our store. Now we put them up for auction at select auction houses... We always venture to buy them back."',
    }),
  },
};
