const storySlot = (index, progressRequired = 0) => ({
  id: `story${index}`,
  actionId: "mariner.sing",
  label: "<Story>",
  description: "<>",
  required: { "mariner.story": 1 },
  forbidden: { "mariner.story.exhausted": 1 },
  ifAspectsPresent: {
    "[~/visible : aspectmax/desire]": progressRequired + 1,
  },
});

function generateMoon({ moonId, phases }) {
  const phaseCompleteId = (index) => `mariner.moon.${moonId}.${index + 1}`;

  // Create the moon phases
  mod.initializeElementFile(`moon.${moonId}`, ["moons", moonId]);
  mod.elements[`moon.${moonId}`] = [...Array(phases.length).keys()].map((index) => ({
    id: `${phaseCompleteId(index)}`,
    icon: "moons/" + phaseCompleteId(index),
    label: phases[index].label,
    description: phases[index].description,
    lifetime: 120,
    decayTo: `${phaseCompleteId((index + 1) % phases.length)}`,
    aspects: {
      "mariner.moon": 1,
      "mariner.sing_allowed": 1,
      "mariner.inspiration": 1,
      "mariner.inspiration.nature": 1,
      ...phases[index].aspects,
    },
    slots: [
      storySlot(1),
      storySlot(2),
      storySlot(3, 1),
      storySlot(4, 2),
      storySlot(5, 3),
      storySlot(6, 4),
      storySlot(7, 5),
      storySlot(8, 6),
    ],
  }));
}

export function generateMoons() {
  mod.initializeAspectFile("aspects.moon", ["moons"]);
  mod.setHiddenAspect("aspects.moon", { id: "mariner.moon" });
  generateMoon({
    moonId: "basic",
    phases: [
      {
        label: "New Moon",
        description: "The New Moon. A time of new beginnings, new passions and new paths.",
        aspects: {
          moth: 4,
          knock: 4,
          "mariner.inspiration.moth": 1,
          "mariner.inspiration.knock": 1,
          "mariner.ship.discretion.bonus": 2,
          "mariner.updateshipstats": 1,
        },
      },
      {
        label: "Waxing Crescent",
        description: 'Also known as "The Liars\' Moon". Now is the time to don new faces and shed them with equal gusto.',
        aspects: { moth: 4, "mariner.inspiration.moth": 1, "mariner.ship.discretion.bonus": 1, "mariner.updateshipstats": 1 },
      },
      {
        label: "First Quarter",
        description:
          "First Quarter Moon. It has been said that the Moon loves the ocean, and the ocean loves the moon. On these nights I can feel their bond the strongest.",
        aspects: {
          moth: 4,
          grail: 4,
          "mariner.inspiration.moth": 1,
          "mariner.inspiration.grail": 1,
          "mariner.updateshipstats": 1,
        },
      },
      {
        label: "Waxing Gibbous",
        description: 'Also known as "The Gluttons\' Moon" Now is the time to savor the sweet and the savory.',
        aspects: { grail: 4, "mariner.inspiration.grail": 1, "mariner.ship.discretion.malus": 1, "mariner.updateshipstats": 1 },
      },
      {
        label: "Full Moon",
        description: "The Full Moon, Bright, Garish, Divine, Clichée. Too many words have been spend failing to describe her.",
        aspects: {
          lantern: 4,
          grail: 4,
          "mariner.inspiration.lantern": 1,
          "mariner.inspiration.grail": 1,
          "mariner.updateshipstats": 1,
          "mariner.ship.discretion.malus": 2,
          "mariner.updateshipstats": 1,
        },
      },
      {
        label: "Waning Gibbous",
        description: 'Also known as "The Scholars\' Moon" Now is the time to spend long nights by pale and whispy candlelight.',
        aspects: {
          lantern: 4,
          "mariner.inspiration.lantern": 1,
          "mariner.ship.discretion.malus": 1,
          "mariner.updateshipstats": 1,
        },
      },
      {
        label: "Third Quarter",
        description: "The Third Quarter moon, when the Moon is to perfectly balanced with itself. Her face is a revelation.",
        aspects: {
          lantern: 4,
          knock: 4,
          "mariner.inspiration.lantern": 1,
          "mariner.inspiration.knock": 1,
          "mariner.updateshipstats": 1,
        },
      },
      {
        label: "Waning Crescent",
        description:
          "Also known as \"the Thieves' Moon\" Now is the time fisherman's wives lock their doors twice, and yet they still unlatch to let the night in.",
        aspects: { knock: 4, "mariner.inspiration.knock": 1, "mariner.ship.discretion.bonus": 1, "mariner.updateshipstats": 1 },
      },
    ],
  });
}
