import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_LINK, ME_RECIPE } from "../generation_helpers.mjs";

export function generateSummons() {
  mod.initializeRecipeFile("recipes.summons", ["summons"]);
  mod.initializeAspectFile("aspects.summons", ["summons"]);
  mod.initializeVerbFile("verbs.summons", ["summons"]);

  mod.setAspect("aspects.summons", { id: "mariner.summon" });

  // Generic rebellion behaviour for most summons
  mod.setAspect("aspects.summons", {
    id: "mariner.crew.traits.canrebel",
    label: "Can Rebel/Will Rebel",
    description: "<It is good to have servants of strong will to pursue your ambitions, but it are the willful servants who migh turn when their own ambition rises. -- Frasier [This Summoned enity is likely to attempt to escape, and worse things still]>",
    timers: {
      longingtick: {
        frequency: 1000 * 60 * 5,
        morpheffects: [
          { morpheffect: "mutate", id: "mariner.crew.longing", level: 1 },
          ME_RECIPE("mariner.summonrebellion", "max(0, [mariner.crew.longing] - 5) * 5"),
        ],
      },
    },
  });

  mod.setAspect("aspects.summons", {
    id: "mariner.crew.traits.candecay",
    label: "Decays",
    description: "<Time holds a particular sway over this ones form. >",
    timers: {
      longingtick: {
        frequency: 1000 * 60 * 5,
        morpheffects: [
          { morpheffect: "mutate", id: "mariner.construct.decay", level: 1 },
          ME_RECIPE("mariner.construct.disassemble", "(max(0, [mariner.construct.decay] - 50)) * 100"),
        ],
      },
    },
  });

  mod.setHiddenAspect("aspects.summons", {
    id: "mariner.summon.spiritrebellion",
    xtriggers: {
      // TODO : unfinished
      [SIGNALS.TRIGGER_REBELLION_BEHAVIOUR]: ME_LINK("some link"),
    },
  });

  // This recipe runs when the summon decides to rebel. It doesn't do anything by itself, it expects the summon to have another aspect describing what they do
  // when they rebel. What this other aspect does is just add a recipe to link to.
  mod.setRecipe("recipes.summons", {
    id: "mariner.summonrebellion",
    furthermore: [{ target: "~/token", aspects: { [SIGNALS.TRIGGER_REBELLION_BEHAVIOUR]: 1 } }],
  });

  // Called when a construct summon reaches 50 decay. Should kill it in some way. Should probably be an induced token of some kind.
  mod.setRecipe("recipes.summons", {
    id: "mariner.construct.disassemble",
    furthermore: [{ target: "~/token", effects: { "mariner.summon.candecay": -1 } }],
  });

  mod.setHiddenAspect("aspects.summons", { id: "mariner.summon.actionsource" });

  // Freaks out anyone
  mod.setAspect("aspects.summons", {
    id: "mariner.crew.traits.freak",
    label: "<Freaks Out People on Board>",
    timers: {
      freakingout: {
        frequency: 1000 * 60 * 4,
        morpheffects: ME_RECIPE("mariner.summon.freaksoutsomeone.spawnverb", 2),
      },
    },
  });

  mod.setRecipe("recipes.summons", {
    id: "mariner.summon.freaksoutsomeone.spawnverb",
    grandReqs: {
      "[~/exterior : { [mortal] } : mariner.crew] + [mariner.passenger]": 1,
    },
    furthermore: [
      {
        target: "~/token",
        mutations: [{ filter: "mariner.summon", mutate: "mariner.summon.actionsource", level: 1, additive: false }],
      },
    ],
    inductions: [
      {
        id: "mariner.summon.freaksoutsomeone",
        expulsion: {
          filter: { "mariner.summon.actionsource": 1 },
          limit: 1,
        },
      },
    ],
  });

  mod.setVerb("verbs.summons", { id: "mariner.verbs.summon.freaksoutsomeone", multiple: true, spontaneous: true });
  mod.setRecipe("recipes.summons", {
    id: "mariner.summon.freaksoutsomeone",
    actionId: "mariner.verbs.summon.freaksoutsomeone",
    label: "<Summon Freaking out Someone>",
    furthermore: [
      { mutations: [{ filter: "mariner.summon", mutate: "mariner.summon.actionsource", level: 0, additive: false }] },
      {
        target: "~/exterior",
        movements: {
          "~/local": {
            "[mariner.passenger] || [mortal]": 1,
          },
        },
      },
      { aspects: { [SIGNALS.FREAK_OUT]: 1 } },
    ],
  });

  // Freaks out crew
  mod.setAspect("aspects.summons", {
    id: "mariner.crew.traits.holdkept",
    label: "<Freaks Out Crew>",
    description: "<Kept hidden from outsiders but still occasionally freaks out crew>",
    timers: {
      freakingout: {
        frequency: 1000 * 60 * 4,
        morpheffects: ME_RECIPE("mariner.summon.freaksoutcrew.spawnverb", 2),
      },
    },
  });

  mod.setRecipe("recipes.summons", {
    id: "mariner.summon.freaksoutcrew.spawnverb",
    grandReqs: {
      "[~/exterior : { [mortal] } : mariner.crew]": 1,
    },
    furthermore: [
      {
        target: "~/token",
        mutations: [{ filter: "mariner.summon", mutate: "mariner.summon.actionsource", level: 1, additive: false }],
      },
    ],
    inductions: [
      {
        id: "mariner.summon.freaksoutcrew",
        expulsion: {
          filter: { "mariner.summon.actionsource": 1 },
          limit: 1,
        },
      },
    ],
  });

  mod.setVerb("verbs.summons", { id: "mariner.verbs.summon.freaksoutcrew", multiple: true, spontaneous: true });
  mod.setRecipe("recipes.summons", {
    id: "mariner.summon.freaksoutcrew",
    actionId: "mariner.verbs.summon.freaksoutcrew",
    label: "<Summon Freaking out Crew>",
    furthermore: [
      { mutations: [{ filter: "mariner.summon", mutate: "mariner.summon.actionsource", level: 0, additive: false }] },
      {
        target: "~/exterior",
        movements: {
          "~/local": { mortal: 1 },
        },
      },
      { aspects: { [SIGNALS.FREAK_OUT]: 1 } },
    ],
  });
}
