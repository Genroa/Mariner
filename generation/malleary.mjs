"use strict";

import fs from "fs";
import path from "path";

export const addAlt = (id) => ({ id, additional: true });

const _undefinedOrArray = (type, recipe, prop) => {
  if (recipe[prop] !== undefined && !Array.isArray(recipe[prop]) && typeof recipe[prop] !== "string")
    throw `${type} with id "${recipe.id}" defined a property "${prop}", but it isn't an array`;
};
const _undefinedOrObject = (recipe, prop) => {
  if (recipe[prop] !== undefined && (typeof recipe[prop] !== "object" || Array.isArray(recipe[prop])))
    throw `${type} with id "${recipe.id}" defined a property "${prop}", but it isn't an object`;
};
const _mandatoryProp = (type, recipe, prop) => {
  if (!recipe[prop]) throw `${type}'s property ${prop} is mandatory, but the recipe with id "${recipe.id}" didn't define it`;
};

export const modContent = () => ({
  aspects: { aspects: [] },
  elements: { elements: [] },
  recipes: { recipes: [] },
  decks: { decks: [] },
  legacies: { legacies: [] },
  endings: { endings: [] },
  shelves: { shelves: [] },
  verbs: { verbs: [] },

  folders: {},
  options: {
    displayHiddenAspects: false,
    displayRecipeIds: false,
    forceMinimalWarmup: false,
  },

  generationStats: {
    generatedElements: 0,
    generatedAspects: 0,
    generatedRecipes: 0,
    generatedDecks: 0,
    warnings: 0,
  },

  setFolder(fileId, foldersPath) {
    this.folders[fileId] = foldersPath;
  },

  initializeAspectFile(fileId, foldersPath) {
    return this.initializeFile("aspects", fileId, foldersPath);
  },
  initializeElementFile(fileId, foldersPath) {
    return this.initializeFile("elements", fileId, foldersPath);
  },
  initializeRecipeFile(fileId, foldersPath) {
    return this.initializeFile("recipes", fileId, foldersPath);
  },
  initializeDeckFile(fileId, foldersPath) {
    return this.initializeFile("decks", fileId, foldersPath);
  },
  initializeLegacyFile(fileId, foldersPath) {
    return this.initializeFile("legacies", fileId, foldersPath);
  },
  initializeEndingFile(fileId, foldersPath) {
    return this.initializeFile("endings", fileId, foldersPath);
  },
  initializeShelfFile(fileId, foldersPath) {
    return this.initializeFile("shelves", fileId, foldersPath);
  },
  initializeVerbFile(fileId, foldersPath) {
    return this.initializeFile("verbs", fileId, foldersPath);
  },
  initializeFile(type, fileId, foldersPath) {
    if (this[type][fileId]) {
      // console.warn(`Warning: file "${fileId}" of type ${type} was already defined. Make sure this isn't a generation mistake.`);
      return fileId;
    }
    this[type][fileId] = [];
    this.folders[fileId] = foldersPath;
    return fileId;
  },

  checkAspectFormat(aspect, ignoreTypography = false) {
    if (ignoreTypography) return;
    this.checkTypos(aspect.id, "id", aspect.id);
    this.checkTypos(aspect.id, "label", aspect.label, true);
    this.checkTypos(aspect.id, "description", aspect.description, true, true);
  },

  checkRecipeFormat(recipe, ignoreTypography = false) {
    const undefinedOrArray = (recipe, prop) => _undefinedOrArray("recipe", recipe, prop);
    const undefinedOrObject = (recipe, prop) => _undefinedOrObject("recipe", recipe, prop);
    const mandatoryProp = (recipe, prop) => _mandatoryProp("recipe", recipe, prop);
    try {
      mandatoryProp(recipe, "id");
      // mandatoryProp(recipe, "actionId");

      undefinedOrArray(recipe, "alt");
      undefinedOrArray(recipe, "linked");
      //undefinedOrArray(recipe, "mutations");
      undefinedOrArray(recipe, "slots");
      if (recipe.slots !== undefined) {
        for (let i = 0; i < recipe.slots.length; i++) {
          const slot = recipe.slots[i];
          if (!slot.id) {
            throw `In recipe ${recipe.id}, slot n°${i} doesn't have an id. Fix that (game crashing problem)`;
          }
        }
      }

      undefinedOrObject(recipe, "requirements");
      undefinedOrObject(recipe, "tablereqs");
      undefinedOrObject(recipe, "extantreqs");
      undefinedOrObject(recipe, "effects");
      undefinedOrObject(recipe, "deckeffects");
      undefinedOrObject(recipe, "aspects");
      undefinedOrObject(recipe, "purge");

      if (ignoreTypography) return;
      this.checkTypos(recipe.id, "id", recipe.id);
      this.checkTypos(recipe.id, "label", recipe.label, true);
      this.checkTypos(recipe.id, "startdescription", recipe.startdescription, true, true);
      this.checkTypos(recipe.id, "description", recipe.description, true, true);
      if (recipe.slots) {
        for (const slot of recipe.slots) {
          this.checkTypos(`${recipe.id} (slot: ${slot.id})`, "id", slot.id);
          this.checkTypos(`${recipe.id} (slot: ${slot.id})`, "label", slot.label, true);
          this.checkTypos(`${recipe.id} (slot: ${slot.id})`, "description", slot.description, true, true);
        }
      }
    } catch (e) {
      console.trace(e);
      throw e;
    }
  },
  checkElementFormat(element, ignoreTypography = false) {
    const undefinedOrArray = (element, prop) => _undefinedOrArray("element", element, prop);
    const undefinedOrObject = (element, prop) => _undefinedOrObject("element", element, prop);
    const mandatoryProp = (element, prop) => _mandatoryProp("element", element, prop);
    try {
      mandatoryProp(element, "id");

      undefinedOrArray(element, "slots");

      undefinedOrObject(element, "xtriggers");
      undefinedOrObject(element, "aspects");
      undefinedOrObject(element, "purge");

      if (ignoreTypography) return;
      this.checkTypos(element.id, "id", element.id);
      this.checkTypos(element.id, "label", element.label, true);
      this.checkTypos(element.id, "dynamicLabel", element.dynamicLabel, true);
      this.checkTypos(element.id, "description", element.description, true, true);
      if (element.slots) {
        for (const slot of element.slots) {
          this.checkTypos(`${element.id} (slot: ${slot.id})`, "id", slot.id);
          this.checkTypos(`${element.id} (slot: ${slot.id})`, "label", slot.label, true);
          this.checkTypos(`${element.id} (slot: ${slot.id})`, "description", slot.description, true, true);
        }
      }
    } catch (e) {
      console.trace(e);
      throw e;
    }
  },
  checkDeckFormat(deck, ignoreTypography = false) {
    const specTypo = (deck) => {
      if (deck.specs !== undefined)
        throw `Deck "${deck.id}" defined a property "specs". This is probably a typo, the property is named "spec".`;
    };
    const mandatoryProp = (deck, prop) => _mandatoryProp("deck", deck, prop);

    try {
      mandatoryProp(deck, "id");
      specTypo(deck);
      mandatoryProp(deck, "spec");

      if (ignoreTypography) return;
      this.checkTypos(deck.id, "id", deck.id);
      this.checkTypos(deck.id, "label", deck.label, true);
      this.checkTypos(deck.id, "description", deck.description, true, true);
    } catch (e) {
      console.trace(e);
      throw e;
    }
  },

  setObject(type, fileId, newObject) {
    try {
      if (this[type] === undefined) throw `ERROR while trying to set ${type} in "${fileId}" : type "${type}" does not exist`;
      if (this[type][fileId] === undefined)
        throw `ERROR while trying to set ${type} in "${fileId}" : file "${fileId}" does not exist`;
      let objects = this[type][fileId];
      objects = objects.filter((o) => o.id !== newObject.id);
      objects.push(newObject);
      this[type][fileId] = objects;
      return newObject;
    } catch (e) {
      console.trace(e);
      throw e;
    }
  },
  setAspect(fileId, newObject, ignoreTypography = false) {
    this.checkAspectFormat(newObject, ignoreTypography);
    this.generationStats.generatedAspects++;
    return this.setObject("aspects", fileId, { isAspect: true, ...newObject });
  },
  setHiddenAspect(fileId, newObject) {
    this.generationStats.generatedAspects++;
    return this.setObject("aspects", fileId, {
      isAspect: true,
      ...newObject,
      noArtNeeded: !this.options.displayHiddenAspects,
      isHidden: !this.options.displayHiddenAspects,
      label: this.options.displayHiddenAspects ? `[HIDDEN] ${newObject.label ?? newObject.id}` : newObject.label,
    });
  },
  setElement(fileId, newObject, ignoreTypography = false) {
    if (newObject.dynamicLabel) newObject.dynamicLabel = newObject.dynamicLabel.trim();
    this.checkElementFormat(newObject, ignoreTypography);
    this.generationStats.generatedElements++;
    return this.setObject("elements", fileId, newObject);
  },
  setRecipe(fileId, newRecipe, ignoreTypography = false) {
    if ((newRecipe.warmup ?? 0) < 10 && this.options.forceMinimalWarmup) newRecipe.warmup = 10;
    if (this.options.displayRecipeIds) newRecipe.label = newRecipe.label ?? newRecipe.id;
    if (this.options.displayRecipeIds) newRecipe.startdescription = `${newRecipe.id}\n` + (newRecipe.startdescription ?? "");
    if (this.options.displayRecipeIds) newRecipe.description = `${newRecipe.id}\n` + (newRecipe.description ?? "");

    if (newRecipe.label) newRecipe.label = newRecipe.label.trim();
    if (newRecipe.startdescription) newRecipe.startdescription = newRecipe.startdescription.trim();
    if (newRecipe.description) newRecipe.description = newRecipe.description.trim();

    this.checkRecipeFormat(newRecipe, ignoreTypography);
    this.generationStats.generatedRecipes++;

    if (newRecipe.jumpstart) {
      const jumpstartRecipeContent = typeof jumpstart === "boolean" ? {} : newRecipe.jumpstart;
      const jumpstartRecipe = {
        id: `jumpstart.${newRecipe.id}`,
        actionId: `jumpstart.${newRecipe.id}`,
        ...jumpstartRecipeContent,
        linked: [{ id: newRecipe.id }],
      };
      this.setRecipe(fileId, jumpstartRecipe, true);
      delete newRecipe.jumpstart;
    }

    return this.setObject("recipes", fileId, newRecipe);
  },
  setDeck(fileId, newDeck, ignoreTypography = false) {
    // newDeck.spec = this.r.shuffle(newDeck.spec);
    this.checkDeckFormat(newDeck, ignoreTypography);
    this.generationStats.generatedDecks++;
    return this.setObject("decks", fileId, newDeck);
  },
  setLegacy(fileId, newObject) {
    return this.setObject("legacies", fileId, newObject);
  },
  setEnding(fileId, newObject) {
    return this.setObject("endings", fileId, newObject);
  },
  setShelf(fileId, newObject) {
    return this.setObject("shelves", fileId, newObject);
  },
  setVerb(fileId, newObject) {
    return this.setObject("verbs", fileId, newObject);
  },
  setAspects(fileId, ...aspects) {
    for (const aspect of aspects) this.setAspect(fileId, aspect);
  },
  setHiddenAspects(fileId, ...aspects) {
    for (const aspect of aspects) this.setHiddenAspect(fileId, aspect);
  },
  setElements(fileId, ...elements) {
    for (const element of elements) this.setElement(fileId, element);
  },
  setRecipes(fileId, ...recipes) {
    for (const recipe of recipes) this.setRecipe(fileId, recipe);
  },
  setDecks(fileId, ...decks) {
    for (const deck of decks) this.setDeck(fileId, deck);
  },
  setLegacies(fileId, ...legacies) {
    for (const legacy of legacies) this.setLegacy(fileId, legacy);
  },
  setEndings(fileId, ...endings) {
    for (const ending of endings) this.setEnding(fileId, ending);
  },
  setShelves(fileId, ...shelves) {
    for (const shelf of shelves) this.setShelf(fileId, shelf);
  },
  setVerbs(fileId, ...verbs) {
    for (const verb of verbs) this.setVerb(fileId, verb);
  },

  getObject(type, fileId, id) {
    return this[type][fileId].find((o) => o.id === id);
  },
  getAspect(fileId, id) {
    return this.getObject("aspects", fileId, id);
  },
  getElement(fileId, id) {
    return this.getObject("elements", fileId, id);
  },
  getRecipe(fileId, id) {
    return this.getObject("recipes", fileId, id);
  },
  getDeck(fileId, id) {
    return this.getObject("decks", fileId, id);
  },
  getLegacy(fileId, id) {
    return this.getObject("legacies", fileId, id);
  },
  getEnding(fileId, id) {
    return this.getObject("endings", fileId, id);
  },
  getVerb(fileId, id) {
    return this.getObject("verbs", fileId, id);
  },

  addToXtriggers(fileId, elementId, xtrigger) {
    let element = this.getElement(fileId, elementId);
    if (!element)
      throw `ERROR while trying to add new xtrigger to element (fileId ${fileId}, elementId ${elementId}): wrong file, or the element file doesn't exist, or the element doesn't exist`;
    let xtriggers = { ...element.xtriggers, ...xtrigger };
    element.xtriggers = xtriggers;
  },

  operationOnRoutingArray(fileId, recipeId, recipeThing, arrayField, op) {
    if (typeof recipeThing === "string") {
      recipeThing = { id: recipeThing };
    }
    let recipe = this.getRecipe(fileId, recipeId);
    if (!recipe)
      throw `ERROR while trying to add new ${arrayField} to recipe (fileId ${fileId}, recipeId ${recipeId}): wrong file, or the recipe file doesn't exist, or the recipe doesn't exist`;
    let arr = (recipe[arrayField] ?? []).filter((r) => r.id !== recipeThing.id);
    op(arr, recipeThing);
    recipe[arrayField] = arr;
  },

  addToLinked(fileId, recipeId, linkedRecipeThing) {
    this.operationOnRoutingArray(fileId, recipeId, linkedRecipeThing, "linked", (arr, r) => arr.push(r));
  },

  addFirstToLinked(fileId, recipeId, linkedRecipeThing) {
    this.operationOnRoutingArray(fileId, recipeId, linkedRecipeThing, "linked", (arr, r) => arr.unshift(r));
  },

  addToAlt(fileId, recipeId, altRecipeThing) {
    this.operationOnRoutingArray(fileId, recipeId, altRecipeThing, "alt", (arr, r) => arr.push(r));
  },

  addFirstToAlt(fileId, recipeId, altRecipeThing) {
    this.operationOnRoutingArray(fileId, recipeId, altRecipeThing, "alt", (arr, r) => arr.unshift(r));
  },

  readFile(name, type, foldersPath) {
    // if we already loaded the file, do not read it again
    if (this[type][name] !== undefined) return;

    let filePath = "content";
    const elementType = type === "aspects" ? "elements" : type;
    if (foldersPath) filePath = filePath + path.sep + foldersPath.join(path.sep);
    const content = JSON.parse(fs.readFileSync(`${filePath}${path.sep}${name}.json`))[elementType];
    this[type][name] = content;
    if (foldersPath) this.folders[name] = foldersPath;
  },

  readAspectsFile(name, folders) {
    this.readFile(name, "aspects", folders);
  },
  readElementsFile(name, folders) {
    this.readFile(name, "elements", folders);
  },
  readRecipesFile(name, folders) {
    this.readFile(name, "recipes", folders);
  },
  readDecksFile(name, folders) {
    this.readFile(name, "decks", folders);
  },
  readLegaciesFile(name, folders) {
    this.readFile(name, "legacies", folders);
  },
  readEndingsFile(name, folders) {
    this.readFile(name, "endings", folders);
  },

  writeFile(type, filePath, name, content) {
    fs.writeFileSync(`${filePath}${path.sep}${name}.json`, JSON.stringify({ [type]: content }, undefined, 2), {
      encoding: "utf8",
      flag: "w",
    });
  },

  generateFolders(foldersPath) {
    let folders = [...foldersPath];
    let folder = "content";

    while (folders.length > 0) {
      folder = folder + path.sep + folders.shift();
      if (!fs.existsSync(folder)) fs.mkdirSync(folder);
    }
  },

  saveContentGroup(group, type) {
    for (let [fileName, fileContent] of Object.entries(group)) {
      if (fileContent.length > 0) {
        const foldersPath = this.folders[fileName];
        let filePath = "content";
        if (foldersPath) {
          this.generateFolders(foldersPath);
          filePath = filePath + path.sep + foldersPath?.join(path.sep);
        }
        this.writeFile(type, filePath, fileName, fileContent);
      }
    }
  },

  save() {
    this.saveContentGroup(this.aspects, "elements");
    this.saveContentGroup(this.elements, "elements");
    this.saveContentGroup(this.recipes, "recipes");
    this.saveContentGroup(this.decks, "decks");
    this.saveContentGroup(this.verbs, "verbs");
    this.saveContentGroup(this.legacies, "legacies");
    this.saveContentGroup(this.endings, "endings");
    this.saveContentGroup(this.shelves, "shelves");
  },

  helpers: {
    capitalize(string) {
      return string.charAt(0).toUpperCase() + string.slice(1);
    },

    integer({ min, max }) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    },
  },

  checkTypos(id, type, string, capitalise = false, isParagraph = false) {
    return;
    if (string === undefined) return;

    const logTypoWarning = (message, extract) => {
      const extractToPrint = extract ?? string;
      const isExtract = extract !== undefined;
      // console.log(`\x1b[33mTYPO WARNING on string of type \x1b[31m${type}\x1b[33m : Id of the culprit is \x1b[36m"${id}",\x1b[33m ${isExtract ? 'Extract ' : ''}"\x1b[36m${extractToPrint}\x1b[33m" -> ${message}\x1b[0m`)
      console.log(
        `\x1b[33mTYPO WARNING on \x1b[31m${type}\x1b[33m,\x1b[33m ${
          isExtract ? "Extract " : ""
        }"\x1b[36m${extractToPrint}\x1b[33m" -> ${message}\x1b[0m`
      );
      this.generationStats.warnings = this.generationStats.warnings + 1;
    };
    function trimTags(s) {
      if (s.startsWith("<br>")) s = s.substring(4);
      if (s.startsWith("<i>")) s = s.substring(3);
      if (s.startsWith("</i>")) s = s.substring(4);
      if (s.startsWith('"')) s = s.substring(1);
      if (s.endsWith('"')) s = s.substring(0, s.length - 1);
      if (s.endsWith("</i>")) s = s.substring(0, s.length - 4);
      return s;
    }
    function cleanSentence(s) {
      s = s.trim();
      if (s.charAt(0) === '"') s = s.substring(1);
      if (s.charAt(s.length - 1) === '"') s = s.substring(0, s.length - 1);
      while (s.startsWith("<br>") || s.startsWith("<i>") || s.startsWith("</i>") || s.endsWith("</i>")) s = trimTags(s);
      if (s.charAt(0) === '"') s = s.substring(1, s.length - 1);
      if (s.charAt(s.length - 1) === '"') s = s.substring(0, s.length - 1).trim();
      return s;
    }
    function startsWithCapital(word) {
      const code = word.charCodeAt(0);
      return code === 201 || (code >= 65 && code <= 90);
    }

    if (string === "") {
      // logTypoWarning("Empty string")
      return;
    }

    if (string.endsWith("succes")) logTypoWarning("ends with 'succes'. Did you mean 'success'?");
    if (string.toLowerCase().includes("forrest")) logTypoWarning("ARH YOU BUGGER! Forest, with only one 'r'!");
    if (!capitalise) return;

    if (!isParagraph && !startsWithCapital(string)) logTypoWarning("Should be capitalised");

    if (!isParagraph) return;
    const sentences = (string.match(/[^\[\.!\?(...)]+[\]\.!\?]+/g) || [string]).map(cleanSentence);
    //console.log(string, sentences)
    for (const sentence of sentences) {
      if (sentence.match(/ {2,}/g)) {
        logTypoWarning("Multiple whitespaces", sentence);
        return;
      }
      if (sentence.startsWith("<")) {
        logTypoWarning("Placeholder string", sentence);
        return;
      }
      if (!startsWithCapital(sentence)) logTypoWarning("Should be capitalised", sentence);
      if (sentence.match(/(\]\.\!?)$/g)) logTypoWarning("Should be capitalised", sentence);
      if (sentence.match(/([\. ]i[\n ])|(^i)/gm)) logTypoWarning("Some I aren't capitalised", sentence);
    }
  },
});
