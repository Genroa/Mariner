export function generateRepairToolAction() {
  mod.initializeRecipeFile("recipes.repairtool", ["tools"]);
  mod.setRecipes(
    "recipes.repairtool",
    {
      id: "mariner.repairtool.cannotinvault",
      actionId: "mariner.navigate",
      requirements: { tool: 1, "mariner.tool.broken": 1 },
      extantreqs: { "mariner.atsea": -1, "mariner.invault": 1 },
      label: "Send Tools for Repairs?",
      startdescription:
        "I cannot repair my tools of power while in the field, without someone of expertese nearby. It must wait until i am back in town.",
      hintOnly: true,
    },
    {
      id: "mariner.repairtool.cannotatsea",
      actionId: "mariner.navigate",
      requirements: { tool: 1, "mariner.tool.broken": 1 },
      extantreqs: { "mariner.atsea": 1 },
      label: "Send Tool for Repairs?",
      startdescription:
        "I cannot repair my tools of power while sailing across the sea, without someone of expertese underm y command. It must wait until i am back in town.",
      hintOnly: true,
    },
    {
      id: "mariner.repairtool",
      actionId: "mariner.navigate",
      requirements: { tool: 1, "mariner.tool.broken": 1 },
      extantreqs: { "mariner.atsea": -1, "mariner.invault": -1 },
      craftable: true,
      label: "Send Tool for Repairs",
      startdescription:
        "I know a local craftsman who can bring this tool back to its functional stage.",
      warmup: 60,
      mutations: [
        {
          filter: "tool",
          mutate: "mariner.tool.broken",
          level: 0,
          additive: false,
        },
      ],
      description:
        "Not as good as new, even better. I asked the worker to keep a few of the dents and notches it had gained in its last use. Tools need to be tended to regularly, but their memory should never be erased.",
    }
  );
}
