import Chance from "./chance.js";
import { SIGNALS, signalId } from "./generate_signal_aspects.mjs";

// dedicated random generator to have the same random generation each time
let r = new Chance("weather");

const WEATHER_LOOP_SIZE_FACTOR = 1;
const WEATHER_LIFETIME_VARIATION = 30;

const WEATHER_TEMPLATES = {
  clear: {
    label: "A Blank Sky",
    description: "The sky is a patchwork of grey and white and blue, partway between stained vellum and a half-shorn fleece",
    loreAspects: [],
    aspects: { "mariner.weathermeta.clear": 1 },
  },
  mist: {
    label: "Mists Muffle",
    description: "All sounds muffled by a haze of grey.",
    loreAspects: ["winter", "moth"],
    aspects: { "mariner.weathermeta.misty": 1, "mariner.ship.discretion.bonus": 2 },
  },
  rain: {
    label: "The Rain's Patter",
    description: "The quiet cacophony of rain.",
    loreAspects: ["moth"],
    aspects: { "mariner.weathermeta.rainy": 1, "mariner.ship.discretion.bonus": 1 },
  },
  snow: {
    label: "Ice Sings",
    description: "Ice snaps in the river. Sometimes it is hours between notes.",
    loreAspects: ["winter"],
    aspects: { "mariner.weathermeta.rainy": 1, "mariner.weathermeta.cold": 1, "mariner.ship.discretion.bonus": 1 },
  },
  storm: {
    label: "A Storm Howls",
    description: "The sky's drum beats a furious rhythm.",
    loreAspects: ["heart", "edge"],
    aspects: {
      "mariner.weathermeta.rainy": 1,
      "mariner.weathermeta.windy": 1,
      "mariner.weathermeta.stormy": 1,
      "mariner.ship.discretion.bonus": 2,
      "mariner.ship.speed.malus": 1,
    },
  },
  wind: {
    label: "Winds Whistle",
    description: "The wind plays the trees, and sings it's own many voiced harmonies.",
    loreAspects: ["moth", "heart"],
    aspects: { "mariner.weathermeta.windy": 1, "mariner.ship.speed.bonus": 2 },
  },
  bright: {
    label: "Bright Day's Bustle",
    description: "Life inevitably finds its way outside on a lovely day. Its sounds follow.",
    loreAspects: ["lantern", "heart"],
    aspects: { "mariner.weathermeta.sunny": 1, "mariner.weathermeta.clear": 1, "mariner.ship.discretion.malus": 2 },
  },
  hot: {
    label: "The Hum Of Heat",
    description: "The air wavers. Even sound does not escape such heat unchanged.",
    loreAspects: ["forge"],
    aspects: { "mariner.weathermeta.sunny": 1, "mariner.weathermeta.sickly": 1 },
  },
  smog: {
    label: "The Hiss of Smog",
    description: "Hateful vapors opress upon the lungs; The vengeful offspring of mankind's most glorious work.",
    loreAspects: ["forge", "winter"],
    aspects: { "mariner.weathermeta.sickly": 1, "mariner.weathermeta.misty": 1, "mariner.ship.discretion.bonus": 1 },
  },
  sirocco: {
    label: "The Whistle Of The Sirocco",
    description: "The winds flow here mercilesly from where the Sun's rule is still felt most oppressively.",
    loreAspects: ["forge", "lantern"],
    aspects: { "mariner.weathermeta.sunny": 1, "mariner.weathermeta.windy": 1, "mariner.ship.speed.bonus": 2 },
  },
  monsoon: {
    label: "The Roar Of The Monsoon",
    description: "Some rains consume the lands as much as they nourish it.",
    loreAspects: ["grail", "heart"],
    aspects: {
      "mariner.weathermeta.rainy": 1,
      "mariner.weathermeta.sickly": 1,
      "mariner.ship.speed.malus": 1,
      "mariner.ship.discretion.bonus": 1,
    },
  },
  tropical: {
    label: "The Thrill of the Tropics",
    description: "The wind thrums and the air prickles. All passions are roused in this heat.",
    loreAspects: ["grail", "edge"],
    aspects: { "mariner.weathermeta.sunny": 1, "mariner.weathermeta.rainy": 1, "mariner.ship.discretion.bonus": 1 },
  },
  trade: {
    label: "Trade Winds' Bellows",
    description: "Unrelenting, unified, unceasing. The sound of air moving with purpose.",
    loreAspects: ["heart"],
    aspects: { "mariner.weathermeta.windy": 1, "mariner.ship.speed.bonus": 2 },
  },
  numa: {
    label: "A Whisp of Nume-Brume",
    description:
      "A swirl of the Ninth season has wafted onto the sea. Beyond the horizon, wherever we are, can now be found the Edge of the World.",
    loreAspects: ["knock"],
    aspects: { "mariner.weathermeta.misty": 1, "mariner.ship.discretion.bonus": 1, "mariner.ship.grammarie.bonus": 2 },
  },
  seasmoke: {
    label: "Seasmoke",
    description: "The violence of the Furies, in a fog as dence as Asphodel. Forget everything except your will to survive.",
    loreAspects: ["moth", "edge"],
    aspects: {
      "mariner.weathermeta.misty": 1,
      "mariner.weathermeta.windy": 1,
      "mariner.weathermeta.rainy": 1,
      "mariner.weathermeta.stormy": 1,
      "mariner.ship.speed.malus": 2,
      "mariner.ship.discretion.bonus": 2,
    },
  },
  blizzard: {
    label: "Blizzard",
    description:
      "When snow and sleet is more pushing then falling, and the cold is so deep it no longer bites but burns. when even the memory of warmth flickers in the soul.",
    loreAspects: ["winter", "edge"],
    aspects: {
      "mariner.weathermeta.windy": 1,
      "mariner.weathermeta.cold": 1,
      "mariner.weathermeta.stormy": 1,
      "mariner.ship.speed.malus": 1,
      "mariner.ship.discretion.bonus": 1,
    },
  },
  sandstorm: {
    label: "Sandstorm",
    description: "The sky thirsts and the wind is laced with a million needles and a thousand sabres.",
    loreAspects: ["forge", "edge"],
    aspects: {
      "mariner.weathermeta.windy": 1,
      "mariner.weathermeta.sandstorm": 1,
      "mariner.ship.speed.malus": 1,
      "mariner.ship.discretion.bonus": 2,
    },
  },
};

function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    let j = r.integer({ min: 0, max: a.length - 1 });
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

const toLoreAspects = (loreAspects) => Object.fromEntries(loreAspects.map((a) => [a, 4]));
const toAspects = (loreAspects) => Object.fromEntries(loreAspects.map((a) => [a, 1]));
const toInspirationAspects = (loreAspects) => Object.fromEntries(loreAspects.map((a) => [`mariner.inspiration.${a}`, 1]));

function generateMetaAspects() {
  mod.initializeAspectFile("aspects.weather", ["weather"]);
  mod.setAspect("aspects.weather", {
    id: "mariner.weather",
    label: "Weather",
    description: "Partway between poetry and painting, weather fills the canvas of the sky.",
  });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.windy" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.clear" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.misty" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.rainy" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.sunny" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.stormy" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.sickly" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.hot" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.cold" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.monsoon" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.tropics" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.trade" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.numa" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.sirocco" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.seasmoke" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.blizzard" });
  mod.setHiddenAspect("aspects.weather", { id: "mariner.weathermeta.sandstorm" });
}

export function generateWeatherBase() {
  generateMetaAspects();
}

function buildWeatherInstance({
  seaId,
  weatherType,
  lifetime,
  suffix,
  clearWeather,
  stormWeather,
  blizzardWeather,
  seasmokeWeather,
  sandstormWeather,
  windWeather,
  siroccoWeather,
  numaWeather,
}) {
  return {
    id: `mariner.weathers.${seaId}.${weatherType}.${suffix}`,
    icon: `weathers/mariner.weathers.${weatherType}`,
    label: WEATHER_TEMPLATES[weatherType].label,
    description: WEATHER_TEMPLATES[weatherType].description,
    lifetime:
      lifetime ??
      360 +
        r.integer({
          min: -WEATHER_LIFETIME_VARIATION,
          max: WEATHER_LIFETIME_VARIATION,
        }),
    aspects: {
      "mariner.inspiration": 1,
      "mariner.weather": 1,
      "mariner.inspiration.nature": 1,
      "mariner.updateshipstats": 1,
      ...(WEATHER_TEMPLATES[weatherType].aspects ?? {}),
      ...toLoreAspects(WEATHER_TEMPLATES[weatherType].loreAspects),
      ...toInspirationAspects(WEATHER_TEMPLATES[weatherType].loreAspects),
    },
    xtriggers: {
      [SIGNALS.FORCE_WEATHER_DECAY]: [{ morpheffect: "decay" }],
      [SIGNALS.PERFORMANCE_CLEAR_WEATHER]: `mariner.weathers.${seaId}.${clearWeather ?? "clear"}.performancespecial`,
      [SIGNALS.PERFORMANCE_ROUSE_WEATHER]: `mariner.weathers.${seaId}.${stormWeather ?? "storm"}.performancespecial`,
      // TODO: make that actually generic. Wtf is going on here
      [SIGNALS.CHANNELRATS_SET_STORM]: `mariner.weathers.${seaId}.${stormWeather ?? "storm"}.event`,
      [SIGNALS.BLIZZARD_SET_BLIZZARD]: `mariner.weathers.${seaId}.${blizzardWeather ?? "blizzard"}.event`,
      [SIGNALS.BORA_SET_SEASMOKE]: `mariner.weathers.${seaId}.${seasmokeWeather ?? "seasmoke"}.event`,
      [SIGNALS.MISTRAL_SET_WIND]: `mariner.weathers.${seaId}.${windWeather ?? "wind"}.event`,
      [SIGNALS.HARMATTAN_SET_SANDSTORM]: `mariner.weathers.${seaId}.${sandstormWeather ?? "sandstorm"}.event`,
      [SIGNALS.SIROCCO_SET_SIROCCO]: `mariner.weathers.${seaId}.${siroccoWeather ?? "sirocco"}.event`,
      [SIGNALS.BRUME_SET_NUMA]: `mariner.weathers.${seaId}.${numaWeather ?? "numa"}.event`,

      // TODO: add all seas
      ["mariner.signal.arriveinsea.northsea"]: "mariner.weathers.changeto.northsea",
      ["mariner.signal.arriveinsea.medsea"]: "mariner.weathers.changeto.medsea",
    },
  };
}

export function generateWeather(seaId, weathers) {
  // Elements file for all weather cards of the sea
  mod.initializeElementFile(`weathers.${seaId}`, ["weather", seaId]);
  let weatherInstances = [];

  for (const weatherType of Object.keys(weathers)) {
    for (let i = 1; i <= weathers[weatherType] * WEATHER_LOOP_SIZE_FACTOR; i++) {
      const element = buildWeatherInstance({ seaId, weatherType, suffix: i });
      mod.setElement(`weathers.${seaId}`, element);
      weatherInstances.push(element);
    }
  }

  mod.setElement(`weathers.${seaId}`, {
    id: `mariner.weathers.changeto.${seaId}`,
    decayTo: weatherInstances.map((o) => o.id),
    lifetime: 1,
  });

  // For now, no check to not decay into the same weather. Simple stuff.
  shuffle(weatherInstances);
  for (let i = 0; i < weatherInstances.length - 1; i++) {
    weatherInstances[i].decayTo = weatherInstances[i + 1].id;
  }
  weatherInstances[weatherInstances.length - 1].decayTo = weatherInstances[0].id;

  // Add "special" weather instances, used by the performances to force a weather
  let customClearWeather = buildWeatherInstance({
    seaId,
    weatherType: "clear",
    suffix: "performancespecial",
    lifetime: 280,
  });
  let customStormWeather = buildWeatherInstance({
    seaId,
    weatherType: "storm",
    suffix: "performancespecial",
    lifetime: 280,
  });
  let eventStormWeather = buildWeatherInstance({
    seaId,
    weatherType: "storm",
    suffix: "event",
    lifetime: 280,
  });
  let eventSandstormWeather = buildWeatherInstance({
    seaId,
    weatherType: "sandstorm",
    suffix: "event",
    lifetime: 280,
  });
  let eventBlizzardWeather = buildWeatherInstance({
    seaId,
    weatherType: "blizzard",
    suffix: "event",
    lifetime: 280,
  });
  let eventSeasmokeWeather = buildWeatherInstance({
    seaId,
    weatherType: "seasmoke",
    suffix: "event",
    lifetime: 280,
  });
  let eventWindWeather = buildWeatherInstance({
    seaId,
    weatherType: "wind",
    suffix: "event",
    lifetime: 280,
  });
  let eventSiroccoWeather = buildWeatherInstance({
    seaId,
    weatherType: "sirocco",
    suffix: "event",
    lifetime: 280,
  });
  let eventNumaWeather = buildWeatherInstance({
    seaId,
    weatherType: "numa",
    suffix: "event",
    lifetime: 280,
  });

  customClearWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  customStormWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  customStormWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventStormWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventBlizzardWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventWindWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventSandstormWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventSeasmokeWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventSiroccoWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;
  eventNumaWeather.decayTo = weatherInstances[r.integer({ min: 0, max: weatherInstances.length - 1 })].id;

  mod.setElement(`weathers.${seaId}`, customClearWeather);
  mod.setElement(`weathers.${seaId}`, customStormWeather);
  mod.setElement(`weathers.${seaId}`, eventBlizzardWeather);
  mod.setElement(`weathers.${seaId}`, eventSandstormWeather);
  mod.setElement(`weathers.${seaId}`, eventSeasmokeWeather);
  mod.setElement(`weathers.${seaId}`, eventStormWeather);
  mod.setElement(`weathers.${seaId}`, eventWindWeather);
  mod.setElement(`weathers.${seaId}`, eventSiroccoWeather);
  mod.setElement(`weathers.${seaId}`, eventNumaWeather);
}
