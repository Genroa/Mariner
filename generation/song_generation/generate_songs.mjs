import {
  buildAdvancedRefinementBlock,
  LORE_ASPECTS,
  ME_APPLY,
  ME_BREAK,
  ME_MUTATE,
  ME_SET_MUTATION,
} from "../generation_helpers.mjs";
import { SONG_COMPOSITION_TEXTS } from "./texts/composition_texts.mjs";
import { UPGRADE_TEXTS } from "./texts/upgrade_texts.mjs";
import { SONG_TEXTS } from "./texts/song_texts.mjs";
import { generateSongInfluences } from "./generate_influences.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";

const INSPIRATION_TYPES = ["nature", "muse", "melody", "location", "instrument", "experience", "urstory"];
/**
 *
 * @param mod the mod object
 * @param aspect the aspect of the song
 * @param tiers an array containing objects describing the song's tiers. They follow the format: {label: String, description: String}
 */
function generateSongElementsOfAspect(aspect) {
  const tiers = SONG_TEXTS[aspect];
  const reversedTiers = [...tiers].reverse();
  const songId = `mariner.song.${aspect}`;
  const maxIntensity = `mariner.song.intensity`;
  const currentIntensity = "mariner.song.intensity.current";

  mod.setHiddenAspect("aspects.songs", { id: maxIntensity, label: "Intensity of a Song" });
  mod.setHiddenAspect("aspects.songs", { id: currentIntensity, label: "Current Intensity of a Song" });
  mod.setAspect("aspects.songs", { id: "mariner.song.exhausted", label: "<Exhausted song>" });

  // Generate song card
  const UPDATE_SONG_STATS_ME = [ME_SET_MUTATION(aspect, `[${currentIntensity}] * 2`)];
  const REFRESH_SONG_ME = [
    ME_SET_MUTATION("mariner.song.exhausted", 0),
    ME_SET_MUTATION(currentIntensity, `[${maxIntensity}]`),
    ME_APPLY(),
    ...UPDATE_SONG_STATS_ME,
  ];
  mod.setElement("songs", {
    id: songId,
    label: buildAdvancedRefinementBlock(
      reversedTiers.map((o, index) => ({ aspect: currentIntensity, value: tiers.length - index, text: o.label }))
    ),
    description: buildAdvancedRefinementBlock(
      reversedTiers.map((o, index) => ({
        aspect: currentIntensity,
        value: tiers.length - index,
        text: o.description,
      }))
    ),
    overlays: tiers.map((_, index) => ({
      image: `songs/mariner.song.${aspect}.${index + 1}`,
      expression: `[${currentIntensity}] = ${index + 1}`,
      layer: "base",
    })),
    aspects: { "mariner.song": 1 },
    xtriggers: {
      [SIGNALS.UPGRADE_SONG]: [ME_MUTATE(maxIntensity, 1), ME_APPLY(), ...REFRESH_SONG_ME],
      [SIGNALS.REFRESH_SONG]: REFRESH_SONG_ME,
      [SIGNALS.DOWNGRADE_SONG]: [ME_MUTATE(maxIntensity, -1), ME_APPLY(), ...REFRESH_SONG_ME],
      [SIGNALS.USE_SONG]: [
        ME_BREAK(`([mariner.song.exhausted] >= ([${maxIntensity}]-1)) * 100`),
        ME_MUTATE("mariner.song.exhausted", 1),
        ME_MUTATE(currentIntensity, -1),
        ME_APPLY(),
        ...UPDATE_SONG_STATS_ME,
      ],
    },
    slots: [
      {
        id: "crowd",
        label: "A Crowd",
        description: "A group, with all noses pointed the same way. In their eyes, music can become miracle.",
        actionId: "mariner.sing",
        required: {
          "mariner.crowd": 1,
        },
      },
      {
        id: "inspiration",
        label: "<Inspiration or story>",
        description:
          "<With a New Inspiration, I can upgrade my songs to new heights. Alternatively, I can use stories to refresh my interest in songs that have grown stale in use.>",
        actionId: "mariner.navigate",
        ifAspectsPresent: { [maxIntensity]: -7 },
        required: {
          "mariner.inspiration": 1,
          // Added for the visuals, because mariner.inspiration is hidden
          ...Object.fromEntries(INSPIRATION_TYPES.map((type) => [`mariner.inspiration.${type}`, 1])),
          "mariner.story": 1,
        },
        forbidden: { "mariner.story.exhausted": 1 },
      },
      {
        id: "storyformaxlevel",
        label: "<Story>",
        description: "<Refresh my songs with a Story.>",
        actionId: "mariner.navigate",
        ifAspectsPresent: { [maxIntensity]: 7 },
        required: { "mariner.story": 1 },
        forbidden: { "mariner.story.exhausted": 1 },
      },
    ],
  });
}

/**
 *
 *
 * Generates the upgrade recipe for the song of the given type and aspect, using influence type of the same aspect
 * @param type the type of inspiration
 * @param aspect the aspect of the song
 */
function generateSongUpgradeRecipeOfTypeAndAspect(type, aspect) {
  const songLoreAspectId = `mariner.song.${aspect}`;
  const upgradeTexts = UPGRADE_TEXTS[aspect][type];
  mod.setRecipe("recipes.upgradesong", {
    id: `mariner.upgradesongwith.${type}.${aspect}`,
    actionId: "mariner.navigate",
    craftable: true,
    label: `Upgrade ${aspect} song with ${mod.helpers.capitalize(type)} Inspiration`,
    startdescription: upgradeTexts.description,
    warmup: 60,
    // Song [aspect] + inspiration [type][aspect] + not already used
    requirements: {
      [songLoreAspectId]: 1,
      [aspect]: 1,
      [`mariner.inspiration.${type}`]: 1,
      [`mariner.song.inspirationused.${type}`]: -1,
    },
    effects: {
      "mariner.inspirationtype.consumable": -1,
      [`mariner.influences.${aspect}`]: 6,
    },
    aspects: {
      [SIGNALS.UPGRADE_SONG]: 1,
    },
    mutations: [
      {
        filter: "mariner.song",
        mutate: `mariner.song.inspirationused.${type}`,
        level: 1,
      },
    ],
    achievements: [`mariner.achievements.firstused${type}inspiration`],
  });
}

function generateSongRefreshForAspect(aspect) {
  const songLoreAspectId = `mariner.song.${aspect}`;
  mod.setRecipe("recipes.refreshsong", {
    id: `mariner.refreshsong.${aspect}`,
    actionId: "mariner.navigate",
    craftable: true,
    label: `Refresh ${aspect} Song`,
    startdescription: `<The powers of this theme have grown stale with use. I should refresh my interest in the themes behind ${aspect} with a story that matches those themes.>`,
    warmup: 60,
    // Song [aspect] + inspiration [type][aspect] + not already used
    grandReqs: {
      [songLoreAspectId]: 1,
      "mariner.song.exhausted": 1,
      [`[~/local : { [${aspect}] } : mariner.story]`]: 1,
    },
    aspects: {
      [SIGNALS.EXHAUST_STORY]: 1,
      [SIGNALS.REFRESH_SONG]: 1,
    },
  });
}

/**
 * Generates the composition recipe for the song of the given aspect, using the right aspected tune and any influence of the same aspect.
 * @param mod the mod object
 * @param aspect the aspect of the tune/song
 * @param songLabel the name of the song that will be composed
 */
function generateSongCompositionRecipeOfAspect(aspect) {
  const compositionTexts = SONG_COMPOSITION_TEXTS[aspect];
  const songId = `mariner.song.${aspect}`;
  mod.setRecipe("recipes.composesong", {
    id: `mariner.composesong.${aspect}`,
    label: compositionTexts.label,
    actionId: "mariner.navigate",
    startdescription: compositionTexts.description,
    craftable: true,
    warmup: 60,
    effects: {
      "mariner.tune": -1,
      [songId]: 1,
    },
    extantreqs: {
      [songId]: -1,
    },
    requirements: {
      [`mariner.tune.${aspect}`]: 1,
      [`mariner.inspiration.${aspect}`]: 1,
    },
    linked: mod.recipes["recipes.upgradesong"].filter((r) => r.requirements[aspect] !== undefined).map((r) => r.id),
  });
}

/**
 * Generates the singing recipes for the given lore aspect
 * @param mod
 * @param aspect
 */
function generateSingingRecipesOfAspect(loreAspect, startingTiersInfo) {
  const songLoreAspectId = `mariner.song.${loreAspect}`;
  const songIntensityId = `mariner.song.intensity`;
  const loreSingRecipesFile = `recipes.sing.${loreAspect}`;

  mod.initializeRecipeFile(loreSingRecipesFile, ["singing"]);
  // 1. Success recipe
  mod.setRecipe(loreSingRecipesFile, {
    id: `mariner.sing.success.${loreAspect}`,
    actionId: "mariner.sing",
    label: "A Glamour Woven",
    startdescription:
      "When a song is sung right, the world does not stay unchanged. The air has been infused with power, and it clings on me like a perfume.",
    linked: [
      {
        id: "mariner.addgroundedness",
      },
    ],
  });

  // 2. Generate starting recipes based on the song tier (desc)
  for (let i = startingTiersInfo.length; i--; i >= 0) {
    const startInfo = startingTiersInfo[i];
    mod.setRecipe(loreSingRecipesFile, {
      actionId: "mariner.sing",
      id: `mariner.sing.${loreAspect}.${i + 1}.start`,
      craftable: true,
      requirements: {
        "mariner.crowd": 1,
        [songLoreAspectId]: 1,
        [songIntensityId]: i + 1,
        "mariner.location.hostile": -1,
      },
      label: "A Special Song",
      startdescription: "The second note follows the first, and soon they become more than a sequence of sounds.",
      warmup: 30,
      effects: { [`mariner.influences.${loreAspect}`]: songIntensityId },
      linked: [
        { id: `mariner.sing.success.${loreAspect}`, chance: (i + 2) * 10 }, // for tier 1, 20%, 2, 30%, etc.
        { id: "mariner.sing.failure" },
      ],
    });
  }
}

/**
 * Generates all the song-singing system
 * @param mod
 */
function generateSingingRecipes() {
  // Common failure recipe
  mod.initializeRecipeFile("recipes.sing", ["singing"]);
  mod.setRecipe("recipes.sing", {
    id: "mariner.sing.failure",
    actionId: "mariner.sing",
    label: "The Crowd Turns",
    description:
      "At first it is slight. People shift on their feet. They eye me, than eachother. They don't know how, but they know they are being duped somehow. Soon the crowd will move against me, and inevitably, the town will follow. They sense the influence I summoned over it.",
    effects: { "mariner.notoriety": 1 },
    linked: [
      {
        id: "mariner.addgroundedness",
      },
    ],
  });

  generateSingingRecipesOfAspect("heart", [{}, {}, {}]);
  generateSingingRecipesOfAspect("grail", [{}, {}, {}]);
  generateSingingRecipesOfAspect("forge", [{}, {}, {}]);
  generateSingingRecipesOfAspect("winter", [{}, {}, {}]);
  generateSingingRecipesOfAspect("edge", [{}, {}, {}]);
  generateSingingRecipesOfAspect("moth", [{}, {}, {}]);
  generateSingingRecipesOfAspect("lantern", [{}, {}, {}]);
  generateSingingRecipesOfAspect("knock", [{}, {}, {}]);
}

/*
Generate all songs for the mod
*/
export function generateSongs() {
  mod.initializeAspectFile("aspects.inspirations", ["inspirations"]);
  mod.readElementsFile("tunes", ["singing"]);
  mod.readRecipesFile("recipes.composesong", ["singing"]);
  mod.initializeAspectFile("aspects.songs", ["singing"]);
  mod.initializeRecipeFile("recipes.upgradesong", ["singing"]);
  mod.initializeRecipeFile("recipes.refreshsong", ["singing"]);

  mod.setHiddenAspect("aspects.inspirations", {
    id: "mariner.inspiration",
    label: "Inspiration",
  });
  for (let aspect of LORE_ASPECTS) mod.setHiddenAspect("aspects.inspirations", { id: `mariner.inspiration.${aspect}` });

  mod.setAspect("aspects.songs", {
    id: "mariner.song.intensity",
    label: "Intensity of a Song",
    ishidden: true,
  });
  mod.setHiddenAspect("aspects.songs", {
    id: "mariner.inspirationtype.consumable",
  });
  mod.setHiddenAspect("aspects.songs", {
    id: "mariner.inspirationtype.nonconsumable",
  });

  // Hint already used nature inspiration
  mod.setRecipe("recipes.upgradesong", {
    id: `mariner.upgradesongwith.nature.alreadyused`,
    actionId: "mariner.navigate",
    label: "Using the same inspiration again?",
    startdescription:
      "The soul is not sustained on the familiar. I cannot mine the same source twice. My music might become trite, or even worse, predictable.",
    hintOnly: true,
    requirements: {
      "mariner.song": 1,
      "mariner.inspiration.nature": 1,
      "mariner.song.inspirationused.nature": 1,
    },
  });

  // Generate elements for all song tiers and all aspects
  mod.initializeElementFile("songs", ["singing"]);
  for (let aspect of LORE_ASPECTS) generateSongElementsOfAspect(aspect);

  // For each type, and each aspect, generate the recipes to upgrade them using the right type of influence of the right aspect
  for (let type of INSPIRATION_TYPES) {
    for (let aspect of LORE_ASPECTS) {
      generateSongUpgradeRecipeOfTypeAndAspect(type, aspect);
    }
  }

  // For each aspect, generate the recipes to compose them using the right tune and any influence of the right aspect
  for (let aspect of LORE_ASPECTS) generateSongCompositionRecipeOfAspect(aspect);
  for (let aspect of LORE_ASPECTS) generateSongRefreshForAspect(aspect);

  mod.setRecipe("recipes.composesong", {
    id: "mariner.composesong.hint",
    label: "Compose a Song?",
    actionId: "mariner.navigate",
    startdescription:
      "Do I wish to Compose a Song? If I supply this tune with an Inspiration that matches in Aspect, I can comepose a Motifs charged with that particular quintessence.",
    craftable: false,
    hintonly: true,
    requirements: {
      "mariner.tune": 1,
    },
  });

  generateSingingRecipes();
  generateSongInfluences();
}
