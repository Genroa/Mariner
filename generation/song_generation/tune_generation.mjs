import { LORE_ASPECTS } from "../generation_helpers.mjs";

export function tunesAspects(principles) {
  let returnAspects = {
    "mariner.tune": 1,
    "mariner.sing_allowed": 1,
  };
  for (const principle of principles) {
    (returnAspects[`mariner.tune.${principle}`] = 1), (returnAspects[principle] = 4);
  }
  return returnAspects;
}

//function generateTunesAspects() {
//  for (const loreAspect of LORE_ASPECTS) {
//    mod.setHiddenAspect("tunes.aspects", {
//      id: `mariner.tune.${loreAspect}`,
//      label: `mariner.tune.${loreAspect}`,
//      description: "test",
//    });
//  }
//}

function generateBasicTune(tunesData) {
  mod.setElement("tunes", {
    id: `mariner.tunes.${tunesData.id}`,
    label: tunesData.label,
    description: tunesData.description,
    icon: `tunes/mariner.tunes.${tunesData.id}`,
    aspects: tunesAspects(tunesData.principles),
    slots: [
      {
        id: "inspiration",
        label: "Inspiration or Story",
        description: "In what manner do I want to engage with this Tune?",
        actionId: "mariner.navigate",
        required: {
          "mariner.inspiration": 1,
          "mariner.story": 1,
        },
      },
      {
        id: "trapping",
        label: "Trapping",
        description: "power begets power",
        actionId: "mariner.sing",
        required: {
          "mariner.trapping": 1,
        },
      },
    ],
    lifetime: 240,
  });
}

function generateLongTune(tunesData) {
  mod.setElement("tunes", {
    id: `mariner.tunes.${tunesData.id}.long`,
    label: tunesData.label,
    description: tunesData.description,
    aspects: tunesAspects(tunesData.principles),
    icon: `tunes/mariner.tunes.${tunesData.id}`,
    slots: [
      {
        id: "inspiration",
        label: "Inspiration",
        description: "In what manner do I want to engage with this Tune?",
        actionId: "mariner.navigate",
        required: {
          "mariner.inspiration": 1,
        },
      },
      {
        id: "trapping",
        label: "Trapping",
        description: "power begets power",
        actionId: "mariner.sing",
        required: {
          "mariner.trapping": 1,
        },
      },
    ],
    lifetime: 2400,
  });
}

const tunesData = {
  forgeheart: {
    id: "forgeheart",
    label: "The Tune of Industry",
    description:
      "Hammers slam, fire roars, foremen yell warnings and instructions. This district thrums with the roar of living metal.",
    principles: ["forge", "heart"],
  },
  grailedge: {
    id: "grailedge",
    label: "The Tune of the Market",
    description:
      "The honey-laced voices of the merchants mix with the metal clink of coin against coin. Here, each transaction is a battle, each smile sharp as knives.",
    principles: ["grail", "edge"],
  },
  heartgrail: {
    id: "heartgrail",
    label: "The Tune of the Docks",
    description:
      "Never quiet, wether it's bars and saloons beckoning the sailors landwards, or water lapping at the quay, beckoning them back to sea.",
    principles: ["heart", "grail"],
  },
  knocklantern: {
    id: "knocklantern",
    label: "Church bells",
    description: "The bright and crisp sound calling a flock to the promise of enlightenment.",
    principles: ["knock", "lantern"],
  },
  winterlantern: {
    id: "winterlantern",
    label: "The Hush of the Library",
    description:
      "The russle of paper, the dull thud of a book returned to its proper place on the proper shelf. Sacred Semi-Silence Serves Studious Souls.",
    principles: ["winter", "lantern"],
  },
  winterknock: {
    id: "winterknock",
    label: "A Funeral Dirge",
    description:
      "The shuffle of feet, watched by downcast eyes. You might expect a widow's wail, but you are just as likely to hear nothing at all.",
    principles: ["winter", "knock"],
  },
  mothedge: {
    id: "mothedge",
    label: "The Tune of the Underworld",
    description: "Hushed conversation, careful footsteps. A dull thud comes from an alley, as a shingle slides from the roof.",
    principles: ["moth", "edge"],
  },
  mothheart: {
    id: "mothheart",
    label: "The Tune of the Bars",
    description:
      "The echoes of laughter promise the impossible merriment of others. There is music in the air, and the dancing will not stop, not yet.",
    principles: ["moth", "heart"],
  },
};

export function generateAllTunes() {
  mod.initializeAspectFile("aspects.tunes", ["singing"]);
  for (const loreAspect of LORE_ASPECTS) {
    mod.setHiddenAspect("aspects.tunes", {
      //content.singing
      id: `mariner.tune.${loreAspect}`,
      label: `mariner.tune.${loreAspect}`,
      description: "test",
    });
  }
  for (const data of Object.values(tunesData)) {
    generateBasicTune(data);
    generateLongTune(data);
  }
}
