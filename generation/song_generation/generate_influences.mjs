export const MOON_INFLUENCE = (id) => `mariner.mooninfluence.${id}`;

const INFLUENCE_FORMULA = (aspect, bonustrait, malustrait) =>
  `([~/exterior : ${MOON_INFLUENCE(
    aspect
  )}] * 10) + ([~/exterior : mariner.influences.${aspect}] * 10) + ([~/exterior:mariner.crew.traits.${bonustrait}] * 5) - ([~/exterior:mariner.crew.traits.${malustrait}] * 5)`;

const influenceFormula = {
  lantern: INFLUENCE_FORMULA("lantern", "bright", "dense"),
  edge: INFLUENCE_FORMULA("edge", "engaging", "cowardly"),
  heart: INFLUENCE_FORMULA("heart", "entertaining", "rambunctious"),
  grail: INFLUENCE_FORMULA("grail", "charming", "unpleasant"),
  forge: INFLUENCE_FORMULA("forge", "handy", "clumsy"),
  moth: INFLUENCE_FORMULA("moth", "shifty", "crude"),
  knock: INFLUENCE_FORMULA("knock", "whistling", "noisy"),
  winter: INFLUENCE_FORMULA("winter", "goodfortuned", "unlucky"),
};

export const INFLUENCE_CHANCE = (lore) => influenceFormula[lore];

const influenceImages = {
  g: 12,
  e: 8,
  c: 4,
  "": 1,
};

function generateSongInfluence({ aspect, label, description, icon, lifetime, isLocal = true }) {
  mod.setElement("influences", {
    id: `mariner.influences.${aspect}`,
    label,
    description,
    overlays: new Array(4).fill(0).map((_, index) => ({
      image: `influence${aspect}${Object.keys(influenceImages)[index]}`,
      layer: "base",
      expression: `[mariner.influences.${aspect}] >= ${Object.values(influenceImages)[index]}`,
    })),
    aspects: {
      "mariner.influence": 1,
      influence: 1,
      [aspect]: 1,
      ...(isLocal ? { "mariner.local": 1 } : {}),
    },
    lifetime,
    icon: icon ? icon : `influence${aspect}c`,
  });

  mod.setAspect("aspects.mooninfluences", {
    id: MOON_INFLUENCE(aspect),
    label: `<${mod.helpers.capitalize(aspect)} Moon Influence>`,
    description: "<The Moon carries with itself particular intent these nights.>",
  });
}

export function generateSongInfluences() {
  mod.initializeAspectFile("aspects.mooninfluences", ["singing"]);
  mod.initializeElementFile("influences", ["singing"]);

  for (const influenceData of INFLUENCES) generateSongInfluence(influenceData);
}

const INFLUENCES = [
  {
    aspect: "heart",
    label: "A Joyous Vibration",
    description:
      "A peal of laugther, a pulse of energy, a bubbling charm. [Gathering this influence around you can protect you from the ravages of Heartache]",
  },
  {
    aspect: "grail",
    label: "A Persisting Glamour",
    description:
      "Trailed by whispers, accentuated by a gasp for breath. [Having gathered this Influence around you will aid you in finding Passengers for the ship]",
  },
  {
    aspect: "forge",
    label: "Echoes Of Action",
    description:
      "Even a cooling engine still shivers and thrums. [Having gathered this Influence around you will aid you when the Kite is wounded]",
  },
  {
    aspect: "lantern",
    label: "A Clarion Air",
    description:
      "A thoughtful hum, considered words, awed silence. [Having gathered this Influence around you will aid you in attracting clever crewmembers]",
  },
  {
    aspect: "moth",
    label: "An Edge Of Static",
    description:
      "Just beyond perception, wings beat within the skull. [Having gathered this Influence around you will aid you once Authorities have their eyes on you]",
  },
  {
    aspect: "winter",
    label: "A Deafening Repose",
    description:
      "I carry a silence that others hesitate to break. [Having gathered this Influence around you will aid you in having an uneventful voyage once you set sail]",
    lifetime: 1600,
    isLocal: false,
  },
  {
    aspect: "edge",
    label: "A Lingering Thrill",
    description:
      "If a challenge is not accepted, your domination is. [Having gathered this Influence around you will aid you attracting an adventurous crew]",
  },
  {
    aspect: "knock",
    label: "A Ring Of Insight",
    description:
      "Lies flounder, voices waver, even silence is a door to truth. [Having gathered this Influence around you will aid you collecting Packages when exploring the streets with an Open Heart]",
  },
];
