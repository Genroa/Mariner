export const UPGRADE_TEXTS = {
  knock: {
    nature: {
      description:
        "The moon is a sliver outside my window, and has granted me a new understanding. I do not pen down, I slice. I carve from the sheet what is not needed, and my notes can be found in the spaces in between.",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  moth: {
    nature: {
      description:
        "Finding the right notes for this song is always a hunt. A hunt of the wind between the leaves, of grey paws through greyer mist. I learn that I do not need to understand what I see, and I do not need to see to understand.",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  lantern: {
    nature: {
      description:
        "Those who think Ideals cannot be found in nature, have not looked up at at the sky, during day or night. I will let it imprint on my retina and compose my song around that perfect circle.",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  forge: {
    nature: {
      description:
        "The heat of the forge was not the first heat in the world. the engine was not the first form of motion. Nature itself presses forward, ever changing, it only needs a mortal hand to guide it. Today however, I will let nature guide mine.",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  edge: {
    nature: {
      description:
        "A man can only know themselves when opposed, and on some days one can feel there is no truer opposition to mankind than nature. I will write an anthem that will inspire you to take up arms against impossible odds.",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  winter: {
    nature: {
      description:
        "Starting this song seems an impossibility, for I loathe to besmirch the perfect snow-white blankness of the page. In the end, I only commit the notes to memory, but there they shall remain for all eternity.",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description:
        "I am inspired by all of the whole score, but I the magic I capture is from the final notes, and the silence that follows.",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  heart: {
    nature: {
      description:
        "The world is a song thats sings eternal, with rhythms expressed in rain and wind and thundering heartbeats. I shall touch them briefly in this composition, and portray a fragment of their intensity,",
    },
    muse: {
      description: "",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
  grail: {
    nature: {
      description:
        "The sweet, the salt, the deep, the bright. This song is inspired by all that we crave for, all that that is to vast and too intense for us too behold.",
    },
    muse: {
      description:
        "I am full of memories of her, but I shall not make my inspiration plain. She has thaught me that desire is as much what you hold back as what you give.",
    },
    location: {
      description: "",
    },
    experience: {
      description: "",
    },
    melody: {
      description: "",
    },
    instrument: {
      description: "",
    },
    urstory: {
      description: "",
    },
  },
};
