export const SONG_TEXTS = {
  heart: [
    {
      label: "A Tap-Toe Motif",
      description:
        "Something to dance the night away. The long notes require hale lungs.",
    },
    {
      label: "Ceaseless Rhythms",
      description:
        "Step like thunder, shake like reeds. Take your partner and go round, and round and round. And Again! Step like thunder, shiver like reeds...",
    },
    {
      label: "Lullaby for the Sleepless Gods",
      description:
        "Oh king of the Sky, sleep no more. Oh Queens of Sea and Shore, be untouched as pearls. Oh Maiden of the Woods, you will be renewed.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  grail: [
    {
      label: "An Aching Motif",
      description:
        "Whistle this, and thirst for more than water. Hum it, and send shivers through your flesh.",
    },
    {
      label: "A Scarlet Solo",
      description:
        "It speaks of sanguine passions and an all-consuming desire, perhaps more literal than the average listener will notice.",
    },
    {
      label: "Ballad for the Pine-knight",
      description:
        "In the first 26 verses I will detail the Pine Knight's exploits in salivating detail. The last seven are harder to perform, but more rewarding. In them, his self-flagellation and subsequent transformation must be both felt and chastised. Avoid peeling at loose skin after finishing the set.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  forge: [
    {
      label: "A Blazing Motif",
      description:
        "Something to hum while working. Quickens the fingers and emboldens the mind.",
    },
    {
      label: "An Incandescent Cadence",
      description:
        "Some songs express new ideas, others inspire them. This song is the tinder that sparks revolutions.",
    },
    {
      label: "Brigid's Chant",
      description:
        "She whispered these words to the fire, she did. She speaks them to the steel, she does. She shouts them to end the night, she will.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  winter: [
    {
      label: "A Mournful Motif",
      description:
        "These dull dark tones remind me of all I have lost, yet tears refuse to roll down my face.",
    },
    {
      label: "A Chilly Elegy",
      description:
        "It is sung not for those who have passed, but those who have yet to. It is rarely performed in groups, but acknoweldges that all will sing the same tune, eventually.",
    },
    {
      label: "Requiem For The Stone",
      description:
        "It is a long and bitter song, which grinds your voice to a hoarse rocky whisper. You sing of the five siblings you buried, and of the terrible compromise you had to make. But your duty remains, and therefore, so shall you.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  edge: [
    {
      label: "A Vengeful Motif",
      description:
        "Usually whistled through gritted teeth, accompagnied only by cracking knuckles.",
    },
    {
      label: "A Valkyrian Hymn",
      description:
        "Rise up! For your battle is not over. Rise up, for your war has not been won. As long as you have breath in you, rise up and hiss this to your enemies.",
    },
    {
      label: "The Issian March",
      description:
        "The song recounts the many reasons the army heading to Issus had for fighting, the betrayal, the lies, the villainy of enemy. It ends with an ironic or chilling reprise in which the same lines are sung by the opposing forces.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  moth: [
    {
      label: "A Wild Motif",
      description:
        "This pops into my mind, unwanted, at the strangest time. No two times do I sing it the same way.",
    },
    {
      label: "A Changeling Capriccio",
      description:
        "It challenges you to follow its melody, rarely going where you expect it to. Like a falling leaf, like a madman's ramble.",
    },
    {
      label: "Moldywarp's Mazurka",
      description:
        "It is hard to predict the rise and fall of this piece. But when you dance to it, your feet will know the steps",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  lantern: [
    {
      label: "A Bright Motif",
      description:
        "I find myself whistling this while I ponder a difficult problems. The insight it brings is often as sharp as its thrills",
    },
    {
      label: "Étude de Gloria",
      description: "This is the practice that perfect makes.",
    },
    {
      label: "The Lily Kings Sonnata ",
      description:
        "This song starts softly, rising consistently in both tone and timbre, until it crescendo's in incredibly pure whistle-tones. It is famous for waking rousing addicts from their stupor, yet dangerous for a too receptive mind.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
  knock: [
    {
      label: "A Subtle Motif",
      description:
        "While we may not share a language, anyone with a soul can understands this song.",
    },
    {
      label: "An Unlocking Overture ",
      description:
        "This unlikely musical piece is most usefully performed before a theater has opened its doors.",
    },
    {
      label: "The Nagi's Bhajan",
      description:
        "Each of the sitar's strings must wound a finger when the song is correctly performed.",
    },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
};
