import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { generateProbabilityChallengeTask } from "../vault_generation/vault_generation.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateCleanHostileBarAction() {
  const recipesFile = "recipes.cleanhostile.bar";
  mod.initializeRecipeFile(recipesFile, ["hostility"]);

  generateProbabilityChallengeTask({
    recipesFile,
    actionId: "talk",
    task: {
      id: "mariner.cleanhostile.bar",
      label: "Tough Crowd",
      startdescription:
        "The crowds here have turned sour to me. My name is a curse here, and I will have trouble finding crewmembers, much less passengers. If I put my best face forward, I might be able to turn the crowd. [Approach this challenge with Heart or Grail.]",
      craftable: true,
      warmup: 0,
      aspects: { [SIGNALS.USE_HEART]: 1 },
      requirements: {
        "mariner.bar": 1,
        "mariner.location.hostile": 1,
      },
    },
    success: {
      label: "Turn the Crowd",
      description:
        "As any performer knows, audiences are fickle. A few well placed words and this one was on my hand again.",
      aspects: { "mariner.location.makenothostile": 1 },
    },
    failure: {
      label: "Unwelcome!",
      description:
        "The drinks I buy are hurled back at my face, and the door slams only inches from it.",
      // aspects: { "mariner.location.makenothostile": 1 },
      effects: { "mariner.notoriety": 1 },
      linked: [{ id: "mariner.trouble.start" }],
    },
    template: probabilityChallengeTemplates.HONEYED_TONGUE,
  });
}
