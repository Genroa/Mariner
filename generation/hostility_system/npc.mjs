import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { generateProbabilityChallengeTask } from "../vault_generation/vault_generation.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateCleanHostileNpcAction() {
  const recipesFile = "recipes.cleanhostile.npc";
  mod.initializeRecipeFile(recipesFile, ["hostility"]);

  generateProbabilityChallengeTask({
    recipesFile,
    actionId: "talk",
    task: {
      id: "mariner.cleanhostile.npc",
      label: "Trailing Shadows",
      startdescription:
        "I have acquired shadows that are not my own. My contacts rely on my discression. If I wanted to meet them to discuss business, I should find an unwatched route. [Approach this challenge with Edge or Moth.]",
      craftable: true,
      warmup: 0,
      aspects: { [SIGNALS.USE_HEART]: 1 },
      requirements: {
        "mariner.npc": 1,
        "mariner.location.hostile": 1,
      },
    },
    success: {
      label: "A Way In",
      description:
        "I unlatch a window in the back, and make my way into the house unseen by sources outside or in. Now the trick is not to scare my contact.",
      aspects: { "mariner.location.makenothostile": 1 },
    },
    failure: {
      label: "Cut Off!",
      description:
        "My route is blocked! I have to cut my losses and return another day.",

      // aspects: { "mariner.location.makenothostile": 1 },
      effects: { "mariner.notoriety": 1 },
      linked: [{ id: "mariner.trouble.start" }],
    },
    template: probabilityChallengeTemplates.SNEAK_IN,
  });
}
