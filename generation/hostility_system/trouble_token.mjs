// mariner.trouble.start is linked to by the failures to clear a hostile local element.
// mariner.haltable.trouble is the token trying to kill the player. Can be stopped by leaving the port

import { LOCAL_REPUTATION } from "../generation_helpers.mjs";

export function generateTroubleToken() {
  const recipesFile = "recipes.trouble";
  mod.initializeRecipeFile(recipesFile, ["hostility"]);
  mod.setRecipe(recipesFile, {
    id: "mariner.trouble.start",
    label: "Trouble Brewing",
    startdescription: "My recent fumble has emboldened my opposition.",
    description: "My recent fumble has emboldened my opposition.",
    inductions: [
      { id: "mariner.trouble.consequence.highestrep" },
      { id: "mariner.trouble.consequence.highrep" },
    ],
  });

  // Rep 4: leave town and give up a crewmember. Now.
  mod.setRecipe(recipesFile, {
    id: "mariner.trouble.consequence.highestrep",
    actionId: "mariner.haltable.trouble",
    warmup: 60,
    grandReqs: {
      [`${LOCAL_REPUTATION} = 4`]: 1,
    },
    label: "They're Coming For Us",
    startdescription:
      "People rushing to arrest me. They may have arrested someone of my crew. It's Unlikely I ever see their face again.",
    description: "<Fled before getting caught>",
    slots: [
      {
        id: "scapegoat",
        label: "Trouble on the Town",
        description: "I know this won't end well.",
        greedy: true,
        required: { "mariner.crew": 1 },
      },
    ],
    effects: { "mariner.crew": -1 },
    linked: [{ id: "mariner.trouble.consequence.highestrep.end" }],
  });
  mod.setRecipe(recipesFile, {
    id: "mariner.trouble.consequence.highestrep.end",
    label: "On my Heels",
    startdescription: "No more decoys, they are coming for me directly now.",
    description: "<Fled before getting caught>",
    warmup: 60,
    ending: "mariner.endings.arrested",
  });

  // Rep 3: leave town. Now.
  mod.setRecipe(recipesFile, {
    id: "mariner.trouble.consequence.highrep",
    actionId: "mariner.haltable.trouble",
    grandReqs: {
      [`${LOCAL_REPUTATION} = 3`]: 1,
    },
    label: "The Opposition Is Organising",
    startdescription: "<Soon they will form a proper Inquisition>",
    description: "<Fled before getting caught>",
    warmup: 120,
    ending: "mariner.endings.arrested",
  });
}
