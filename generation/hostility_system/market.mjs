import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { generateProbabilityChallengeTask } from "../vault_generation/vault_generation.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateCleanHostileMarketAction() {
  const recipesFile = "recipes.cleanhostile.market";
  mod.initializeRecipeFile(recipesFile, ["hostility"]);

  generateProbabilityChallengeTask({
    recipesFile,
    actionId: "explore",
    task: {
      id: "mariner.cleanhostile.market",
      label: "Shuttered Doors",
      startdescription:
        "My mercantile contacts cannot be seen associating with me, and have therefor shuttered their window and barred their door. If I wish to trade, I must find my way in unobserved.",
      craftable: true,
      warmup: 0,
      aspects: { [SIGNALS.USE_HEART]: 1 },
      requirements: {
        "mariner.market": 1,
        "mariner.location.hostile": 1,
      },
    },
    success: {
      label: "Commerce finds a Way",
      description:
        "Every proper shop has a backroom window to leave open for just the right kind of unexepcted visitor.",
      aspects: { "mariner.location.makenothostile": 1 },
    },
    failure: {
      label: "No Route of Access",
      description:
        "The shadows are not our friends today. We are spotted on the roof of a nearby window, and trouble starts anew.",
      // aspects: { "mariner.location.makenothostile": 1 },
      effects: { "mariner.notoriety": 1 },
      linked: [{ id: "mariner.trouble.start" }],
    },
    template: probabilityChallengeTemplates.SNEAK_IN,
  });
}
