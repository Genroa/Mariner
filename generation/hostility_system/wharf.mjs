import { probabilityChallengeTemplates } from "../probability_challenge_templates.mjs";
import { generateProbabilityChallengeTask } from "../vault_generation/vault_generation.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";

export function generateCleanHostileWharfAction() {
  const recipesFile = "recipes.cleanhostile.wharf";
  mod.initializeRecipeFile(recipesFile, ["hostility"]);

  generateProbabilityChallengeTask({
    recipesFile,
    actionId: "explore",
    task: {
      id: "mariner.cleanhostile.wharf",
      label: "Press Gangs",
      startdescription:
        "Some harbor press gangs have decided I am no longer welcome to conduct business on their turf. They suggest I leave town, quickly. [Adress this challenge with Forge or Edge.]",
      craftable: true,
      warmup: 0,
      requirements: {
        "mariner.wharf": 1,
        "mariner.location.hostile": 1,
      },
    },
    success: {
      label: "A Show of Strength",
      description:
        "I part them like a wave and leave them crawling in my wake.",
      aspects: { "mariner.location.makenothostile": 1, [SIGNALS.USE_HEART]: 1 },
    },
    failure: {
      label: "Outmuscled",
      description:
        "Their wiry, stained overalls form a wall as insurmountable as reinforced concrete. I must retreat, and try again later.",
      // aspects: { "mariner.location.makenothostile": 1 },
      effects: { "mariner.notoriety": 1, [SIGNALS.USE_HEART]: 1 },
    },
    template: probabilityChallengeTemplates.OVERCOME,
  });
}
