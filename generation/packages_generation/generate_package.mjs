import { ALL_SEAS, intentionSlot } from "../generation_helpers.mjs";
import { r, randomEntry, randomInArray } from "./helpers.mjs";
import { DESCRIPTIVE_SENTENCES } from "./description_data.mjs";
import { PACKAGE_DETAILS } from "./package_details.mjs";
// #############

function getRandomGenerator(pd, generators, statObject) {
  const filteredGenerators = Object.entries(generators).filter(([id, o]) => o.filter(pd));
  const selectedEntry = randomInArray(filteredGenerators);
  if (!selectedEntry) {
    statObject["failed"] = (statObject["failed"] ?? 0) + 1;
    return;
  }
  const [id, selectedGenerator] = selectedEntry;
  statObject[id] = (statObject[id] ?? 0) + 1;
  return selectedGenerator;
}

function getRandomDescriptionGenerator(pd) {
  return getRandomGenerator(pd, DESCRIPTIVE_SENTENCES, mod.packageGenerationDescriptionStats);
}

const typeEntry = (labels, materials) => (pd) => {
  pd.label = randomInArray(labels);
  pd.material = randomInArray(materials);
};
const PACKAGE_TYPES = {
  box: typeEntry(["box"], ["wooden", "metallic", "ivory"]),
  bundle_papers: typeEntry(["bundle of notes", "bundle of envelopes"]),
  cylinder: typeEntry(["cylinder"], ["wooden", "metallic", "ivory"]),
};

function selectArtwork(pd) {
  if (pd.label === "box") {
    if (pd.wrapped) return "mariner.package.bulk.base";
    return "mariner.package.box.wrapped";
  }
  if (pd.label === "cylinder") {
    if (pd.material === "wooden") return "mariner.package.woodencylinder.base";
    if (pd.material === "metal") return "mariner.package.metalcylinder.base";
    return "mariner.package.abstractcylinder.base";
  }
  if (["bundle of notes", "bundle of envelopes"].includes(pd.label)) {
    if (pd.wrapped === "crumpled paper") return "mariner.package.abstractpaper.crumpled";
    return "mariner.package.abstractpaper.base";
  }
  return "mariner.package.bulk.base";
}

/*
Delivery Targets declare a number of informations about them, used to stitch a convincing element + navigation recipe.
We read the delivery options to generate random packages informations, by selecting five details, and then correct sentences are selected based on that.
The system can set flags during the generation to keep some context about it
All package labels follow this format: [adjective][type of product]
All package descriptions follow this format: [description sentence]
*/
export function generateDeliveryPackage(deliveryOptions) {
  const p = {};
  const pd = { ...deliveryOptions }; //packageDescription

  // 1. Generate a random package description with a few details
  // 2. Try to get enough suitable sentences to produce the description and inspection.
  // 3. If it doesn't work, you cannot gather enough sentences, go back to step 1.

  // Select random item to deliver + material type
  randomEntry(PACKAGE_TYPES)(pd);

  // Pick five detail types
  r.unique(() => randomInArray(Object.keys(PACKAGE_DETAILS)), 5).map((d) => PACKAGE_DETAILS[d](pd));
  // console.log("PackageDescription:", pd);

  const elementDescriptionGenerator = getRandomDescriptionGenerator(pd);

  if (!elementDescriptionGenerator) {
    console.log("\x1b[31mCould not find an adequate description for this combination.\x1b[0m");
    console.log("\n##################");
    return;
  }

  const elementDescription = elementDescriptionGenerator.generate(pd);

  p.label = `${pd.material ? r.capitalize(pd.material) + " " : ""}${r.capitalize(pd.label)}`;

  p.description = `${pd.material === "ivory" ? "An" : "A"} ${
    p.label
  }. ${elementDescription}\n\nThe note on it says:\n<i>${randomInArray(pd.deliveryRiddles)}</i>`;

  // console.log("Final Object:", p);
  // console.log("\n##################");

  const finalPackageId = `mariner.package.${pd.portRegion}.${pd.recipientId}.${pd.packageId}`;
  mod.setElement(`packages.${pd.portRegion}`, {
    id: finalPackageId,
    icon: "packages/" + selectArtwork(pd),
    label: p.label,
    description: p.description,
    aspects: {
      "mariner.package": 1,
      [`mariner.packagerecipient.${pd.portRegion}.${pd.portId}.${pd.recipientId}`]: 1,
    },
    slots: [
      intentionSlot({
        actionId: "talk",
        description: "Do I ask for a worldly payment with my Half-Heart, or with intel for further mysteries with my Lack-heart",
      }),
    ],
  });

  // Add the package to the deck
  if (!mod.decks["decks.packages"]) {
    mod.initializeDeckFile("decks.packages", ["packages"]);
    for (const sea of ALL_SEAS) {
      mod.setDeck("decks.packages", {
        id: `mariner.decks.packages.${sea}`,
        spec: [],
        resetonexhaustion: true,
      });
    }
  }
  mod.getDeck("decks.packages", `mariner.decks.packages.${pd.portRegion}`).spec.push(finalPackageId);
}
