import { SOFT_CHEATING_EXPR } from "./generate_question.mjs";

export const LORE_QUESTIONS = {
  lantern: {
    questions: [
      {
        label: "Fathers' Eyes",
        question:
          "A story about the Sun and the Moon, both eyes of the same godhead. Each of the stars are their courtiers, and mortals below could look up and watch wisdom bleed down from the sky. But unsatisfied with the drops of wisdom they recieved on earth, they invented Dreaming, and forged a pathway up into the sky. [Respond with a Lantern Story.]",
      },
      {
        label: "Sun's shadow",
        question:
          "A story about a great king, who ruled as steward of the gods on the earth. An extension of them, not a representation, but a shadow. All that this flat world can contain [Respond with a Lantern Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("lantern", [
      "mariner.recipient.northsea.london.morland",
    ]),
  },
  forge: {
    questions: [
      {
        label: "Island Maker",
        question:
          "The Story of the Goddess of the Vulcano, who was loved and who was feared. She destroyed the land, and gave it sparks of life. With her fire-stick she summoned the old, blind fire of the earth. All around the island you can still find examples of her craft. [Respond with a Forge Story.]",
      },
      {
        label: "The Devil and the Blacksmith",
        question:
          "The Story of the blacksmith, who bargained with a God-from-Below. She promised him her service, if in trade she would gain the power of the forge, the power to make and remake all. He granted her her boon, but in repayment she used her power to shatter him, and ascended from his lair.  [Respond with a Forge Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("forge"),
  },
  edge: {
    questions: [
      {
        label: "War of the Fishes",
        question:
          "The Story of a war fought under the sea. It was fought between all the large fish against their smaller kin. The Shark fought the Minnow, the whale against the haddock, the narwhal against the pufferfish. But even with all the strength of the great fish, they could not defeat the guile, tactics or tools of their kin. And so a begrudging stalemate was reached below the waves. [Respond with a Edge Story.]",
      },
      {
        label: "the Teaching of a Hero",
        question:
          "The Story of a young warrior, send to the northern reaches, to be trained at a Fortress of Shades, on an isle of rock and salt. His mistress teaches him the blade, and lends him the slaying spear. It is in her honor that he takes up the weapon to defend the land against her sister, but her defeat will lead to his downfall... [Respond with a Edge Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("edge"),
  },
  winter: {
    questions: [
      {
        label: "The Mistake of Death",
        question:
          "Once, when the world was young, Chameleon was send from above to tell mortals not to fear death, and let them know where their souls would go. but the hare Kalulu thought the mortals could not wait on slow chameleon, and decided to bring them the news himself. But he got the words jumbled, and told them to fear death, nowhere their souls would go. When Chamelieon reach earth, it was allready to late. The direction of death was down. [Respond with a Winter Story.]",
      },
      {
        label: "The Bird Whose Song Remembered",
        question:
          "Tells a story about Cavurra, who traveled all his life, and worked hard to build up wealth. When he was old and rich, he returned to the village of his birth, to bring gifts and share in his accomplishments. But on the road, two men of his village grew envious of his riches and slew him. They came to town and claimed it was their wealth. But a lamenting bird from the bushes gave them away. He had seen the murder, and sang elegies for Cavurra, who name he would not let forget. [Respond with a Winter Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("winter", [
      "mariner.recipient.northsea.copenhagen.harald",
    ]),
  },
  heart: {
    questions: [
      {
        label: "The Parrot and the Orchard",
        question:
          "About a parrot who flew over the seven oceans and braved the four winds to reach an orchard with mango-trees of golden light. There all parrots eat of the fruits to gain youth and vigor. But the parrot did not think they deserved this gift for themselves. Instead, he carried it home to the priest who took care of it, for he thought them more deserving. But even that brahman did not think themselves worthy, and decided to gift it to the king. The king considered to gift, but decided not to eat the mango, but instead plant it in his garden, and shared the gift with all the kingdom. [Respond with a Heart Story.]",
      },
      {
        label: "Malaika and Pearls of Love",
        question: "Once, there was a poor, but kind man. He took care of his community, but could not find a partner. He decided to carve a woman from wood to keep him company. He placed his mothers pearl around their neck, and the woman came to life! Malaika loved her kind man, but her beauty held danger too. A king saw her, and stole her away to marry her. With her Husband begging the king, who would not be swayed, Malaika thought of a plan. She begged the king to just return the pearls, and then the king could take her with him. So the king threw the pearls to the ground, and with that the spell broke. [Respond with a Heart Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("heart", [
      "mariner.recipient.northsea.london.agdistis",
    ]),
  },
  grail: {
    questions: [
      {
        label: "Flesh and Loyalty",
        question:
          "A story about a man, who always ate the children his wife would bear him. To save her progeny, the clever woman hid her last son in the baobab tree, and fed him every day, in secret. When he was grown, the man discovered his son, and chased him into a honey-bearing tree. But a boy sang a song that called forth four dogs, and together they devoured the devourer. [Respond with a Grail Story.]",
      },
      {
        label: "The Wife who Never Ate",
        question:
          "A story about a young woman who arrived in a small town. She was the most beautiful woman they had even seen, and a rich, miserly man decided to take her as his wife. She never ate, and worked diligently, so he was very content, but he did not understand why his stores of rice were lowering. One day, he spied on her in the house, and saw he unlatch her hair, and grow a mouth hidden on the back of her skull. [Respond with a Grail Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("grail", [
      "mariner.recipient.northsea.amsterdam.veiledlady",
    ]),
  },
  moth: {
    questions: [
      {
        label: "Blood and Silt and Froth",
        question:
          "The story where a god was murdered, and his body carved and thrown into the flooding river. His winged wife, took flight and sail and collected every piece of him, except that most precious. From tide and foam and bandages he was reborn, and that which he had given now flooded the river every year. What can be lost, but still be man reborn. [Respond with a Moth Story.]",
      },
      {
        label: "Taranga's Holy Hairs",
        question:
          "The story of a mother named Taranga, and the child she bore to early. It was sickly and small, and she cut off her own hair to wrap it in. She went to the ocean surf, and begged the question if her child would live or die. A moment later, the surf had stolen it away, and raised it among the seabirds and the fishes and the spirits of the deep. But the sea is too salty for a mortal, and one day he would return to the world of man, Maui of Tarangi holy hairs. [Respond with a Moth Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("moth"),
  },
  knock: {
    questions: [
      {
        label: "The Gifts of Night",
        question:
          "A story of the man, E Mkong, who dove into the river and rose in the land where snakes walk like men. There he was gifted the secret knowledge of fire. He took it back to earth and with the fire came the night, to separate days from each other and allow men to dream. [Respond with a Knock Story.]",
      },
      {
        label: "Trick-born, Robber-Queen",
        question:
          "A story of the  youngest of seven daughters. She leaves the home she grew up in, and becomes an apprentice of the robbers that have terrorized her county. They lay before her the challenges to steal the oxen, which she does with guile and trickery. After that she is chosen as the robber-leader, and she drives the band away. Then she returns the robbed riches and the tricked oxen and the robber-house is left empty. [Respond with a Knock Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("knock"),
  },
};
