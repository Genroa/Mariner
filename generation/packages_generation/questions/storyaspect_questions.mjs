import { SOFT_CHEATING_EXPR } from "./generate_question.mjs";

export const STORY_ASPECT_QUESTIONS = {
  romantic: {
    aspect: "mariner.storyaspect.romantic",
    questions: [
      {
        label: "The Lasting Love of Quas and Laila",
        question:
          "The story of two youngsters, who fell in love in their village on the edges of rock and sand. Their fate should have been to grow old and happy and insignificant, if the stars had been only kinder. Now she was carried off to another life with another man, in the north, and he languished in the desert. But he left, carved into the rock, three verses of poetry so famous that Laila and his love are still remembered today. [Respond with a Romantic Story.]",
      },
      {
        label: "The Love of the Cut Sleeve",
        question:
          "The story of an emperor, who loved his companion so dearly, that he could not do anything to bother him. One day, his companion had fallen asleep as they reposed so that the emperor’s clothing was caught under. Rather than wake up his lover, he took a knife and cut off the embroidered silks. He held court for the day with a sleeve of cut silk, and all could witness his devotion. [Respond with a Romantic Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.romantic"),
  },
  grim: {
    aspect: "mariner.storyaspect.grim",
    questions: [
      {
        label: "la Llorona",
        question:
          "The story of a weeping woman, who is sometimes seen on the waterfronts and river-sides. What does she weep for? Her children, lost to her forever, or is it simply hunger that drives her wailing. One thing that is for sure, is that children should not wander too far from their mothers at the waterfront, for whatever her dark desire, the weeping woman may claim them. [Respond with a Grim Story.]",
      },
      {
        label: "King In Darkness",
        question:
          "The story of a king ruled in a kingdom of darkness, but hated that darkness, and he reviled our world for its light. So he sends out a hound to steal for him the sun. It’s great maw darkened it in our sky, but the sun's fire rebuffed it.. Another hound goes after the moon, many nights in a row, but when only  a sliver remains, the hound cougs it back up. its cold mirror surface rebuffs its bites. [Respond with a Grim Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.grim"),
  },
  rousing: {
    aspect: "mariner.storyaspect.rousing",
    questions: [
      {
        label: "Maui Steals the Secret of Fire",
        question:
          "One day, Maui wished to learn the secrets of fire from the lands of the ancestors. He went to his grandmother, the fire goddess Mahuika to beg for flame. 10 times, she ripped of a fingernail to produce flame, and 10 times he ‘accidentally’ doused it in the river. In her anger, she challenged him to a duel, and in his craftiness, he challenged her for the secrets to produce flame. They clashed in a contest of strength, and with his powers of transformation, he won secrets of fire and brought it to his mortal kin. [Respond with a Rousing Story.]",
      },
      {
        label: "The Princes of Serendip and the Monstrous Hand",
        question:
          "Once upon a time, there were three princes, journeying from the Land of Serendip. One day, when they were in the employ of a great Sjah, they were sent to fight a monster that was terrorizing the coast. 5 headed and giant it was, and it had slain all champions that had opposed it. But the youngest prince watched the monster, and simply raised two fingers, in the signal of victory. Each head perished before his brazen piety. [Respond with a Rousing Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.rousing"),
  },
  wise: {
    aspect: "mariner.storyaspect.wise",
    questions: [
      {
        label: "The Princes of Serendip and Forced Succession",
        question:
          "Once the king of Serendip had three sons, taught in his court by scholars, advisors and wise men. The king decided to test their wisdom, and declared his intention to retire, and told his sons to decide who should rule. They all declined in turn, stating it improper to rule with an elder relative ahead in the line. They passed this test, but the king feigned anger and banished all three from his court. Because while the answer was fair in the eyes of law and morals, he saw they needed a worldly education as well. [Respond with a Wise Story.]",
      },
      {
        label: "The Shipwrecked Sailor",
        question:
          "After a Shipwreck, a sailor arrives on the island ruled by a giant Snake, the last member of his tribe after a rain of stars whiped the other seventyfive from existance. The Snake regales him with gifts and comforts, and the sailor promises that, while he cannot repay him now, he will tell the Pharaoh of the snakes generocity, and the pharaoh will reward him in turn. Days later, the sailor is rescued by a passing ship, and tells of his miraculous journey to the pharaoh. [Respond with a Wise Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.wise"),
  },
  whimsical: {
    aspect: "mariner.storyaspect.whimsical",
    questions: [
      {
        label: "Fox and Crow",
        question:
          "The story of a fox that bullied a crow, and how the bird got its revenge. A lazy fox made his nest next to a crows nest. Under thread of eating the crows eggs, he first extorted the bird for food, then drink, then a bed. The bird supplied him with a cooked chicken, a water pale, and then the down from his own nest. The the fox requested laughter, and the bird brought him to the hunman village, playing tricks on the men and woman there. But in his antics he lead the village dogs right to fox. [Respond with a Whimsical Story.]",
      },
      {
        label: "Why the Kookaburra Laughs",
        question:
          "Once, in the dreamtime the Kookaburra was a very stern and dignified bird. One day, the Kookaburra was dosing, as a snake lay on a stone below. The kookaburra battled to convince his stomach he didn't need to eat another snake, as a wagtail landed on the stone. It hopped around, until it hopped right into the snakes mouth. the snake seemed contented, until it coiled and recoiled, and spit up the wagtail, carrying a frog in its mouth. The Kookaburra considered he would have almost eaten a snake, which contained a wagtail which contained a frog. And that strange proposed occurance caused the kookaburra such merriment that they are laughing to this day. [Respond with a Whimsical Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.whimsical"),
  },
  witty: {
    aspect: "mariner.storyaspect.witty",
    questions: [
      {
        label: "Tricking the Dokkaibi",
        question:
          "Of a man, who found his house haunted by a dokkaebi. But instead of fearing the goblin, he decided to use its own cleverness to trick it. They talked and shared their life, and eventually the monster admitted his one fear was blood, and the man told him the one thing he feared was money. The next day, the man bought a bag of oxen blood, and dumped it on the dokkaebi, and in revenge the goblin dumped a sack of gold onto the man. [Respond with a Witty Story.]",
      },
      {
        label: "The Princes of Serendip and the Queen",
        question:
          "Of three princes, from the kingdom of Serendip. One day, on their travels, they arrive at the court of a virgin queen. Her father has declared that only a clever suitor could court her, and a challenge to test them. The oldest prince was tasked to divide 5 snake eggs fairly between the queen, her advisor and himself. After consideration he gave 3 to the queen, 1 to the advisor and 1 for himself. When asked, he explained that since the men carried a pair of eggs in the folds of their pants, this division was only fair. [Respond with a Witty Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.witty"),
  },
  occult: {
    aspect: "mariner.storyaspect.occult",
    questions: [
      {
        label: "Kitling Ripe Gets Around",
        question:
          "The story of a cat-thing called Kitling Ripe, and her journeys for her other grandmother. The clever little thing interrupts the dealings of seven wood-witches, and steals from each site. With her prizes, she wakes her other grandmother from her grave by dancing on the loam. [Respond with a Occult Story.]",
      },
      {
        label: "The Golden General and the Secrets of betrayal",
        question:
          "Have you heard of the general that was trained by a god? In the greatest stories, the Shadowless Empire armies were led by a boy of gold, who got trained in the arts of cunning and strategy by the Scarred Man of Perseids. After that education, he led the empire to victory after victory… until the Perseids came to attack. He refused to fight those aligned to his master… The King of Kings needed him, and in a gamble, taught him the secret of oath breaking. It did not serve the king well, as the general betrayed not once but twice, and left the empire behind. [Respond with a Occult Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.occult"),
  },
  tragic: {
    aspect: "mariner.storyaspect.tragic",
    questions: [
      {
        label: "The Twelve Works",
        question:
          "A story of a man, who battlelust-blind slew his own family. He is known for twelve heroic works, but could he be called a hero, when they were all enacted in penance for his great misdeed? [Respond with a Tragic Story.]",
      },
      {
        label: "The Waiting Wives",
        question:
          "In a brutal war, with the Dynasty to the North, all the men of the village where taken of to war. The women of the village would visit the cliffs, and stare out over the sea, waiting for their men returns. As day passed nad the winds buffeted them, the days turned to nights, and the seasons turned. The woman remained, but where no longer there. Now a row of rocks could be seen, ever vigilant over the unforgiving sea. [Respond with a Tragic Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.tragic"),
  },
  apollonian: {
    aspect: "mariner.storyaspect.apollonian",
    questions: [
      {
        label: "Pan Twardowski",
        question:
          "A Noble of Krakow made a deal with a Demon of flame, a deal for skill and knowledge. He became a famous advisor for the king, knowledged in crafting and astrology, and he conjured the dead in a mirror. When the price of the deal came due, the demon came to collect him for his service. After his Prayer, Mary of Reflected Glory intervened, and the Devil dropped Pan. He Landed on the Moon, where he remains to this day, in the house of his mistress. [Respond with a Apollonian Story.]",
      },
      {
        label: "The Alchemist Who Flew to Heaven",
        question:
          "In the Middle Kingdom lived an official, skilled in the arts of gunpowder and light. As he saw the fire arrows ascend into the sky, he fealt inspired he could reach Heaven this way as well. he deviced a Chair, with fourty-seven strapped to it. After he took seat, he lit each flower of fire in a great conflagration. when the smoke cleared, both the chair and official where gone. [Respond with a Apollonian Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.apollonian"),
  },
  dionysian: {
    aspect: "mariner.storyaspect.dionysian",
    questions: [
      {
        label: "How Beer was Invented",
        question:
          "Sekhmet, Goddess of War and Slaughter, descended to Earth thirsting for blood. Those few who survived her rampage knew they had to trick her. They brewed a red beer of the wheat of the land, and one man called the goddess, offering her the vats as the blood of his people. She drunk, and drowsy and sleepy, she stumbled home. This is how beer saved humanity, and why it is drunk at celebrations. [Respond with a Dionysian Story.]",
      },
      {
        label: "How the Fletcher became a Cannibal",
        question:
          "Once, a fletcher knicked a finger on a stone arrowhead. He sucked on the finger and delighted by the taste, knibbled on his flesh until there was nothing left on his bones. Then, still hungry, he continued to consume every person in his village. His wife and son escaped, and fled grass-tip quiet. But the Cannibal-fletcher still chased them. They found refuge with the Tabacco-eaters, who slew the fletcher by piercing his last remaining flesh, his beating heart, with one of his own arrow-heads. [Respond with a Dionysian Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.dionysian"),
  },
  mortal: {
    aspect: "mariner.storyaspect.mortal",
    questions: [
      {
        label: "The Hero of Harlem",
        question:
          "One evening, a boy, strolling alongside the dike on his way home, noticed that a leak had sprung in it. Knowing the danger that posed, if the hole were to widen or the dike to wash away, the boy pushed his forearm into the hole, plugging it. He could not leave, knowing it would spell doom to his town. So all night he stood there, until he was found by farmers in the morning. [Respond with a Mortal Story.]",
      },
      {
        label: "The Magnolia Warrioress",
        question:
          "Once, the call came for all families of the middle kingdom to send a man from their family to defend the borders. The Hua family had no sons, and Old Father Hua prepared to go to war. But his intrepid daughter Magnolia stole his armaments, bound her hair and her body, and went in his stead. As a boy now, Magnolia won many victories, and he rose through the ranks of the army. In the end, he rejected a permanent office, unbound their body and returned to her family. [Respond with a Mortal Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.mortal"),
  },
  cthonic: {
    aspect: "mariner.storyaspect.cthonic",
    questions: [
      {
        label: "Baba Yaga Visits in the Night",
        question:
          "In the Old Country, if you live too close to the Woods Without End, you have to honor the Baba Yaga. If she visits, do not look at the glint her iron teeth, or the movements her long fingers. Simply let her count your spoons, and if the number pleases her, she will soon be on her way. [Respond with a C'thonic Story.]",
      },
      {
        label: "Maui Travels to the Land of the Ancestors",
        question:
          "Once, Maui the great warrior wished to know who his father was. He begged his mother Taranga, but she would not budge. But then he tricked her into returning to the Land of the Ancestors, and he followed her through her secret ways in the guise of a bird. Down he descended, past silent oceans and great nephrite trees. [Respond with a C'thonic Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.cthonic"),
  },
  horomachistry: {
    aspect: "mariner.storyaspect.horomachistry",
    questions: [
      {
        label: "The Feast of the Gods",
        question:
          "At the close of any era, the Gods hold a great feast... The Crow sits with the Moonsickle, and the Lilyking dances with the Double-Edged. They will discard it when they are done, as they do with everything beneath their notice. [Respond with a Horomachistry Story.]",
      },
      {
        label: "The Corrivality",
        question:
          "Many times you can find the same people opposing each other on different battlefields. In a real way, all those battles are one, and each participant is just an avatar of the same dyad. Samson and goliath, revolution versus order, recruit opposing veteran, scars versus strength. [Respond with a Horomachistry Story.]",
      },
    ],
    chance: SOFT_CHEATING_EXPR("mariner.storyaspect.horomachistry"),
  },
};
