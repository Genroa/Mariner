import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { defineQuestionChance } from "../helpers.mjs";

export const SOFT_CHEATING_EXPR = (aspect, npcs) =>
  `max(([~/extant : { [mariner.story]>1 } : ${aspect}]${
    npcs ? "+" + npcs.map((s) => `[~/extant : ${s}]`).join(" + ") : ""
  }) * 100, 20)`;

export function generateQuestion({ type, chance, questionId, index, label, startdescription, requirements }) {
  const entireId = `mariner.getpackage.question.ask.${type}.${questionId + (index ? `.${index}` : "")}`;
  mod.setRecipes(
    "recipes.getpackage.questions",
    {
      id: entireId,
      warmup: 30,
      label,
      startdescription,
      slots: [{ id: "story", label: "An Answer", required: { "mariner.story": 1 } }],
      linked: [{ id: `mariner.getpackage.question.${type}.${questionId}.success` }, { id: "mariner.getpackage.failure" }],
    },
    // This recipe is actually redefined for each question asking the same answer. In the end, only one instance of success is defined for each answer. Better.
    {
      id: `mariner.getpackage.question.${type}.${questionId}.success`,
      requirements: { "mariner.story": 1, ...requirements },
      furthermore: [
        {
          effects: { "mariner.howmanyraconteurquestionsanswered": 1 },
        },
        {
          aspects: { [SIGNALS.EXHAUST_STORY]: 1 },
        },
        {
          movements: {
            "~/tabletop": ["mariner.story.exhausted"],
          },
        },
      ],
      linked: [{ id: "mariner.getpackage.end" }, { id: "mariner.getpackage.loop" }],
    }
  );
  if (chance !== undefined) defineQuestionChance(entireId, chance);
}
