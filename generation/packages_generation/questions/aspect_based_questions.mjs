import { generateQuestion } from "./generate_question.mjs";
import { LORE_QUESTIONS } from "./lore_questions.mjs";
import { STORY_ASPECT_QUESTIONS } from "./storyaspect_questions.mjs";

const QUESTIONS = {
  ...LORE_QUESTIONS,
  ...STORY_ASPECT_QUESTIONS,
};
function generateAspectBasedQuestion(id, aspect) {
  let i = 1;
  for (const question of QUESTIONS[id]?.questions ?? []) {
    generateQuestion({
      type: "aspectbased",
      chance: QUESTIONS[id]?.chance,
      questionId: id,
      index: i,
      label: question.label,
      startdescription: question.question,
      requirements: { [aspect ?? id]: 1 },
    });
    i++;
  }
}

export function generateAspectBasedQuestions() {
  for (const id of Object.keys(QUESTIONS)) {
    generateAspectBasedQuestion(id, QUESTIONS[id].aspect);
  }
}
