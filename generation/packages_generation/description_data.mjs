import { randomInArray } from "./helpers.mjs";

export const DESCRIPTIVE_SENTENCES = {
    // BLOOD SPOTS
    bloodSpatters: {
        filter: pd => pd.stained === "spotted" && pd.stainMaterial === "blood",
        generate: pd => randomInArray([
            "the edges of the package are spotted with dark brown stains. simple paper cuts or was the package wrapped with bleeding fingers"
        ])
    },
    // BLOOD SPATTERS
    bloodSpatters: {
        filter: pd => pd.stained === "speckled" && pd.stainMaterial === "blood",
        generate: pd => randomInArray([
            "tiny specks cover the entirety of the package. They remind me uncomfortably much of bloodspatter"
        ])
    },
    // BLOOD STAINS
    bloodStains: {
        filter: pd => pd.stained === "smeared" && pd.stainMaterial === "blood",
        generate: pd => randomInArray([
            "It is smeared in dark, dried blood. Was it bathed in it, or did someone pay a big price?",
            "Thick blood stains the entire package. I do not want to know how it ended up here."
        ])
    },
    // MOLD STAINS
    moldStains: {
        filter: pd => ["smeared", "spotted"].includes(pd.stained) && pd.stainMaterial.includes("mold"),
        generate: pd => randomInArray([
            `Parts of the package are covered in ${pd.stainMaterial}. The elements haven't been gentle. How long has it been traveling?`,
            `The bottom part is being slowly eaten by some kind of mold.`,
            `Strange blooms of ${pd.stainMaterial} form weaving patterns over the package. I try not to touch them`
        ])
    },
    // MOLD SPECKS
    moldspecks: {
        filter: pd => ["speckled"].includes(pd.stained) && pd.stainMaterial.includes("mold"),
        generate: pd => randomInArray([
            `small specks of ${pd.stainMaterial} stain the package surface. I think it might be growing from the inside.`
        ])
    },
    // MUD
    mudStains: {
        filter: pd => ["smeared", "spotted"].includes(pd.stained) && pd.stainMaterial.includes("mud"),
        generate: pd => randomInArray([
            "Mud and dirty handprints decorate the entire thing.",
            "this package appears it may have been buried and dug up again. Clumps of earth come out between the packages"
        ])

    },
    // STAINED + THICK/WRAPPED WITH care
    stainedWrapped: {
        filter: pd => ["spotted", "smeared"].includes(pd.stained) && pd.wrapped && ["with care", "hastily"].includes(pd.wrappingType),
        generate: pd => `It is ${pd.stained} with ${pd.stainMaterial}, which probably explains why it was wrapped  ${pd.wrappingType}, to protect it with as much protection as possible,`
    },
    // WRAPPING
    wrappedManyLayersCare: {
        filter: pd => pd.wrapped === "many layers" && pd.wrappingType === "with care",
        generate: pd => "It is wrapped in many thick layers to protect it, just like an onion."
    },
    wrappedManyLayersHaste: {
        filter: pd => pd.wrapped === "many layers" && pd.wrappingType === "hastily",
        generate: pd => randomInArray(["It is wrapped in many thick disorganized layers, like if someone had grabbed every sheets of paper they could find to hide it from praying eyes.",
            "it is wrapped shodily, with a variety of materials. Clearly whatever is inside is not meant to see the light of day."
        ])
    },
    wrappedExpensive: {
        filter: pd => pd.wrapped && pd.wrappingType === "thick expensive paper",
        generate: pd => randomInArray([
            "It is wrapped in really thick and expensive paper. Someone <i>really</i> didn't want others to see through it.",
            "Wrapped in some kind of thick art paper. Could there...could there be drawings on the other side of those? I am not sure."
        ])
    },
    wrappedBlood: {
        filter: pd => pd.wrapped && pd.wrappingType === "thick expensive paper" && pd.stainMaterial === "blood",
        generate: pd => "Wrapped in a thick and expensive paper; it is soaked in blood in quite a few places. On second sight, theses could look like drawings..."
    },
    wrappedShellfish: {
        filter: pd => pd.wrappingType === "partially covered by shellfish",
        generate: pd => "In places, shells protude from the surface of this object as if they were protecting it. Or maybe they attempted to consume it? There might not be much left to deliver."
    },
    wrappedCareStamped: {
        filter: pd => pd.wrappingType === "with care" && pd.stamped,
        generate: pd => "This package has been wrapped meticulously, in perfect symmetry. even the stamps have been placed with mathematical precision."
    },
    wrappedCareStinking: {
        filter: pd => pd.wrappingType === "with care" && pd.stinking,
        generate: pd => `Each corner is neatly folded and each edge sealed with wax, but still it does not stop the smell of ${pd.odour}.`
    },
    wrappedCareStain: {
        filter: pd => pd.wrappingType === "with care" && pd.stained,
        generate: pd => randomInArray([
            `The package is pristinely wrapped, but the presentation is ruined by the spots of ${pd.stainMaterial}`
        ])
    },
    wrappedmold: {
        filter: pd => pd.wrapped && pd.stainMaterial === "mold",
        generate: pd => randomInArray([
            `From a tear in the wrapping, long white tendrils of mold have spread out over the outside. It is quite evident they are growing from within.`
        ])
    },
    wrappedAndEngraved: {
        filter: pd => pd.wrapped && pd.engraved,
        generate: pd => randomInArray([
            `The package is wrapped in thick paper, but when I hold it against the light I can see symbols shining through`,
            `The packing material of this package hangs around it in rags. It seems the engravings on the objects have burnt through the paper whever they touched it.`
        ])
    },
    // STAMPS
    stamped: {
        filter: pd => ["unknown stamps", "occult stamps"].includes(pd.stamped) && pd.stampType !== "old",
        generate: pd => `Its surface is covered in ${pd.stampType} stamps. Dozens of them.`
    },
    bloodyStamps: {
        filter: pd => ["unknown stamps", "occult stamps"].includes(pd.stamped) && pd.stained && pd.stainMaterial === "blood",
        generate: pd => randomInArray([`Its surface is covered in ${pd.stampType} stamps, partially covered by blood. Hold up... Are they <i>made</i> of blood?`,
            `Unfamiliar stamps adorn this package. They are stuck on there with what reminds me a suspicous amount of blood.`
        ])
    },
    // Old stamps
    oldStamps: {
        filter: pd => ["unknown stamps", "occult stamps"].includes(pd.stamped) && pd.stampType !== "old",
        generate: pd => "Lots of old stamps seem to indicate this package already traveled a <i>lot</i>."
    },
    noStamp: {
        filter: pd => pd.stamped === "no stamp",
        generate: pd => "There isn't a single stamp on this one. Am I the first to transport it?"
    },
    //TAGGED
    taggedWrongAddress: {
        filter: pd => pd.tagged === "wrong address",
        generate: pd => randomInArray([
            `A little note added here says: "${randomInArray([
                "For some reason this traveled through all of the Continent. The previous carrier seemed to have troubles and suddenly disappeared.",
                "This one traveled to a number of locations already. The previous carrier somehow tried to deliver it to a series of wrong addresses, claiming 'it was the right one this time and to take it before the address changes again', according to some reports.",
            ])}"`,
            "There is an address on it. It is is crossed out, and a different pen wrote a new address. And a new one. And a new one. Based on that, I'm not even sure someone will be there to accept it when I deliver it."
        ])
    },
    taggedPartiallyErased: {
        filter: pd => pd.tagged === "partially erased address",
        generate: pd => "There is an address on it but it has almost been erased. Good luck finding the recipient..."
    },

    //engraved
    engravedPaper: {
        filter: pd => pd.engraved && pd.label.includes("bundle"),
        generate: pd => randomInArray([
            `Somehow, ${pd.engraved} are burnt into the paper, like if someone had tried to write something using the flame of a candle.`,
            `The paper is full of deep cuts that seem to form ${pd.engraved}...`
        ])
    },
    engraved: {
        filter: pd => pd.engraved && !pd.label.includes("bundle"),
        generate: pd => randomInArray([
            `${mod.helpers.capitalize(pd.engraved)} are etched directly into the material. They look...${randomInArray([
                "disturbing", "fascinating", "bright, like if they were glowing. Maybe it is some trick of the light"
            ])}.`,
            `${mod.helpers.capitalize(pd.engraved)} are beautifully etched all over it.`
        ])
    },
    engravedStainedBlood: {
        filter: pd => pd.engraved && !pd.label.includes("bundle") && pd.stained && pd.stainMaterial === "blood",
        generate: pd => randomInArray([
            `${mod.helpers.capitalize(pd.engraved)} are etched directly into the material. They look deeper than what the material of the object seem to allow... It's stained with blood. Is it flowing out <i>from the scars</i>?`,
            `${mod.helpers.capitalize(pd.engraved)} are etched directly into the material, like a dozen thirsty mouths. It's probably just an illusion, but it <i>looks</i> like the blood that stains it is drunk by them.`
        ])
    },
    engravedMold: {
        filter: pd => pd.engraved && pd.stained && pd.stainMaterial === "mold",
        generate: pd => randomInArray([
            "The engravings are half covered by a pervasive mold, which has enveloped much of the package.",
            "Spiraling designs etched into the covering of the package are opposed by strange mold blooms, which seem to attempt to cover each rune."
        ])
    },
    engravedAndBarnacles: {
        filter: pd => pd.engraved && pd.wrapped && pd.wrappingType === "covered in shellfish",
        generate: pd => randomInArray([
            "A package pockmarked by barnacles and coral. On the surface left empty I can just still see the ancient alphabet inscribed on it.",
        ])
    },
    // stinking
    stinking: {
        filter: pd => pd.odour && pd.odourStrength === "reeking",
        generate: pd => randomInArray([
            `No need to approach package to notice that it reeks of ${pd.odour}.`,
            `There are many details the eye could inspect... But all the attention is grabbed by <i>that smell</i>. I'm not sure of what it is...${pd.odour}?`,
            `It stinks terribly. I think I recognize it: ${pd.odour}. The package almost feels wet to the touch...`
        ])
    },
    subtleOdour: {
        filter: pd => pd.odour && pd.odourStrength === "subtle",
        generate: pd => randomInArray([
            `When approaching it, I can definitely feel a subtle odor of...${pd.odour}.`
        ])
    },
    freshSea: {
        filter: pd => pd.odour === "sea water" && pd.wrappingType === "partially covered by shellfish",
        generate: pd => randomInArray([
            `This barnacle-encrusted package still smells of salt and decay. This one was recently pulled out of the sea.`
        ])
    },
    deadFish: {
        filter: pd => pd.odour === "decomposition" && pd.wrappingType === "partially covered by shellfish",
        generate: pd => randomInArray([
            `This package is overgrown with coral, barnacles and shells. It Also stinks of rotten fish. I think some of these shells might have still been alive until recently.`
        ])
    },
    BOG: {
        filter: pd => pd.odour === "decomposition" && pd.stainMaterial === "mud",
        generate: pd => randomInArray([
            `The entirety of this package is coated in a thick, black sludge. I think it was recently unearthed from a bog.`
        ])
    },
    //WEIGHT
    abnormallyHeavy: {
        filter: pd => pd.weight === "abnormally heavy",
        generate: pd => `Something is wrong. I should be able to easily lift it, but somehow it feels <i>heavier</i> than possible. It quickly tires my arm.`
    },
    abnormallyLight: {
        filter: pd => pd.weight === "abnormally light",
        generate: pd => `I almost injured myself when lifting it... It feels so light! It's almost as if my hand isn't holding anything.`
    },
    normalWeight: {
        filter: pd => ["quite heavy", "quite light"].includes(pd.weight),
        generate: pd => `It's ${pd.weight}, for its size. What could it be?`
    }
}