import { ALL_SEAS } from "../generation_helpers.mjs";
import { generateQuestions } from "./questions/generate_questions.mjs";

function generatePickupAction() {
  mod.initializeElementFile("packages.utils", ["packages"]);
  mod.setElement("packages.utils", {
    id: "mariner.howmanyraconteurquestionsanswered",
    isHidden: true,
  });

  mod.initializeRecipeFile("recipes.getpackage", ["packages"]);
  mod.initializeRecipeFile("recipes.getpackage.questions", ["packages"]);
  mod.initializeRecipeFile("recipes.giveuppackage", ["packages"]);

  mod.setRecipe("recipes.getpackage", {
    id: "mariner.getpackage.start",
    actionId: "mariner.getpackage",
    linked: [
      { id: "mariner.getpackage.localto.*", chance: 30 },
      { id: "mariner.getpackage.for.*", randomPick: true },
    ],
  });

  for (const sea of ALL_SEAS) {
    mod.setRecipe("recipes.getpackage", {
      id: `mariner.getpackage.localto.${sea}`,
      grandReqs: {
        [`[~/visible : mariner.locations.${sea}.sailingthere]`]: 1,
      },
      deckeffects: {
        [`mariner.decks.packages.${sea}`]: 1,
      },
      linked: [{ id: "mariner.getpackage.loop" }],
    });

    mod.setRecipe("recipes.getpackage", {
      id: `mariner.getpackage.for.${sea}`,
      grandReqs: {
        [`[~/visible : mariner.locations.${sea}.sailingthere] or [~/visible : mariner.locations.${sea}.presentthere]`]: 1,
      },
      deckeffects: {
        [`mariner.decks.packages.${sea}`]: 1,
      },
      linked: [{ id: "mariner.getpackage.loop" }],
    });
  }

  mod.setRecipe("recipes.getpackage", {
    id: "mariner.getpackage.loop",
    alt: [{ id: "mariner.getpackage.loop.manypackages" }],
    warmup: 10,
    label: "One Of Them?",
    startdescription:
      "Do they think I'm a member? Can I be sure they are? We are exchanging stories, or shaking hands the way members do? I am not sure. All I know is, if they are, my stories please them, they might give something more substantial than tales to me.\n\n[The package they might offer is displayed at the bottom of the window.]",
    linked: [{ id: "mariner.getpackage.question.ask.*", randomPick: true }],
  });
  mod.setRecipe("recipes.getpackage", {
    id: "mariner.getpackage.loop.manypackages",
    requirements: { "mariner.howmanyraconteurquestionsanswered": 2 },
    label: "They're Suspicious",
    startdescription:
      "Does the Network think I'm hoarding too many packages? If this person is indeed a member, they're suspicious about my behaviour. They're telling me more stories than usually needed to conclude an initiate's handshake.",
    linked: [{ id: "mariner.getpackage.question.ask.*", randomPick: true }],
  });

  mod.setRecipe("recipes.getpackage", {
    id: "mariner.getpackage.end",
    grandReqs: {
      "mariner.howmanyraconteurquestionsanswered": "[~/exterior : mariner.package]+1",
    },
    effects: {
      "mariner.howmanyraconteurquestionsanswered": -30,
    },
    label: "Package Received",
    description:
      "The Connection made, the conversation flows to different topics, before we part ways, now my back burdened with another satchel, and theirs lighter by equal measure.",
  });

  // Either no story was provided, or the wrong one was provided
  mod.setRecipe("recipes.getpackage", {
    id: "mariner.getpackage.failure",
    effects: {
      "mariner.package": -1,
      "mariner.howmanyraconteurquestionsanswered": -30,
    },
    label: "Network Frayed",
    description:
      "The conversation falters, then halts entirely. We could not establish with certainty the other was a contact, so we drift apart again. I leave, unsure if I missed out something very important or missed nothing at all.",
  });

  mod.setRecipe("recipes.giveuppackage", {
    actionId: "mariner.navigate",
    id: "mariner.giveuppackage",
    label: "Send a Package back into the Network",
    startdescription:
      "If I cannot fulfill a delivery, or wish to unburden myself from it, and send it of with another raconteur.",
    description:
      "After the verbal posturing that is so typical in a meeting between two raconteurs, I pass on the package and the clue, for my troubles I am left with a small sum. A triffle compared to what I could have gotten completing the delivery.",
    requirements: {
      "mariner.package": 1,
    },
    craftable: true,
    warmup: 30,
    effects: {
      "mariner.package": -1,
      funds: 1,
    },
  });
}

function generateDeliveryScaffholding() {
  mod.initializeRecipeFile("recipes.deliveries", ["packages"]);
  mod.recipes["recipes.deliveries"].push(
    ...[
      {
        id: "mariner.deliverpackage.start",
        label: "Deliver Package",
        actionId: "talk",
        startdescription: "Deliver the package",
        craftable: true,
        warmup: 30,
        requirements: {
          "mariner.recipient": 1,
          "mariner.package": 1,
          "mariner.location.nothostile": 1,
        },
        linked: [{ id: "mariner.deliverpackage.failure" }],
      },
      {
        id: "mariner.deliverpackage.failure",
        label: "Wrong Address",
        actionId: "talk",
        startdescription: "Mistaken Identity, Failed Delivery",
        craftable: false,
        warmup: 30,
        effects: {
          "mariner.package": -1,
          "mariner.notoriety": 1,
        },
      },
    ]
  );
}

export function generateDeliverySystem() {
  generatePickupAction();
  generateDeliveryScaffholding();
  generateQuestions();
}
