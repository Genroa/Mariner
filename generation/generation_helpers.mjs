export const LORE_ASPECTS = ["lantern", "forge", "edge", "winter", "heart", "grail", "moth", "knock"];
export const LORE_SUBVERSION_WHEEL = ["lantern", "forge", "edge", "winter", "heart", "grail", "moth"];

export function buildRefinementBlock(aspectsAndTexts, defaultText) {
  return (
    "@#" +
    Object.entries(aspectsAndTexts)
      .map(([aspect, text]) => `${aspect}|${text}`)
      .join("#") +
    ((defaultText && `#|${defaultText}`) || "") +
    "@"
  );
}

export function buildAdvancedRefinementBlock(parts, defaultText) {
  return (
    "@#" +
    parts.map(({ aspect, value, text }) => `${aspect}|${value}|${text}`).join("#") +
    ((defaultText && `#|${defaultText}`) || "") +
    "@"
  );
}

export const wanderingEventScaffhold = (eventId) => ({
  RECIPE_FILE: `recipes.${eventId}`,
  QUEST_PREFIX: `mariner.${eventId}`,
  EVENT_START_ID: `mariner.wanderingevents.start.${eventId}`,
  EVENT_ID: eventId,
  EVENT_PREFIX: `mariner.wanderingevents.${eventId}`,
});

export const specialWanderingEventScaffhold = (eventId) => ({
  RECIPE_FILE: `recipes.${eventId}`,
  QUEST_PREFIX: `mariner.${eventId}`,
  EVENT_START_ID: `mariner.wanderingevents.special.start.${eventId}`,
  EVENT_ID: eventId,
  EVENT_PREFIX: `mariner.wanderingevents.${eventId}`,
});

export const setWanderingEventChance = (eventId, chance) => {
  if (!mod.recipes["recipes.wanderlust"]) mod.readRecipesFile("recipes.wanderlust", ["wanderlust"]);
  const link = mod
    .getRecipe("recipes.wanderlust", "mariner.tide.wanderlust")
    .linked.find((o) => o.id === "mariner.wanderingevents.start.*");
  link.chances = { ...link.chances, [eventId]: chance };
};

export const setSpecialWanderingEventChance = (eventId, chance) => {
  if (!mod.recipes["recipes.wanderlust"]) mod.readRecipesFile("recipes.wanderlust", ["wanderlust"]);
  const link = mod
    .getRecipe("recipes.wanderlust", "mariner.tide.wanderlust")
    .linked.find((o) => o.id === "mariner.wanderingevents.special.start.*");
  link.chances = { ...link.chances, [eventId]: chance };
};

export function intentionSlot({ label, description, actionId }) {
  return {
    id: "intent",
    label: label ?? "Intent",
    description: description ?? "Do I chose my material, active self, or my recieving, sensitive self?",
    actionId,
    required: {
      "mariner.lackheart": 1,
      "mariner.halfheart": 1,
    },
  };
}

export function aLocal(isLocal, amount = 1) {
  return isLocal ? { "mariner.local": amount } : {};
}

export function exists(el, obj) {
  return el !== undefined ? obj : {};
}

export function requireAndDestroy(element, amount) {
  return {
    requirements: { [element]: amount ?? 1 },
    effects: { [element]: -(amount ?? 1) },
  };
}

export function negativeValuesObject(obj) {
  return obj && Object.fromEntries(Object.entries(obj).map(([aspectId, value]) => [aspectId, -value]));
}

function crewEventTokenInduction(id) {
  return {
    id: `mariner.genericevents.${id}`,
    expulsion: {
      limit: 1,
      filter: {
        "mariner.crew": 1,
      },
    },
  };
}

// PHYSICAL DAMAGE
export const INJURE_CREWMEMBER = crewEventTokenInduction("injurecrewmember");
// SUPERNATURAL DAMAGE
export const UNRAVEL_CREWMEMBER = crewEventTokenInduction("unravelcrewmember");
// WITNESSED SUPERNATURAL HORROR
export const WITNESS_SUPERNATURAL_EVENT = crewEventTokenInduction("traumatizecrewmember.supernatural");
// WITNESSED PHYSICAL HORROR
export const WITNESS_HORRIFIC_EVENT = crewEventTokenInduction("traumatizecrewmember.horror");

export const SPAWN_SUSPICION = {
  id: "mariner.genericevents.spawnsuspicion",
  additional: true,
};

export const SINK_SHIP = {
  id: "mariner.sailing.sinkingfromevents",
  additional: true,
  expulsion: {
    limit: 1,
    filter: { "mariner.ship": 1 },
  },
};

// long id requirement shorthands
export const IN_PORT = (seaId, portId) => ({
  [`mariner.locations.${seaId}.${portId}`]: 1,
});

export const NOT_LOCAL_TO = (seaId, portId, elementId) => ({
  [`mariner.locations.${seaId}.${portId}.${elementId}`]: -1,
});

export const LOCAL_TO = (seaId, portId, elementId) => ({
  [`mariner.locations.${seaId}.${portId}.${elementId}`]: 1,
});

export const IN_SEA = (seaId) => ({
  [`mariner.locations.${seaId}.sailingthere`]: 1,
});

export const LOCAL_REPUTATION = "[~/exterior : {[mariner.docked]} : mariner.reputation]";

export const SET_PORT_TRACKLIST = {
  setBGMusic: ["Deuxieme Abysse.ogg", "La rieuse.ogg", "1 Circular Theme Muse REV3.mp3", "11 Red Grail Mimimal.mp3"],
};

export const SET_SEA_TRACKLIST = {
  setBGMusic: ["Desastreprout.ogg", "Declin du Jour.ogg", "5 Drawing Room Full.mp3"],
};

export const SET_ADVENTURE_TRACKLIST = {
  setBGMusic: [
    "Jacinthe.ogg",
    "Premier Abysse.ogg",
    "2 Sun in Rags 5 New End.mp3",
    "3 Door in Eye 2REV3 (longer).mp3",
    "8 Thunderskin #1 L Spare.mp3",
    "12 Subtle Bckgrnd Theme 1.mp3",
    "13 Subtle Bckgrnd Theme 2.mp3",
  ],
};

export function greyscaledOverlay(imageName) {
  return {
    image: imageName,
    grayscale: true,
    color: "#000000",
  };
}

// If any crewmember has the bad aspect, the value of the expression will be "worst". Otherwise, it is the sum of the non ethereal picked team members.
export function TEAM_SIZE(selectionId, worst = 8) {
  return `([: { [mariner.crew.traits.individualistic] = 1} : ${selectionId}] && ${worst}) || [: { [mariner.crew.traits.incorporeal] = 0 } : ${selectionId}]`;
}

export const ME_DECAY = (chance = undefined) => ({ morpheffect: "decay", chance });
export const ME_DESTROY = (chance = undefined) => ({ morpheffect: "destroy", chance });
export const ME_TRANSFORM = (id, chance = undefined) => ({ morpheffect: "transform", id, chance });
export const ME_INDUCE = (id, chance = undefined) => ({ morpheffect: "induce", id, chance });
export const ME_LINK = (id, chance = undefined) => ({ morpheffect: "link", id, chance });
export const ME_RECIPE = (id, chance = undefined) => ({ morpheffect: "recipe", id, chance });
export const ME_BREAK = (chance) => ({ morpheffect: "break", chance });
export const ME_APPLY = () => ({ morpheffect: "apply" });
export const ME_SET_MUTATION = (id, level = 1, chance = undefined, vfx = undefined) => ({
  morpheffect: "setmutation",
  id,
  level,
  chance,
  vfx,
});
export const ME_MUTATE = (id, level = 1, chance = undefined, vfx = undefined) => ({
  morpheffect: "mutate",
  id,
  level,
  chance,
  vfx,
});
export const ME_TIMESPEND = (id, level = 1, chance = undefined, vfx = undefined) => ({
  morpheffect: "timespend",
  id,
  level,
  chance,
  vfx,
});

export const ALL_SEAS = ["northsea", "medsea"];
