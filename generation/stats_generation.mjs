import { SIGNALS } from "./generate_signal_aspects.mjs";
import { ME_RECIPE } from "./generation_helpers.mjs";

export function SHIP_STAT(id) {
  return `mariner.ship.${id}`;
}

export function SHIP_STAT_BONUS(id) {
  return `mariner.ship.${id}.bonus`;
}

export function SHIP_STAT_MALUS(id) {
  return `mariner.ship.${id}.malus`;
}

export function CURRENT_SHIP_STAT_VALUE(id) {
  return `(max(0, ([~/extant : ${SHIP_STAT(id)}] + [~/extant : ${SHIP_STAT_BONUS(id)}] - [~/extant : ${SHIP_STAT_MALUS(id)}])))`;
}

function generateShipStatsUpdateAspect() {
  mod.setAspects("aspects.ship", {
    id: "mariner.updateshipstats",
    timers: {
      signal: {
        frequency: 1,
        repeat: false,
        morpheffects: ME_RECIPE("mariner.signalshipstatsupdate", undefined, "None"),
      },
    },
  });
  mod.setHiddenAspects(
    "aspects.ship",
    { id: "mariner.ship.speed.current", label: "Current ship speed", icon: "mariner.ship.speed" },
    { id: "mariner.ship.discretion.current", label: "Current ship discretion", icon: "mariner.ship.discretion" },
    { id: "mariner.ship.grammarie.current", label: "Current ship grammarie", icon: "mariner.ship.grammarie" }
  );

  const mutateCurrentStat = (id) => ({
    filter: "mariner.ship.kite",
    mutate: `mariner.ship.${id}.current`,
    level: CURRENT_SHIP_STAT_VALUE(id),
    additive: false,
  });

  mod.initializeRecipeFile("recipes.updateshipstats", ["ship"]);
  mod.setRecipes(
    "recipes.updateshipstats",
    {
      id: "mariner.signalshipstatsupdate",
      furthermore: {
        target: "~/extant",
        aspects: { [SIGNALS.COMPUTE_SHIP_STATS]: 1 },
      },
    },
    {
      id: "mariner.ship.computestats",
      furthermore: [
        {
          target: "~/extant",
          mutations: [mutateCurrentStat("speed"), mutateCurrentStat("discretion"), mutateCurrentStat("grammarie")],
        },
      ],
    }
  );
}

export function generateShipStats() {
  mod.initializeAspectFile("aspects.ship", ["ship"]);
  mod.setAspects(
    "aspects.ship",
    {
      id: "mariner.ship",
      label: "Ship",
      isHidden: true,
    },
    {
      icon: "mariner.ship.holdsize",
      id: "mariner.ship.holdsize",
      label: "Hold Size",
      description: "In a properly stowed ship, no space is wasted, no thing out of place.",
    },
    {
      icon: "mariner.ship.cabinsize",
      id: "mariner.ship.cabinsize",
      label: "Cabin Size",
      description: "A ship is never spatious, but there is a cramped comfort in its confines.",
    },
    {
      icon: "mariner.ship.sanctumsize",
      id: "mariner.ship.sanctumsize",
      label: "Sanctum Size",
      description: "<>",
    },
    {
      id: "mariner.ship.damage",
      icon: "shipdamage",
      label: "A Wound of Timber",
      description: "Mortal wounds spill salted liquid, a ship's leaks let it in. Both will be fatal in the end.",
    },
    {
      id: "mariner.ship.patched",
      label: "<Patched>",
      description: "<>",
    },
    {
      icon: "mariner.ship.durability",
      id: "mariner.ship.durability",
      label: "Durability",
      description: "How many breaches can this hull bear?",
    },
    {
      id: "mariner.cabinuse",
      icon: "crew",
    },
    {
      id: "mariner.holduse",
      icon: "mariner.ship.holdsize",
    },
    {
      id: "mariner.sanctumuse",
      icon: "mariner.ship.sanctumsize",
    },
    {
      id: "mariner.ship.speed",
      label: "Ship Speed",
      description:
        "<[Represents the current speed of the ship. It represents the sum of its base speed, plus any potential bonuses related to its configuration, weather, and upgrades.]>",
    },
    {
      id: "mariner.ship.speed.bonus",
      label: "Ship Speed Bonus",
      description: "<>",
    },
    {
      id: "mariner.ship.speed.malus",
      label: "Ship Speed Malus",
      description: "<>",
    },
    {
      id: "mariner.ship.discretion",
      label: "Ship Discretion",
      description: "<[Represents how discrete the ship is. The higher the value, the better.]>",
    },
    {
      id: "mariner.ship.discretion.bonus",
      label: "Ship Discretion Bonus",
      description: "<>",
    },
    {
      id: "mariner.ship.discretion.malus",
      label: "Ship Discretion Malus",
      description: "<>",
    },
    {
      id: "mariner.ship.grammarie",
      label: "Ship Grammarie",
      description: "<[Represents how attuned to the hidden world we are.]>",
    },
    {
      id: "mariner.ship.grammarie.bonus",
      label: "Ship Grammarie Bonus",
      description: "<>",
    },
    {
      id: "mariner.ship.grammarie.malus",
      label: "Ship Grammarie Malus",
      description: "<>",
    }
  );

  generateShipStatsUpdateAspect();
}
