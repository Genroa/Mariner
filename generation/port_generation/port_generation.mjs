import { generatePortElement } from "./port_element_generation.mjs";
import { exists, ME_INDUCE, ME_LINK, ME_RECIPE, ME_TRANSFORM, SET_PORT_TRACKLIST } from "../generation_helpers.mjs";
import { generateStoryTrading } from "../stories_generation/trading_helper.mjs";
import { generateShelf } from "../shelves_generation/generate_shelves.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
import { CALL_FUNCTION } from "../generate_function_calls.mjs";
import { generatePortAILogic } from "./generate_ais/agents.mjs";
import { generateWharf } from "./generate_wharf.mjs";
import { generateBar } from "./generate_bar.mjs";
import { generateStore } from "./generate_store.mjs";
import { INFLUENCE_CHANCE } from "../song_generation/generate_influences.mjs";

function computeGetRumoursRouter(portRegions) {
  let outcomes = [];
  let regionsToCompute = [...portRegions];
  while (regionsToCompute.length > 0) {
    outcomes.push({
      id: `mariner.explore.bar.getrumourfor.${regionsToCompute[0]}`,
      chance: 100 * (1 / regionsToCompute.length),
    });
    regionsToCompute.shift();
  }
  return outcomes;
}

function computeSailingThereAspects(portRegions) {
  const aspects = {};
  for (let portRegion of portRegions) {
    aspects[`mariner.locations.${portRegion}.sailingthere`] = 1;
  }
  return aspects;
}

function computePresentThereAspects(portRegions) {
  const aspects = {};
  for (let portRegion of portRegions) {
    aspects[`mariner.locations.${portRegion}.presentthere`] = 1;
  }
  return aspects;
}

export function generatePort({
  portRegions,
  discoverable = true,
  portId,
  label,
  description,
  recipeDescriptions,
  portElements,
  isGateway,
  tunes,
  barName,
  wharf,
  storeName,
  storyTrading,
} = {}) {
  const portRegion = portRegions.length === 1 ? portRegions[0] : "gateways";

  const uniquenessGroupAspectId = `mariner.locations.${portRegion}.${portId}.uqg`;

  const destinationAspectId = `mariner.locations.${portRegion}.${portId}.destination`;

  const portCompleteId = `mariner.locations.${portRegion}.${portId}`;
  const portCompleteAwayId = portCompleteId + ".away";
  const portCompleteLockedId = portCompleteId + ".locked";

  // Create the aspect "destination" locations[away] will have, to flip them
  mod.setAspect("aspects.destinations", {
    id: destinationAspectId,
    label: `In ${label}`,
    description: `This is not here; It is in ${label}.`,
    decayTo: "anything really",
  });

  mod.setHiddenAspect("aspects.seas.uniquenessgroups", {
    id: uniquenessGroupAspectId,
  });

  // Add the flipcard of the port to the sea elements
  mod.elements[`locations.${portRegion}`].push(
    ...[
      {
        id: portCompleteId,
        $derives: ["mariner.composables.reputabledestination", "mariner.composables.dockedport"],
        uniquenessgroup: uniquenessGroupAspectId,
        xexts: {
          $background: `locations/${portRegion}/${portId}/${portId}`,
          $frame: `locations/${portRegion}/${portId}/${portId}_frame`,
        },
        label: `${label} [docked]`,
        description,
        aspects: computeSailingThereAspects(portRegions),
        xtriggers: {
          [SIGNALS.LEAVE_PORT]: portCompleteAwayId,
          [SIGNALS.LOCK_PORT]: portCompleteLockedId,
        },
      },
      {
        id: portCompleteAwayId,
        uniquenessgroup: uniquenessGroupAspectId,
        $derives: ["mariner.composables.reputabledestination", "mariner.composables.port"],
        xexts: {
          $background: `locations/${portRegion}/${portId}/${portId}`,
          $frame: `locations/${portRegion}/${portId}/${portId}_frame`,
        },
        label,
        description,
        aspects: {
          "mariner.destination": 1,
          ...exists(isGateway, { "mariner.gatewayport": 1 }),
          ...computeSailingThereAspects(portRegions),
        },
        xtriggers: {
          [SIGNALS.ARRIVE_TO_DESTINATION]: [
            ME_TRANSFORM(portCompleteId),
            ME_INDUCE(`mariner.grabpassengersonarrival.${portRegion}.${portId}`),
            ME_RECIPE("mariner.enablestandbyexperiences"),
            ME_LINK("mariner.sailing.arriving.handlelonging"),
          ],
          [SIGNALS.LOCK_PORT]: [ME_TRANSFORM(portCompleteLockedId)],
        },
      },
      {
        id: portCompleteLockedId,
        $derives: ["mariner.composables.reputabledestination", "mariner.composables.port"],
        xexts: {
          $background: `locations/${portRegion}/${portId}/${portId}`,
          $frame: `locations/${portRegion}/${portId}/${portId}_frame`,
        },
        uniquenessgroup: uniquenessGroupAspectId,
        decayTo: portCompleteAwayId,
        label,
        description,
        aspects: {
          "mariner.destination.locked": 1,
          ...computePresentThereAspects(portRegions),
        },
        xtriggers: {
          [`mariner.signal.arriveinsea.${portRegion}`]: portCompleteAwayId,
        },
      },
    ]
  );

  // Add port to its sea decks
  for (const region of portRegions) {
    mod.getDeck("decks.seas", `mariner.decks.locations.${region}.ports`).spec.push(portCompleteId);
    mod.getDeck("decks.seas", `mariner.decks.locations.${region}.alldestinations`).spec.push(portCompleteId);
    if (discoverable) mod.getDeck("decks.seas", `mariner.decks.locations.${region}.discoverable`).spec.push(portCompleteAwayId);
  }

  // generate the "get rumour" recipe for this port
  mod.readRecipesFile("recipes.explore.bar.getrumour");
  const initialGetRumourRecipeId = `mariner.explore.bar.getrumourfor.${portRegion}.${portId}`;
  mod.addToLinked("recipes.explore.bar.getrumour", "mariner.explore.bar.getrumour", initialGetRumourRecipeId);
  mod.setRecipe("recipes.explore.bar.getrumour", {
    id: initialGetRumourRecipeId,
    extantreqs: {
      [portCompleteId]: 1,
    },
    linked: computeGetRumoursRouter(portRegions),
  });

  // Create the dockto recipe
  mod.setRecipe(`recipes.dockto.${portRegion}`, {
    id: `mariner.dockto.${portRegion}.${portId}`,
    label: `Docking to ${label}`,
    actionId: "docking",
    startdescription: recipeDescriptions.docking ?? "Docking!",
    requirements: {
      [portCompleteId]: 1,
    },
    purge: {
      [destinationAspectId]: 20,
    },
    addCallbacks: { DOCKTO: `mariner.dockto.${portRegion}.${portId}` },
    linked: [
      { id: "mariner.docking.displaynobar" },
      { id: "mariner.docking.displaynowharf" },
      { id: "mariner.docking.displaynostore" },
    ],
    ...SET_PORT_TRACKLIST,
  });

  if (barName) {
    mod.setRecipe(`recipes.displaylocalbar.${portRegion}`, {
      id: `mariner.docking.${portRegion}.${portId}.displaybar`,
      grandReqs: {
        "~/exterior : mariner.nobaratsea": 1,
        [`~/exterior : mariner.discoveredbars.${portRegion}.${portId}`]: 1,
      },
      furthermore: [
        {
          target: "~/exterior",
          aspects: { [`mariner.displaylocalbar.${portRegion}.${portId}`]: 1 },
        },
      ],
      linked: [{ id: `mariner.dockto.${portRegion}.${portId}` }],
    });
    mod.addFirstToLinked(
      `recipes.dockto.${portRegion}`,
      `mariner.dockto.${portRegion}.${portId}`,
      `mariner.docking.${portRegion}.${portId}.displaybar`
    );
  }

  if (storeName) {
    mod.setRecipe(`recipes.displaylocalstore.${portRegion}`, {
      id: `mariner.docking.${portRegion}.${portId}.displaystore`,
      grandReqs: {
        "~/exterior : mariner.nostoreatsea": 1,
        [`~/exterior : mariner.discoveredstores.${portRegion}.${portId}`]: 1,
      },
      furthermore: [
        {
          target: "~/exterior",
          aspects: { [`mariner.displaylocalstore.${portRegion}.${portId}`]: 1 },
        },
      ],
      linked: [{ id: `mariner.dockto.${portRegion}.${portId}` }],
    });
    mod.addFirstToLinked(
      `recipes.dockto.${portRegion}`,
      `mariner.dockto.${portRegion}.${portId}`,
      `mariner.docking.${portRegion}.${portId}.displaystore`
    );
  }

  if (wharf) {
    mod.setRecipe(`recipes.displaylocalwharf.${portRegion}`, {
      id: `mariner.docking.${portRegion}.${portId}.displaywharf`,
      grandReqs: {
        "~/exterior : mariner.nowharfatsea": 1,
        [`~/exterior : mariner.discoveredwharves.${portRegion}.${portId}`]: 1,
      },
      furthermore: [
        {
          target: "~/exterior",
          aspects: { [`mariner.displaylocalwharf.${portRegion}.${portId}`]: 1 },
        },
      ],
      linked: [{ id: `mariner.dockto.${portRegion}.${portId}` }],
    });
    mod.addFirstToLinked(
      `recipes.dockto.${portRegion}`,
      `mariner.dockto.${portRegion}.${portId}`,
      `mariner.docking.${portRegion}.${portId}.displaywharf`
    );
  }

  // mod.addToLinked(
  //   "recipes.sailing.arriving",
  //   "mariner.sailing.arriving.docking",
  //   `mariner.dockto.${portRegion}.${portId}`
  // );

  // Create things to discover deck. Created elements will add their id into it.
  const exploreHHDeckId = `mariner.decks.locations.${portRegion}.${portId}.halfheart`;
  const exploreLHDeckId = `mariner.decks.locations.${portRegion}.${portId}.lackheart`;
  const tunesIds = tunes?.map((id) => `mariner.tunes.${id}`) ?? mod.elements["tunes"].map((e) => e.id);

  mod.setDeck(`decks.locations.${portRegion}`, {
    id: exploreHHDeckId,
    label: `Things to discover in ${label}`,
    description: `The things that can be discovered in ${label}.`,
    spec: [],
    defaultcard: "mariner.oddcorners",
    resetonexhaustion: false,
  });

  mod.setDeck(`decks.locations.${portRegion}`, {
    id: exploreLHDeckId,
    label: `Things to discover in ${label}`,
    description: `The things that can be discovered in ${label}.`,
    spec: [...tunesIds],
    resetonexhaustion: true,
  });

  // Create file that will contain the conversations
  mod.initializeRecipeFile(`recipes.conversations.${portRegion}.${portId}`, ["conversations", portRegion]);

  // Create active exploration recipe
  // Half heart
  mod.setRecipe(`recipes.explore.${portRegion}`, {
    id: `mariner.explore.${portRegion}.${portId}.halfheart`,
    label: `Exploring ${label}`,
    actionId: "explore",
    craftable: true,
    requirements: {
      [portCompleteId]: 1,
      "mariner.halfheart": 1,
    },
    warmup: 30,
    startdescription: recipeDescriptions.exploreHH ?? `${label} !`,
    furthermore: [
      {
        deckeffects: {
          [exploreHHDeckId]: 1,
        },
      },
      {
        aspects: { [SIGNALS.APPLY_DEFAULT_ROUTINE]: 1 },
      },
      {
        aspects: { [SIGNALS.START_ROUTINE]: 1 },
      },
      {
        aspects: { [SIGNALS.DISCOVER_LOCAL_ELEMENT]: 1 },
      },
      {
        aspects: { [SIGNALS.USE_HEART]: 1 },
      },
    ],
  });

  // Create file that will contain the locations linked to this port
  mod.initializeElementFile(`locations.${portRegion}.${portId}`, ["locations", portRegion]);
  // Create elements attached to the port geographically.
  if (portElements) portElements.map((el) => generatePortElement({ portRegion, portId, exploreHHDeckId, ...el }));
  if (barName) generateBar({ portRegion, portId, exploreHHDeckId, barName });
  if (storeName) generateStore({ portRegion, portId, exploreHHDeckId, storeName });
  if (wharf) generateWharf({ portRegion, portId, exploreHHDeckId, wharf });

  // Lackheart (& find packages)
  mod.setRecipe(`recipes.explore.${portRegion}`, {
    id: `mariner.explore.${portRegion}.${portId}.lackheart`,
    actionId: "explore",
    craftable: true,
    requirements: {
      [portCompleteId]: 1,
      "mariner.lackheart": 1,
    },
    warmup: 30,
    label: `Exploring ${label} With An Open Soul`,
    startdescription: recipeDescriptions.exploreLH, //`The Raconteur network is dispersed and disparate, and look a little different in every city. But if you know the tunes, the work will find you.`,
    linked: [
      {
        chance: `25 + ${INFLUENCE_CHANCE("knock")}`,
        id: `mariner.explore.${portRegion}.${portId}.lackheart.foundpackage`,
      },
      { id: `mariner.explore.${portRegion}.${portId}.lackheart.foundtune` },
    ],
    aspects: { [SIGNALS.USE_HEART]: 1 },
    furthermore: CALL_FUNCTION("addgroundedness"),
  });

  mod.setRecipe(`recipes.explore.${portRegion}`, {
    id: `mariner.explore.${portRegion}.${portId}.lackheart.foundtune`,
    actionId: "explore",
    label: `Found Tune in ${label}`,
    description: recipeDescriptions.exploreLH,
    deckeffects: {
      [exploreLHDeckId]: 1,
    },
    furthermore: CALL_FUNCTION("addgroundedness"),
  });
  mod.setRecipe(`recipes.explore.${portRegion}`, {
    id: `mariner.explore.${portRegion}.${portId}.lackheart.foundpackage`,
    actionId: "explore",
    label: `Met a Member of the Network in ${label}`,
    description:
      "The city is rife with tale-tellers. Nannies rambling about tales for a coin, sailors with their belly full of stories and liquid, old beggars and young peddlers. As I explore, one of them locks eyes with me. They stare at me with glintful eyes and let a package stick out of their pocket.",
    warmup: 10,
    purge: {
      "mariner.influences.knock": 2,
    },
    inductions: [{ id: "mariner.getpackage.start" }],
    furthermore: CALL_FUNCTION("addgroundedness"),
  });

  if (storyTrading) {
    generateStoryTrading({
      id: portId,
      path: ["stories", "trading", portRegion, portId],
      story: storyTrading.story,
      requirements: { [portCompleteId]: 1 },
      label: storyTrading.label,
      startdescription: storyTrading.startdescription,
      description: storyTrading.description,
    });
  }

  generatePortAILogic(portRegion, portId);

  // generateShelf("shelves.ports", {
  //   id: `mariner.shelves.locations.${portRegion}.${portId}`,
  //   category: "other",
  //   name: `[Mariner] ${label}`,
  //   columns: 6,
  //   background: `mariner.shelves.locations.${portRegion}.${portId}`,
  //   style: "aligncenter",
  //   areas: [
  //     {
  //       id: `${portId}_port`,
  //       expression: `[${portCompleteAwayId}] || [${portCompleteLockedId}]`,
  //       columns: 1,
  //     },
  //     {
  //       id: `${portId}_elements`,
  //       expression: destinationAspectId,
  //       x: 2,
  //       columns: 5,
  //     },
  //   ],
  // });
}
