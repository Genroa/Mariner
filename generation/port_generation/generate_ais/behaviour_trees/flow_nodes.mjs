import { Node, indentForDepth } from "./behaviour_tree.mjs";
import {
  DockTask,
  PickDestinationTask,
  UndockTask,
  WaitTask,
} from "./task_nodes.mjs";

// Flow nodes
class FlowNode extends Node {
  children;
  constructor({ id, warmup, label, grandReqs, children }) {
    super({ id, label, warmup, grandReqs });
    this.children = children;
  }

  build({
    depth,
    prefix,
    btree,
    stepFollowupRecipeId,
    successFollowupRecipeId,
  }) {
    super.build({
      depth,
      prefix,
      btree,
      stepFollowupRecipeId,
      successFollowupRecipeId,
    });
    this.buildChildren({
      depth,
      prefix,
      btree,
      stepFollowupRecipeId,
      successFollowupRecipeId,
    });
  }

  buildChildren({
    depth,
    prefix,
    btree,
    stepFollowupRecipeId,
    successFollowupRecipeId,
  }) {
    // console.log(
    //   `${indentForDepth(depth)}Building children of Btree node ${this.getTaskId(
    //     prefix
    //   )}...`
    // );
    for (const child of this.children) {
      child.build({
        depth: depth + 1,
        prefix: prefix + `.${this.id}`,
        btree,
        stepFollowupRecipeId,
        successFollowupRecipeId,
      });
    }
  }
}

export class Sequence extends FlowNode {
  getSequenceTokenId(prefix) {
    return `${this.getTaskId(prefix)}.sequencetoken`;
  }

  buildAction({
    prefix,
    depth,
    btree,
    stepFollowupRecipeId,
    successFollowupRecipeId,
    startRecipe,
  }) {
    // Sequence uses a token unique to this node to know where to keep going after reaching it again.
    mod.setElement(btree.ELEMENTS_FILE, {
      id: this.getSequenceTokenId(prefix),
      label: this.getSequenceTokenId(prefix),
    });
    btree.resetRecipe.effects[this.getSequenceTokenId(prefix)] =
      -btree.maxSequenceBranches;

    // Sequence needs an "action", and an "end" recipe to catch when stuff under it ends. It then needs to do
    mod.setRecipe(btree.RECIPES_FILE, {
      id: `${this.getTaskId(prefix)}.action`,
      grandReqs: this.grandReqs,
      linked: [
        ...this.children.map((c) => ({
          id: `${c.getStartRecipeId(this.getTaskId(prefix))}.${
            this.id
          }.checkvalidity`,
        })),
        { id: btree.resetRecipe.id },
      ],
    });

    mod.setRecipes(
      btree.RECIPES_FILE,
      ...this.children.map((c, index) => ({
        id: `${c.getStartRecipeId(this.getTaskId(prefix))}.${
          this.id
        }.checkvalidity`,
        grandReqs: {
          [`[${this.getSequenceTokenId(prefix)}] = ${index}`]: 1,
          ...c.grandReqs,
        },
        linked: [{ id: c.getStartRecipeId(this.getTaskId(prefix)) }],
      }))
    );

    mod.setRecipe(btree.RECIPES_FILE, {
      id: `${this.getTaskId(prefix)}.end`,
      effects: {
        [this.getSequenceTokenId(prefix)]: 1,
      },
      linked: [
        {
          id: `${this.getTaskId(prefix)}.clear`,
          requirements: {
            [this.getSequenceTokenId(prefix)]: this.children.length,
          },
          effects: {
            [this.getSequenceTokenId(prefix)]: -btree.maxSequenceBranches,
          },
          linked: successFollowupRecipeId,
        },
        stepFollowupRecipeId,
      ],
    });

    mod.setRecipe(btree.RECIPES_FILE, {
      id: `${this.getTaskId(prefix)}.step`,
      linked: [{ id: stepFollowupRecipeId }],
    });

    startRecipe.linked = [
      `${this.getTaskId(prefix)}.action`,
      btree.resetRecipe.id,
    ];
  }

  buildChildren({ depth, prefix, btree }) {
    // console.log(
    //   `${indentForDepth(depth)}Building children of Btree node ${this.getTaskId(
    //     prefix
    //   )}...`
    // );
    for (const child of this.children) {
      child.build({
        depth: depth + 1,
        prefix: prefix + `.${this.id}`,
        btree,
        stepFollowupRecipeId: `${this.getTaskId(prefix)}.step`,
        successFollowupRecipeId: `${this.getTaskId(prefix)}.end`,
      });
    }
  }
}

// Picks the first child node it can run and links to it. Otherwise, goes back to the parent.
export class Selector extends FlowNode {
  buildAction({ prefix, btree, startRecipe }) {
    // Here we build the recipe doing the actual action. Selector picks the first valid child of the bunch, so it's a fairly easy thing to do (use their grandReqs)
    // Since it has zero other logic to handle, we can directly set that in the startRecipe.
    // Why do we have these internal recipes? Because the .start recipe of the child node might not be the one doing the grandReq checks. For instance, the Sequence node
    // sets stuff up and checks its conditions before acting, and resets its states otherwise (immediate link to its "end"). So we use this wrapper.
    startRecipe.linked = [
      ...this.children.map((c) => ({
        id: `${c.getStartRecipeId(this.getTaskId(prefix))}.${
          this.id
        }.checkvalidity`,
      })),
      btree.resetRecipe.id,
    ];

    mod.setRecipes(
      btree.RECIPES_FILE,
      ...this.children.map((c) => ({
        id: `${c.getStartRecipeId(this.getTaskId(prefix))}.${
          this.id
        }.checkvalidity`,
        grandReqs: c.grandReqs, // hopefully children have grandReqs, otherwise, why bother using a Selector?
        linked: [{ id: c.getStartRecipeId(this.getTaskId(prefix)) }],
      }))
    );
  }
}

export class TravelToLocalPortSequence extends Sequence {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "traveltolocalport",
      label: label ?? "Travel to other local port Sequence",
      warmup,
      grandReqs,
    });
    this.children = [
      new PickDestinationTask(),
      // Undock if docked
      new UndockTask(),
      // Wait a bit (simulate travelling)
      new WaitTask({ warmup: 200 }),
      // "Dock" somewhere.
      new DockTask(),
    ];
  }
}
