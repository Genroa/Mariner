import { SIGNALS } from "../../../generate_signal_aspects.mjs";
import { missionOrderId } from "../generate_ais.mjs";
import { Node } from "./behaviour_tree.mjs";

// Tasks
export const NO_ACTION = ({ successFollowupRecipeId, startRecipe }) =>
  (startRecipe.linked = [successFollowupRecipeId]);

export class Task extends Node {
  action;

  constructor({ id, warmup, label, grandReqs, action }) {
    super({ id, label, warmup, grandReqs });
    this.action = action ?? NO_ACTION;
  }

  buildAction({
    prefix,
    depth,
    btree,
    failingFollowupRecipeId,
    successFollowupRecipeId,
    startRecipe,
  }) {
    startRecipe.linked = [successFollowupRecipeId];
    this.action({
      prefix,
      depth,
      btree,
      failingFollowupRecipeId,
      successFollowupRecipeId,
      startRecipe,
    });
  }
}

// Just an alias to make things more readable
export class WaitTask extends Task {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({ id: id ?? "wait", label: label ?? "Wait", warmup, grandReqs });
  }
}

export class SignalAgentTask extends Node {
  signal;

  constructor({ id, warmup, label, grandReqs, signal }) {
    super({ id, label, warmup, grandReqs });
    this.signal = signal ?? "";
  }

  buildAction({ successFollowupRecipeId, startRecipe }) {
    startRecipe.linked = [successFollowupRecipeId];
    if (this.signal === "") return;
    startRecipe.aspects = { [this.signal]: 1 };
  }
}

export class UndockTask extends SignalAgentTask {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({ id: id ?? "undock", label: label ?? "Undock", warmup, grandReqs });
    this.signal = SIGNALS.AGENT_UNDOCK;
  }
}

export class DockTask extends SignalAgentTask {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({ id: id ?? "dock", label: label ?? "Dock", warmup, grandReqs });
    this.signal = SIGNALS.AGENT_DOCK;
  }
}

export class PickDestinationTask extends SignalAgentTask {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "pickdestination",
      label: label ?? "Pick Destination",
      warmup,
      grandReqs,
    });
    this.signal = SIGNALS.AGENT_PICK_DESTINATION;
  }
}

export class InvestigateTask extends SignalAgentTask {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "investigate",
      label: label ?? "Investigate",
      warmup,
      grandReqs,
    });
    this.signal = SIGNALS.AGENT_INVESTIGATE;
  }
}

export class SailTowardsNeighboringSea extends SignalAgentTask {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "leavesea",
      label: label ?? "Sail towards other sea",
      warmup,
      grandReqs,
    });
    this.signal = SIGNALS.AGENT_LEAVE_SEA;
  }
}

export class ArriveInSea extends SignalAgentTask {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "arriveinsea",
      label: label ?? "Arrive in sea",
      warmup,
      grandReqs,
    });
    this.signal = SIGNALS.AGENT_ARRIVE_IN_SEA;
  }
}

export class EndMissionOrderTask extends Node {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "endmissionorder",
      label: label ?? "End mission order",
      warmup,
      grandReqs,
    });
  }

  buildAction({ successFollowupRecipeId, startRecipe }) {
    startRecipe.linked = [successFollowupRecipeId];
    startRecipe.effects = {
      ["mariner.ais.missionorder"]: -10,
      ...(startRecipe.effects ?? {}),
    };
  }
}

export class ReceiveMissionOrderTask extends Node {
  constructor({ id, warmup, label, grandReqs } = {}) {
    super({
      id: id ?? "receivemissionorder",
      label: label ?? "Receive mission order",
      warmup,
      grandReqs: { ...grandReqs, ["mariner.ais.missionorder"]: -1 },
    });
  }

  buildAction({ btree, prefix, successFollowupRecipeId, startRecipe }) {
    mod.setRecipe(btree.RECIPES_FILE, {
      id: `${prefix}.pickmissionorder.patrolcurrentregion`,
      effects: { [missionOrderId("patrolcurrentregion")]: 1 },
      linked: [successFollowupRecipeId],
    });
    mod.setRecipe(btree.RECIPES_FILE, {
      id: `${prefix}.pickmissionorder.changesea`,
      effects: { [missionOrderId("changesea")]: 1 },
      linked: [successFollowupRecipeId],
    });

    startRecipe.linked = [
      { id: `${prefix}.pickmissionorder.*`, randomPick: true },
    ];
  }
}
