export const indentForDepth = (depth) => new Array(depth).fill("  ").join("") + "|";

export class Node {
  id;
  label;
  warmup;
  grandReqs;

  constructor({ id, warmup, label, grandReqs }) {
    this.id = id;
    this.label = label ?? id;
    this.warmup = warmup;
    this.grandReqs = grandReqs;
  }

  getStartRecipeId(prefix) {
    return `${this.getTaskId(prefix)}.start`;
  }

  getTaskId(prefix) {
    return `${prefix}.${this.id}`;
  }

  build({ prefix, depth, btree, stepFollowupRecipeId, successFollowupRecipeId }) {
    // console.log(
    //   `${indentForDepth(depth)}Building BTree Node ${this.getTaskId(prefix)}...`
    // );

    let startRecipe = {
      id: this.getStartRecipeId(prefix),
      actionId: btree.actionId,
      label: this.label,
      warmup: this.warmup ?? btree.defaultWarmup,
      startdescription: this.label,
    };
    mod.setRecipe(btree.RECIPES_FILE, startRecipe);

    this.buildAction({
      prefix,
      depth,
      btree,
      stepFollowupRecipeId,
      successFollowupRecipeId,
      startRecipe,
    });
  }

  buildAction({ prefix, depth, btree, stepFollowupRecipeId, successFollowupRecipeId, startRecipe }) {}
}

export class BehaviourTree {
  ELEMENTS_FILE;
  RECIPES_FILE;
  id;
  prefix;
  root;
  currentPrefix;
  defaultWarmup;
  maxSequenceBranches;
  resetRecipe;

  constructor({ id, defaultWarmup, root, maxSequenceBranches }) {
    this.id = id;
    this.ELEMENTS_FILE = `ais.${id}`;
    this.RECIPES_FILE = `recipes.ais.${id}`;
    this.actionId = `ais.${id}`;
    this.prefix = `mariner.ais.${id}`;
    this.rootRecipe = root.getStartRecipeId(this.prefix);
    this.resetRecipe = `${this.prefix}.resetsequences`;
    this.root = root;
    this.defaultWarmup = defaultWarmup;
    this.maxSequenceBranches = maxSequenceBranches ?? 10;
  }

  build() {
    //console.log(`+Building BTree ${this.prefix}...`);
    mod.initializeElementFile(this.ELEMENTS_FILE, ["ais"]);
    mod.initializeRecipeFile(this.RECIPES_FILE, ["ais"]);
    mod.initializeVerbFile("verbs.ais", ["ais"]);
    mod.setVerb("verbs.ais", {
      id: this.actionId,
      label: `<AI Btree token ${this.id}>`,
      multiple: true,
    });

    this.resetRecipe = {
      id: `${this.prefix}.resetsequences`,
      effects: {},
      linked: [{ id: this.rootRecipe }],
    };
    mod.setRecipe(this.RECIPES_FILE, this.resetRecipe);

    this.root.build({
      prefix: this.prefix,
      btree: this,
      depth: 0,
      successFollowupRecipeId: this.rootRecipe,
      stepFollowupRecipeId: this.rootRecipe,
    });
  }
}
