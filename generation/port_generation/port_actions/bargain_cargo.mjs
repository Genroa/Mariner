import { INFLUENCE_CHANCE } from "../../song_generation/generate_influences.mjs";

export function generateBargainCargoAction() {
  mod.initializeRecipeFile("recipes.explore.bargaincargo", ["locations", "portactions"]);
  mod.setRecipes(
    "recipes.explore.bargaincargo",
    {
      id: "mariner.explore.bargaincargo",
      craftable: true,
      actionId: "explore",
      requirements: {
        "mariner.port": 1,
        "mariner.docked": 1,
        "mariner.cargo": 1,
      },
      label: "<Bargain Cargo in the streets>",
      startdescription: "<>",
      warmup: 60,
      effects: { "mariner.cargo": -1 },
      linked: [
        { id: "mariner.explore.bargaincargo.sold", chance: `10 + ${INFLUENCE_CHANCE("moth")}` },
        { id: "mariner.explore.bargaincargo.fled", chance: "25" },
        { id: "mariner.explore.bargaincargo.thrown" },
      ],
    },
    {
      id: "mariner.explore.bargaincargo.sold",
      label: "<Found a seller>",
      effects: { funds: 1 },
      purge: { "mariner.influences.moth": 2 },
    },
    {
      id: "mariner.explore.bargaincargo.fled",
      label: "<Authorities took notice>",
      effects: { "mariner.notoriety": 1 },
    },
    {
      id: "mariner.explore.bargaincargo.thrown",
      label: "<No one interested>",
    }
  );
}
