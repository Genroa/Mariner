import { generateBargainCargoAction } from "./bargain_cargo.mjs";
import { generateGetRumourAction } from "./get_rumour.mjs";

export function generatePortActions() {
  generateGetRumourAction();
  generateBargainCargoAction();
}
