import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_RECIPE, aLocal, exists } from "../generation_helpers.mjs";
import { computeHostileSlots } from "./port_element_generation.mjs";

export const defaultNotorietyLabels = [
  {
    label: "Rumours",
    description: "Things happened. People are talking.",
  },
  {
    label: "Investigation",
    description:
      "The Local Authorities have taken an interest in me and have begun to collect information.",
  },
  {
    label: "Arrest Warrent",
    description:
      "The Local Authorities are trying to find and arrest me for further questioning.",
  },
  {
    label: "Wanted",
    description:
      "The Local Authorities are willing to pay anyone to put an end to my activities...",
  },
];

export const UPGRADE_REPUTATION = (
  seaId,
  portId,
  newLevel,
  additive = true
) => [
  {
    target: "~/extant",
    mutations: [
      {
        filter: `mariner.locations.${seaId}.${portId}.uqg`,
        mutate: "mariner.reputation",
        level: newLevel ?? 1,
        additive: additive,
      },
      {
        filter: `mariner.locations.${seaId}.${portId}.uqg`,
        mutate: "mariner.reputation.heat",
        level: 0,
        additive: false,
      },
    ],
  },
  {
    target: "~/extant",
    aspects: { [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: 1 },
  },
  {
    target: "~/extant",
    aspects: { [SIGNALS.GENERATE_REPUTATION_HEAT]: 1 },
  },
];

export function generateDestinationNotorietyElements(id, levels) {
  mod.initializeAspectFile(`aspects.cosmeticreputation.${id}`, [
    "reputation",
    "cosmeticaspects",
  ]);

  mod.setAspects(
    `aspects.cosmeticreputation.${id}`,
    {
      id: `mariner.cosmeticreputation.${id}.1`,
      label: levels[0].label,
      description: `<size=120%><color=#201d59>Reputation Level 1</color></size>\n\n${levels[0].description}`,
    },
    {
      id: `mariner.cosmeticreputation.${id}.2`,
      label: levels[1].label,
      description: `<size=120%><color=#201d59>Reputation Level 2</color></size>\n\n${levels[1].description}`,
    },
    {
      id: `mariner.cosmeticreputation.${id}.3`,
      label: levels[2].label,
      description: `<size=120%><color=#201d59>Reputation Level 3</color></size>\n\n${levels[2].description}`,
    },
    {
      id: `mariner.cosmeticreputation.${id}.4`,
      label: levels[3].label,
      description: `<size=120%><color=#201d59>Reputation Level 4</color></size>\n\n${levels[3].description}`,
    }
  );
}

function generateDestinationCosmeticReputationUpdateRecipes(id, isVault) {
  mod.initializeRecipeFile(`recipes.cosmeticreputation.${id}`, [
    "reputation",
    "cosmeticaspects",
  ]);

  const removeCosmeticReputation = (level) => ({
    filter: "mariner.port",
    mutate: `mariner.cosmeticreputation.${id}.${level}`,
    level: 0,
    additive: false,
  });

  const applyCosmeticReputationId = (level) => ({
    id: `mariner.updatedisplayedreputation.${id}.${level}`,
  });
  const applyCosmeticReputation = (level, isVault) => ({
    id: `mariner.updatedisplayedreputation.${id}.${level}`,
    grandReqs: {
      "~/source : mariner.reputation": level,
    },
    furthermore: [
      {
        target: "~/source",
        mutations: [
          {
            filter: isVault ? "mariner.vault" : "mariner.port",
            mutate: `mariner.cosmeticreputation.${id}.${level}`,
            level: level,
          },
        ],
      },
    ],
  });

  mod.setRecipes(
    `recipes.cosmeticreputation.${id}`,
    {
      id: `mariner.updatedisplayedreputation.${id}`,
      grandReqs: {
        "~/source : mariner.reputation": 1,
        "~/source : mariner.reputation.heat": -1,
      },
      furthermore: [
        {
          target: "~/source",
          mutations: [
            removeCosmeticReputation(1),
            removeCosmeticReputation(2),
            removeCosmeticReputation(3),
            removeCosmeticReputation(4),
          ],
        },
      ],
      linked: [
        applyCosmeticReputationId(4),
        applyCosmeticReputationId(3),
        applyCosmeticReputationId(2),
        applyCosmeticReputationId(1),
      ],
    },
    applyCosmeticReputation(1, isVault),
    applyCosmeticReputation(2, isVault),
    applyCosmeticReputation(3, isVault),
    applyCosmeticReputation(4, isVault)
  );
}

const backlightOverlays = [
  {
    expression: "[mariner.reputation] = 1",
    image: "reputation/backlight_1",
    layer: "backlight",
  },
  {
    expression: "[mariner.reputation] = 2",
    image: "reputation/backlight_2",
    layer: "backlight",
  },
  {
    expression: "[mariner.reputation] = 3",
    image: "reputation/backlight_3",
    layer: "backlight",
  },
  {
    expression: "[mariner.reputation] = 4",
    image: "reputation/backlight_4",
    layer: "backlight",
  },
];

const reputationBadgeOverlays = [
  {
    expression: "[mariner.reputation] = 1",
    image: "reputation/badge_1",
    layer: "reputationbadge",
  },
  {
    expression: "[mariner.reputation] = 2",
    image: "reputation/badge_2",
    layer: "reputationbadge",
  },
  {
    expression: "[mariner.reputation] = 3",
    image: "reputation/badge_3",
    layer: "reputationbadge",
  },
  {
    expression: "[mariner.reputation] = 4",
    image: "reputation/badge_4",
    layer: "reputationbadge",
  },
];

export function generateGeneralReputationData() {
  mod.initializeElementFile("composables.locations", ["_composables"]);
  mod.setElements(
    "composables.locations",
    {
      id: "mariner.composables.reputabledestination",
      overlays: [
        {
          image: "$background",
          expression: "[mariner.destination.locked]",
          grayscale: true,
          color: "#000000",
          layer: "background",
        },
        {
          image: "$background",
          layer: "background",
        },
        ...backlightOverlays,
        "$frame",
        {
          image: "locations/docked",
          expression: "[mariner.docked]",
          layer: "docked",
        },
        ...reputationBadgeOverlays,
      ],
      xtriggers: {
        [SIGNALS.GENERATE_REPUTATION_HEAT]: ME_RECIPE(
          "mariner.reputation.generateheat"
        ),
        // Default rendering, used by ports, redefined by vaults
        [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: ME_RECIPE(
          "mariner.updatedisplayedreputation.default"
        ),
        [SIGNALS.DESTROY_REPUTATION]: ME_RECIPE(
          "mariner.reputation.destroyreputation"
        ),
      },
    },
    {
      id: "mariner.composables.dockedreputabledestination",
      $derives: ["mariner.composables.reputabledestination"],
      xtriggers: {
        [SIGNALS.DESTROY_LOCAL_REPUTATION]: ME_RECIPE(
          "mariner.reputation.destroyreputation"
        ),
      },
    }
  );

  mod.setElement("composables.locations", {
    id: "mariner.composables.canbecomehostile",
    aspects: { "mariner.location.nothostile": 1 },
  });

  mod.setElement("composables.locations", {
    id: "mariner.composables.hostile",
    slots: computeHostileSlots("explore"),
    aspects: { "mariner.location.hostile": 1 },
  });

  generateDestinationReputationData("default", defaultNotorietyLabels);
}

export function generateDestinationReputationData({ id, levels, isVault }) {
  if (levels) {
    generateDestinationNotorietyElements(id, levels);
    generateDestinationCosmeticReputationUpdateRecipes(id, isVault);
  }
  return `mariner.updatedisplayedreputation.${levels ? id : "default"}`;
}
