import { intentionSlot } from "../generation_helpers.mjs";

export const STORY_TRADE_SLOT = {
  id: "story",
  label: "Story to trade",
  description: "The story I'm willing to share in order to get them to share theirs.",
  actionId: "mariner.navigate",
  required: { "mariner.story": 1 },
};

export function localBar(label) {
  return {
    elementId: "bar",
    label,
    description: "Different town, same crowd. Different language, same stories.",
    localAspects: {
      "mariner.bar": 1,
      "mariner.crowd": 1,
      modded_talk_allowed: 1,
      "mariner.location": 1,
    },
    canBeHostile: true,
    slots: [
      intentionSlot({ actionId: "explore" }),
      {
        id: "ship",
        label: "Ship or Story",
        description: "In a bustling bar, I could hunt for potential travelers, or proces past experiences",
        actionId: "talk",
        required: {
          "mariner.ship": 1,
          "mariner.experience": 1,
        },
      },
      STORY_TRADE_SLOT,
    ],
  };
}

export function wharf() {
  return {
    elementId: "wharf",
    label: "A Wharf",
    description:
      "A cradle of timber and steel. Here lies the heart of naval trade. [You can repair your ship here, or find commerce for the many things that travel over the sea]",
    localAspects: {
      "mariner.wharf": 1,
      modded_talk_allowed: 1,
      "mariner.location": 1,
    },
    canBeHostile: true,
    slots: [
      {
        id: "ship",
        label: "Ship",
        description: "Which body to repair.",
        actionId: "explore",
        required: {
          "mariner.ship": 1,
        },
      },
      STORY_TRADE_SLOT,
    ],
  };
}

export function instrumentShop({ id, label, description, notDiscoverable }) {
  return {
    elementId: id,
    label,
    description,
    notDiscoverable,
    localAspects: { "mariner.instrumentshop": 1, "mariner.location": 1 },
    canBeHostile: false,
    slots: [
      {
        id: "song",
        label: "Song",
        description: "For which Aspect do I wish to buy an Attuned Instrument",
        actionId: "explore",
        required: {
          "mariner.song": 1,
        },
      },
    ],
  };
}

export function smith({ id, label, smithType, description, notDiscoverable }) {
  return {
    elementId: id,
    label,
    description,
    notDiscoverable,
    localAspects: {
      [`mariner.smiths.${smithType.id}`]: 1,
      "mariner.toolsmith": 1,
      modded_talk_allowed: 1,
    },
    canBeHostile: false,
    slots: [
      {
        id: "tool",
        label: "Tool",
        description: "<Tool to improve>",
        actionId: "talk",
        required: {
          ...Object.fromEntries(smithType.handles.map((aspect) => [`mariner.tool.${aspect}`, 1])),
        },
      },
    ],
  };
}

export function market({ id, label, description, types, storyTrading }) {
  return {
    elementId: id ?? "market",
    label,
    description,
    localAspects: {
      "mariner.market": 1,
      modded_explore_allowed: 1,
      "mariner.location": 1,
    },
    canBeHostile: true,
    slots: [
      {
        id: "trapping",
        label: "Goods",
        description: "What to sell.",
        actionId: "explore",
        required: Object.fromEntries(types.map((t) => [`mariner.trappingtypes.${t}`, 1])),
      },
      STORY_TRADE_SLOT,
    ],
    storyTrading,
  };
}

export function theater({ id, label, description, slots, options, storyTrading }) {
  return {
    elementId: id ?? "theater",
    label: label ?? "<Some Random Theater I Guess>",
    description: description ?? "<Some random theater where people do performances and theaters things (idk I'm no actor)>",
    localAspects: {
      "mariner.theater": 1,
      "mariner.sing_allowed": 1,
      "mariner.location": 1,
    },
    canBeHostile: true,
    slots: [
      {
        id: "verbSong",
        actionId: "mariner.sing",
        label: "Song",
        description: "I should begin my performance, by declaring my intentions, my method, my staging.",
        required: {
          "mariner.song": 1,
        },
      },
      // {
      //   id: "verbTrapping1",
      //   actionId: "mariner.sing",
      //   label: "Trapping",
      //   description:
      //     "I should bring symbolically important trappings to help my performance.",
      //   required: {
      //     "mariner.trapping": 1,
      //   },
      // },
      STORY_TRADE_SLOT,
      ...slots,
    ],
    ...options,
    storyTrading,
  };
}

export function theaterSlots(acceptedAspectsForEachSlot) {
  const slotLabels = {
    ["mariner.instrument"]: "Instrument",
    ["mariner.inspiration.melody"]: "Melody",
    ["mariner.inspiration.nature"]: "Nature",
    ["mariner.inspiration.muse"]: "Muse",
    ["mariner.inspiration.urstory"]: "Ur-Story",
    ["mariner.crew"]: "Helper",
    ["mariner.trapping"]: "Trapping",
    ["mariner.story"]: "Story",
  };

  return acceptedAspectsForEachSlot.map((aspect, index) => ({
    id: `aspect${index + 1}`,
    actionId: "mariner.sing",
    label: slotLabels[aspect] ?? "???",
    required: {
      [aspect]: 1,
    },
  }));
}

export function repeatableConversation({
  id,
  useRecipientId,
  element,
  label,
  startdescription,
  description,
  additionalProperties,
}) {
  return {
    id,
    useRecipientId,
    label,
    startdescription,
    description,
    requirements: {
      [element]: 1,
    },
    additionalProperties,
  };
}

export function uniqueConversation({
  id,
  useRecipientId,
  element,
  quest,
  label,
  startdescription,
  description,
  effects,
  additionalProperties,
  additionalRequirements = {},
}) {
  return {
    id,
    useRecipientId,
    maxexecutions: 1,
    label,
    startdescription,
    description,
    // Actually used as grandReqs
    requirements: {
      [element]: 1,
      ...(quest ? { [quest]: 1 } : {}),
      ...additionalRequirements,
    },
    effects,
    additionalProperties,
  };
}

export function convertSourceConversation({
  id,
  useRecipientId,
  element,
  label,
  startdescription,
  description,
  effects,
  additionalProperties,
}) {
  let source = `${element}.source`;
  return {
    id,
    useRecipientId,
    maxexecutions: 1,
    label,
    startdescription,
    description,
    requirements: {
      [source]: 1,
    },
    effects: {
      ...effects,
      [source]: -1,
      [element]: 1,
    },
    additionalProperties,
  };
}
