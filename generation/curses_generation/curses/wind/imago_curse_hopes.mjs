import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "imagoshopes";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Imago's Hopes Curse",
    "Imago's Hopes are approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.imagoshopes",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Imago's Hopes are approaching...",
    linked: [
      { id: "mariner.curses.imagoshopes.applyeffect" },
      { id: "mariner.curses.imagoshopes.end" },
      { id: "mariner.curses.imagoshopes" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.imagoshopes.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.imagoshopes.end" },
      { id: "mariner.curses.imagoshopes" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.imagoshopes.end",
    label: "Imago's Hopes Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.imagoshopes.start",
  additional: true,
};
