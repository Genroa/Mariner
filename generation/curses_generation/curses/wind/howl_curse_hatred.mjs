import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "howlshatred";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Howl's Hatred Curse",
    "Howl's Hatred is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.howlshatred",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Howl's Hatred is approaching...",
    linked: [
      { id: "mariner.curses.howlshatred.applyeffect" },
      { id: "mariner.curses.howlshatred.end" },
      { id: "mariner.curses.howlshatred" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.howlshatred.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.howlshatred.end" },
      { id: "mariner.curses.howlshatred" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.howlshatred.end",
    label: "Howl's Hatred Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.howlshatred.start",
  additional: true,
};
