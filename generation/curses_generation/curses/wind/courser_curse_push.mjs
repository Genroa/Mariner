import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "courserspush";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "The Courser's Push Curse",
    "The Courser's Push is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.courserspush",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Courser's Push is approaching...",
    linked: [
      { id: "mariner.curses.courserspush.applyeffect" },
      { id: "mariner.curses.courserspush.end" },
      { id: "mariner.curses.courserspush" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.courserspush.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.courserspush.end" },
      { id: "mariner.curses.courserspush" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.courserspush.end",
    label: "The Courser's Push Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.courserspush.start",
  additional: true,
};
