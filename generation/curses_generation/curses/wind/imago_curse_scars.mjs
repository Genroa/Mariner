import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "imagosscars";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Imago's Scars Curse",
    "Imago's Scars are approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.imagosscars",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The Imago's Scars is approaching...",
    linked: [
      { id: "mariner.curses.imagosscars.applyeffect" },
      { id: "mariner.curses.imagosscars.end" },
      { id: "mariner.curses.imagosscars" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.imagosscars.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.imagosscars.end" },
      { id: "mariner.curses.imagosscars" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.imagosscars.end",
    label: "Imago's Scars Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.imagosscars.start",
  additional: true,
};
