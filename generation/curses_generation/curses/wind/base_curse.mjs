import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "xxx";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "YYY Curse",
    "YYY Curse is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.xxx",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The YYY is approaching...",
    linked: [
      { id: "mariner.curses.xxx.applyeffect" },
      { id: "mariner.curses.xxx.end" },
      { id: "mariner.curses.xxx" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.xxx.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.xxx.end" },
      { id: "mariner.curses.xxx" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.xxx.end",
    label: "YYY Curse Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.xxx.start",
  additional: true,
};
