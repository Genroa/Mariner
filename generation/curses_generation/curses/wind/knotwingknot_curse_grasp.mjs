import { generateConcealedStateScaffhold } from "../../generate_curses.mjs";

export function generateBaseCurseCurse() {
  const CURSE_ID = "knotwingknotsgrasp";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "KnotwingKnot's Grasp Curse",
    "KnotwingKnot's Grasp is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.knotwingknotsgrasp",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    startdescription: "The KnotwingKnot's Grasp is approaching...",
    linked: [
      { id: "mariner.curses.knotwingknotsgrasp.applyeffect" },
      { id: "mariner.curses.knotwingknotsgrasp.end" },
      { id: "mariner.curses.knotwingknotsgrasp" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.knotwingknotsgrasp.applyeffect",
    grandReqs: {
      "~/tabletop:{verb/sing}:recipeaspect/mariner.signal.performing": 1,
    },
    furthermore: [
      { target: "~/tabletop!sing[situationstorage]+", effects: { dread: 1 } },
    ],
    linked: [
      { id: "mariner.curses.knotwingknotsgrasp.end" },
      { id: "mariner.curses.knotwingknotsgrasp" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.knotwingknotsgrasp.end",
    label: "KnotwingKnot's Grasp Clearing",
    description: "nnn like nnn...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_PARCHED_THROAT_CURSE = {
  id: "mariner.curses.knotwingknotsgrasp.start",
  additional: true,
};
