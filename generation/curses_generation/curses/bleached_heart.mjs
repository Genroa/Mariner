import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { generateConcealedStateScaffhold } from "../generate_curses.mjs";

export function generateBleachedHeartCurse() {
  const CURSE_ID = "bleachedheart";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(CURSE_ID, "Bleached Heart Curse", "The Bleached Heart Curse is approaching...");

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.bleachedheart",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    label: "<Bleached Heart Curse>",
    startdescription: "The Bleached Heart Curse is approaching...",
    linked: [
      { id: "mariner.curses.bleachedheart.applyeffect" },
      { id: "mariner.curses.bleachedheart.end" },
      { id: "mariner.curses.bleachedheart" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.bleachedheart.applyeffect",
    label: "Bleached Heart: Brittle like Coral Graves",
    startdescription:
      "Through veins like dirty ice, a pale, limpid liquid is pumped that could not resemble the vibrancy of blood. I project no power, I command no space, I exude no flare, all I can do is remain in a limp mockery of life.",
    warmup: 30,
    furthermore: [
      {
        target: "~/visible",
        aspects: { [SIGNALS.EXHAUST_HH]: 1 },
      },
    ],
    linked: [{ id: "mariner.curses.bleachedheart.end" }, { id: "mariner.curses.bleachedheart" }],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.bleachedheart.end",
    label: "Bleached Heart Curse Clearing",
    description: "Fleeting like morning mists...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_BLEACHED_HEART_CURSE = {
  id: "mariner.curses.bleachedheart.start",
  additional: true,
};
