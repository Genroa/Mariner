import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { generateConcealedStateScaffhold } from "../generate_curses.mjs";

export function generateLeadHeartCurse() {
  const CURSE_ID = "leadheart";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(CURSE_ID, "Lead Heart Curse", "The Lead Heart Curse is approaching...");

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.leadheart",
    warmup: 20,
    effects: { "mariner.curses.lifetimecounter": 1 },
    label: "Lead Heart Curse",
    startdescription: "The Lead Heart Curse is approaching...",
    linked: [
      { id: "mariner.curses.leadheart.applyeffect" },
      { id: "mariner.curses.leadheart.end" },
      { id: "mariner.curses.leadheart" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.leadheart.applyeffect",
    label: "The Lead Heart: Weighed Down from Within",
    startdescription: "Each dark thought, each bitter truth, each weighs us down, and pulls us down into that abyss.",
    warmup: 30,
    furthermore: [
      {
        target: "~/visible",
        aspects: { [SIGNALS.EXHAUST_LH]: 1 },
      },
    ],
    linked: [{ id: "mariner.curses.leadheart.end" }, { id: "mariner.curses.leadheart" }],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.leadheart.end",
    label: "Lead Heart Curse Clearing",
    description: "Crumbling like rusting ore...",
    requirements: { "mariner.curses.lifetimecounter": 50 },
    effects: { "mariner.curses.lifetimecounter": -50 },
  });
}

export const SPAWN_LEAD_HEART_CURSE = {
  id: "mariner.curses.leadheart.start",
  additional: true,
};
