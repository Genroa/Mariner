import { INJURE_CREWMEMBER } from "../../generation_helpers.mjs";
import { generateConcealedStateScaffhold } from "../generate_curses.mjs";

export function generateMisfortuneCurse() {
  const CURSE_ID = "misfortune";
  const RECIPE_FILE = `recipes.curses.${CURSE_ID}`;
  generateConcealedStateScaffhold(
    CURSE_ID,
    "Curse of Misfortune",
    "The Curse of Misfortune is approaching..."
  );

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune",
    warmup: 180,
    label: "Curse of Misfortune",
    startdescription: "The Curse of Misfortune is approaching...",
    effects: { "mariner.curses.lifetimecounter": 1 },
    linked: [
      { id: "mariner.curses.misfortune.applyeffect" },
      { id: "mariner.curses.misfortune.end" },
      { id: "mariner.curses.misfortune" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.applyeffect",
    linked: [
      { id: "mariner.curses.misfortune.applyeffect.*", randomPick: true },
    ],
  });

  // Apply notoriety
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.applyeffect.notoriety",
    label: "Serendipitous Notoriety",
    startdescription:
      "My name is mentioned in the wrong company for the wrong reasons. This time, my rising infamy has little to do with me.",
    warmup: 10,
    effects: { "mariner.notoriety": 1 },
    furthermore: [
      {
        movements: {
          "~/tabletop": ["mariner.notoriety"],
        },
      },
    ],
    linked: [
      { id: "mariner.curses.misfortune.end" },
      { id: "mariner.curses.misfortune" },
    ],
  });

  // Apply wound
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.applyeffect.wound",
    linked: [
      {
        id: "mariner.curses.misfortune.applyeffect.wound.dread",
        randomPick: true,
      },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.applyeffect.wound.dread",
    label: "Unexpected Dread",
    startdescription: "Nightmares cling to me like cogwebs.",
    warmup: 10,
    effects: { "mariner.experiences.rattling": 1 },
    furthermore: [
      {
        movements: {
          "~/tabletop": ["mariner.experiences.rattling"],
        },
      },
    ],
    linked: [
      { id: "mariner.curses.misfortune.end" },
      { id: "mariner.curses.misfortune" },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.applyeffect.wound.fascination",
    label: "Fascination Blooms",
    startdescription: "Coincidences become indistinguisable from patterns. The clouds speak. My reflections beckon.",
    warmup: 10,
    effects: { "mariner.experiences.unsettling": 1 },
    furthermore: [
      {
        movements: {
          "~/tabletop": ["mariner.experiences.unsettling"],
        },
      },
    ],
    linked: [
      { id: "mariner.curses.misfortune.end" },
      { id: "mariner.curses.misfortune" },
    ],
  });

  // Apply longing
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.applyeffect.longing",
    label: "Division Widens",
    startdescription: "It was merely a misunderstanding, yet it grows into a rift that might soon be insurmountable. One of my crew is one step closer to leaving my employ.",
    warmup: 10,
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.traits.loyal]<1": {
            mutate: "mariner.crew.longing",
            level: 1,
            additive: true,
            limit: 1,
          },
        },
      },
    ],
    linked: [
      { id: "mariner.curses.misfortune.end" },
      { id: "mariner.curses.misfortune" },
    ],
  });

  // End
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.curses.misfortune.end",
    label: "Curse of Misfortune Curse Clearing",
    description: "Hollow like echoed laughter...",
    requirements: { "mariner.curses.lifetimecounter": 7 },
    effects: { "mariner.curses.lifetimecounter": -10 },
  });
}

export const SPAWN_MISFORTUNE_CURSE = {
  id: "mariner.curses.misfortune.start",
  additional: true,
};
