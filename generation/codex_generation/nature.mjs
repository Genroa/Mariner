import { generateCodexEntry } from "./codex_generation.mjs";

export function generateMyNatureCodexEntries() {
  generateCodexEntry("codex.nature", "My Nature", "<>");
  generateCodexEntry(
    "codex.nature.lackheart",
    "Lack-Heart",
    "There is a pit in my chest, A maw above my gut. If I do not feed it with fresh experiences, it begins to consume me. If I stick around too long, my blood runs cold and my joy starts to fade. So back out onto the sea I go.",
    "codex.nature"
  );
  generateCodexEntry(
    "codex.nature.halfheart",
    "Half-Heart",
    "They called me a Half-Heart: Morland, Agdistis and some others before. A split soul. All I know is that something is missing. That my senses are on edge like an exposed nerve.",
    "codex.nature"
  );
}
