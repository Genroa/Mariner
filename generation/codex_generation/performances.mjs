import {
  buildSequentialCodexEntry,
  generateCodexEntry,
} from "./codex_generation.mjs";

export function generatePerformancesCodexEntries() {
  generateCodexEntry(
    "codex.performance.base",
    "[The Arts] Orphean Magick",
    "The Stage is a temple, and with the audience as both congregation and holy vessel, I can try to reach beyond the Real with my performances. [For now, only some Performances are available, and only those with the Intent of Edge, Winter, and Lantern."
  );
  generateCodexEntry(
    "codex.performance.conversation.morland",
    "[The Arts]",
    "<>"
  );
  generateCodexEntry(
    "codex.performance.conversation.harald.winter",
    "[The Arts] Harald on Winter",
    "Winters are not that harsh here, not like our neighbors to the north. Here, the Intent of Winter can still be a kindness: a wiping of the slate, a quieting of the noise. I have not sought what lies under the Direction of Winter. But perhaps... Let me know if you find what lies there."
  );
  generateCodexEntry(
    "codex.performance.conversation.harald.heart",
    "[The Arts] Harald on Heart",
    "The Direction of Heart I studied. It is referenced in all of my most famous works. It contains in itself the Largest and the Smallest, the most real and the most Ephemeral. In it's Direction, it represents both the infinity of the sky and the infinity of the self. If Heart is your Intent, it is like the supportive breath of life. It will sustain and maintain your Direction, if possible."
  );
  generateCodexEntry(
    "codex.performance.conversation.harald.lantern",
    "[The Arts] Harald on Lantern",
    '"the Lantern of Enlightenment, Hekate\'s seeking torches... Lantern, like the Watchman Illuminates. If something remains obscured to you, try affecting it with the intent of Lantern. "'
  );
  generateCodexEntry(
    "codex.performance.conversation.harald.grail",
    "[The Arts] Harald on Grail",
    '"Cybele\'s ministrations I never mastered. Those perform better at more private stages I think, unless you want to get infamous for debauchery." He rolls his shoulder. "Perhaps a vaudevillian can help you further?"'
  );
  generateCodexEntry(
    "codex.performance.conversation.harald.moth",
    "[The Arts] Harald on Moth",
    '"Moth? Moth is the distraction. Moth is that which clings to you, buzzes around. Moth is the distractions of nature, the miseries of the world. if you are chased by a shadow that you wish to expose or expell, your direction should be aimed at Moth."'
  );
  generateCodexEntry(
    "codex.performance.conversation.veiledlady.moth",
    "[The Arts] Veiled Lady on Moth",
    '"Moth guides what hunts and stalks us. Our doubts, our fears, our shadow, our past. Any negativity still clinging to your clothes or hair. With the Intent of Lantern and the Direction of Moth, you can reveal the nature of a curse that has latched on to you. To Quell the curse, you will need to change your Intention, but keep the Direction the same."'
  );
  generateCodexEntry(
    "codex.performance.conversation.veiledlady.grail",
    "[The Arts] Veiled Lady on Grail",
    '"The acts of Grail? And here I picked up that your Heart belonged to antother? Oh I tease mon chéri. The Intent of Grail is to charm, subsume the will of another with their desire to please you. If you wish to set of in that Direction though... well... You allready are, in a way. In the Direction of Grail lies both the Sea, and it\' drowning waters, as well as the depths of your Desire itself."'
  );
  generateCodexEntry(
    "codex.performance.conversation.veiledlady.winter",
    "[The Arts] Veiled Lady on Winter",
    '"I do not like to deal with Winter. I have come quite close enough to death, thank you. The Winter Direction is a dangerous road... one for the desperate or the desolate. One who believes they have lost all may seek to pursue it further..."'
  );
  generateCodexEntry(
    "codex.performance.conversation.veiledlady.lantern",
    "[The Arts] Veiled Lady on Lantern",
    '"The Light of Lantern was never mine to persue, I have much preferred the kindness of shadows. But just like my dances, the Direction of lantern is also one of Revelation."'
  );
  generateCodexEntry(
    "codex.performance.conversation.agdistis.grail",
    "[The Arts] Agdistis on Grail",
    '"Our Arts are even less precise than those performed by aspirants and scholars. Take the weather, for example. If your Direction in a Performance is aimed at Grail, Paired with an Intent of Edge, You can Rouse Storms from Sea and Sky. To quell storms however, both Direction and Intent change... "'
  );
  generateCodexEntry(
    "codex.performance.conversation.agdistis.moth",
    "[The Arts] Agdistis on Moth",
    '"The Club does do Performances in the Old Path, though they focus on dance, not music. If Sulochana had been here... Perhaps you can seek out another shedder of skins to elaborate on these mysteries?"'
  );
  generateCodexEntry(
    "codex.performance.conversation.agdistis.edge",
    "[The Arts] Agdistis on Edge",
    '"We try for a particular ambiance here at the Ecdysis club, and the clanging of blades is rarely a part of that. Still, if you perform with the Intent of Edge, you can rouse many things beyond mere battle lust."'
  );
  generateCodexEntry(
    "codex.performance.conversation.agdistis.heart",
    "[The Arts] Agdistis on Heart",
    '"The Revolutions of the Heart are what I studied the deepest, though the pursuit left it scars on both my body and soul. Many of the Powers of the Wood and sea share their interest in Heart, but it most closely connected to its Patron Hour, The Storm Relentless. With the right performance, the Sky can be commanded, by pairing this Direction with what it springs from."'
  );
}
