import {
    buildSequentialCodexEntry,
    generateCodexEntry,
} from "./codex_generation.mjs";

export function generateMedseaQuestsEntries() {
    generateCodexEntry(
        "codex.hours.ringyew",
        "The Ring-Yew",
        buildSequentialCodexEntry([
            "Among sailors, there are few rules as universal as the absolute prohibition to use yew wood to build the cabins of a ship, lest they be brought to dark, moonlight-dappled woods in their dreams. It is said to anger the “Ring-Yew”, sometimes called “Queen of the Wood” or “Malachite”. She is the Tree and The Hive, the Honey and the Sting.",
            {
                "codex.hours.ringyew.1":
                    "The Queen in the Woods loved deeply in the story of the Pine Knight, but I don't think the Malachite is a being of love, not like mortals know it.\n\nIn the end she chose to sacrifice her lover, complying with Red Desire and Cold Revenge. I wonder if she regretted it...",
            },
        ]),
        "codex.hours"
    );
}
