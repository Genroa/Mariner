import {
    buildCodexEntryBlock,
    buildSequentialCodexEntry,
    generateCodexEntry,
} from "./codex_generation.mjs";

export function generatepersonCodexEntries() {
    generateCodexEntry("codex.persons", "Persons of Interest", "<Who I hear off and who I meet. Who holds interest to me and interest in me.>");
    generateCodexEntry(
        "codex.persons.galantha",
        "<The Last Limiean>",
        "<The Galantha is an anicent Long, dedicated to the Traditions of Ivory. Where they have spend the past millennium is mostly unknown, but it seems they have now returned to the Iberian peninsula.>",
        "codex.persons"
    );
}
