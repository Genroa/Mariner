import {
  buildCodexEntryBlock,
  buildSequentialCodexEntry,
  generateCodexEntry,
} from "./codex_generation.mjs";

export function generateVagabondAscensionCodexEntries() {
  generateCodexEntry(
    "codex.ascensions.exemplum",
    "[Exemplum]",
    "The world can be a haze of soot and rain. Can I follow the path to a brighter place?"
  );

  generateCodexEntry(
    "codex.ascensions.exemplum.hiraezh",
    buildCodexEntryBlock(
      {
        "codex.ascensions.exemplum.hiraezh.solvedmystery":
          "[Exemplum] The Sunken City of Ker Ys",
        "codex.ascensions.exemplum.hiraezh.solvedvault": "[Exemplum] Hiraezh",
      },
      "[Exemplum] A Sunken City"
    ),
    buildCodexEntryBlock(
      {
        "codex.ascensions.exemplum.hiraezh.solvedmystery":
          "The world can be a haze of soot and rain. Do I wish to follow the Centipede's Pilgrimage? Morland has informed me of its first step: I am to visit a Sunken City, and learn its story there.\n\nThe Sunken City mentioned is the city of Ker Ys, and I have learned the mood of the winds that can carry me there, and the conditions under which the city surfaces.",
        "codex.ascensions.exemplum.hiraezh.solvedvault":
          "I have taken the first step on the pilgrimage of the Centipede. She has led me to Ker Ys, to hear the tale of Old Mother Anguish. She has taught me of her Hiraezh, and the meaning of irrevocable loss. I carry that sentiment in my flesh, my blood and my bones, and even my Lack-Heart cannot drain me of its knowledge.",
      },
      "The world can be a haze of soot and rain. Do I wish to follow the Centipede's Pilgrimage? Morland has informed me of its first step: I am to visit a Sunken City, and learn its story there. But first: <color=yellow>I must scour story, song and sea to find it.</color>"
    )
  );

  generateCodexEntry(
    "codex.ascensions.exemplum.struggimento",
    buildCodexEntryBlock(
      {
        "codex.ascensions.exemplum.struggimento.solvedmystery":
          "[Exemplum] <>",
        "codex.ascensions.exemplum.struggimento.solvedvault": "[Exemplum] Struggimento",
      },
      "[Exemplum] <>"
    ),
    buildCodexEntryBlock(
      {
        "codex.ascensions.exemplum.struggimento.solvedmystery":
          "",
        "codex.ascensions.exemplum.struggimento.solvedvault":
          "<>",
      },
      "<>"
    )
  );
}
