import { buildCodexEntryBlock, buildSequentialCodexEntry, generateCodexEntry } from "./codex_generation.mjs";

export function generateTwinsAscensionCodexEntries() {
  generateCodexEntry(
    "codex.ascensions.constellate",
    "[Constellate]",
    "There is a tugging at my soul, a half that wants to be whole..."
  );

  generateCodexEntry(
    "codex.ascensions.constellate.character",
    "[Constellate] Mending Character",
    buildSequentialCodexEntry([
      "Before one does anything, one must first decide to do that thing. My Soul has been torn, and If I want to no longer be dragged by the tides of life, but find my strength of Character, and mend it.\n\n\n",
      buildCodexEntryBlock(
        {
          "codex.ascensions.constellate.character.solvedhowto":
            "I have found my inspiration in the secret story of the Thunderskin, Ascended Wind, robbed of his agency. A knot of choices, schemes, intentions lead to his Ascension. I will invoke those choices, those schemes, and Enact this great drama upon the stage.",

          "codex.ascensions.constellate.character.mended":
            "I have mended my Character, and have made a choice with the utmost clarity. I will pursue the path to becoming whole, and I will find my Missing Half.",
        },
        "Any mending of the Soul would be an Orphean Act, but which one? I will discover the answers where I always do, in the world, and the stories it holds."
      ),
    ])
  );

  generateCodexEntry(
    "codex.ascensions.constellate.wist",
    buildCodexEntryBlock(
      {
        "codex.ascensions.constellate.wist.solvedmystery": "[constellate] <>",
        "codex.ascensions.constellate.wist.solvedvault": "[constellate] wist",
      },
      "[constellate] <>"
    ),
    buildCodexEntryBlock(
      {
        "codex.ascensions.constellate.wist.solvedmystery": "",
        "codex.ascensions.constellate.wist.solvedvault": "<>",
      },
      "<>"
    )
  );
}
