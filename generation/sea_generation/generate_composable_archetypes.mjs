import { ME_LINK, ME_RECIPE, intentionSlot } from "../generation_helpers.mjs";
import { STORY_TRADE_SLOT } from "../port_generation/helpers.mjs";
import { SIGNALS } from "../generate_signal_aspects.mjs";
import { computeHostileSlots } from "../port_generation/port_element_generation.mjs";
import { generatePetsittingOverlays } from "../pets_generation/generate_pets.mjs";

const SEVERANCE_SLOT = {
  id: "severancepackage",
  label: "Severance",
  actionId: "talk",
  required: {
    funds: 1,
  },
  ifAspectsPresent: {
    "mariner.crew.wantstoleave": -1,
  },
};

const BARGAIN_SLOT = (index) => ({
  id: "bargain_" + index,
  label: "An Incentive",
  description: "The scales of any decision can be amended with the right prosaic weight.",
  actionId: "talk",
  required: {
    funds: 1,
  },
  ifAspectsPresent: {
    "mariner.crew.wantstoleave": 1,
  },
});
const BARGAIN_SLOTS = [BARGAIN_SLOT(1), BARGAIN_SLOT(2), BARGAIN_SLOT(3), BARGAIN_SLOT(4), BARGAIN_SLOT(5)];

export function generateComposableArchetypes() {
  mod.initializeElementFile("composables.locations", ["_composables"]);
  mod.initializeElementFile("composables.people", ["_composables"]);

  mod.setElement("composables.locations", {
    id: "mariner.composables.port",
    aspects: { "mariner.port": 1 },
    unique: true,
  });

  mod.setElement("composables.locations", {
    id: "mariner.composables.dockedport",
    $derives: ["mariner.composables.port"],
    aspects: {
      "mariner.docked": 1,
      "mariner.crowd": 1,
      modded_explore_allowed: 1,
    },
    slots: [
      {
        id: "intent",
        label: "Intent",
        description:
          "Do I explore with my active self, or let my sentivity lead me... or am I here to advance on some caper or escapade",
        actionId: "explore",
        required: {
          "mariner.lackheart": 1,
          "mariner.halfheart": 1,
          "mariner.quest": 1,
        },
      },
      STORY_TRADE_SLOT,
    ],
    xtriggers: {
      [SIGNALS.UPDATE_DISPLAYED_REPUTATION]: ME_RECIPE("mariner.updatedisplayedreputation.default"),
    },
  });

  // NPCS
  const npcComposableBase = {
    overlays: ["$background", "$frame", ...generatePetsittingOverlays()],
    aspects: {
      modded_talk_allowed: 1,
      "mariner.local": 1,
      "mariner.npc": 1,
    },
    unique: true,
    slots: [
      {
        id: "object",
        label: "Topic",
        actionId: "talk",
        addsSlots: true,
        required: {
          "mariner.location": 1,
          "mariner.song": 1,
          "mariner.mystery": 1,
          "mariner.letter": 1,
          "mariner.story.source": 1,
          "mariner.story": 1,
          "mariner.topic": 1,
          "mariner.pet": 1,
          desire: 1,
          //"mariner.package": 1,
        },
      },
      {
        id: "moneyforpetsitting",
        actionId: "talk",
        ifaspectspresent: {
          "mariner.pet": 1,
        },
        label: "<Fee>",
        description: "<Fee>",
        required: {
          funds: 1,
        },
      },
      {
        id: "story",
        label: "Story to trade",
        description: "The story I'm willing to share in order to get them to share theirs.",
        actionId: "mariner.navigate",
        required: {
          "mariner.story": 1,
        },
      },
    ],
  };

  mod.setElements(
    "composables.locations",
    {
      id: "mariner.composables.npc",
      ...npcComposableBase,
    },
    {
      id: "mariner.composables.recipientnpc",
      ...npcComposableBase,
      aspects: {
        ...npcComposableBase.aspects,
        "mariner.recipient": 1,
      },
      slots: [
        {
          ...npcComposableBase.slots[0],
          required: {
            ...npcComposableBase.slots[0].required,
            "mariner.package": 1,
          },
        },
        ...npcComposableBase.slots.slice(1),
      ],
    },
    {
      id: "mariner.composables.npc.hostile",
      $derives: ["mariner.composables.npc", "mariner.composables.hostile"],
      slots: computeHostileSlots("talk"),
    }
  );

  // Wharves
  mod.setElements(
    "composables.locations",
    {
      id: "mariner.composables.wharf",
      label: "Wharf",
      description:
        "A cradle of timber and steel. Here lies the heart of naval trade. [You can repair your ship here, or find commerce for the many things that travel over the sea]",
      decayTo: "mariner.nowharfatsea",
      aspects: {
        "mariner.location": 1,
        "mariner.wharf": 1,
        "mariner.local": 1,
        modded_explore_allowed: 1,
      },
      unique: true,
    },
    {
      id: "mariner.composables.wharf.available",
      $derives: ["mariner.composables.wharf"],
      slots: [
        {
          id: "ship",
          label: "Ship",
          description: "Which body to repair.",
          actionId: "explore",
          required: {
            "mariner.ship": 1,
          },
        },
        {
          id: "action",
          label: "Action?",
          description: "<Configure or Upgrade Ship>",
          actionId: "explore",
          required: {
            "mariner.halfheart": 1,
            "mariner.lackheart": 1,
          },
          ifaspectspresent: {
            "mariner.wharf": 1,
          },
        },
        STORY_TRADE_SLOT,
      ],
    }
  );

  // Bars
  mod.setElements(
    "composables.locations",
    {
      id: "mariner.composables.bar.base",
      aspects: {
        "mariner.location": 1,
        "mariner.bar": 1,
        "mariner.crowd": 1,
        "mariner.local": 1,
        modded_talk_allowed: 1,
        modded_explore_allowed: 1,
      },
      unique: true,
      decayTo: "mariner.nobaratsea",
    },
    {
      id: "mariner.composables.bar",
      $derives: ["mariner.composables.bar.base"],
      slots: [
        intentionSlot({ actionId: "explore" }),
        {
          id: "ship",
          label: "Ship or Experience",
          description:
            "In a bustling bar, I could find business with those who seek a sea voyage, or process my past experiences through tales of my adventures to the willing and unwilling alike.",
          actionId: "talk",
          addsSlots: true,
          required: {
            "mariner.ship": 1,
            "mariner.experience": 1,
          },
        },
        STORY_TRADE_SLOT,
      ],
    }
  );

  // Supply Stores
  mod.setElements(
    "composables.locations",
    {
      id: "mariner.composables.store.base",
      aspects: {
        "mariner.location": 1,
        "mariner.store": 1,
        "mariner.local": 1,
        modded_talk_allowed: 1,
      },
      unique: true,
      decayTo: "mariner.nostoreatsea",
    },
    {
      id: "mariner.composables.store",
      $derives: ["mariner.composables.store.base"],
      slots: [],
    }
  );

  mod.setElements(
    "composables.people",
    {
      id: "mariner.composables.crew.mortal",
      aspects: {
        "mariner.crew": 1,
        mortal: 1,
        "mariner.cabinuse": 1,
        modded_talk_allowed: 1,
      },
      slots: [SEVERANCE_SLOT, ...BARGAIN_SLOTS],
      xtriggers: {
        [SIGNALS.WITNESS_CANNIBALISM]: ME_RECIPE("mariner.crew.witnesscannibalism"),
        [SIGNALS.TRIGGER_PHYSICAL_ATTACK_RESULT]: ME_LINK("mariner.genericevents.injurecrewmember.injuremortal"),
        [SIGNALS.TRIGGER_UNRAVEL_ATTACK_RESULT]: ME_LINK("mariner.genericevents.unravelcrewmember.kill"),
        [SIGNALS.WITNESS_HORROR_TRAUMA]: ME_LINK("mariner.genericevents.traumatizecrewmember.horror.injuremortal"),
        [SIGNALS.WITNESS_SUPERNATURAL_TRAUMA]: ME_LINK("mariner.genericevents.traumatizecrewmember.supernatural.injuremortal"),
        [SIGNALS.TURN_INTO_LUNATIC]: "lunatic", // TODO make an actual lunatic?
        [SIGNALS.FREAK_OUT]: [
          { morpheffect: "mutate", id: "mariner.crew.exhausted", level: 1 },
          { morpheffect: "mutate", id: "mariner.crew.longing", level: 1 },
        ],
      },
    },
    {
      id: "mariner.composables.recruitmentopportunity",
      aspects: {
        "mariner.local": 1,
        modded_talk_allowed: 1,
        "mariner.recruitmentopportunity": 1,
      },
    }
  );
}
