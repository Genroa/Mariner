import { ALL_SEAS } from "../generation_helpers.mjs";

/*
THIS METHOD ASSUMES ALL SEAS HAVE BEEN GENERATED.
*/
export function generateLocationsDiscovery() {
  mod.initializeRecipeFile("recipes.combinetofindlocation", ["locations"]);
  for (const seaId of ALL_SEAS) {
    mod.setRecipe("recipes.combinetofindlocation", {
      id: `mariner.combinetofindlocationin.${seaId}`,
      craftable: true,
      actionId: "mariner.navigate",
      warmup: 60,
      label: "Pull a Thread",
      startdescription:
        "If I weigh the local news I have scavenged in bars against the snippets I have obtained from my contacts, I may be able to make new connections, find new opportunities and discover truly extraordinary places.",
      requirements: {
        "mariner.scrapofinformation": 1,
        [`mariner.rumour.${seaId}`]: 1,
      },
      deckeffects: {
        [`mariner.decks.locations.${seaId}.discoverable`]: 1,
      },
      effects: {
        "mariner.scrapofinformation": -1,
        [`mariner.rumour.${seaId}`]: -1,
      },
      linked: [{ id: "mariner.applyactionpostfindlocation.start.*" }],
    });
  }
}
