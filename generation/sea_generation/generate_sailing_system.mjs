import { ME_DESTROY, buildRefinementBlock } from "../generation_helpers.mjs";
import { CURRENT_SHIP_STAT_VALUE, SHIP_STAT } from "../stats_generation.mjs";
import { generateSailingActions } from "./sailing_actions/generate_sailing_actions.mjs";
import { generateSailingRoutes } from "./generate_sailing_routes.mjs";
import { INFLUENCED_NO_EVENT } from "./sailing_actions/helpers.mjs";

function generateSailingSystemElements() {
  mod.initializeElementFile("sailingelements", ["sailing"]);
  mod.setElements(
    "sailingelements",
    {
      id: "mariner.sailing.eventopportunity",
      label: "Event Opportunity",
      description: "I may encounter trails and tribulations on my travels.",
    },
    // TODO: hide, eventually
    {
      id: "mariner.sailing.timespentatsea",
      label: "[Time spent at sea]",
    },
    // Counter since last meal
    // TODO: hide, eventually
    {
      id: "mariner.sailing.timesincelastmeal",
      label: "[Time since last meal]",
    },
    // Counter since last event opportunity triggered
    // TODO: hide, eventually
    {
      id: "mariner.sailing.timesincelastevent",
      label: "[Time since last event]",
    },
    // Total distance of the destination
    {
      id: "mariner.sailing.totaldistance",
      label: "Total distance to travel",
      xtriggers: { "mariner.signal.arrivetodestination": ME_DESTROY() },
    },
    // Distance travelled towards destination
    {
      id: "mariner.sailing.distancetravelled",
      label: "Distance travelled",
      xtriggers: { "mariner.signal.arrivetodestination": ME_DESTROY() },
    },
    // TODO: hide, eventually
    // Distance travelled towards destination, in percents (0 to 100). Used for the progress bar.
    {
      id: "mariner.sailing.distancetravelledinpercents",
      label: "[Distance travelled in percents]",
      xtriggers: { "mariner.signal.arrivetodestination": ME_DESTROY() },
    }
  );
}

function generateSailingMainLoop(actionsAtSea) {
  mod.initializeRecipeFile("recipes.sailing.mainloop", ["sailing"]);
  mod.setRecipes(
    "recipes.sailing.mainloop",
    {
      id: "mariner.sailing.mainloop",
      label: "Sailing",
      actionId: "mariner.sail",
      startdescription: `<b><u>Current stats:</u></b><br><u>Speed</u>: ${buildRefinementBlock(
        {
          [SHIP_STAT("speed")]: "$value",
        },
        "0"
      )} (ship: ${buildRefinementBlock({
        [SHIP_STAT("speed")]: "$value",
      })})<br><u>Discretion</u>: ${buildRefinementBlock(
        {
          [SHIP_STAT("discretion")]: "$value",
        },
        "0"
      )} (ship: ${buildRefinementBlock({ [SHIP_STAT("discretion")]: "$value" })})${buildRefinementBlock({
        [SHIP_STAT("grammarie")]: "<br><u>Grammarie</u>: ",
      })}${buildRefinementBlock({
        [SHIP_STAT("grammarie")]: "$value",
      })} (ship: ${buildRefinementBlock({ [SHIP_STAT("grammarie")]: "$value" })})<br>Progress: ${buildRefinementBlock(
        {
          "mariner.sailing.distancetravelled": "$value",
        },
        "0"
      )}/${buildRefinementBlock({ "mariner.sailing.totaldistance": "$value" })} (${buildRefinementBlock(
        {
          "mariner.sailing.distancetravelledinpercents": "$value",
        },
        "0"
      )}%)<br><br>What might find us next upon the sea?`,
      craftable: false,
      warmup: 4,
      furthermore: [{ movements: { "~/tabletop": ["mariner.crew"] } }],
      linked: [
        INFLUENCED_NO_EVENT,
        {
          id: "mariner.sailing.pickevent",
          chance: 70,
        },
        { id: "mariner.sailing.noevent" },
      ],
      alt: ["mariner.sailing.sinking", ...actionsAtSea],
      slots: [
        {
          id: "action",
          actionId: "mariner.sail",
          label: "<Action?>",
          description:
            "<There is always enough to do while at sea. I can attempt to patch the ship with a repair kit, lighten our load by tossing cargo over board, or set one of my crew members to work.>",
          required: {
            "mariner.repairkit": 1,
            "mariner.cargo": 1,
            "mariner.crew": 1,
            "mariner.thrill": 1,
          },
          forbidden: { "mariner.crew.exhausted": 1, }
        },
      ],
    },
    {
      id: "mariner.sailing.openseamainloop",
      label: "Sailing",
      actionId: "mariner.sail",
      startdescription: "What might find us next upon the sea? [OPEN SEA]",
      craftable: false,
      warmup: 20,
      requirements: { "mariner.destination": -1 },
      furthermore: [{ movements: { "~/tabletop": ["mariner.crew"] } }],
      slots: [
        {
          id: "action",
          actionId: "mariner.sail",
          label: "<New Course? Action to take?>",
          description: "<There are always many things to do at sea, but we can still choose where we focus our attention.>",
          required: {
            "mariner.destination": 1,
            "mariner.repairkit": 1,
            "mariner.cargo": 1,
            "mariner.crew": 1,
            "mariner.thrill": 1,
          },
          forbidden: { "mariner.crew.exhausted": 1, }
        },
      ],
      linked: [
        INFLUENCED_NO_EVENT,
        {
          id: "mariner.sailing.pickevent",
          chance: 70,
        },
        { id: "mariner.sailing.noevent" },
      ],
      alt: ["mariner.sailing.sinking", "mariner.sailing.setcourse.from.*", ...actionsAtSea],
    }
  );
}

function CURRENT_GRAMMARIE_STAT_AT_LEAST(level) {
  return `(${CURRENT_SHIP_STAT_VALUE("grammarie")} >= ${level})`;
}

function generateSailingPickEventRouter() {
  mod.initializeRecipeFile("recipes.sailing.pickevent", ["sailing"]);
  mod.setRecipes(
    "recipes.sailing.pickevent",
    {
      id: "mariner.sailing.pickevent",
      requirements: { "mariner.sailing.eventopportunity": 1 },
      linked: [
        {
          id: "mariner.sailing.specialseaevents.start*",
          randomPick: true,
        },
        {
          id: "mariner.sailing.grammarievents.level4.start.*",
          chance: `0.25 * ${CURRENT_SHIP_STAT_VALUE("grammarie")} * ${CURRENT_GRAMMARIE_STAT_AT_LEAST(7)}`,
          randomPick: true,
        },
        {
          id: "mariner.sailing.grammarievents.level3.start.*",
          chance: `0.33 * ${CURRENT_SHIP_STAT_VALUE("grammarie")} * ${CURRENT_GRAMMARIE_STAT_AT_LEAST(5)}`,
          randomPick: true,
        },
        {
          id: "mariner.sailing.grammarievents.level2.start.*",
          chance: `0.5 * ${CURRENT_SHIP_STAT_VALUE("grammarie")} * ${CURRENT_GRAMMARIE_STAT_AT_LEAST(3)}`,
          randomPick: true,
        },
        {
          id: "mariner.sailing.grammarievents.level1.start.*",
          chance: `${CURRENT_SHIP_STAT_VALUE("grammarie")} * ${CURRENT_GRAMMARIE_STAT_AT_LEAST(1)}`,
          randomPick: true,
        },
        {
          id: "mariner.sailing.seaevents.start*",
          randomPick: true,
        },
      ],
    },
    {
      id: "mariner.sailing.checkforevent.influenced",
      requirements: { "mariner.sailing.eventopportunity": 1 },
      purge: { "mariner.influences.winter": 2 },
      linked: "mariner.sailing.postevent",
    },
    {
      id: "mariner.sailing.noevent",
      effects: { "mariner.sailing.timesincelastevent": 1 },
      linked: "mariner.sailing.cycleend",
    }
  );
}

export function generateSailingSystem() {
  const actionsAtSea = generateSailingActions();
  generateSailingSystemElements();
  generateSailingRoutes();
  generateSailingMainLoop(actionsAtSea);
  generateSailingPickEventRouter();
}
