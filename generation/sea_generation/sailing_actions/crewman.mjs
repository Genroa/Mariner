import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";
import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateCrewmanAction() {
  mod.initializeRecipeFile("recipes.sailing.crewman", ["sailing", "actionsatsea"]);

  mod.initializeElementFile("elements.sailing.crewman", ["sailing", "actionsatsea"]);
  mod.setElement("elements.sailing.crewman", {
    id: `mariner.speedbonus.card`,
    aspects: { "mariner.ship.speed.bonus": 1 },
  });
  mod.setRecipes("recipes.sailing.crewman", {
    id: "mariner.sailing.crewman",
    label: "<Employ a Crewman for Speed>",
    startdescription: "<with a little extra effort, we can gain a little speed for a little while.>",
    grandReqs: {
      "[mariner.crew.basic] || [mariner.crew.intro.doc] || [mariner.crew.intro.grumpy] || [mariner.crew.intro.bashful] || [mariner.crew.intro.sneezy] || [mariner.crew.intro.happy]|| [mariner.crew.intro.dopey]|| [mariner.crew.intro.sleepy]": 1,

    },

    furthermore: [
      { effects: { "mariner.speedbonus.card": 1 }, },
      {
        mutations: {
          "mariner.crew": {
            mutate: "mariner.crew.exhausted",
            level: 1,
          },
        },
      },
      { movements: { "~/tabletop": ["mariner.crew"] } },
      {
        target: "~/extant",
        aspects: { [SIGNALS.COMPUTE_SHIP_STATS]: 1 },
      },
    ],
    linked: POST_SAILING_ACTION_LINK,
  });
  return "mariner.sailing.crewman";
}
