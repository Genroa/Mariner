// TODO: maybe the recipe this links to, should link to cyclenend instead?

import { INFLUENCE_CHANCE } from "../../song_generation/generate_influences.mjs";

// TODO: should be in a more generic file than here
export const INFLUENCED_NO_EVENT = {
  id: "mariner.sailing.checkforevent.influenced",
  chance: INFLUENCE_CHANCE("winter"),
};

export const POST_SAILING_ACTION_LINK = [
  INFLUENCED_NO_EVENT,
  {
    id: "mariner.sailing.pickevent",
    chance: 70,
  },
  { id: "mariner.sailing.noevent" },
];
