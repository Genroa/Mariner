import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateCookAction() {
  mod.initializeRecipeFile("recipes.sailing.cook", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.cook",
    {
      id: "mariner.sailing.cook",
      label: "<Replenish my Crew with a Feast>",
      startdescription:
        "<If I have the rations to spend, I can have my cook provide a feast to refresh my whole crew. Even if I do not have the rations, they can attempt to scrounge something together>",
      requirements: {
        "mariner.crew.specialist.cook": 1,
      },
      linked: [
        {
          id: "mariner.sailing.cook.attempt",
        },
      ],
    },
    {
      id: "mariner.sailing.cook.attempt",
      warmup: 30,
      slots: [
        {
          id: "help1",
          label: "filling",
          description: "with what do I create the feast?",
          required: {
            "mariner.rations": 1,
            "mariner.trappings.grail": 1,
          },
        },
      ],
      mutations: {
        "mariner.crew": {
          mutate: "mariner.crew.exhausted",
          level: 3,
        },
      },
      linked: [
        {
          id: "mariner.sailing.cook.success",
          requirements: { "mariner.rations": 1 },
        },
        {
          id: "mariner.sailing.cook.success",
          chance: "10 + (5 * [grail])",
        },
        {
          id: "mariner.sailing.cook.failure",
        },
      ],
    },
    {
      id: "mariner.sailing.cook.success",
      label: "<A feast!>",
      startdescription: "<Success>",
      effects: {
        "mariner.rations": -1,
        "mariner.trappings.grail": -1,
      },
      furthermore: [
        {
          target: "~/tabletop",
          mutations: {
            "[mariner.crew] && [mariner.crew.exhausted]=1": {
              mutate: "mariner.crew.exhausted",
              level: 0,
              limit: 10,
            },
          },
        },
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.cook.failure",
      label: "<Attempt to provide a feast: Failure>",
      startdescription: "<Failure>",
      furthermore: { movements: { "~/tabletop": ["mariner.crew"] } },
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.cook";
}
