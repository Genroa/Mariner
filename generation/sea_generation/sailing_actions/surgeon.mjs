import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateSurgeonAction() {
  mod.initializeRecipeFile("recipes.sailing.surgeon", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.surgeon",
    {
      id: "mariner.sailing.surgeon",
      label: "<Use a Surgoen to distill Medical Supplies>",
      startdescription: "<Take stock of our medical supplies to prepare for emergencies, with Knock or Grail.>",
      requirements: {
        "mariner.crew.specialist.surgeon": 1,
      },
    },
    {
      id: "mariner.sailing.surgeon.attempt",
      warmup: 30,
      slots: [
        {
          id: "help1",
          label: "Help",
          description: "Which implements are available to help put together medical supplies",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help2",
          label: "Help",
          description: "Which implements are available to help put together medical supplies",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help3",
          label: "Help",
          description: "Which implements are available to help put together medical supplies",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
      ],
      linked: [
        {
          id: "mariner.sailing.surgeon.success",
          chance: "(5 * [knock])",
        },
        {
          id: "mariner.sailing.surgeon.success",
          chance: "(5 * [grail])",
        },
        {
          id: "mariner.sailing.surgeon.failure",
        },
      ],
    },
    {
      id: "mariner.sailing.surgeon.success",
      label: "<medical supplies created.>",
      startdescription: "<Success>",
      effects: { "mariner.medicalsupplies": 1 },
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.surgeon.failure",
      label: "<Attempt to Repair At Sea: Failure>",
      startdescription: "<Failure>",
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.surgeon";
}
