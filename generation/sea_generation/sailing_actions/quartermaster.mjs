import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateQuartermasterAction() {
  mod.initializeRecipeFile("recipes.sailing.quartermaster", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.quartermaster",
    {
      id: "mariner.sailing.quartermaster",
      label: "<Ensure good Rationing with a Quartermaster>",
      startdescription: "<The Crew cannot sustain on Air, and hunger is so heavy. But a Skilled Quartermaster always has a little store prepared, and knows what to make out of a bad situation.>",
      requirements: {
        "mariner.crew.specialist.quartermaster": 1,
      },
    },
    {
      id: "mariner.sailing.quartermaster.attempt",
      warmup: 30,
      slots: [
        {
          id: "help1",
          label: "Help",
          description: "Which implements are available to help put together medical supplies",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help2",
          label: "Help",
          description: "Which implements are available to help put together medical supplies",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help3",
          label: "Help",
          description: "Which implements are available to help put together medical supplies",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
      ],
      linked: [
        {
          id: "mariner.sailing.quartermaster.success",
          chance: "(5 * [forge])",
        },
        {
          id: "mariner.sailing.quartermaster.success",
          chance: "(5 * [heart])",
        },
        {
          id: "mariner.sailing.quartermaster.failure",
        },
      ],
    },
    {
      id: "mariner.sailing.quartermaster.success",
      label: "<medical supplies created.>",
      startdescription: "<Success>",
      effects: { "mariner.rations": 1 },
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.quartermaster.failure",
      label: "<Attempt to Repair At Sea: Failure>",
      startdescription: "<Failure>",
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.quartermaster";
}
