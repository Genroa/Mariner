import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateMerchantAction() {
  mod.initializeRecipeFile("recipes.sailing.merchant", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.merchant",
    {
      id: "mariner.sailing.merchant",
      label: "<Have a merchant plan a better sale strategy>",
      startdescription: "<This upgrades one of your cargo to an improved contract>",
      requirements: {
        "mariner.crew.specialist.merchant": 1,
      },
      linked: [{ id: "mariner.sailing.merchant.attempt" }]
    },
    {
      id: "mariner.sailing.merchant.attempt",
      warmup: 30,
      slots: [
        {
          id: "help1",
          label: "Help",
          description: "Which implements are available to help put together increase our sale values",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help2",
          label: "Help",
          description: "Which implements are available to help put increase our sale values",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help3",
          label: "Help",
          description: "Which implements are available to help put increase our sale values",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
      ],
      linked: [
        {
          id: "mariner.sailing.merchant.success",
          chance: "(5 * [edge])",
        },
        {
          id: "mariner.sailing.merchant.success",
          chance: "(5 * [grail])",
        },
        {
          id: "mariner.sailing.merchant.failure",
        },
      ],
    },
    {
      id: "mariner.sailing.merchant.success",
      label: "<medical supplies created.>",
      startdescription: "<Success>",
      effects: { "mariner.medicalsupplies": 1 },
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.merchant.failure",
      label: "<Attempt to Repair At Sea: Failure>",
      startdescription: "<Failure>",
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.merchant";
}
