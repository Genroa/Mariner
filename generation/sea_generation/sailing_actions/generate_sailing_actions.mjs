import { generateRepairAtSeaAction } from "./repair_at_sea.mjs";
import { generateThrowCargoAction } from "./throw_cargo.mjs";
import { generateCarpenterAction } from "./carpenter.mjs";
import { generateCrewmanAction } from "./crewman.mjs";
import { generateMateAction } from "./mate.mjs";
import { generateCookAction } from "./cook.mjs";
import { generateOfficerAction } from "./officer.mjs";
import { generateMerchantAction } from "./merchant.mjs";
import { generateConmanAction } from "./conman.mjs";
import { generateQuartermasterAction } from "./quartermaster.mjs";
import { generateSurgeonAction } from "./surgeon.mjs";
import { generateRefreshSongAction } from "./refresh_song.mjs";

export function generateSailingActions() {
  //mariner.sailing.throwcargo
  return [
    generateRepairAtSeaAction(),
    generateThrowCargoAction(),
    generateCrewmanAction(),
    generateMateAction(),
    generateCarpenterAction(),
    generateCookAction(),
    generateOfficerAction(),
    generateConmanAction(),
    generateMerchantAction(),
    generateQuartermasterAction(),
    generateSurgeonAction(),
    generateRefreshSongAction(),
  ];
}
