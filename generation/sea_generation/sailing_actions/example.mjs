import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateXXXXAction() {
  mod.initializeRecipeFile("recipes.sailing.xxxx", ["sailing", "actionsatsea"]);
  mod.setRecipes("recipes.sailing.xxxx", {
    id: "mariner.sailing.xxxx",
    label: "<>",
    startdescription: "<>",
    requirements: {
      "<>": 1,
    },
    effects: { "<>": -1 },
    linked: POST_SAILING_ACTION_LINK,
  });
  return "mariner.sailing.xxxx";
}
