import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateRepairAtSeaAction() {
  mod.initializeRecipeFile("recipes.sailing.repairatsea", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.repairatsea",
    {
      id: "mariner.sailing.repairatsea",
      label: "<Attempt to Repair At Sea>",
      startdescription: "<Attempting : forge amount>",
      requirements: {
        "mariner.ship.damage": 1,
        "mariner.repairkit": 1,
      },
      linked: [
        {
          id: "mariner.sailing.repairatsea.attempt",
        },
      ],
    },
    {
      id: "mariner.sailing.repairatsea.attempt",
      warmup: 30,
      slots: [
        {
          id: "help1",
          label: "Help",
          description: "Which implements are available to help attempting to repair the ship.",
          required: {
            "mariner.crew": 1,
            tool: 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help2",
          label: "Help",
          description: "Which implements are available to help attempting to repair the ship.",
          required: {
            "mariner.crew": 1,
            tool: 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help3",
          label: "Help",
          description: "Which implements are available to help attempting to repair the ship.",
          required: {
            "mariner.crew": 1,
            tool: 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
      ],
      effects: {
        "mariner.repairkit": -1,
      },
      mutations: {
        "mariner.crew": {
          mutate: "mariner.crew.exhausted",
          level: 1,
        },
      },
      linked: [
        {
          id: "mariner.sailing.repairatsea.success",
          chance: "80 - (20 * [mariner.ship.patched]) + (5 * [forge])",
        },
        {
          id: "mariner.sailing.repairatsea.failure",
        },
      ],
    },
    {
      id: "mariner.sailing.repairatsea.success",
      label: "<Attempt to Repair At Sea: Success>",
      startdescription: "<Success>",
      mutations: [
        {
          filter: "mariner.ship",
          mutate: "mariner.ship.damage",
          level: -1,
          additive: true,
        },
        {
          filter: "mariner.ship",
          mutate: "mariner.ship.patched",
          level: 1,
          additive: true,
        },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.repairatsea.failure",
      label: "<Attempt to Repair At Sea: Failure>",
      startdescription: "<Failure>",
      linked: POST_SAILING_ACTION_LINK,
    }
  );

  return "mariner.sailing.repairatsea";
}
