import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateRefreshSongAction() {
  mod.initializeRecipeFile("recipes.sailing.refreshsong", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.refreshsong",
    {
      id: "mariner.sailing.refreshsong",
      label: "<Refresh a Song At Sea>",
      startdescription: "<Requires available thrill!>",
      requirements: {
        "mariner.song": 1,
      },
      linked: [
        "mariner.sailing.refreshsong.nothrill",
        "mariner.sailing.refreshsong.notexhausted",
        "mariner.sailing.refreshsong.success",
      ],
    },
    {
      id: "mariner.sailing.refreshsong.nothrill",
      label: "<No Thrill>",
      startdescription: "<No thrill found>",
      warmup: 20,
      grandReqs: {
        "[~/exterior : mariner.thrill]": -1,
      },
      furthermore: [
        {
          movements: {
            "~/tabletop": ["mariner.song"],
          },
        },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.refreshsong.notexhausted",
      label: "<Not Exhausted>",
      startdescription: "<This song isn't exhausted>",
      warmup: 20,
      grandReqs: {
        "mariner.song.exhausted": -1,
      },
      furthermore: [
        {
          movements: {
            "~/tabletop": ["mariner.song"],
          },
        },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.refreshsong.success",
      label: "<Success>",
      startdescription: "<This song is refreshed>",
      warmup: 20,
      furthermore: [
        { target: "~/exterior", effects: { "mariner.thrill": -1 } },
        { aspects: { [SIGNALS.REFRESH_SONG]: 1 } },
        {
          movements: {
            "~/tabletop": ["mariner.song"],
          },
        },
      ],
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.refreshsong";
}
