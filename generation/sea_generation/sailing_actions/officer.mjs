import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateOfficerAction() {
  mod.initializeRecipeFile("recipes.sailing.officer", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.officer",
    {
      id: "mariner.sailing.officer",
      label: "<Discipline my Crew back into Action>",
      startdescription:
        "<An Officer can stir the tired, sick and spent back into action, but they will not appreciate it in the long run.>",
      requirements: {
        "mariner.crew.specialist.officer": 1,
      },
      linked: [
        {
          id: "mariner.sailing.officer.success",
        },
      ],
    },
    {
      id: "mariner.sailing.officer.success",
      label: "<Back to work>",
      startdescription: "<Success>",
      effects: {
        "mariner.rations": -1,
        "mariner.trappings.grail": -1,
      },
      furthermore: [
        {
          target: "~/tabletop",
          mutations: {
            "[mariner.crew] && [mariner.crew.exhausted]>1": {
              mutate: "mariner.crew.exhausted",
              level: 0,
              limit: 10,
            },
          },
        },
        {
          target: "~/tabletop",
          mutations: {
            "[mariner.crew] && [mariner.crew.exhausted]>1": {
              mutate: "mariner.crew.longing",
              limit: 20,
              level: 1,
              additive: true,
            },
          },
        },
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.officer";
}
