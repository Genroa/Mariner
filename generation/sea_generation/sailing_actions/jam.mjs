import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateJamAction() {
  mod.initializeRecipeFile("recipes.sailing.jam", ["sailing", "actionsatsea"]);
  mod.setRecipes("recipes.sailing.jam", {
    id: "mariner.sailing.jam",
    label: "<Jamming with my Crew>",
    startdescription: "<Music, for me, can often grow into something so large and important, as you prepare for the stage and it's rigors. But the simple joy that can come from having an instrument on board, and playing with the crew who is not .>",
    requirements: {
      "mariner.instrument": 1,
    },
    effects: { "mariner.thrill": 1 },
    linked: POST_SAILING_ACTION_LINK,
  });
  return "mariner.sailing.jam";
}
