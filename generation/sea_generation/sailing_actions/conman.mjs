import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateConmanAction() {
  mod.initializeRecipeFile("recipes.sailing.conman", ["sailing", "actionsatsea"]);
  mod.setRecipes("recipes.sailing.conman", {
    id: "mariner.sailing.conman",
    label: "<have my con-man swindle my own crew. >",
    startdescription: "<This can be an emergency source of cash, but it will drive my other crew members away.>",
    requirements: {
      "<mariner.crew.specialist.conman>": 1,
    },
    effects: { funds: 2 },
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.longing",
            limit: 2,
            level: 2,
            additive: true,
          },
        ],
      },
      {
        mutations: {
          "mariner.crew": {
            mutate: "mariner.crew.exhausted",
            level: 1,
          },
        },
      },
      {
        mutations: {
          "mariner.crew": {
            mutate: "mariner.crew.exhausted",
            level: 1,
          },
        },
      },
      { movements: { "~/tabletop": ["mariner.crew"] } },
    ],
    linked: POST_SAILING_ACTION_LINK,
  });
  return "mariner.sailing.conman";
}
