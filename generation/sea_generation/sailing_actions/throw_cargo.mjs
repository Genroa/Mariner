import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateThrowCargoAction() {
  mod.initializeRecipeFile("recipes.sailing.throwcargo", ["sailing", "actionsatsea"]);
  // TODO: we should probably put a second recipe here, with a small warmup, to describe the action having taken place?
  mod.setRecipes("recipes.sailing.throwcargo", {
    id: "mariner.sailing.throwcargo",
    label: "<Throw Cargo Overboard>",
    startdescription: "<Throw Cargo Overboard>",
    requirements: {
      "mariner.cargo": 1,
    },
    effects: { "mariner.cargo": -1 },
    linked: POST_SAILING_ACTION_LINK,
  });
  return "mariner.sailing.throwcargo";
}
