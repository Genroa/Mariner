import { SIGNALS } from "../../generate_signal_aspects.mjs";
import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateMateAction() {
  mod.initializeRecipeFile("recipes.sailing.mate", ["sailing", "actionsatsea"]);
  mod.setRecipes("recipes.sailing.mate", {
    id: "mariner.sailing.mate",
    label: "<Employ a Crewman for Speed>",
    startdescription: "<A good mate runs a tight ship. A mate exerting themselves can make a ship fly across the water.>",
    requirements: {
      "mariner.crew.specialist.mate": 1,
    },
    furthermore: [
      { effects: { "mariner.speedbonus.card": 2 }, },
      {
        mutations: {
          "mariner.crew": {
            mutate: "mariner.crew.exhausted",
            level: 1,
          },
        },
      },
      { movements: { "~/tabletop": ["mariner.crew"] } },
      {
        target: "~/extant",
        aspects: { [SIGNALS.COMPUTE_SHIP_STATS]: 1 },
      },
    ],
    linked: POST_SAILING_ACTION_LINK,
  });
  return "mariner.sailing.mate";
}
