import { POST_SAILING_ACTION_LINK } from "./helpers.mjs";

export function generateCarpenterAction() {
  mod.initializeRecipeFile("recipes.sailing.carpenter", ["sailing", "actionsatsea"]);
  mod.setRecipes(
    "recipes.sailing.carpenter",
    {
      id: "mariner.sailing.carpenter",
      label: "<Attempt to Repair At Sea>",
      startdescription: "<Attempting : forge amount>",
      requirements: {
        "mariner.ship.damage": 1,
        "mariner.crew.carpenter": 1,
      },
      linked: [
        {
          id: "mariner.sailing.carpenter.attempt",
        },
      ],
    },
    {
      id: "mariner.sailing.carpenter.attempt",
      warmup: 30,
      slots: [
        {
          id: "help1",
          label: "Help",
          description: "Which implements are available to help attempting to repair the ship.",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help2",
          label: "Help",
          description: "Which implements are available to help attempting to repair the ship.",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
        {
          id: "help3",
          label: "Help",
          description: "Which implements are available to help attempting to repair the ship.",
          required: {
            "mariner.crew": 1,
            tool: 1,
            "mariner.trapping": 1,
          },
          forbidden: {
            "mariner.crew.exhausted": 1,
          },
        },
      ],
      effects: {
        "mariner.repairkit": -1,
      },
      linked: [
        {
          id: "mariner.sailing.carpenter.success",
          chance: "80 - (20 * [mariner.ship.patched]) + (5 * [forge])",
        },
        {
          id: "mariner.sailing.carpenter.failure",
        },
      ],
    },
    {
      id: "mariner.sailing.carpenter.success",
      label: "<Attempt to Repair At Sea: Success>",
      startdescription: "<Success>",
      mutations: [
        {
          filter: "mariner.ship",
          mutate: "mariner.ship.damage",
          level: -1,
          additive: true,
        },
        {
          filter: "mariner.ship",
          mutate: "mariner.ship.patched",
          level: 1,
          additive: true,
        },
      ],
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    },
    {
      id: "mariner.sailing.carpenter.failure",
      label: "<Attempt to Repair At Sea: Failure>",
      startdescription: "<Failure>",
      furthermore: [
        {
          mutations: {
            "mariner.crew": {
              mutate: "mariner.crew.exhausted",
              level: 1,
            },
          },
        },
        { movements: { "~/tabletop": ["mariner.crew"] } },
      ],
      linked: POST_SAILING_ACTION_LINK,
    }
  );
  return "mariner.sailing.carpenter";
}
