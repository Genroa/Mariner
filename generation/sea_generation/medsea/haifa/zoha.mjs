import { buildRefinementBlock } from "../../../generation_helpers.mjs";
import { repeatableConversation, convertSourceConversation } from "../../../port_generation/helpers.mjs";

export const ZOHA = {
  type: "npc",
  elementId: "zoha",
  dynamicLabel: buildRefinementBlock({ "mariner.knowname": "Zoha" }, "The Dreamer"),
  description:
    "A young woman with an evidently a hard life allready behind her, but a fire still raging inside. She has walked down many roads, and now found herself back to Haifa, but not back to home.",
  discoveryLabel: "<An Acquaintance In the Shade>",
  discoveryDescription:
    "<I often stop in one of the numerous teahouses, close to the Bahá'í gardens, to take in the lush scenery. Here, under a neighboring shade, I notice a familiar face, ever present, ever reading the same books. Religious texts, of the most peculiar kind.>",
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: ["arabic", "italian", "hebrew", "cyrillic"],
    usedNames: [],
    usedInitials: [],
    gender: "female",
  },
  deliveryRiddles: [
    "<Explorer of the Astral>",
    "<Apprentice of The many masters and the more>",
    "Prodigal Daughter returned to the Crescent",
    "The Only streetsmart Adept",
    "Penniless, Perrilous, Princess of Dream and Dust",
    "The Waning Crescent Moon in Haifa",
  ],
  storyTrading: {
    story: {
      label: "The Alchemy of Babylon",
      icon: "onthewhite",
      description: "<A Tower was wrought from oppressive stone by those before us. Those that build it, cared more for the shape of the stones then the screams that rang at their reshaping. Amongst those on their knees, one rose through black sand, red dust and gold blood. He reaches the unreachable, and retreats as glory retreats. At that, stone shatters like eggshell, and the jagged glass stairs remain.>",
      aspects: {
        winter: 2,
      },
    },
    label: "<Exchange Stories with A Dreamer>",
    startdescription: '"<Stories? of course. Of that the world is a treasuretrove, and it\'s one of the few riches that is increased by sharing.>"',
    description: '<when the currency is stories, suddenly the vagrant can be king. but still, my favorite are the stories that point to something deeper. Come, listen here. You will have heard this one, but I doubht you have heard it this way. All stories are one story, as all things in the universe are a representation of the universe, and our histories are the prismatic shadows of the one true House. Why must we talk in riddles? Worry not, let me babble on. >"',
  },
  conversations: [
    repeatableConversation({
      id: "twins.dedication",
      element: "mariner.ascension.twins.dedication",
      label: "<The Self, the Fractured>",
      startdescription: '"You seek to mend your Soul?"',
      description:
        '"<The self is composed of different parts in different traditions. Three elements, 5 instars, 7 marks... but most scholars seem to agree on nine. I have been told my Fet is strong, but my Character is stronger still... Perhaps we can help eachother. You help me with my pursuits, and I help you with yours.>"',
    }),
    repeatableConversation({
      id: "vagabond.dedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "<The Secret Pilgrimage>",
      startdescription: '<"you are another one of them, huh?>',
      description:
        "She scrumples up her face.\"<I have traveled far and wide and 'Vageries' are everywhere, on their endless pilgrmages, trying to sniff out the scent of the Vagabonds Footsteps in the dust. That path was never for me. My is aimed up, and at the one place the Vagabond is barred from.>\"",
    }),
    //repeatableConversation({
    //  id: "zoha.gift.books",
    //  element: "mariner.books.imago",
    //  label: "<Gifts for the Drifter>",
    //  startdescription: '<"You wish to give these to me?">',
    //  description:
    //    "\"why?\" She eyes me suspiciously. \" I will take them of your hands, but don\'t think me too attached. I travel light, and the sum these would fetch could keep me fed for months.\" she looks down at the writing, traling a line with her finger. \"But I will keep them in my pack for now. Some light reading before bed.\"",
    //}),
    repeatableConversation({
      id: "pinned.collection.reward",
      element: "mariner.diaphenous.sheets",
      label: "<Show the Diaphenous Sheet>",
      startdescription: '<"What do you have there?">',
      description:
        'She reaches for it, and her touch does not shred it further. She holds it up to her face, her cheeks, her lips. The membrame shivers from her breath."look how it breaks the light. I think these are the colours I cannot see in the bleak darkness of the Woods."',
    }),
    repeatableConversation({
      id: "music.knock",
      element: "mariner.song.knock",
      label: "<Discuss the Power of my Songs>",
      startdescription: '<"I learned about song magic here and there, even beyond the usual chanting...">',
      description:
        '"<Knock is both the invitation and the exploration, the Lesion and the lesson. Sing with the Intent of Knock and call Spirits or Entities towards you... Paired with the direction of Forge it calls forth a being from the scorching desert winds.>"',
    }),
    repeatableConversation({
      id: "music.lantern",
      element: "mariner.song.lantern",
      label: "<Discuss the Power of my Songs>",
      startdescription: '<"I learned about song magic here and there, even beyond the usual chanting...">',
      description:
        "\"I don';t know as much as I wish about the Intricacies of Lantern, be it in the orphic, suffic or heterodox traditions. But I know lanterns intent is to reveal and clarify, and it's direction is that of the Sun's Authority, and it's mortal counterparts.\"",
    }),
    repeatableConversation({
      id: "music.edge",
      element: "mariner.song.edge",
      label: "<Discuss the Power of my Songs>",
      startdescription: '<"I learned about song magic here and there, even beyond the usual chanting...">',
      description:
        '"<While I have lived at the Edges for most of my life, I have mostly kept my nose clean, and prefer to avoid direct confrontations. Maybe ask someone who is more at home in dark dealings, or one who strives to oppose them?>"',
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "<Discuss my Halfheart>",
      startdescription: '<"You say you are a Half Heart?">',
      description:
        '"<I figured you might be, you got the look. I have met others, once or twice, heard a couple others mentioned. You don\'t seem to last very long... Though maybe they just slip out of sight. I am not one to keep in contact.>"',
    }),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "<Discuss my Lackheart>",
      startdescription: '<"You cannot dream, can you?>',
      description:
        '"<That what I heard of your kind. Barred fom the Mansus, or perhaps just incapable as long as your soul remains incom0plete. I am so sorry. You are cut off from the House, The woods and the Bounds... I don\'t even have the words to describe what you are missing. Radiance and Wonder..."',
    }),
    repeatableConversation({
      id: "<vagabonddedication>",
      element: "mariner.ascension.vagabond.dedication",
      label: "<Discuss my path with Zoha>",
      startdescription: '"<Not you as well?>" She scrunches up her face.',
      description:
        '"Vageries are absolutely everywhere. on their endless Pilgrimages. Trying to sniff out the scen of her Footsteps in the dust,. No thank you, not for me. My goal is aimed up. But perhaps we can be of aid to eachother, helping eachother into strange places."',
      effects: {},
    }),
    repeatableConversation({
      id: "<twinsdedication>",
      element: "mariner.ascension.twins.dedication",
      label: "<Discuss My Path with Zoha>",
      startdescription: '"<You wish to mend your Soul?>"',
      description:
        '"The Self is made of different numbers of parts in diffferent traditions. Three elements, five instars, seven marks. But most reliable scholars seem to agree on nine... I have been told my Fet is strong, but my Character is stronger."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.student.1",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Student Exsanguinate>",
      startdescription: '"<>"',
      description:
        '"There are many reasons someone might end up gutted in a hotel room. It’s only shocking to the shielded and the naive. It would be a gift if you can give closure to Hakim, but that is a rare gift to give in the world we live in."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.student.2",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Student Exsanguinate>",
      startdescription: '"<>"',
      description:
        '"That is gruesome… But in a strangely beautiful way. It is rare to find intentions so pure in a methodology so vile…  but I do doubt this is a cover for a darker truth, it smells to me of sincerity. I do wonder what allows these Antivivisectionists to sustain those straining injuries. What are they? Who sponsors an immortality such as this?"',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stench.1",
      element: "mariner.song.knock",
      label: "<Discuss the Stench of Marseille>",
      startdescription: '"<>"',
      description:
        '"All cities stink, if your point of view is the gutter. Everywhere the vulnerable vanish. The old, the young, the innocent. “Hold hope for kindness in your heart, but keep a knife in your hand.” perhaps the wisest lesson I learned in the Mamurren Society. Though most of those would not recognise a gutter when they trip over it."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.1",
      element: "mariner.song.knock",
      label: "<Discuss the bewitchment of a City>",
      startdescription: '"<>"',
      description:
        '"Sereno Sombruiell? I have met many Sereno’s in my days, and Serena’s. That nom de guerre tells us nothing of his intentions. But what he asked you to do… I can guess the intent. Inflame. Sow a little chaos. Invite disorder into town."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.towers.1",
      element: "mariner.song.knock",
      label: "<Discuss the Crumbling towers>",
      startdescription: '"<>"',
      description:
        '"It’s a vexing problem, but that is not unfamiliar to me. I have sniffed out hidden truths before, it\'s called occult for a reason. We will find our crack into this mystery as well, as soon as we realize where here is."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.truth.1",
      element: "mariner.song.knock",
      label: "<Discuss the Recovery of Discarded Truth>",
      startdescription: '"<>"',
      description:
        '"So the old maid is still kicking around? I suspect that even When convent is leveled, and nothing remains of  her eternal city, she would still be sitting with a cup of coffee to greet the sun. Do try your best for her, perhaps has done her best for me once."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.sisterhood.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reunion of the Sisterhood>",
      startdescription: '"<>"',
      description:
        '"What a thought. I wonder if I have met any of them, or all? How far might they have spread? I bet they would have traveled here to here at some point. Few places are so drenched in Enlightenment as the holy land."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.caper.1",
      element: "mariner.song.knock",
      label: "<Discuss the Caper of the Copper Manse>",
      startdescription: '"<>"',
      description:
        '"A noble playing the thief and the pauper?" She raises her hands to heaven. "I will never understand the hobbies of the idle classes. He is a practitioner too? Then perhaps I can begrudge him some sympathy.. While I cannot understand why one would take the hard road when an easy one is available, I can admit there are some experiences that money cannot buy"',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.1",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<>"',
      description:
        '"For a society dedicated to obscurity, their mark upon these lands is remarkable. If you know to look for the signs, you can find them with your eyes closed."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.struggimento.1",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplery Path>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.muse.1",
      element: "mariner.song.knock",
      label: "<discuss the Chase of the Missing Muse>",
      startdescription: '"<>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "<Discuss the Unknown Glyphs with Zoha>",
      startdescription: '"Something you encountered you do not understand? Show me, and maybe I can help!"',
      description:
        'She smiles and shakes her head. "I have seen some of these before, and been told not to look at them. But those blessed teachers did not feel the need to expound much more. Might be dangerous, so mind your self."',
    }),
  ],
};

mod.readRecipesFile("recipes.imago", ["mysteries", "imago"]);
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.convert.aramaic.first",
  maxexecutions: 1,
  label: "Shade and Sharing",
  startdescription:
    "You need something translated from aramaic? Not my mother tongue, but I have encountered it enough now, both mad ravings and profound realisations.",
  description:
    "It was the second language I thought myself, after Hebrew and before Mandeic. But it was learning English that took me the longest. Hand me the work and buy me a meal, and I will supply you with a translation, and then you can compliment me on my language skills.",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.zoha": 1,
  },
  linked: [{ id: "mariner.zoha.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.convert.aramaic.second",
  maxexecutions: 1,
  label: "Shade and Sharing",
  startdescription: "Another aramaic Translation?",
  description:
    "Keeping me sharp huh? I haven't read much aramaic since I was in the Zagros Mountains with the Last Hunters. Also the last time I have worn chest binders. Oh well... Buy me a cool drink to toast the good memories and wash away the bad ones while I work on your translation.",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.zoha": 1,
  },
  linked: [{ id: "mariner.zoha.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.convert.aramaic.third",
  maxexecutions: 1,
  label: "Shade and Sharing",
  startdescription: "You completed the set? Great!",
  description:
    "I am glad. I have gotten invested... Hunting in the Woods, climbing in the House. There is beauty in the struggle and in the sacrifice.",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.zoha": 1,
  },
  linked: [{ id: "mariner.zoha.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.convert.infinite",
  label: "Shade and Sharing",
  startdescription: "Something else need translating?",
  description: "Well, you know the drill! You supply sweets, I supply the translations...",
  warmup: 30,
  actionid: "talk",
  craftable: true,
  requirements: {
    "mariner.aramaic.untranslated": 1,
    "mariner.locations.medsea.haifa.zoha": 1,
  },
  linked: [{ id: "mariner.zoha.output.router" }],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.output.router",
  warmup: 5,
  slots: [
    {
      id: "funds",
      label: "Funds",
      description: "Enough to buy a libation and a treat.",
      greedy: true,
      required: {
        funds: 1,
      },
    },
  ],
  //requirements: {},
  linked: [
    {
      id: "mariner.zoha.output.transformation",
    },
    {
      id: "mariner.zoha.output.loss",
    },
    {
      id: "mariner.zoha.output.pursuit",
    },
    { id: "mariner.zoha.output.witness" },
  ],
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.output.loss",
  maxexecutions: 1,
  label: "Matthias and Imago: Loss Translated",
  description: `\"Here it is, fully translated. Gruesome stuff. I can commend them for willing to sacrifice so much, but the details make my skin crawl\"`,
  warmup: 25,
  requirements: { "mariner.matthiasamethystlossaramaic": 1 },
  effects: {
    "mariner.matthiasamethystloss": 1,
    "mariner.matthiasamethystlossaramaic": -1,
  },
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.output.transformation",
  maxexecutions: 1,
  label: "On Matthias and Imago: Transformation Translated",
  description: `\"into the fire they fly...\" Zoha's eyes are far, shining. \"Risen beyond our sight... But not beyond ours, perhaps\" She raises her eyes up at the sun filtering through the palm leaves and sighs.`,
  warmup: 25,
  requirements: { "mariner.matthiasamethysttransformationaramaic": 1 },
  effects: {
    "mariner.matthiasamethysttransformation": 1,
    "mariner.matthiasamethysttransformationaramaic": -1,
    funds: -1,
  },
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.output.pursuit",
  maxexecutions: 1,
  label: "On Matthias and Imago: Pursuit Translated",
  description: `\"The Woods... all too familiar. I cannot imagine going there to hunt, though. It seems perverse to choose that darkness, instead of guiding Light.\"`,
  warmup: 25,
  requirements: { "mariner.matthiasamethystpursuitaramaic": 1 },
  effects: {
    "mariner.matthiasamethystpursuit": 1,
    "mariner.matthiasamethystpursuitaramaic": -1,
    funds: -1,
  },
});
mod.setRecipe("recipes.imago", {
  id: "mariner.zoha.output.witness",
  maxexecutions: 1,
  label: "On Matthias and Imago: Witness Translated",
  description: `\"Well that was quite more of a challenge. Not only a much older script, but much worse caligraphy, and if I can be honest, grammar. But it was illuminating... Matthias was meticulous, obsessive. They clashed in the Woods, but as she had a natural advantage there, he sought to lay a trap for her. He studied his prey closely, discovered things of her history from times before the languages he speaks was even common place. This way, he discovered a pattern in her behavior, that on nights when the moon was in her first quarter, she would ride the winds from the verdent crescent past the isle of cypresses. He claims you could catch her, on a ship laden with enough Knock. After that... well let's say they exited that.\"`,
  warmup: 25,
  requirements: { "mariner.matthiasamethystwitnessaramaic": 1 },
  effects: {
    "mariner.matthiasamethystwitness": 1,
    "mariner.matthiasamethystwitnessaramaic": -1,
    funds: -1,
  },
});
