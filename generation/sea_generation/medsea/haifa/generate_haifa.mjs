import { generatePort } from "../../../port_generation/port_generation.mjs";
import { market, smith } from "../../../port_generation/helpers.mjs";
import { theater, theaterSlots } from "../../../port_generation/helpers.mjs";
import { ARCHEOLOGIST } from "./archeologist.mjs";
import { ZOHA } from "./zoha.mjs";
import { Galantha } from "../galantha.mjs";
import { buildCodexEntryBlock, generateCodexEntry } from "../../../codex_generation/codex_generation.mjs";
import { SMITH_TYPES } from "../../../tools_generation/generate_tools.mjs";
import { generateTowersOfLightQuest } from "./towers_of_light.mjs";

export function generateHaifa() {
  generatePort({
    portRegions: ["medsea"],
    portId: "haifa",
    label: "Haïfa",
    description:
      "<Sloping down of tripple sacred mount Carmel is a city mosaiced in monotheistic faiths. A polyphony of worship wafts into the sky.>",
    recipeDescriptions: {
      docking: "<From far away, Mount Carmel allready leads us. The harbor is nestled in a nook of a bay under the slopes of the mountain, and from there, in three directions, the city rises.>",
      exploreHH:
        "<From the harbor of Haifa, every way to travel is a climb. But after a day of exploring, you can around to the sunset sea, and see all encounters the city gave you laid down below you.>",
      exploreLH:
        "<From the harbor of Haifa, every way to travel is a climb. But after a day of exploring, you can around to the sunset sea, and see all encounters the city gave you laid down below you.>",
    },
    storyTrading: {
      story: {
        label: "<Death's Angel and the Gardener>",
        description:
          "<Long ago, when Salomon was king, his court was bright and just and glorious. In that court was a garden, and in that garden worked a gardener. One night, the gardener dreamed of the statuary in the garden, crisp with frost, and a thrice-winged figure standing in the darkness before dawn. He begged the king for leave, for he had forseen Azazel, Herald of Death visiting the garden. Salomon let him travel to Isthphahan for peace of mind. Days pass, in the chilly hour before dawn, Salomon walked through his garden in contemplation, as one of the statues spoke to him, and Azazel spoke to him, and thanked him for sending the gardener to the Istphahan, the city in which Azazel had been destined to meet him,>",
        aspects: {
          moth: 1,
        },
        icon: "thosewhodonotsleep",
      },
      label: "Trade Stories at the Streets of Haifa",
      startdescription:
        "<Stories hang in the air refracted rainbows from the white, baking walls, or are written in dust on the ground.. If I engage with the locals, I can pluck Haifa's story and tell it as my own.>",
      description:
        "<Haifa is a mosaic, a pattern whose beauty is filled in by its contrasts. I could sample stories from at least three traditions, but the one I find could have been a fable, or a simile, or a lesson, or a highly distorted truth.>",
    },
    tunes: ["grailedge", "heartgrail", "mothedge", "mothheart"],
    barName: "Mirsa",
    wharf: {
      cargoOptions: [
        { portRegion: "medsea", distance: "local", label: "Med Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
      ],
    },
    portElements: [
      ARCHEOLOGIST,
      ZOHA,
      Galantha,
      market({
        id: "ancientmarket",
        label: "<5th Pier, 7th warehouse>",
        description: "<Many cities have been called the gateway to the Orient. And from many a port a flood of wealth, treasure and culture is bled from the peninsula. The bigger the port, the more traffic, the smaller the port, the less oversight. Perhaps haifa sits in that perfect middle ground ripe for the thrilling commerce of export and exploit.>",
        types: [],
        storyTrading: {
          story: {
            label: "<The Curse from the Tombs>",
            icon: "locksmithsdream2",
            description: "<>",
            aspects: {
              forge: 1,
            },
          },
          label: "<Gathering Stories at the Dock>",
          startdescription: "<>",
          description: "<Stories of great bounties spread like wildfire, but stories of great misfortune spread like an ashcloud, descending in unexpected places. Among the smugglers, dynamite archeologists and hawkers, ghost stories are shared of the teams in Eqypts and that have followed that team home from their ancient deserts. thankfully the Egyptian curses are worse then the Asyrian ones, eh> but still, perhaps you would like o ward of the evil eye, just to be sure.>",
        },
      }),
      smith({
        id: "toolsmith_weaponsmith",
        smithType: SMITH_TYPES.TOOLSMITH,
        label: "<Oğuz, The Mendicant Armorer>",
        description:
          "<Empires rise and empires fall, but Oguz has not found a successor state worthy of his fealty or his preternatural smithing skills. until that ja[[ems his shop is mobile, disappearing between sand and rock like the Bedouins and long before the empire of Empire of the Nabatean.>",
        notDiscoverable: true,
      }),
    ],
  });

  generateCodexEntry(
    "codex.houseoflethe",
    "[Med. Sea] House of Lethe",
    buildCodexEntryBlock(
      {
        "codex.miriam.questgiven": "secret1",
      },
      "1"
    ) +
    "\n\n\n" +
    buildCodexEntryBlock(
      {
        "codex.miriam.pursuitlosstransformation": "secret",
      },
      "2"
    ) +
    "\n\n\n" +
    buildCodexEntryBlock(
      {
        "codex.miriam.mattiaswitness": "secret",
      },
      "3"
    ) +
    "\n\n\n" +
    buildCodexEntryBlock(
      {
        "codex.miriam.pursuitlosstransformation": "secret",
      },
      "4"
    )
  );
}

generateTowersOfLightQuest();
