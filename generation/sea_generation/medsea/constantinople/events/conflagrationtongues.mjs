import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateConflagrationOfTonguesEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.conflagration"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Conflagration of Tongues`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.conflagration", 25),
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.conflagration") },
    startdescription:
      'Tonight the cafe was not set up for a performance, yet at some point a call and response song is started. Familiar tunes are played, and in rounds, people sing the songs written for that tune in their language, layered in canon. More and more languages and rhythms get layered on eachother, until no single one is still parseable, yet all are understood.',
    effects: {
      "mariner.influences.knock": 4,
      "mariner.influences.heart": 4,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
