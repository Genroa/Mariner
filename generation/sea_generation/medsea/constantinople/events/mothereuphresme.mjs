import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateMotherEuphresmeEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.mothereuphresme"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Simplicity of Faith`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.mothereuphresme", 25),
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.mothereuphresme") },
    startdescription:
      'When a procession passes following a Icon of saint Tarasios, I see a familiar woman in a familiar habit, but in such an unfamiliar fashon. she is quiet, and her hands are stilll, clasped behind. There is something shocking about seeing what the weight of worship can accomplish even on someone as sturdy and self-possessed as Mother Euphresme. ',
    effects: {
      "mariner.influences.knock": 4, //refeshes wither knock song
      "mariner.influences.winter": 4,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
