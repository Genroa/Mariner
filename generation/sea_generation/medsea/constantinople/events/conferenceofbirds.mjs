import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateConferneceOfBirdsEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.conferenceofbirds"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Conference of Birds`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.conferenceofbirds", 25),
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.conferenceofbirds") },
    startdescription:
      'Only a single singer performances tonight, led by a setar and a flute, as they sing in a language that touches no seas. All that is clear to me is that the the songstress molds her voice to portray seven different voices. As the song carries on, so does it\'s frantic, thrilling intensity. Then, in stunning conclusion, she sings with six voices at once, leaving the last silenced for ever.',
    effects: {
      "mariner.influences.knock": 4, //knock grail bird
      "mariner.influences.heart": 4, // sister
      "mariner.influences.grail": 4, // witch
      "mariner.influences.winter": 4, // elegiast
      "mariner.experiences.sophorific": 1, //
      "mariner.experiences.vivid": 1,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
