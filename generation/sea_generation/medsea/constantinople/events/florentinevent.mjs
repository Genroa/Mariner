import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateFlorentinEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.florentin"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Old and New`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.florentin", 25),
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.florentin") },
    startdescription:
      'Florentin has entered one of his dour moods.  "I fear the biggest impact I have ever made is in my absence. That one place I do not return..., Perhaps all my footfalls on unlikely tresholds have only to make up for that fact.',
    furthermore: CALL_FUNCTION("addgroundedness.30"),
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
