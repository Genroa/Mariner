import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateGrandMasqueEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.masque"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Grand Masque`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.masque", 25),
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.masque") },
    startdescription:
      'A performance in French, an operetta about a great masked ball, held at the court of the Sun King. As always in these, there are matters of confusion and mistaken identity. However the longer the play goes, the more ominous it becomes, as more and more of the characters are appearantly replaced by Worms, a growing chorus singing to a different rhythm in a different language to the rest of the cast.',
    effects: {
      "mariner.influences.moth": 4,
      "mariner.influences.winter": 4,
      "mariner.experiences.dreadful": 1,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
