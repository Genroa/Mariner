import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateCopperManseEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.coppermanse"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Parade of Fools`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.coppermanse", 25),
      [READ_QUEST_FLAG("bewitch", "completed")]: 1,
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.coppermanse") },
    startdescription:
      'As I walk the streets of Baltalimani, I watch a great line of fancy cars is trapped in very unglamorous traffic. guards and servants try to usher in the right persons in. A recreation of the Grand Masque of the French Nobility, held in the Cerulean Manse, inviting the new elite and what remains of the old. Someone tries to push forward, grabbing something from their coat. but they get elbowed in throngs of people, and what they had grabbed is dropped. Silver bullet casings scatter like bugs. while his revolutionary screams get lost in the melee of the carnavale.',
    effects: {
      "mariner.trappings.edge.1": 1
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
