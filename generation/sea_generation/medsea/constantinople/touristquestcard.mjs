import { generateQuest } from "../../../quest_generation.mjs";

export function generateHeistQuestCard() {
  generateQuest({
    id: "tinyheist",
    // Quest card informations
    label: "The Caper of the Copper Manse",
    description:
      "The Count Florentin Hunyadi has enrolled me in one of his infamous schemes.",
    // Quest stages
    updates: [
      {
        id: "salutations",
        label: "Salutations and Explanations",
        description:
          '""I have been planning some mischief…", But there is a palace, supposedly one of the grandest still standing from the days that this was a city of palaces.\",  \"This I cannot abide. I have been hatching a little scheme. If you could lend me your aid, I would be most grateful. I have just a few supplies that I think you can provide. Please return to me with what I need.\"',
        slots: [
          {
            id: "tool",
            label: "A Tool",
            description:
              "The Implements of Entering, easily on a portly mans billowing costumes. [this needs a Knock Tool]",
            actionId: "talk",
            required: {
              "mariner.tool.knock": 1,
            },
          },
          {
            id: "trapping",
            label: "Make Up",
            description:
              "The tools to trade your face. [This needs a Moth Trapping]",
            actionId: "talk",
            required: {
              "mariner.trappings.moth": 1
            },
          },
          {
            id: "song",
            label: "The Matter of Tongues",
            description:
              "the sweet, the rich, the poison. [this needs a Grail Song]",
            actionId: "talk",
            required: {
              "mariner.song.grail": 1,
            },
          },
          {
            id: "influence",
            label: "Influence",
            description:
              "An air of Affability, an alignment with the rhythms of the worlds. [this needs a Heart Influence]",
            actionId: "talk",
            required: {
              "mariner.influences.heart": 1
            },
          },
        ],
      },
      {
        id: "preparations",
        icon: 2,
        label: "Preparations and Reparations",
        description:
          "\"All preparations have been made? All tools provided? Wunderbar!\" Florentin has departed on his caper.",
        effects: {
          updateCodexEntries: {
            "codex.tourist": {
              add: ["codex.tourist.neg"],
            },
          },
        },
      },
      {
        id: "invitations",
        icon: 3,
        label: "Invitation an Intimations",
        description:
          "Florentin has returned form his caper, and regaled me with his tales. Then he bid me one more favor... to deliver a parcel pilfered. To Marseille!",
        effects: {
          updateCodexEntries: {
            "codex.tourist": {
              add: ["codex.tourist.s"],
            },
          },
        },
      },
      {
        id: "delivered",
        icon: 4,
        label: "Deportees and Deliveries",
        description:
          "I have delivered a package for the Count in the worst part of Marseille, and was rewarded with gratitued and a cookie tin.",
        effects: {
          updateCodexEntries: {
            "codex.tourist": {
              add: ["codex.tourist.neg"],
            },
          },
        },
      },
      {
        id: "completed",
        effects: {
          updateCodexEntries: {
            "codex.tourist": {
              add: ["codex.tourist.2"],
            },
          },
        },
      },
    ],
  });
}
