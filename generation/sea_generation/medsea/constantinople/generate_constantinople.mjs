import { generatePort } from "../../../port_generation/port_generation.mjs";
import { theater, theaterSlots } from "../../../port_generation/helpers.mjs";
import { NUN } from "./nun.mjs";
import { TOURIST } from "./tourist.mjs";
import { Galantha } from "../galantha.mjs";
import { generateTouristQuest } from "./tourist_quest.mjs";
import { generateHeistQuestCard } from "./touristquestcard.mjs";
import { generateHeistHints } from "./touristquesthints.mjs";
import { generateBuriedSecretsQuest } from "./nunquest.mjs";
import { generateBuriedSecretsQuestCard } from "./nunquestcard.mjs";

export function generateConstantinople() {
  generatePort({
    portRegions: ["medsea"],
    portId: "constantinople",
    label: "Constantinople",
    description:
      "Ancient history and recent strife. But the hundred thousand cats of the city remain, and so does its grandeur, and its noise.",
    recipeDescriptions: {
      docking: "<Each arrival is blinding. not just the Horn of Gold, but also the white walls of the city rising above.>",
      exploreHH:
        "<The first choice in exploring Constantinople... Which continent do you wish to discover? The city is a crossroads and a bridge. And after that first choice, many more... If I apply my charm, I may find extrordinary peoples, places, or opportunities.>",
      exploreLH:
        "<The first choice in exploring Constantinople... Which continent do you wish to discover? The city is a crossroads and a bridge. And after that first choice, many more... If I stay open and receptive, I may find discover the peculiar sounds of the city, or be approached by a Raconteur.>",
    },
    storyTrading: {
      story: {
        label: "<The Walls That Stood>",
        description:
          "<a description of the story of when the walls where broken, and the emperor got the gangs of chariot racing fans to fix the walls, by turning it into a race. >",
        aspects: {
          moth: 1,
        },
        icon: "thosewhodonotsleep",
      },
      label: "Trade Stories at the Streets of Constantinople",
      startdescription:
        "<Stories hang in the air refracted rainbows from the white, baking walls, or are written in dust on the ground.. If I engage with the locals, I can pluck Haifa's story and tell it as my own.>",
      description:
        "<Slowly, but faster every day, this city is outgrowing it's constraints. The Walls of Theodosian. >",
    },
    barName: "Çapa",
    wharf: {
      cargoOptions: [
        { portRegion: "medsea", distance: "local", label: "Med Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
      ],
    },
    portElements: [
      NUN,
      TOURIST,
      Galantha,
      theater({
        id: "constantinopletheater",
        label: "Café Nocturne",
        description:
          "Not many theaters from the time of the Ottoman Empire still remain now that the Republic has founded. But Cafe Nocturne has remained in its nook in Beyoglu. On the right nights, they still dig out the works penned in Armanian, Italian or even more occluded tongues.",
        slots: theaterSlots(["mariner.trapping"]),
        storyTrading: {
          story: {
            label: "<What is Gained in Translation>",
            icon: "burningofunburntgod",
            description:
              "<All of the visible text is in the newly minted alphabet, and at any point, there is at least one conversation about its merits and importances. the Inteligentsia, artists and scholars gather here from throughout the city, the empire and beyond. Ideas are transformed, molding into new shapes and rhythms for new ears. if one language does not suffice, seek another! through daisy chains of translations, the continent of the Hooded princes can be connected to the descendants of the shepherd kings, and those that have settled the Shadowless Desert.>",
            aspects: {
              lantern: 1,
              knock: 1,
              "mariner.storyaspect.mortal": 1,
              "mariner.storyaspect.wise": 1,
              "mariner.storyaspect.witty": 1,
            },
          },
          label: "Trade Stories in the Café Nocturne",
          startdescription:
            "<The Café has been a meeting ground for over half a century, where Armenians, Greeks and Italians have been trading stories, scripts and books with the Kurds and the Turks since the time of the Empire.>",
          description:
            "<In the streets, no turkish citizen is allowed to speak anything but turkish, but here, where the crowd is so rich with foreigners and their foreign tongues, some locals to unlatch their ancestral tongues.>",
        },
      }),
    ],
  });

  generateTouristQuest();
  generateHeistQuestCard();
  generateHeistHints();
  generateBuriedSecretsQuest();
  generateBuriedSecretsQuestCard();
}
