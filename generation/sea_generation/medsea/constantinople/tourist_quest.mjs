import { LOCAL_TO } from "../../../generation_helpers.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { UPDATE_QUEST } from "../../../quest_generation.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";

export function generateTouristQuest() {
  const RECIPE_FILE = mod.initializeRecipeFile("recipes.medsea.constantinople.quest.tinyheist", [
    "locations",
    "medsea",
    "constantinople",
  ]);
  const ELEMENT_FILE = mod.initializeElementFile("medsea.constantinople.quest.tinyheist", [
    "locations",
    "medsea",
    "constantinople",
  ]);
  const QUEST_PREFIX = "mariner.medsea.constantinople.tinyheist";
  const MYSTERY_PREFIX = `${QUEST_PREFIX}.mystery`;

  // Beginning
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.first`,
    label: "Salutations and Explanations",
    actionId: "talk",
    startdescription: '"My friend! How fortuitous for us to meet again on this day."',
    description:
      '"I have been planning some mischief… Nothing too grand! But there is a palace, supposedly one of the grandest still standing from the days that this was a city of palaces." A dreamy look passes over rounded features." But it is now in the hands of the greediest of greedy. Not only a money-lender, miser, and war-profiteer… but he has even closed off the grounds to perfectly respectable sorts such as moi." His face wrinkles up into a frown of shocked disdain.',
    maxexecutions: 1,
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "constantinople", "tourist"),
    },
    warmup: 60,
    linked: [`${QUEST_PREFIX}.second`],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.second`,
    label: "Salutations and Explanations",
    actionId: "talk",
    description:
      '"This I cannot abide. I have been hatching a little scheme. If you could lend me your aid, I would be most grateful. I have just a few supplies that I think you can provide. Please return to me with what I need."',
    craftable: false,
    warmup: 10,
    effects: { "mariner.quests.tinyheist": 1 },
    furthermore: [UPDATE_QUEST("tinyheist", "salutations")],
  });

  //items fulfilled
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.fulfilled`,
    label: "Preparations and Reparations",
    actionId: "talk",
    startdescription: '"All preparations have been made? All tools provided? Wunderbar!"',
    description:
      '"Wish me luck, and if all goes well, I shall return with souvenirs. If not… I will still return with a thrilling story!"',
    craftable: true,
    grandReqs: {
      [READ_QUEST_FLAG("tinyheist", "salutations")]: 1,
    },
    requirements: {
      ...LOCAL_TO("medsea", "constantinople", "tourist"),
      "mariner.tool.knock": 1,
      "mariner.trappings.moth": 1,
      "mariner.song.grail": 1,
      "mariner.influences.heart": 1,
    },
    warmup: 60,
    inductions: [{
      id: `${QUEST_PREFIX}.capering`,
      expulsion: {
        limit: 4,
        filter: { "mariner.trappings.moth": 1, "mariner.tool.knock": 1, "mariner.song.grail": 1, ...LOCAL_TO("medsea", "constantinople", "tourist") },
      },
    }],
    furthermore: [UPDATE_QUEST("tinyheist", "preparations")],
  });

  // returned from capering
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intimations.first`,
    label: "Invitation an Intimations",
    actionId: "talk",
    startdescription:
      "When I arrive I find Florentin spread out on the divan, clad in a powder blue petticoat and with a policeman's fez at a jaunty angle on his curls",
    description:
      '"Ah! Fellow traveler, come on in! Let me regale you with the tales of shadowed hallways and lavish furnishings!" That night we drink absinthe and eat sweet almond patiseries as the count recounts stories believable, unbelievable and in some cases, perhaps even true.',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "constantinople", "tourist"),
    },
    warmup: 60,
    linked: [`${QUEST_PREFIX}.intimations.second`],
    grandReqs: {
      [READ_QUEST_FLAG("tinyheist", "preparations")]: 1,
      [READ_QUEST_FLAG("tinyheist", "invitations")]: -1,
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intimations.second`,
    label: "Invitation an Intimations",
    actionId: "talk",
    description:
      'When the bottles are empty and the crumbs are left for the cat, Florentin leans in conspiratorially. "I got you a gift while I was there. I am having a friend reset it for you… it was… too gaudy. No taste! No, no, do not press me. You will see. I can keep a secret when there is a need.  And I hope you can as well… I have a delivery I would like to enlist you for… Ach, do not look so glum. That is your job is it not? Please bring this package to Marseille, to the Ivanova family. They are not attached to the network, but I will provide you the address. I would go myself, but there is someone in Marseille that it is best for me to avoid.”',
    craftable: false,
    warmup: 60,
    furthermore: [UPDATE_QUEST("tinyheist", "invitations")],
  });

  //hand in
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.marseille.first`,
    label: "Darkened Stones",
    actionId: "explore",
    startdescription:
      "In the tar-kettle alleys of Marseille, there is a pension; a block of gray cement once washed white. In there, stewing in the compounded heat of the sun and the press of bodies, refugees from the old imperial russia are corralled.",
    description:
      "With Florentins package, I walk past the row of men smoking in the mockery of the coolness one finds in the midday shade. Inside, between rotting wallpaper, and the sweet smells of flypaper and sweat, I hand the package to a dark-eyed matron, wrapped in her shawls and her beleaguered dignity.",
    craftable: true,
    requirements: {
      "mariner.locations.medsea.marseille": 1,
    },
    grandReqs: {
      [READ_QUEST_FLAG("tinyheist", "delivered")]: -1,
      [READ_QUEST_FLAG("tinyheist", "invitations")]: 1,
    },
    warmup: 60,
    linked: [`${QUEST_PREFIX}.marseille.second`],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.marseille.second`,
    label: "Darkened Stones",
    actionId: "explore",
    description:
      "Later, after I have already reached the sun-soaked cobbles of the street, I hear the patter of lacquered shoes behind me. A teenage girl comes running up, grabs my hand with a cascade of praise, and leaves me with a small cookie tin of almond biscuits.",
    craftable: false,
    warmup: 15,
    furthermore: [UPDATE_QUEST("tinyheist", "delivered")],
  });

  //end
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.conclusions`,
    label: "Constitutionals and Conclusions",
    actionId: "talk",
    startdescription: 'On a ramble through the Arab quarter, I run into Florentin, dressed in an extravagant suit of lavish purple. "Fellow traveler! What a delight to run into you once again."',
    description:
      'Fret yourself not about my garb. In some spaces, standing out is the best way to blend in. But I got a delightful letter from madam Ivanova in Marseille. I am glad that we managed to return some of their belongings.',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "constantinople", "tourist"),
    },
    linked: [`${QUEST_PREFIX}.conclusions.second`],
    grandReqs: {
      [READ_QUEST_FLAG("tinyheist", "delivered")]: 1,
    },
    warmup: 60,

  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.conclusions.second`,
    label: "Constitutionals and Conclusions",
    actionId: "explore",
    description:
      "While I can understand a soldier’s desire for trophies, there is no reason family pictures need to end up as the spoils of war. Ironically, they grow in value to owners when displayed upon a strange hearth, but lose value to all other eyes, wrenched from their proper context.",
    craftable: false,
    warmup: 15,
    furthermore: [UPDATE_QUEST("tinyheist", "completed"), {
      effects: { "mariner.quests.tinyheist": -1 }
    }]
  });


  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.capering`,
    label: "The Caper of The Copper Manse",
    actionId: "mariner.caper",
    description: '"Florentin is on a caper... I will not be seeing him for a while"',
    maxexecutions: 1,
    warmup: 1000,
    rootAdd: {
      [QUEST_FLAG_ID("tinyheist", "caperedout")]: 1,
    },
    effects: { "mariner.trappings.moth": -1, "mariner.influences.heart": -1 },
  });
}
