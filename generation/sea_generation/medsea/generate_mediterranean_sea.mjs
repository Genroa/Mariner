import { generateSea } from "../sea_generation.mjs";
import { generateMarseille } from "./marseille/generate_marseille.mjs";
import { generateBizerta } from "./bizerta/generate_bizerta.mjs";
import { generateConstantinople } from "./constantinople/generate_constantinople.mjs";
import { generateHaifa } from "./haifa/generate_haifa.mjs";
import {
  DOCKSIDE_WAREHOUSE,
  RESCUE_MISSION,
  SALVAGE_MISSION,
} from "../../vault_generation/vault_templates.mjs";
import { generateVaultLocationElements } from "../../vault_generation/vault_generation.mjs";
import { trappingAspects } from "../../generate_trappings.mjs";
import { generateStudentDeathEvents } from "./student_death_events.mjs";
import { generateNoonWaterQuest } from "./marseille/noonwater.mjs";
import { generateSightlessFightEvent } from "../../sea_events_generation/sea_events/med_sea/sightlessfight.mjs";
import { generateShadowEvent } from "../../sea_events_generation/sea_events/med_sea/shadow.mjs";
import { generateGalanthaSunkSeaEvent } from "../../sea_events_generation/sea_events/med_sea/galantha_sink.mjs";
import { generateStruggimentoFinale } from "./imago_chase.mjs";
import { FLAG_ID, READ_QUEST_FLAG, READ_FLAG } from "../../global_flags_generation.mjs";

export function generateMedSea() {
  // Generate Mediterranean Sea
  generateSea({
    portRegion: "medsea",
    label: "Mediterranean Sea",
    rewardsByType: {
      precious: ["mariner.trappings.lantern.1", "mariner.trappings.grail.1"],
      jumble: [
        "mariner.trappings.edge.1",
        "mariner.trappings.moth.1",
        "mariner.trappings.heart.1",
      ],
      scholarly: ["mariner.scrapofinformation"],
      ancient: [
        "mariner.scrapofinformation",
        "mariner.trappings.winter.1",
        "mariner.trappings.lantern.1",
      ],
    },
    weather: {
      mist: 2,
      rain: 2,
      wind: 2,
      storm: 2,
      clear: 3,
      bright: 2,
      sirocco: 2,
      hot: 2,
      trade: 2
    },
  });
  // Ports
  generateMarseille();
  generateBizerta();
  generateConstantinople();
  generateHaifa();

  // Sea wide events
  generateStudentDeathEvents();

  // Repeatable vaults
  generateVaultLocationElements({
    portRegion: "medsea",
    ...DOCKSIDE_WAREHOUSE,
  });
  generateVaultLocationElements({
    portRegion: "medsea",
    ...SALVAGE_MISSION,
  });
  generateVaultLocationElements({
    portRegion: "medsea",
    ...RESCUE_MISSION,
  });

  generateVaultLocationElements({
    portRegion: "medsea",
    portId: "thebes",
    label: "The Theban Branch",
    description:
      "The southern most branch of the House of Lethe, in a city now named Luxor.",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.thebes.riverbanks": 1,
          "mariner.mysteries.hospital": 1,
        },
      },
    ],
    recipeContentOnLeave: {
      furthermore: [
        {
          rootSet: {
            [FLAG_ID("luxor.wood.draw")]: 0
          },
        },
        {
          rootSet: {
            [FLAG_ID("luxor.stone.draw")]: 0,
          },
        },
        {
          rootSet: {
            [FLAG_ID("luxor.thread.draw")]: 0
          }
        },
        {
          rootSet: {
            [FLAG_ID("luxor.city.draw")]: 0
          }
        },
        {
          rootSet: {
            [FLAG_ID("luxor.branch.draw")]: 0
          }
        }
      ],
    },
    authorizeRetreat: true,
    notorietyLabels: [
      {
        label: "<The River Rouses>",
        description:
          "<A glance from a distant shore, a frown on a passing ship. We are noted, but not yet noted upon.>",
      },
      {
        label: "<Rumours on the Shores>",
        description:
          "<One comment to the next, and soon necks might crane to catch a glimpse.>",
      },
      {
        label: "<Circling Ships>",
        description:
          "<Ships sail out to investigate what we are attampting on the waters.>",
      },
      {
        label: "<Imptomptu Blockade>",
        description:
          "<A ring has formed around our ship. We are not to remain, but we are definitely not to leave with our behavior unanswered for. >",
      },
    ],
  });

  generateVaultLocationElements({
    portRegion: "medsea",
    portId: "aleppo",
    label: "The Allepene Branch",
    description:
      "Like so many treasures of the arabian antiquities, this Branch of the House of Lethe can no longer be found in the city it once belonged too.",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.aleppo.maze": 1,
        },
      },
    ],
    authorizeRetreat: true,
  });

  generateVaultLocationElements({
    portRegion: "medsea",
    portId: "damascus",
    label: "The Damascene Branch",
    description: "<>",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.damascus.museum": 1,
        },
      },
    ],
    authorizeRetreat: true,
    notorietyLabels: [
      {
        label: "Idle Interest",
        description:
          "<The air is laced with polite disinterest, both from the visitors and the guards.>",
      },
      {
        label: "<Eyes Fixed>",
        description:
          "<Heckles have been raised, haunches elbowed, and all gaurds stand at attention.>",
      },
      {
        label: "<Actively Scanning>",
        description:
          "<They know they are looking for something, and some may even know it is me.>",
      },
      {
        label: "<High Alarm>",
        description:
          "<Our group is identified, and calls are made for our seizure>",
      },
    ],
  });

  generateVaultLocationElements({
    portRegion: "medsea",
    portId: "bizerta_morgue",
    label: "<The Bizerta Morgue>",
    description: "<>",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.bizerta_morgue.body": 1,
          "mariner.vaults.bizerta_morgue.casefile": 1,
        },
      },
    ],
    recipeContentOnLeave: {
      furthermore: {
        target: "~/visible",
        mutations: [
          {
            filter: `mariner.crew`,
            mutate: "mariner.vaults.bizerta_morgue.selected",
            level: 0,
            additive: false,
          },
        ],
      },
    },
    authorizeRetreat: true,
  });

  generateVaultLocationElements({
    portRegion: "medsea",
    portId: "galicia",
    label: "Galician Port",
    description: "<>",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.galicia.limia": 1,
        },
      },
    ],
    authorizeRetreat: true,
    notorietyLabels: [
      {
        label: "River Gossip",
        description: "<>",
      },
      {
        label: "<Mouras Stirring>",
        description: "<>",
      },
      {
        label: "<Immortal Ire>",
        description: "<>",
      },
      {
        label: "<The Galantha Is Watching>",
        description: "<>",
      },
    ],
  });

  generateVaultLocationElements({
    portRegion: "medsea",
    portId: "garden",
    label: "Saint Respice Hospital",
    description:
      "<A hospital nestled against the mountains of Marseille, after the epidemic of the a few decades ago, it's reputation has improved.>",
    arrivalEffects: [
      {
        effects: {
          "mariner.vaults.garden.hospital": 1,
        },
      },
    ],
    authorizeRetreat: true,
    notorietyLabels: [
      {
        label: "Guests in the Corridors",
        description:
          "<we are foreign entities within the system of the hospital, but so far, we have not been rejected.>",
      },
      {
        label: "<Concerned Staff>",
        description:
          "<like sparking electricy, concerned reports are making their way across the hospital. soon it will reach the central hub, and a response may come.s>",
      },
      {
        label: "<Orderlies Patroling>",
        description:
          "<Armed with steeled grins and soothing voices, the white-garbed orderlies flow through the artories of the hospital, trying to flush us out.>",
      },
      {
        label: "<The Heart Knows>",
        description:
          "<Somewhere deep inside the hospital, something knows we are here.>",
      },
    ],
    recipeContentOnLeave: {
      furthermore: {
        target: "~/visible",
        mutations: [
          {
            filter: `mariner.crew`,
            mutate: "mariner.vaults.garden.disguise.staff",
            level: 0,
            additive: false,
          },
          {
            filter: `mariner.crew`,
            mutate: "mariner.vaults.garden.disguise.patient",
            level: 0,
            additive: false,
          },
          {
            filter: `mariner.crew`,
            mutate: "mariner.vaults.garden.disguise.docter",
            level: 0,
            additive: false,
          },
        ],
      },
    },
  });

  mod.readElementsFile("mystery.imago", ["mysteries", "imago"]);

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethystloss",
    label: "On Matthias and the Amethyst Imago: Loss",
    description:
      "An account of an occult romance. A certain Para, son of Aaron, claims to have written this in a single day and a single night as part of his initiation into the Damascene branch of the House of Lethe - that tight-disciplined order of immortals who reject their past and future.",
    aspects: {
      ...trappingAspects({
        value: 2,
        types: ["readable", "artifact"],
        aspects: { moth: 2 },
      }),
      "mariner.booktalksabout.imago": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethystlossaramaic",
    label: "On Matthias and the Amethyst Imago: Loss",
    description:
      "The diagrams and illustrations, even across the centuries, are elegant, hypnotic, occasionally erotic. In Aramaic.",
    aspects: {
      ...trappingAspects({
        value: 2,
        types: ["readable", "artifact"],
        aspects: { moth: 2 },
      }),
      "mariner.aramaic.untranslated": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethystpursuit",
    label: "On Matthias and the Amethyst Imago: Pursuit",
    description:
      "An account of an occult romance. A certain Mek claims to have written this in a single night as part of his initiation into the Theban branch of the House of Lethe - that order of immortals who used the arts of water to conceal themselves.",
    aspects: {
      ...trappingAspects({
        value: 2,
        types: ["readable", "artifact"],
        aspects: { grail: 2 },
      }),
      "mariner.booktalksabout.imago": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethystpursuitaramaic",
    label: "On Matthias and the Amethyst Imago: Pursuit",
    description:
      "The diagrams and illustrations, even across the centuries, are elegant, hypnotic, occasionally erotic. In Aramaic.",
    aspects: {
      ...trappingAspects({
        value: 2,
        types: ["readable", "artifact"],
        aspects: { grail: 2 },
      }),
      "mariner.aramaic.untranslated": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethysttransformation",
    label: "On Matthias and the Amethyst Imago: Transformation",
    description:
      "An account of an occult romance. A certain Iyavogos claims to have written this in a single night as part of his initiation into the Aleppine branch of the House of Lethe - that order of immortals who would not pledge themselves to any Hour.",
    aspects: {
      ...trappingAspects({
        value: 2,
        types: ["readable", "artifact"],
        aspects: { forge: 3 },
      }),
      "mariner.booktalksabout.imago": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethysttransformationaramaic",
    label: "On Matthias and the Amethyst Imago: Transformation",
    description:
      "The diagrams and illustrations, even across the centuries, are elegant, hypnotic, occasionally erotic. In Aramaic.",
    aspects: {
      ...trappingAspects({
        value: 2,
        types: ["readable", "artifact"],
        aspects: { forge: 3 },
      }),
      "mariner.aramaic.untranslated": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethystwitness",
    label: "On The Amethyst Imago by Matthias",
    description:
      "An account of a fateful hunt. A certain Matthias seems to have written this in his final months in the Wake charting the mutual maneauvering between him and a Moth Name, The Violet Imago - it chronicles their pursuits, their feints and their eventual courting.",
    aspects: {
      ...trappingAspects({
        value: 0,
        types: ["readable", "artifact"],
        aspects: { heart: 4 },
      }),
      "mariner.booktalksabout.imago": 1,
      "mariner.topic": 1,
    },
  });

  mod.setElement("mystery.imago", {
    id: "mariner.matthiasamethystwitnessaramaic",
    label: "On The Amethyst Imago by Matthias",
    description:
      "The diagrams and illustrations, even across the centuries, are elegant, hypnotic, occasionally erotic. In Aramaic.",
    aspects: {
      ...trappingAspects({
        value: 0,
        types: ["readable", "artifact"],
        aspects: { heart: 4 },
      }),
      "mariner.aramaic.untranslated": 1,
      "mariner.topic": 1,
    },
  });

  generateNoonWaterQuest();
  generateSightlessFightEvent();
  generateShadowEvent();
  generateGalanthaSunkSeaEvent();
  generateStruggimentoFinale();
}
