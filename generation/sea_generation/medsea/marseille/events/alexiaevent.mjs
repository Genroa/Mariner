import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateGiftofMagiEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.magi"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Gift and the Magi`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.magi", 25),
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.magi") },
    startdescription:
      'Alexia has requested that I check in regularly when I am in town. Wether this is because she wishes to see me, or wishes to keep an eye on me I am not sure. This day, the door anticipates my knock, swinging towards me. Alexia is pointing two grey gentlemen to leave. "No, sirs. Not this time, not again. I have payed my dues. adieu." The door closes without acknoledgement, nds Alexia does not appear to be entertaining visiters at this time, so I slink away with the scholarly callers. They tell me of their troubles, an artifact, a box, to open only when entry is to be permitted. They at the Institute only as researchers, and do not have the practical skills to open it. Do I wish to lend a hand?',
    slots: [
      {
        id: "heart",
        label: "Interest",
        description: "if I wish to help, i need to apply myself.",
        required: {
          "mariner.heart": 1,
        },
      },
    ],
    linked: [{ id: "medsea.randomevent.alexia.continued" }],
  });
  setWanderingEventChance(EVENT_START_ID, 20);

  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.alexia.continued",
    label: `The Magi and the Gift`,
    requirements: { id: "mariner.heart" },
    startdescription:
      '"We are both experts in our fields. Experts! But we cannot come to agree. Is this box Haunted by Roses or has a Metaplaxis of Galliana? Impossible to encertain whose theory is right iwthout looking. So could you take a handle of of these binds? Knock should do it, or Edge."',
    description:
      "More puzzle then box, more containment then container, more winter then spring.",
    slots: [
      {
        id: "help1",
        label: "Interest",
        description: "if I wish to help, i need to apply myself.",
        required: {
          "mariner.heart": 1,
          "mariner.crew": 1,
          "mariner.tool": 1,
          "mariner.trapping": 1,
          "mariner.song": 1,
        },
      },
      {
        id: "help2",
        label: "Interest",
        description: "if I wish to help, i need to apply myself.",
        required: {
          "mariner.heart": 1,
          "mariner.crew": 1,
          "mariner.tool": 1,
          "mariner.trapping": 1,
          "mariner.song": 1,
        },
      },
      {
        id: "help3",
        label: "Interest",
        description: "if I wish to help, i need to apply myself.",
        required: {
          "mariner.heart": 1,
          "mariner.crew": 1,
          "mariner.tool": 1,
          "mariner.trapping": 1,
          "mariner.song": 1,
        },
      },
    ],
    linked: [
      { id: "medsea.randomevent.alexia.knock" },
      { id: "medsea.randomevent.alexia.edge" },
      { id: "medsea.randomevent.alexia.failure" },
    ],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.alexia.knock",
    label: `Magnaminous`,
    requirements: {},
    startdescription: "",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.alexia.edge",
    label: `Gifts given in Mercy`,
    startdescription: "",
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.alexia.failure",
    label: `Gifts given in Mercy`,
    startdescription: "",
  });
}
