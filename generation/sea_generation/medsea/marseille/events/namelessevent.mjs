import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateXXXXEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.xxx"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: ``,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.xxx", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.xxx") },
    startdescription:
      '',
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
