import { generatePort } from "../../../port_generation/port_generation.mjs";
import { ALEXIA } from "./alexia.mjs";
import { generateFrenchGrailCultQuest } from "./french_grail_cult.mjs";
import { MFM } from "./manfrommarseille.mjs";
import { Galantha } from "../galantha.mjs";
import { generateBewitchQuestCard } from "./serenoquestcard.mjs";
import { PROFESSOR } from "./professor.mjs";
import { generateFrenchHeartCultQuest } from "./french_heart_cult.mjs";
import { generateNoonWaterQuest } from "./noonwater.mjs";
import { generateNoonWaterQuestCard } from "./noonwaterquestcard.mjs";
import { generateOzoneEvent, generateStormbreakEvent } from "./bewitch_events.mjs";

export function generateMarseille() {
  generatePort({
    portRegions: ["medsea"],
    portId: "marseille",
    label: "Marseille",
    description: "<The beating heart of the Riviera. They say this is the closest you can get to Noon in continental Europe.>",
    recipeDescriptions: {
      exploreHH:
        "<Under the Relentless Riviera Sun, we walk streets hazed by the fermented smells of the Mediterranean, The afternoon sometimes brings the rel of a thunderstorm down from the mountains, to wash away the sins of the day.>",
      exploreLH:
        "<Under the Relentless Riviera Sun, we walk streets hazed by the fermented smells of the Mediterranean, The afternoon sometimes brings the relief of a thunderstorm down from the mountains, to wash away the sins of the day.>",
    },
    storyTrading: {
      story: {
        label: "<>",
        description:
          "<>",
        aspects: {
          moth: 1,
        },
        icon: "thosewhodonotsleep",
      },
      label: "Trade Stories at the Streets of Marseille",
      startdescription:
        "<Stories hang in the air refracted rainbows from the white, baking walls, or are written in dust on the ground. If I engage with the locals, I can pluck Haifa's story and tell it as my own.>",
      description:
        "<>",
    },
    barName: "L'Ancre",
    wharf: {
      cargoOptions: [
        { portRegion: "medsea", distance: "local", label: "Med Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
      ],
    },
    portElements: [ALEXIA, PROFESSOR, MFM, Galantha],
  });

  generateFrenchGrailCultQuest();
  generateFrenchHeartCultQuest();
  generateBewitchQuestCard();
  generateOzoneEvent();
  generateStormbreakEvent();
  generateNoonWaterQuest();
  generateNoonWaterQuestCard();
}
