import {
  repeatableConversation,
  convertSourceConversation,
  uniqueConversation,
} from "../../../port_generation/helpers.mjs";
import { SECONDS, swapToAnotherRoutine } from "../../../routine_generation.mjs";

export const ALEXIA = {
  type: "npc",
  elementId: "alexia",
  label: "<Miss. Alexia Bastarache-Corbin>",
  description:
    "A serious-faced young woman with an official title from an obscure agency. In some circles, her name carries a heavy weight.",
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: [],
    usedNames: [],
    usedInitials: [],
    gender: "male",
    whatToDoWithIt: ["<>", "<>"],
  },
  deliveryRiddles: [
    "<Fiercer then Alexander, Brighter then Alexandria>",
    "Joad d'arc of Reason",
    "The Institute Sphinx",
    "Bane of Reims, Fear of Aspirants",
    "Lamp-in-darkness, Maiden Sovereign, Gatekeeper of The Continent",
    "She Carries Torch, Key and Quill where the night is darkest",
    "She who blackmails us true occultist to help serve her Totalitarian Institution",
  ],
  storyTrading: {
    story: {
      label: "The Beast of Gévaudin",
      icon: "onthewhite",
      description:
        "<Once, before our current Age of Folly or the preceding Age of Reason, A small town in the countryside was rocked to it's core by a great monster. a monster stalked the hill, slaughtering both livestock and humans, killed for sport as often as for sustenance. the folks spoke of a monster, the bishop spoke of a punishment the authorities spoke of a wolf. first the Church solution was the Prayer of Fourty Hours, which now would be heritical. Then the Sun King sent three hunters. A bickering father and son, followed a military beast itself. But in the end the kill went to a local youth, and for years the Maid of Gevaudin was compared to Jeanne D'Arc herself>",
      aspects: {
        edge: 2,
        grail: 2,
        "mariner.storyaspect.mortal": 1,
        "mariner.storyaspect.rousing": 1,
      },
    },
    label: "<Trading Stories with Alexia>",
    startdescription:
      '"You want a story from me?" She smiles down at the table. "Are you sure you can handle it?"',
    description:
      'My stories tend to end up in case files, and sealed in Institute Vaults. Here is one from before the Institute was founded, so there is no duty of secrecy that binds me."',
  },
  conversations: [
    repeatableConversation({
      id: "chest",
      element: "mariner.vaults.thebes.chest",
      label: "<Ask for help with a Ward>",
      startdescription: '"<A Ward on a Chest?>"',
      description:
        '"<Pilfered from some forgotten corner of history?" She examines the chest, following the grain of the wood and inspecting the chains closely. "Triple chained... The work of the House of Lethe, I would venture. " She laces her fingers, then frowns."These wards and curses are extensive, more extensive then I could tackle here in the field. And if I took it back to the Institute, you are unlikely to see the contents again. I have a contact in Constantinople, Mother Euphresme. I would trust her capable of opening this lock."',
    }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "<Discuss the Unknown Glyphs with Miss Alexia>",
      startdescription: '"<You have something you wish to show me?>"',
      description:
        '"I do now recognise this script, though I can see some symbols that look much like Egyptian Hieroglyphs. With your permission i will make a copy to enter into our own archives. Perhaps one of the researchers there can identify what it is." For the next couple of hours, Alexia is bend over the page, copying the copy meticulously, and taking copious notes. I do get back my rubbings at the end, thought she seems conflicted to let them go. I am unsure wether this is for her own curiosity, or a concern of public safety.',
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "<Ask Miss Alexia after my Half-Heart>",
      startdescription: '"<Half-Heart? The term rings a bell...>"',
      description:
        '"It is not something I would be called to deal with often. Most Half hearts are not much trouble, so I heard. But they are contained on our lists, when we are made aware of them. Since they are special births, but not special blood, they cannot be tracked preemptively."',
      effects: {},
    }),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "<Ask Miss Alexia about my Lack-Heart>",
      startdescription: '"The darkness that dwells within? I am familiar"',
      description:
        '"<Sometimes, when the nights are long and the work shows it worst face, I find myself envisioning myself on a stairwell, spiralling down into a dark pit below. Faintly, far above I can see light, but I am so tired, and my body aches. It would be so much easier just to sink into the darkness. But I do not, Because every step brings me closer to the light. That\'s why I keep moving.>"',
      effects: {},
    }),
    repeatableConversation({
      id: "vagabonddedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "<Discuss with Alexia of the Centipedes Pilgrimage>",
      startdescription: '""',
      description:
        '"I heard a rumour, that the Pilgrimage the Centipede sets out for Half Hearts is quite unlike those the other Vageries go on. Ofcourse there are very few historical documents of confirmed Halfhearts, so it\'s hard to speak on it with much certainty."',
      effects: {},
    }),
    repeatableConversation({
      id: "twinsdedication",
      element: "mariner.ascension.twins.dedication",
      label: "<Discuss with Alexia the Path of Becoming Whole>",
      startdescription: '"<You seek to repair your lonely heart?>"',
      description:
        '"No man is an island, or so the saying goes. But in the hazy cacophony of modernity, I something worry we are all islands, drifting apart in the fog of confusion. I wish you luck, but I bid you caution. If the costs to the world becomes too great... I would hate for you to become another assignment."',
      effects: {},
    }),
    repeatableConversation({
      id: "forgesong",
      element: "mariner.song.forge",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"You wish to discuss the magic of chants and incantations?"',
      description:
        '"I studied the Arts of Brigid, Skadi and the Skalds. Songs of Light and dancing shadows, the sputtering of fires and the wail of brass and steel. [insert something it forge songs can do]"',
      effects: {},
    }),
    repeatableConversation({
      id: "knocksong",
      element: "mariner.song.knock",
      label: "<Discuss the Power of my Songs>",
      startdescription:
        '"<You wish to discuss the practice of chants and incantations?>"',
      description:
        '"The songs of the underplaces and the spaces between... I have come across them in my Investigations. Some aspects of the mansus are best described in mathematics, but it\'s dimensions are best expressed in Song. But my experiences are just in the field, not in study. Perhaps someone who holds more of a focus on the spiritual can bring you further."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.studentexsanguinate.1",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Student Exsanguinate>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"You have kept pushing? I understand the fascination of investigation. Don’t let it pull you too deep, and if you find something I should know, don’t be a stranger. "',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.studentexsanguinate.2",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Student Exsanguinate further>",
      startdescription: '"<There have been developments?>"',
      description:
        '"Peculiar in the least, troubling at worst. At this time, the ties to this city cannot be ignored."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stink.1",
      element: "mariner.song.knock",
      label: "Discuss the Stench of Marseille",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"I make it a point not to speculate about my theories when there are witnesses present. It might just lead them to foolish conclusions. Let me process this on my own until we have acquired some more data, and I can sort the threats from possible to probable to faint."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.1",
      element: "mariner.song.knock",
      label: "<Discuss the Bewitchment of the City>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"A minor cretin at the best of times. I would tell you to be careful with dealing with him, but I trust you will be able to handle him. Just remember that all of his motives will be of the utmost selfishness, and that he can only be relied on only in his villainy."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.2",
      element: "mariner.song.knock",
      label: "<Discuss the Bewitchment of the City further>",
      startdescription: '"<There have been further developments?>"',
      description:
        '"Well from the stench on the wind I can guess who is behind these latest irregularities. I hope what you get from him will be worth it. If you speak with your accomplice, you can let him know that he has not escaped my notice, but that he shares a level of priority on my to do list, with the other cockroaches that crawl through the gutter."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.towers.1",
      element: "mariner.song.knock",
      label: "<Discuss the Vision of the Many Towers>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '" I am not sure what to tell you of the Mansus… as little as I can explain a sunset to the blind. But yes, there are directions in dreams. but it is a rare breed that can reject the rich delight of the Glory and not immediately flee back into the inviting ignorance of the dark Woods."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.caper.1",
      element: "mariner.song.knock",
      label: "<Discuss the Caper of the Copper Manse>",
      startdescription: '""',
      description:
        '"A burgling Count, you say?" Alexia\'s eyes narrow for just a moment. "I am sure I could not imagine something so odd. As for aiding and abetting them… even a witty crime is still illegal."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.discardedtruth.1",
      element: "mariner.song.knock",
      label: "<Discuss the Recovery of Discarded Truth>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"Unearthing buried secrets, at the behest of one who cannot tell you what you might uncover… I can offer you nothing except a word of caution. Even doting old women can be dangerous… There is a tale I could tell you from London that would curdle your blood."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.sisterhood.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reunion of the Sisterhood>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"A heterodox sect hidden inside the orthodox church, disbanded out of fear of the inquisitions? I cannot say I am surprised. Organized religions always seem to vacillate between sponsoring witch-hunts and sheltering cults of enlightenment within their midst. The more things change..."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.1",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"The House of Lethe is fascinating. An ancient fraternity but its principles still live on to this day: The Obliviates…  Never  the greatest source of my troubles, but their influence spreads far, and even when they are benign, they are so damn slippery."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.2",
      element: "mariner.song.knock",
      label: "<Discuss the Path to Constellation further>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        "\"They say the elegiast is not a kind hour, but he is never cruel. Yet I fear for what he represents… Should we remember all that is now past, even those things wicked, cruel or profane? The Institute's official stance is that knowledge itself poses no danger, it's the all in the hands that hold it. But some days I believe the Watchman's Tree is right to bloom its flowers, and my hands have been known to stain red or black or white.\"",
      effects: {},
    }),
    repeatableConversation({
      id: "quest.imago.1",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplary Path>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"The tale of Mattias is a classic, each of its versions is in their own right. The Imago herself is now invoked as one of the Cardinals, and her presence is felt all across the world, but she rarely makes herself available for comment. But of Mattias… I do not know what became of him. I don’t think I know of anyone making contact with him after their rise into the service of the Forge, nor have I ever heard any suggestion that he might have been cast down. Might he burn still? Or does he serve as fuel for another\'s flame. "',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.muse.1",
      element: "mariner.song.knock",
      label: "<Discuss the Chase of the Missing Muse>",
      startdescription: '"<You have a case you wish to discuss with me?>"',
      description:
        '"Blomkvist? That is more the kind of music that my father is into, and I have done my best to avoid it.  But I have heard of the Stemmelos Havfrue. A seminal piece of music, showing how to incorporate the mysteries of Heart into music without compelling the audience to flay itself. Occultists might call it gutless, but I can appreciate its approachability. But having seen the trajectory of other mystic artists like Savage, perhaps it is best if mr. Blomkvist and his muse are never reunited."',
      effects: {},
    }),
  ],
};
