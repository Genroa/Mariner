import {
  repeatableConversation,
  convertSourceConversation,
} from "../../../port_generation/helpers.mjs";

export const MFM = {
  type: "npc",
  elementId: "mfm",
  label: "Mr. Sereno Sombreuil",
  description:
    "<Sharply dressed, and grinning like the wolf. to be known by mister Sombruiel could be considered dangerous, but to not know of him could be much worse. >",
  deliveryOptions: {
    previousPackageLocations: [],
    knownLanguages: [],
    usedNames: [],
    usedInitials: [],
    gender: "male",
    whatToDoWithIt: [
      "<If the letterbox is wired shut, cram it under the door. if the door is stopped, nail it to the door.>",
      "<if you are refused entry, just shout it is for a fellow Leshii>",
    ],
  },
  deliveryRiddles: [
    "<Vulture of the Riviera>",
    "Bargain-bin Spymaster",
    "The most common Subriquet, Poorly localized",
    "Spider in a Web of Greed",
    "A stranger to the Republic of Liberte",
  ],
  storyTrading: {
    story: {
      label: "The Immortal and the Witch",
      icon: "onthewhite",
      description: "<>",
      aspects: {
        winter: 2,
      },
    },
    label: "<Trading Storiees with Sereno Sombrueill>",
    startdescription:
      '"<A story? is it a childrens tale you want? with clear hero\'s and foul villains yes?>"',
    description:
      "< Then Here: across seven mountains and seven forests, where the onlye House we knew walked by on bird-leg stilts above us.>",
  },
  conversations: [
    repeatableConversation({
      id: "chest",
      element: "mariner.vaults.thebes.chest",
      label: "Show of the Chest to mister Sombrueil",
      startdescription: '"<Some loot you want to deciminate?>"',
      description:
        '"Let\'s see what we have here..." He hardly touches the chest before his hand recoils, and he snaps back at me, expression darkened. "What is this? are you trying to trap me?". His lanky, sharp frame suddenly cast an menacing shadow in the room. "In the future, be mindful not to bring items trapped in active snares to your business partners. I am not a saint willing to risk my hide for your profit."',
    }),
    repeatableConversation({
      id: "hyksos",
      element: "mariner.vaults.damascus.glyphs.hidden",
      label: "<Discuss the unknown glyphs with mister Sombrueil>",
      startdescription:
        '"Something mysterious and illicit? my, that could be my calling card."',
      description:
        'Sereno studies the rubbings for a long time, pushing aside my hands not to miss a single stroke. He sighs, a not unhappy sound, before he turns back to me."My, My, quite the find. You hold there a Treshold Ward written in Hyksos, the Tongue of the western Kings. Sell it for a premium, or use it in schemes where desire and danger meet."',
      effects: {
        "mariner.vaults.damascus.glyphs.hidden": -1,
        "mariner.vaults.damascus.glyphs": 1,
      },
    }),
    //repeatableConversation({
    //  id: "halfheart",
    //  element: "mariner.halfheart",
    //  label: "<>",
    //  startdescription: '"<>"',
    //  description:
    //    "\"\"",
    //}),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "<Talk to Sereno about my Lack Heart>",
      startdescription: '"<You feel a dark emptyness inside?>"',
      description:
        '"Yes. That what is there at the heart of things. Some people flee that knowledge, instead of facing the fact. Others ply themselves with a false paradise, imagined beauty. But it takes true strength to aknowledge the pit we find ourself in. Only then, can you begin to rise."',
    }),
    repeatableConversation({
      id: "vagabonddedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "<Talk to Sereno Sombrueil about the Centipedes Pilgrimage>",
      startdescription: '"<Listening to the Centipedes whsiper, are you>"',
      description:
        "\"It'll bring you to interesting places, if it doesn't bring you death. The world does not look kind upon percieved vermin crawling into places uninvited, belief me.\"",
    }),
    repeatableConversation({
      id: "twinsdedication",
      element: "mariner.ascension.twins.dedication",
      label: "<Talk to Sereno Sombruiel about the Path to Becoming Whole>",
      startdescription: '"<So the Twins guiding your path huh?>"',
      description:
        '"Well, I cannot blame you. Everyone deserves to feel whole. If this is your path, I wish you all the luck. They are fickle patrons to have picking at your soul."',
    }),
    repeatableConversation({
      id: "songedge",
      element: "mariner.song.edge",
      label: "<<Discuss the Power of my Songs>>",
      startdescription:
        '"<I am not very musically minded, but Power I find irresistable.>"',
      description:
        '"Conflict is the engine on which the world runs. The Direciton of Edge is moving in the direcitons of your obstacles, but also what drives you forward. If you wish to confuse your enemies, for example, combine the puzzling Intent of Moth with the Direction of Edge."',
    }),
    repeatableConversation({
      id: "songwrong",
      element: "mariner.song",
      label: "<<Discuss the Power of my Songs>>",
      startdescription:
        '"<I am not very musically minded, but Power I find irresistable.>"',
      description:
        '"Yes? I see. Tell what you have discovered so far..." While the we talk for a while, Sereno stays cagey. After a while it becomes clear he is knows nothing, or is unwilling to share, and is simply trying to get me to spill what I know.',
    }),
    repeatableConversation({
      id: "quest.student.1",
      element: "mariner.song.knock",
      label: "<Discuss the Case of the Student Exsanguinate>",
      startdescription: '"<You are on a job?>"',
      description:
        '"Grisly scene huh? Isn’t it amazing what men can inflict with just a scalpel and a blade… I wonder if there was anyone to witness the gruesome act… "',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stench.1",
      element: "mariner.song.knock",
      label: "<Discuss the Stench of Marseille>",
      startdescription: '"<You are on a job?>"',
      description:
        '"Smells like peaches and cream to me… the smell of greed and opportunity. There is nothing else this city should smell like."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.stench.2",
      element: "mariner.song.knock",
      label: "<Discuss the Stench of Marseille further>",
      startdescription: '"<Still on the job?>"',
      description:
        '"A hospital? I am sure I could tell you nothing of interest about that. All I will say is, if you find yourself injured or sick, I would recommend being so at the other side of town."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.bewitch.1",
      element: "mariner.song.knock",
      label: "<Discuss the Bewitchment of the city>",
      startdescription: '"<You are on a job?>"',
      description:
        '"Don’t worry about the details, I will be sure to make it worth your while."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.towers.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reverie of the Crumbling Towers>",
      startdescription: '"<You are on a job?>"',
      description:
        '"Don’t you love these rats running in mazes? Even when they think to tunnel through the walls, they still go for the stinking cheese."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.truth.1",
      element: "mariner.song.knock",
      label: "<Discuss The Recovery of Discarded Truth>",
      startdescription: '"<You are on a job?>"',
      description:
        '"Can’t let the past lie buried? I am the same. I pick at my past like a dog licking its wounds back open. Its pain can be a sweet succor "',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.sisterhood.1",
      element: "mariner.song.knock",
      label: "<Discuss the Reunion of a Sisterhood>",
      startdescription: '"<You are on a job?>"',
      description:
        '"I know what it is to be persecuted. What is seen as vermin is rarely tolerated and never celebrated. These little flies have been scattered. I wish you good luck to catch your quarry."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.caper.1",
      element: "mariner.song.knock",
      label: "<Discuss The Caper of the Copper Manse>",
      startdescription: '"<You are on a job?>"',
      description: '""',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.1",
      element: "mariner.song.knock",
      label: "<Discuss the path to Constellation>",
      startdescription: '"You are on a job?',
      description:
        '"<Cowards, generations and generations of cowards. If turning away from the world makes you special, then just brick yourself into a wall and suffocate in your splendid isolation.>"',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.wist.2",
      element: "mariner.song.knock",
      label: "<Discuss the path to Constellation further>",
      startdescription: '"You still on a job?"',
      description:
        '<I never considered this anonymity of there\'s a  wound to the soul… but scars hold power, the Apostate Hound can tell us that. I wonder what power the scarring of other parts of the soul might yield… He smiles his predator grin. "I wonder which parts I even have left whole."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.struggimento.1",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplory Path>",
      startdescription: '"<You are on a job?>"',
      description:
        '"Pwah! The Cardinal Bug.. But I can’t say I know much about her. She was chased. She was plucked of her wings. She was immolated. All the things hunters are likely to do to bugs."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.struggimento.2",
      element: "mariner.song.knock",
      label: "<Discuss the Exemplory Path>",
      startdescription: '"<You still on a job?>"',
      description:
        '"Ancient history, told and retold and chewed up and spit out again. Don’t trick yourself into thinking you will get to the truth. History is told by the victors and when they hold their peace, the silence is filled by fools instead."',
      effects: {},
    }),
    repeatableConversation({
      id: "quest.muse.1",
      element: "mariner.song.knock",
      label: "<Discuss with Sereno the Chase of the Missing Muse",
      startdescription: '"<You are on a job?>"',
      description:
        '"The Old form" He laughs a wheezing laugh. "Gone. Dead or Gone. Vermin either way."',
      effects: {},
    }),
  ],
};
