import { generateQuest } from "../../../quest_generation.mjs";

export function generateBewitchQuestCard() {
  generateQuest({
    id: "bewitch",
    // Quest card informations
    label: "Bewitch the City",
    description:
      '<Sereno seeks to set up some Chaos in Marseille. The reasons why are obscure, but I have been promised it will be worth my while. With Sereno\'s connections, that could mean anything...>',
    // Quest stages
    updates: [
      {
        id: "proposition",
        icon: 1,
        label: "A Proposition",
        description:
          "Sereno has made me a proposition. If you could put the city in the right mood with your instruments or will your rituals, I will owe you one, and you should know of me, that I like to keep my ledger clean. [set up 5 Edge manifested influences, 3 Moth influences and 3 Knock influences]",
      },
      {
        id: "ozone",
        icon: 2,
        label: "Ozone and Tinder",
        description:
          "The air is volatile tonight. The clouds rolling in are dark as black powder, and people’s moods are fickle like snapping flags. The whole city is waiting for the thunder roll and lightning crash, but for now, all that power remains in potentia.",
      },
      {
        id: "stormbreaks",
        icon: 2,
        label: "Storm Broke",
        description:
          "Arguments break out, neighbors bicker as the mountain thunder fires like artillery, workers riot in the streets. The national guard, pushed back to their holds, threatens much violence and performs more still. And somewhere in all that confusion, no doubt one man is accomplishing all of his dealings. Whatever those might be, I hope he is satisfied. ",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.neg"],
            },
          },
        },
      },
      {
        id: "satisfaction",
        icon: 3,
        label: "Satisfaction",
        description:
          '"Well I can do nothing else than extend you my thanks and congratulations. You played your role well. Everything has fallen into place, and we\'ll both see the results soon, I am sure. Let me know when you need that favor called in."',
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.s"],
            },
          },
        },
      },
      {
        id: "favor",
        icon: 4,
        label: "A Favor Owed",
        description:
          '"So you have come to decide on your prize eh?" He rubs his stubbled chin, a long, raspy sound. " Steep… but not impossible, not for me at least. Come back in a month, and I will have a bottle for you. I hope you won’t drink it though. It would be a waste of talent to fade into obscurity." [Sereno is working on acquiring you the noonwater]',
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.s"],
            },
          },
        },
      },
      {
        id: "completed",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {},
          },
        },
      },
    ],
  });
}
