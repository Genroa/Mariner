import { LOCAL_TO } from "../../../generation_helpers.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { trappingAspects } from "../../../generate_trappings.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import { UPDATE_QUEST } from "../../../quest_generation.mjs";

export function generateNoonWaterQuest() {
  const RECIPE_FILE = mod.initializeRecipeFile("recipes.medsea.marseille.quest.noonwater", ["locations", "medsea", "marseille"]);
  const ELEMENTS_FILE = mod.initializeElementFile("medsea.marseille.quest.noonwater", ["locations", "medsea", "marseille"]);
  const QUEST_PREFIX = "mariner.medsea.marseille.noonwater";
  const MYSTERY_PREFIX = `${QUEST_PREFIX}.mystery`;

  mod.setElement(ELEMENTS_FILE, {
    id: "mariner.noonwater",
    label: "Noonwater",
    description: "Bottled obscurity. Some say it is drawn up from the underworld, but others think it must be melted Snow.",
    aspects: trappingAspects({
      value: 10,
      types: ["natural"],
      aspects: { moth: 5, winter: 5 },
    }),
  });

  // Beginning
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.first`,
    label: "A Proposition of Mutual Benefit",
    actionId: "talk",
    startdescription:
      'I could use your aid in a venture of mine and if you could be of aid to me, I am willing to extend to you a favor." Sereno looks like a starved wolf, leaning grinning in his gray wool suit.. "A favor to call in when you have the need for something illicit, rare or hard to find. My extensive network will be at your leisure, and to an enterprising individual such as yourself, I am sure a need will arise." ',
    description:
      '"I have plans for this town, but I need a little chaos for her to be… Receptive. If you could put the city in the right mood with your instruments or will your rituals, I will owe you one, and you should know of me, that I like to keep my ledger clean." [set up 5 Edge manifested influences, 3 Moth influences and 3 Knock influences]',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "marseille", "mfm"),
    },
    effects: { "mariner.quests.bewitch": 1 },
    furthermore: UPDATE_QUEST("bewitch", "proposition"),
    grandReqs: {
      [READ_QUEST_FLAG("bewitch", "proposition")]: -1,
    },
    warmup: 60,
  });

  // satisfaction
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.satisfaction`,
    label: "Satisfaction",
    actionId: "talk",
    startdescription: '"Just a moment, I have something to say."',
    description:
      '"Well I can do nothing else than extend you my thanks and congratulations. You played your role well. Everything has fallen into place, and we\'ll both see the results soon, I am sure. Let me know when you need that favor called in. "',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "marseille", "mfm"),
      "mariner.quests.bewitch": 1,
    },
    furthermore: UPDATE_QUEST("bewitch", "satisfaction"),
    grandReqs: {
      [READ_QUEST_FLAG("bewitch", "ozone")]: 1,
      [READ_QUEST_FLAG("bewitch", "satisfaction")]: -1,
    },
    warmup: 15,
  });

  // favor
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.favor`,
    label: "A Favor Owed",
    actionId: "talk",
    startdescription: '"So you have come to decide on your prize eh?"',
    description:
      'He rubs his stubbled chin, a repeated, raspy sound. " Steep… but not impossible, not for me at least. Come back in a month, and I will have a bottle for you. I hope you won’t drink it though. It would be a waste of talent to fade into obscurity."',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "marseille", "mfm"),
      "mariner.quests.bewitch": 1,
    },
    furthermore: UPDATE_QUEST("bewitch", "favor"),
    grandReqs: {
      [READ_QUEST_FLAG("bewitch", "satisfaction")]: 1,
      [READ_QUEST_FLAG("bewitch", "favor")]: -1,
      [READ_QUEST_FLAG("noonwater", "desired")]: 1,
    },
    warmup: 300,
    linked: [{ id: `${QUEST_PREFIX}.bargain` }]
  });

  // bargain
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.bargain`,

    label: "Bargain Fulfilled",
    actionId: "talk",
    startdescription: '"I always make good on my promises, and I press on you to remember that..."',
    description:
      '"Here, one of the only three bottles that are known to be this side of the Evening Isles. I did some digging into why you might request this. Apologies if that rankles your feathers, but a professional has to know what he gets involved in." "I suppose you won’t be drinking this, I will bid you success on your endeavors then. And be careful. This item is worth more than your ship and the lives of your crew combined. Don’t spill a drop."',
    effects: { "mariner.noonwater": 1 },
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "marseille", "mfm"),
    },
    furthermore: [UPDATE_QUEST("bewitch", "completed"), { effects: { "mariner.quests.bewitch": - 1 } }],
    grandReqs: {
      [READ_QUEST_FLAG("bewitch", "completed")]: -1,
      [READ_QUEST_FLAG("bewitch", "favor")]: 1,
    },
    warmup: 15,
  });

  // indignation
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.indignation`,
    label: "A Crime-Lords Indignation",
    actionId: "talk",
    startdescription: '"The water was stolen from you?"',
    description:
      '"Well, with an item so in demand as that, one should not be surprised. There are bound to be other interested parties.But it will not do for my business partner to get singled out. I will tug on some strings, and see where the web vibrates. I will get back to you as soon as this pesky fly has been identified."',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "marseille", "mfm"),
      "mariner.quests.noonwater": 1
    },
    effects: { "mariner.quests.noonwater": 1 },
    furthermore: [UPDATE_QUEST("noonwater", "indignation")],
    warmup: 15,
    grandReqs: {
      [READ_QUEST_FLAG("noonwater", "pilfered")]: 1,
      [READ_QUEST_FLAG("noonwater", "indignation")]: -1,
    },
  });

  // apology
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.apology`,

    label: "A Kind of Apology",
    actionId: "talk",
    startdescription: '"My suspicions have been proven correct..."',
    description:
      '"And with it, my partial culpability was proven as well. This entity was the original recipient of the bottle which I then arranged for you. I was sure my acquisition would not have been traceable, at least not traceable to you. But I have been proven wrong, and that is a feeling I cannot stand." Sereno contorts his face into a smile, which does not reach his eyes. "I will attempt to make it up to you. I will let you know not only who they are, but also where to find them, and all that I know of their situation."',
    craftable: true,
    requirements: {
      ...LOCAL_TO("medsea", "marseille", "mfm"),
      "mariner.quests.noonwater": 1
    },
    effects: { "mariner.locations.medsea.galicia.away": 1 },
    furthermore: {
      rootAdd: {
        [QUEST_FLAG_ID("noonwater", "apology")]: 1,
      },
    },
    grandReqs: {
      [READ_QUEST_FLAG("noonwater", "indignation")]: 1,
      [READ_QUEST_FLAG("noonwater", "apology")]: -1,
    },
    unlockCodexEntries: ["codex.persons.galantha", "codex.organisations.limia"],
    warmup: 15,
  });

  // repeatable find galantha
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intel`,
    label: "In the Right Direction",
    actionId: "talk",
    startdescription: '"You need to find the Galantha again?"',
    description:
      'Sereno shakes his head in disbelief. "Taking revenge on an immortal should not be an act that requires a repeat performance..." Sereno plays with a coin in his hands, before slamming it down on the desk. It stands up, thrilling on it\'s edge. "You know, with the company you keep, one might begin to assume you have a disregard for your own safety."',
    craftable: true,
    requirements: {
      "mariner.locations.medsea.marseille.mfm": 1,
    },
    grandReqs: {
      [READ_QUEST_FLAG("noonwater", "fled")]: 1
    },
    alt: [`${QUEST_PREFIX}.intel.router`],
    warmup: 15,
  });

  // repeatable find galantha
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intel.router`,
    actionId: "token",
    grandReqs: {
      [READ_QUEST_FLAG("noonwater", "fled")]: 1
    },
    linked: [
      { id: `${QUEST_PREFIX}.intel.marseille`, chance: 25 },
      { id: `${QUEST_PREFIX}.intel.haifa`, chance: 33 },
      { id: `${QUEST_PREFIX}.intel.constantinople`, chance: 50 },
      { id: `${QUEST_PREFIX}.intel.bizerta` },
    ],
    warmup: 75,
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intel.marseille`,
    actionId: "token",
    effects: { "mariner.locations.medsea.marseille.galantha.away": 1 },
    warmup: 15,
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intel.haifa`,
    actionId: "token",
    effects: { "mariner.locations.medsea.haifa.galantha.away": 1 },
    warmup: 15,
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intel.constantinople`,
    actionId: "token",
    effects: { "mariner.locations.medsea.constantinople.galantha.away": 1 },
    warmup: 15,
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.intel.bizerta`,
    actionId: "token",
    effects: { "mariner.locations.medsea.bizerta.galantha.away": 1 },
    warmup: 15,
  });
}
