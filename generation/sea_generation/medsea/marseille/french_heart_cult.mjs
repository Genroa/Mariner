import { READ_QUEST_FLAG, QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";
import { TIMER_LESS_THAN } from "../../../global_timers_generation.mjs";
import { LOCAL_TO, ME_LINK } from "../../../generation_helpers.mjs";
import { QUEST_ID, UPDATE_QUEST, generateQuest } from "../../../quest_generation.mjs";
import { generateDistanceMatchingMinigame } from "../../../distance_matching_minigame_generation.mjs";
import {
  SITUATION_STAGE,
  MINIGAME_STEP_EQUALS,
  generateScoreSequenceMinigame,
  QUESTION_STAGE,
  ELEMENT_IS_PRESENT,
  HIGHEST_LORE_QUANTITY,
  WARNING_ABOVE_THRESHOLD,
  WARNING_BELOW_THRESHOLD,
} from "../../../score_sequence_minigame_generation.mjs";

export function generateFrenchHeartCultQuest() {
  generateQuest({
    id: "frenchheartcult",
    label: "<FHC>",
    description: "<A young student was found dead in Bizerta. Rumours are pointing at strange stories coming from Marseille.>",
    updates: [
      // You talked to Hakim and he gave you the quest
      {
        id: "startedviahakim",
        label: "A Grizzly Death",
        description: "<I met Hakim. He implored me to look into his friend's death.>",
        sort: 1,
        slots: [
          {
            id: "findings",
            required: { "mariner.vaults.bizerta_morgue.casefile": 1 },
          },
        ],
      },
      // You ignored Hakim until Alexia gave you the Professor and you unmasked him
      {
        id: "startedviatheprofessor",
        sort: 1,
      },
      // Broad flag to know the quest started
      {
        id: "started",
        sort: 1,
        isHidden: true,
      },

      // You collected findings in the mortuary
      {
        id: "collectedfindings",
        sort: 2,
      },

      // You showed your findings to Hakim
      {
        id: "showedfindingstohakim",
        sort: 3,
      },

      // You showed your findings to Alexia
      {
        id: "showedfindingstoalexia",
        sort: 3,
      },

      // You completed the Grail Cult quest before completing this one (= you did the hospital). This update shows you it was a red herring.
      {
        id: "notthehospital",
        sort: 4,
      },

      // You followed the teacher and unmasked him
      // AFTER THIS UPDATE IS APPLIED, THE QUEST CHANGES NAME AND DESCRIPTION.
      // If you go talk to Alexia about this quest at this stage, and you don't have the FGC quest yet, she will give a special monologue and hand out the FGC card with the updates up to "nottheteacher" and redirect you towards the hospital lead.
      {
        id: "unmaskedprofessor",
        sort: 4,
      },

      // You stole the body. This could be applied before showing findings to Hakim, or after unmasking the teacher.
      {
        id: "stolebody",
        sort: 99,
      },

      // You delivered the body to the Ossuary
      {
        id: "completed",
        effects: {},
      },
    ],
  });

  // Unique interactions with the professor NPC
  mod.setRecipe("recipes.conversations.medsea.marseille", {
    id: "mariner.quests.frenchheartcult.bothertheprofessor",
    actionId: "talk",
    craftable: true,
    grandReqs: {
      "mariner.locations.medsea.marseille.professor": 1,
      "root/mariner.globalflags.frenchheartcult.unmaskedprofessor": -1,
      "mariner.routines.professor.day": 1,
    },
    label: "<Bothering the Professor?>",
    startdescription: "<>",
    description: "<>",
    warmup: 60,
    effects: { "mariner.notoriety": 1 },
  });

  mod.setRecipe("recipes.conversations.medsea.marseille", {
    id: "mariner.quests.frenchheartcult.bothertheprofessor",
    actionId: "talk",
    craftable: true,
    grandReqs: {
      "mariner.locations.medsea.marseille.professor": 1,
      "root/mariner.globalflags.frenchheartcult.unmaskedprofessor": -1,
      "[mariner.routines.professor.rest] + [mariner.routines.professor.rest2]": 1,
    },
    label: "<Bothering the Professor?>",
    startdescription: "<>",
    description: "<>",
    warmup: 60,
    effects: { "mariner.notoriety": 1 },
  });

  mod.setRecipe("recipes.conversations.medsea.marseille", {
    id: "mariner.quests.frenchheartcult.tailtheprofessor",
    actionId: "explore",
    craftable: true,
    grandReqs: {
      "mariner.locations.medsea.marseille.professor": 1,
      "root/mariner.globalflags.frenchheartcult.unmaskedprofessor": -1,
      "mariner.routines.professor.nightowl": 1,
    },
    label: "<Following the Professor?>",
    linked: [{ id: "mariner.minigames.distancematching.tailingprofessor.start" }],
  });
  mod.setRecipe("recipes.conversations.medsea.marseille", {
    id: "mariner.quests.frenchheartcult.unmaskprofessor",
    label: "<Unmasked Professor>",
    warmup: 60,
    effects: {
      "mariner.locations.medsea.ossuary.away": -1,
    },
  });

  generateScoreSequenceMinigame({
    id: "tailingprofessor",
    startScore: 10,
    scoreStyle: { label: "Distance From the Professor", description: "" },
    startRecipe: {},
    warnings: [WARNING_BELOW_THRESHOLD(2, "Dangerously close!"), WARNING_ABOVE_THRESHOLD(8, "Dangerously far!")],
    stages: [
      SITUATION_STAGE({
        id: "club",
        label: "<Diving into a Club>",
        startdescription: "<>",
        requirements: MINIGAME_STEP_EQUALS(1),
        availableActions: [
          {
            id: "lantern",
            requirements: HIGHEST_LORE_QUANTITY("lantern", 3),
            label: "<Use Lantern>",
            startdescription: "<>",
            outcomes: [{ id: "success", effects: { "mariner.minigames.score": 2 } } /*, failure outcome? Is there any check?*/],
          },
          {
            id: "edge",
            requirements: HIGHEST_LORE_QUANTITY("edge", 3),
            quantity: 3,
            label: "<Use Edge>",
            startdescription: "<>",
            outcomes: [{ id: "success", effects: { "mariner.minigames.score": -4 } } /*, failure outcome? Is there any check?*/],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "wards",
        label: "<Wards in the Street>",
        startdescription: "<>",
        requirements: MINIGAME_STEP_EQUALS(4),
        availableActions: [
          {
            id: "knock",
            requirements: HIGHEST_LORE_QUANTITY("knock", 3),
            label: "<Use Knock>",
            startdescription: "<>",
            outcomes: [{ id: "success", effects: { "mariner.minigames.score": 2 } } /*, failure outcome? Is there any check?*/],
          },
          {
            id: "winter",
            requirements: HIGHEST_LORE_QUANTITY("winter", 3),
            label: "<Use Winter>",
            startdescription: "<>",
            outcomes: [{ id: "success", effects: { "mariner.minigames.score": -2 } } /*, failure outcome? Is there any check?*/],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "port",
        label: "<He's Crossing Over the Port Waters>",
        startdescription: "<>",
        grandReqs: MINIGAME_STEP_EQUALS(6),
        availableActions: [
          {
            id: "forge",
            requirements: HIGHEST_LORE_QUANTITY("forge", 3),
            label: "<Use Forge>",
            startdescription: "<>",
            outcomes: [
              // TODO: this action success makes no sense
              { id: "success", effects: {} } /*, failure outcome? Is there any check?*/,
            ],
          },
          {
            id: "heart",
            requirements: HIGHEST_LORE_QUANTITY("heart", 3),
            label: "<Use Heart>",
            startdescription: "<>",
            outcomes: [{ id: "success", effects: { "mariner.minigames.score": 3 } } /*, failure outcome? Is there any check?*/],
          },
        ],
      }),
      SITUATION_STAGE({
        id: "streets",
        label: "<Tailing Professor>",
        startdescription: "<>",
        availableActions: [
          {
            id: "oddcorners",
            requirements: ELEMENT_IS_PRESENT("mariner.oddcorners"),
            label: "",
            startdescription: "",
            doNotExpulseHelp: true,
            outcomes: [
              {
                id: "success",
                furthermore: [
                  {
                    effects: {
                      "mariner.oddcorners": -1,
                      "mariner.minigames.score": -15,
                    },
                  },
                  { effects: { "mariner.minigames.score": 5 } },
                ],
              } /*, failure outcome? Is there any check?*/,
            ],
          },
          {
            id: "lantern",
            requirements: HIGHEST_LORE_QUANTITY("lantern", 3),
            label: "<>",
            startdescription: "<>",
            outcomes: [{ id: "success", effects: { "mariner.minigames.score": -2 } } /*, failure outcome? Is there any check?*/],
          },
        ],
      }),
    ],
    endings: [
      {
        id: "lostprofessor",
        requirements: { "mariner.minigames.score": 20 },
        label: "<Lost Professor>",
      },
      {
        id: "unmaskprofessor",
        requirements: { "mariner.minigames.step": 7 },
        linked: [{ id: "mariner.quests.frenchheartcult.unmaskprofessor" }],
      },
    ],
  });

  // generateDistanceMatchingMinigame({
  //   id: "tailingprofessor",
  //   postWarmupRecipe: {
  //     linked: [
  //       {
  //         requirements: { "mariner.minigames.step": 7 },
  //         linked: [{ id: "mariner.quests.frenchheartcult.unmaskprofessor" }],
  //       },
  //       {
  //         id: "mariner.minigames.distancematching.tailingprofessor.updatedistance",
  //       },
  //     ],
  //   },
  //   distanceElement: {
  //     label: "Distance From the Professor",
  //   },
  //   states: [
  //     {
  //       id: "club",
  //       warmupRecipe: {
  //         grandReqs: { "[mariner.minigames.step] = 1": 1 },
  //         label: "<Diving into a Club>",
  //         startdescription: "<>",
  //       },
  //       tooHighRecipe: { label: "<We're Losing Him!>", startdescription: "<>" },
  //       tooLowRecipe: { label: "<We're too close!>", startdescription: "<>" },
  //       postWarmupRecipe: {},
  //       helpEffects: [
  //         {
  //           aspect: "lantern",
  //           quantity: 3,
  //           label: "<Use Lantern>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": 2 },
  //           },
  //         },
  //         {
  //           aspect: "edge",
  //           quantity: 3,
  //           label: "<Use Edge>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": -4 },
  //           },
  //         },
  //       ],
  //     },
  //     {
  //       id: "wards",
  //       warmupRecipe: {
  //         grandReqs: { "[mariner.minigames.step] = 4": 1 },
  //         label: "<Wards in the Street>",
  //         startdescription: "<>",
  //       },
  //       tooHighRecipe: { label: "<We're Losing Him!>", startdescription: "<>" },
  //       tooLowRecipe: { label: "<We're too close!>", startdescription: "<>" },
  //       postWarmupRecipe: {},
  //       helpEffects: [
  //         {
  //           aspect: "knock",
  //           quantity: 3,
  //           label: "<Use Knock>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": 2 },
  //           },
  //         },
  //         {
  //           aspect: "winter",
  //           quantity: 3,
  //           label: "<Use Winter>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": -2 },
  //           },
  //         },
  //       ],
  //     },
  //     {
  //       id: "port",
  //       warmupRecipe: {
  //         grandReqs: { "[mariner.minigames.step] = 6": 1 },
  //         label: "<He's Crossing Over the Port Waters>",
  //         startdescription: "<>",
  //       },
  //       tooHighRecipe: { label: "<We're Losing Him!>", startdescription: "<>" },
  //       tooLowRecipe: { label: "<We're too close!>", startdescription: "<>" },
  //       postWarmupRecipe: {},
  //       helpEffects: [
  //         {
  //           aspect: "forge",
  //           quantity: 3,
  //           label: "<Use Forge>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": 0 },
  //           },
  //         },
  //         {
  //           aspect: "heart",
  //           quantity: 3,
  //           label: "<Use Heart>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": 3 },
  //           },
  //         },
  //       ],
  //     },
  //     {
  //       id: "streets",
  //       warmupRecipe: { label: "<Tailing Professor>", startdescription: "<>" },
  //       tooHighRecipe: { label: "<We're Losing Him!>", startdescription: "<>" },
  //       tooLowRecipe: { label: "<We're too close!>", startdescription: "<>" },
  //       postWarmupRecipe: {},
  //       helpEffects: [
  //         {
  //           id: "oddcorners",
  //           aspect: "mariner.oddcorners",
  //           label: "",
  //           startdescription: "",
  //           noInduction: true,
  //           recipe: [
  //             {
  //               effects: {
  //                 "mariner.oddcorners": -1,
  //                 "mariner.minigames.score": -15,
  //               },
  //             },
  //             { effects: { "mariner.minigames.score": 5 } },
  //           ],
  //         },
  //         {
  //           aspect: "lantern",
  //           quantity: 3,
  //           label: "<>",
  //           startdescription: "<>",
  //           recipe: {
  //             effects: { "mariner.minigames.score": -2 },
  //           },
  //         },
  //       ],
  //     },
  //   ],
  // });

  // Ossuary + end of quest generation

  mod.initializeRecipeFile("recipes.quests.frenchheartcult", ["quests"]);
  mod.setAspects("aspects.seas.uniquenessgroups", {
    id: "mariner.locations.medsea.ossuary.uqg",
  });
  mod.setElements(
    "locations.medsea",
    {
      id: "mariner.locations.medsea.ossuary.away",
      $derives: ["mariner.composables.port"],
      overlays: [
        {
          image: "$background",
          layer: "background",
        },
      ],
      xexts: { $background: "locations/medsea/ossuary" },
      label: "The Ossuary",
      description: "<>",
      aspects: {
        "mariner.destination": 1,
        "mariner.locations.medsea.sailingthere": 1,
      },
      uniquenessgroup: "mariner.locations.medsea.ossuary.uqg",
      xtriggers: {
        "mariner.signal.arrivetodestination": ME_LINK("mariner.quests.frenchheartcult.end.waiting"),
        "mariner.signal.lockport": "mariner.locations.medsea.ossuary.locked",
      },
    },
    {
      id: "mariner.locations.medsea.ossuary.locked",
      $derives: ["mariner.composables.reputabledestination", "mariner.composables.port"],
      xexts: { $background: "locations/medsea/ossuary" },
      uniquenessgroup: "mariner.locations.medsea.ossuary.uqg",
      decayTo: "mariner.locations.medsea.ossuary.away",
      label: "The Ossuary",
      description: "<>",
      aspects: {
        "mariner.destination.locked": 1,
        "mariner.locations.medsea.presentthere": 1,
      },
      xtriggers: {
        "mariner.signal.arriveinsea.medsea": "mariner.locations.medsea.ossuary.away",
      },
    },
    {
      id: "mariner.inspirations.locations.winter",
      label: "<Memory: the Ossuary>",
      description: "<>",
      aspects: {
        "mariner.inspiration": 1,
        "mariner.inspiration.location": 1,
        winter: 4,
        "mariner.inspiration.winter": 4,
      },
    }
  );

  mod.setRecipes(
    "recipes.quests.frenchheartcult",
    {
      id: "mariner.quests.frenchheartcult.end.waiting",
      slots: [
        {
          id: "body",
          label: "Body",
          description: "<>",
          required: { "mariner.vaults.bizerta_morgue.body": 1 },
        },
      ],
      warmup: 30,
      linked: [{ id: "mariner.quests.frenchheartcult.end" }, { id: "mariner.quests.frenchheartcult.end.missingbody" }],
    },
    {
      id: "mariner.quests.frenchheartcult.end",
      requirements: { "mariner.vaults.bizerta_morgue.body": 1 },
      label: "<Delivering the Body>",
      startdescription: "",
      warmup: 30,
      furthermore: [
        //1. Complete the quest
        UPDATE_QUEST("frenchheartcult", "completed"),
        //2. Remove the body, the quest card and the Ossuary, prepare for open sea sailing
        {
          effects: {
            // Complete quest and cleanup
            [QUEST_ID("frenchheartcult")]: -1,
            "mariner.vaults.bizerta_morgue.body": -1,
            "mariner.locations.medsea.ossuary.uqg": -1,
            // Winter location inspiration
            "mariner.inspirations.locations.winter": 1,
          },
        },
      ],
      linked: [{ id: "mariner.sailing.openseamainloop" }],
    },
    {
      id: "mariner.quests.frenchheartcult.end.missingbody",
      requirements: { "mariner.vaults.bizerta_morgue.body": -1 },
      label: "<Delivering the Body: no body?>",
      startdescription: "",
      warmup: 30,
      furthermore: [
        {
          movements: {
            "~/tabletop": ["mariner.locations.medsea.ossuary.uqg"],
          },
        },
      ],
      linked: [{ id: "mariner.sailing.openseamainloop" }],
    }
  );
}
