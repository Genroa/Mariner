import { generateQuest } from "../../../quest_generation.mjs";

export function generateNoonWaterQuestCard() {
  generateQuest({
    id: "noonwater",
    // Quest card informations
    label: "Noon Water and its Mistress",
    description:
      "<A prize allready won, the Noonwater I had in my position has been pilfered away. I need to retrieve it, if I am to complete the Letheian Ritual and restore my Wist.>",
    // Quest stages
    updates: [
      {
        id: "pilfered",
        label: "Noon Water Pilfered",
        description:
          "A Frightful presence descended upon the ship, and pilfered the noonwater away. Uncanny as the incident was, it was someone of flesh and blood, and thus someone who could be pursued and confronted.",
      },
      {
        id: "indignation",
        icon: 2,
        label: "A Crime-Lords Indignation",
        description:
          "The water was stolen from you? Well, with an item so in demand as that, one should not be surprised. There are bound to be other interested parties.But it will not do for my business partner to get singled out. ",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.indignant"],
            },
          },
        },
      },
      {
        id: "limiean",
        icon: 3,
        label: "The Galantha",
        description:
          "The Galantha is an old Long, dedicated to the Traditions of Ivory. Where they have spend the past millennium is mostly unknown, but it seems they have now returned to the Iberian peninsula.",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.s"],
            },
          },
        },
      },
      {
        id: "fled",
        icon: 3,
        label: "The Shadow of The Galantha",
        description:
          "The Galantha has left me behind, but I still need the water she stole from me. Perhaps Sereno can help me again.",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.s"],
            },
          },
        },
      },
      {
        id: "bargained",
        icon: 4,
        label: "The Galantha: Bargained",
        description:
          '"As simple as that." The Galantha has returned the Noon water to me, in exchange for the location of Abzu.',
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.s"],
            },
          },
        },
      },
      {
        id: "sunk",
        icon: 4,
        label: "The Galantha: Sunk",
        description:
          "We left the Galantha beneath the waves. Sometimes we still feel the eyes peeling up at me.",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {
              add: ["codex.sereno.s"],
            },
          },
        },
      },
      {
        id: "completed",
        effects: {
          updateCodexEntries: {
            "codex.sereno": {},
          },
        },
      },
    ],
  });
}
