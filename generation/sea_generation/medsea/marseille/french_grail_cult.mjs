import {
  READ_QUEST_FLAG,
  QUEST_FLAG_ID,
} from "../../../global_flags_generation.mjs";
import { TIMER_LESS_THAN } from "../../../global_timers_generation.mjs";
import { LOCAL_TO } from "../../../generation_helpers.mjs";
import { generateQuest } from "../../../quest_generation.mjs";

export function generateFrenchGrailCultQuest() {
  const RECIPE_FILE = mod.initializeRecipeFile(
    "recipes.medsea.marseille.quest.frenchgrailcult",
    ["locations", "medsea", "marseille"]
  );
  const ELEMENT_FILE = mod.initializeElementFile(
    "medsea.marseille.quest.frenchgrailcult",
    ["locations", "medsea", "marseille"]
  );
  const QUEST_PREFIX = "mariner.northsea.copenhagen.frenchgrailcult";
  const MYSTERY_PREFIX = `${QUEST_PREFIX}.mystery`;

  // mod.setRecipe(RECIPE_FILE, {
  //   id: `${QUEST_PREFIX}.tooearly`,
  //   actionId: "talk",
  //   craftable: true,
  //   // Talk to Alexia, before the student's death becomes known, and after the med sea cult narrative started
  //   grandReqs: {
  //     ...LOCAL_TO("medsea", "marseille", "alexia"),
  //     ...TIMER_LESS_THAN("medsea.studentdeath", 6),
  //     [READ_QUEST_FLAG("medseacults", "started")]: 1,
  //   },
  //   label: "<Too Early>",
  //   startdescription: '"Why the long face? Why do you ask? Why do you care?">',
  //   description: '""',
  //   unlockCodexEntries: ["codex.alexia"],
  //   updateCodexEntries: {
  //     "codex.frenchgrailcult": {
  //       add: ["codex.frenchgrailcult.1"],
  //       unlock: true,
  //     },
  //   },
  //   warmup: 60,
  // });

  generateQuest({
    id: "frenchgrailcult",
    label: "<FGC>",
    description:
      "<Alexia is pursuing rumours of something very bad in Marseille. She wants me to pursue some leads for her.>",
    updates: [
      {
        id: "completed",
        effects: {
          // updateCodexEntries: {
          //   "codex.veiledlady": {
          //     add: ["codex.veiledlady.2"],
          //   },
          // },
        },
      },
    ],
  });
}
