import { IN_SEA, IN_PORT, LOCAL_TO, specialWanderingEventScaffhold } from "../../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../global_flags_generation.mjs";
import { TIMER_BETWEEN, TIMER_ELAPSED, TIMER_LESS_THAN } from "../../global_timers_generation.mjs";
import { UPDATE_QUEST } from "../../quest_generation.mjs";

function generateStudentLocalDeathEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold("medsea.quest.medseacults.studentdeath.localevent");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    tablereqs: IN_PORT("medsea", "bizerta"),
    grandReqs: {
      [READ_QUEST_FLAG("medseacults", "started")]: 1,
      ...TIMER_LESS_THAN("medsea.studentdeath", 10),
    },
    label: `<Heavy Atmosphere>`,
    startdescription: "<>",
    maxexecutions: 1,
    warmup: 20,
  });
}

function generateStudentDeathBroadcastEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold("medsea.quest.medseacults.studentdeath.newsofdeath");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    tablereqs: IN_SEA("medsea"),
    grandReqs: {
      [READ_QUEST_FLAG("medseacults", "started")]: 1,
      ...TIMER_BETWEEN("medsea.studentdeath", 10, 20),
    },
    label: `<News of a Grizzly Death in Newspaper>`,
    startdescription: "<>",
    maxexecutions: 1,
    warmup: 20,
  });
}

function generateStudentDeathFriendCallEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold("medsea.quest.medseacults.friendcall");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea"]);
  mod.initializeElementFile("medseacult.quest.items", ["locations", "medsea"]);
  mod.setElement("medseacult.quest.items", {
    id: "mariner.quests.medseacult.newspaperclipping",
    label: "<Newspaper clip>",
    description: "",
    aspects: { "mariner.topic": 1 },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    tablereqs: IN_SEA("medsea"),
    grandReqs: {
      [READ_QUEST_FLAG("medseacults", "started")]: 1,
      ...TIMER_ELAPSED("medsea.studentdeath", 20),
    },
    label: `<Appeal in a Newspaper>`,
    startdescription: "<>",
    effects: { "mariner.quests.medseacult.newspaperclipping": 1 },
    maxexecutions: 1,
    warmup: 20,
  });

  // Finding Hakim
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.findhakim.hint",
    hintOnly: true,
    requirements: { "mariner.quests.medseacult.newspaperclipping": 1 },
    label: `<Finding "Hakim"?>`,
    startdescription: "<I need to be in Bizerta before I try to find the local address printed in this newspaper clip>",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.quests.medseacults.findhakim",
    craftable: true,
    requirements: { "mariner.quests.medseacult.newspaperclipping": 1 },
    grandReqs: IN_PORT("medsea", "bizerta"),
    label: `<Finding "Hakim">`,
    startdescription: "<>",
    effects: {
      "mariner.locations.medsea.bizerta.hakim.wary": 1,
    },
    warmup: 60,
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.quests.frenchheartcult.meethakim",
    actionId: "talk",
    craftable: true,
    grandReqs: {
      ...LOCAL_TO("medsea", "bizerta", "hakim"),
      [READ_QUEST_FLAG("medseacults", "started")]: 1,
    },
    maxexecutions: 1,
    label: "<Talking to Hakim>",
    startdescription: "<>",
    description: "<>",
    furthermore: [
      {
        effects: {
          "mariner.locations.medsea.bizertamorgue.away": 1,
          "mariner.quests.frenchheartcult": 1,
        },
      },
      UPDATE_QUEST("frenchheartcult", "started"),
      UPDATE_QUEST("frenchheartcult", "startedviahakim"),
    ],
    warmup: 60,
  });
}

export function generateStudentDeathEvents() {
  generateStudentLocalDeathEvent();
  generateStudentDeathBroadcastEvent();
  generateStudentDeathFriendCallEvent();
}
