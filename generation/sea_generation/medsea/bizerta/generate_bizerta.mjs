import { generatePort } from "../../../port_generation/port_generation.mjs";
import { market } from "../../../port_generation/helpers.mjs";
import { theater } from "../../../port_generation/helpers.mjs";
import { theaterSlots } from "../../../port_generation/helpers.mjs";
import { HAKIM, HAKIM_WARY } from "./hakim.mjs";
import { Galantha } from "../galantha.mjs";

export function generateBizerta() {
  generatePort({
    portRegions: ["medsea"],
    portId: "bizerta",
    label: "Bizerta",
    description:
      "On the Northernmost tip of Africa, lies a port founded by the Phoenicians. Where Carthage is dust on dust, Bizerte thrives and glitters.",
    recipeDescriptions: {
      docking:
        "Bizerta lies in the crest between a lake of Emerald and a sea of Saphire. Of it's many docks, harbors and marina's, we dock in the Vieux Port",
      exploreHH:
        "<From the Old Harbor, the many-coloured houses and the cool breeze of the water gives way white buildings and loud bazaars. If I apply my charm, I may find extrordinary peoples, places, or opportunities.>",
      exploreLH:
        "<From the Old Harbor, the many-coloured houses and the cool breeze of the water gives way white buildings and loud bazaars. If I stay open and receptive, I may find discover the peculiar sounds of the city, or be approached by a Raconteur.>",
    },
    storyTrading: {
      story: {
        label: "Sky Stories",
        description: "<>",
        aspects: {
          moth: 1,
          edge: 1,
          "mariner.storyaspect.mortal": 1,
          "mariner.storyaspect.rousing": 1,
          "mariner.storyaspect.grim": 1,
        },
        icon: "thosewhodonotsleep",
      },
      label: "Trade Stories at the Streets of Bizerta",
      startdescription:
        "<Stories hang in the air refracted rainbows from the white, baking walls, or are written in dust on the ground. If I engage with the locals, I can pluck Haifa's story and tell it as my own.>",
      description: "The Djinn arrived in Tunisia at the same time as the arabic language, but have found these desert just as habitable as the ones from their home penisula. Listen to the story of the djinn and there princes.",
    },
    tunes: ["grailedge", "heartgrail", "mothedge", "mothheart"],
    barName: "Merset",
    wharf: {
      cargoOptions: [
        { portRegion: "medsea", distance: "local", label: "Med Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
        { portRegion: "northsea", distance: "close", label: "North Sea" },
      ],
    },
    portElements: [
      HAKIM,
      HAKIM_WARY,
      Galantha,
      market({
        id: "naturalmarket",
        label: "<The House of the Green Crescent>",
        description:
          "<Cool, green glazed pannels; the soft tinkling of dripping water; shade under a tralliswork on the edge of the desert stands a great hortuarium has been founded  build uponthose old oasis. From the nurture of those c'thonic water many species now flourish in the shelter of the Green House.>",
        types: ["natural", "crafts", "artifact"],
        storyTrading: {
          story: {
            label: "Sand Stries>",
            icon: "locksmithsdream2",
            description: "<>",
            aspects: {
              forge: 1,
            },
          },
          label: "<Trade Stories at the House of the Green Crescent>",
          startdescription:
            "<The Green House stands on the last stop before the city of old Bizerta is reached. But now the city is growing, and louder and brighter. But still the stories from the Atlas mountains and the Berbers reach the Green House first.>",
          description:
            "<Tea is served in the House shade. Travellers sit cross legged on the many pillows and benches. Listen to the stories of the sun and of the desert mouse>",
        },
      }),
      theater({
        id: "placedesol",
        label: "Place de Sol",
        description:
          "Technically, street performances are forbidden everywhere in the city, practically, puppetry and storytellers can be found on every corners, but essentially, the heart of Bizerte Racconteurs can be found on Place de Sol. The old arts of the griots of the south and the storytellers of the dunes meet with the ingenious puppetry the Ottoman Empire brought here three centuries ago.Any storyteller of each tradiition can unite here in ttheir craft and, of course, the mocking of Europeans. ",
        slots: theaterSlots(["mariner.story", "mariner.song", "mariner.trapping"]),
        storyTrading: {
          story: {
            label: "Madama in Triplicate",
            description: "<>",
            aspects: {
              moth: 2,
              heart: 2,
              "mariner.storyaspect.mortal": 1,
              "mariner.storyaspect.dionysian": 1,
              "mariner.storyaspect.rousing": 1,
            },
            icon: "franklinbancroftdiaries",
          },
          label: "Trade Stories at the Place de Sol",
          startdescription:
            "Stories are told in layers, and stories are told in frames. In this case, the frame is a small wooded box, on the blazing heat of the oldest continent, between billowing layers of tarp. but the frame is also the storyteller, bright-eyed and dust-drenched, the country, occupied first by one master to the west and now another to the north.",
          description:
            "I watch this story with all the schizophrenic delight in three layers at once, the childrens laughing, the wisdom passing on, the the revolution brewing.",
        },
        options: {
          notDiscoverable: true,
        },
      }),
    ],
  });

  // He's afraid, doesn't know if you can be trusted, really wants to know more about his friend's death.
  // So this recipe is supposed to be a catch-all.
  mod.setRecipe("recipes.conversations.medsea.bizerta", {
    id: "mariner.conversations.medsea.bizerta.hakim.wary",
    craftable: true,
    actionId: "talk",
    label: "<Too Wary To Talk>",
    startdescription: "<>",
    description: "<Hakim is still too weary to talk.>",
    requirements: {
      "mariner.locations.medsea.bizerta.hakim.wary": 1,
    },
  });
}
