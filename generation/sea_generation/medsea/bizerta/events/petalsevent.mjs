import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generatePetalsEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.petals"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Dust and Petals`,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.petals", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.petals") },
    startdescription:
      'Streamers are hanging from the palms like many-coloured snakes. The plurality of music is sometimes interrupted by a great stomping thunder roll, as the horses race across the desert flats outside. "The main celebration is in Douz, but we keep our own private rememberences for our debt to the Berbers." The house keeper explains to me. Exchanges of gifts, bud for flesh, seed for hide, son for daughter. After the horses have disappeared once again into the dust, some of the desert dwellers remain. I can offer them a stay on my ship, and heed their talents... though it is unlikely they will know much about sailing.',
    effects: {
      "mariner.crew.specialist.cook.opportunity.free": 1,
    },
    tablereqs: { "mariner.locations.medsea.bizerta.naturalmarket": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
