import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateGuignolEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.guignol"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Guignol and the Littlest Opera`,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.guignol", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.guignol") },
    startdescription:
      'The authorities are always lenient of the stalls that put up the Guignol dolls. In theory, this means they play only the permitted European plays. But I am surprised that instead of Romeo and Juliet or the  I am greated to les humeurs d\'une Bourgeois gentilhomme. Samuel Savage is not quite a classic, but his snide dialogue translates surprisingly well itno the slapstick comedy of the puppetry. Guignol stays remarkably aloof during the show, as if the other puppets keep their distance from his egg-shaped visage and stabbed nails eyes.',
    effects: {
      "mariner.influences.lantern": 4,
      "mariner.influences.forge": 4,
      "mariner.influences.knock": 4,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
