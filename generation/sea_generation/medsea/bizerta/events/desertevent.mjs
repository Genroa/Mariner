import { setWanderingEventChance, wanderingEventScaffhold } from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { INJURE_CREWMEMBER } from "../../../../generation_helpers.mjs";
import { SIGNALS } from "../../../../generate_signal_aspects.mjs";

export function generateDesertEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("medsea.randomevent.desert");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<The Wind That Bleeds>`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.desert", 25),
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.desert") },
    startdescription:
      "It is a rare and bitter pleasure when life's misfortunes come to you announcing their presence in advance. A dark line on the horizon is the first indication, and as more people stop to look in the direction we gaze, the darkness grows. Soon movement explodes. First of the locals trippling their efforts to get all things sorted and safe inside, and then the howling and flaying of the air itself. The Haboob has reached us, and with dusty tongues it strips skin, tills earth and peels wood. We must wait and see what comes out of the Haboob unscathed.",
    tablereqs: { "mariner.weathermeta.sandstorm": -1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.HARMATTAN_SET_SANDSTORM]: 1 },
      },
    ],
    linked: [
      {
        id: "medsea.randomevent.desert.damageship",
        chance: 25,
      },
      {
        id: "medsea.randomevent.desertdamageme",
        chance: 33,
      },
      {
        id: "medsea.randomevent.desertdamagecrew",
        chance: 50,
      },
      {
        id: "medsea.randomevent.desert.damagetool",
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);

  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.desert.damageship",
    label: `<Splinters from Wood>`,
    startdescription: "My ship takes the brunt of the damage",
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.ship",
            mutate: "mariner.ship.damage",
            level: 1,
            additive: true,
          },
        ],
      },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.desert.damageme",
    label: `<Blood from Skin>`,
    startdescription: " I am whipped and flayed and bled.",
    effects: { "mariner.experiences.pain": 1 },
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.desert.damagecrew",
    label: `<Breath from Throat>`,
    startdescription: "One of my crew gets caught unaware.",
    inductions: [INJURE_CREWMEMBER],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "medsea.randomevent.desert.damagetool",
    label: `<Tool from Grip>`,
    startdescription: "Such a small thing, so minor, but this too can be ruined by the flurry of wind and earth",
    furthermore: [
      {
        target: "~/exterior",
        aspects: {
          [SIGNALS.MISUSE_TOOL]: 1,
        },
      },
    ],
  });
}
