import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateShipyardEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.shipyard"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<An Exchange of Talent>`,
    effects: { funds: 4 },
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.crew.specialist.carpenter": -1 },
      }
    ],
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.shipyard", 25),
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.shipyard") },
    startdescription:
      '"One amongst your crew, captain, has expressed an interest in learning of our ways." The Shipwright\'s eyes stand judging as ever, but wether that emotion is aimed at me, my crewmember or the wider world is hard to tell. "I cannot promise you they will be returned to you, but I can promise you that if they are, they will be wiser then they were before." in two moons we can be sure that their education will be at its end.',
    tablereqs: { "mariner.crew.specialist.carpenter": 1 }, //Todo: add the effec: the carpenter should be taken in, and returned with a new trait, that allows it to give some bonus
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
