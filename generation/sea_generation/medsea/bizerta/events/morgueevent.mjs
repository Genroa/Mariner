import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateMorgueEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.morgue"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<>`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.morgue", 25),
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.morgue") },
    startdescription:
      "At the Merset Bar, you meet a man three cups deep in despair. He laments the curious misfortunes that have befallen him to the uncurious crowd around. He was recently fired from his job at the coroners office, for an incident completely outside of his control. This thrice cursed country conspires for his misery specifcally, he is quite sure. when he hears you are a captain, he wonders if he can book passage on your ship. I don't have much cash, but I have some supplies I pilfered before i left. most will help stich up a living body as well as a dead one.",
    effects: {
      "mariner.medicalsupplies": 1,
      "mariner.opportunity.passenger": 1, //todo check if the morgue is done.
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
