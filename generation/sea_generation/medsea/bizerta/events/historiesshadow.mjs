import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateMythologyShadowEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.mythologiesshadow"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Mythologies' Long Shadow`,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.mythologiesshadow", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.mythologiesshadow") },
    startdescription:
      'A white sheet is framed by the cerulian sky. and on the sheet, projected, shades dance. with golden tongue and many hands. A Hero arrives.Two Lovers quarrels, A Queen Burns. A curse weaves into the fate of two empires. I watch the flickering figures and hear the gas lamp hiss.',
    effects: {
      "mariner.influences.forge": 4,
      "mariner.influences.edge": 4,
      "mariner.influences.winter": 4,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
