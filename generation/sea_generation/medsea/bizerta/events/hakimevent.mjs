import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateHakimEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.hakim"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Of endeavors great and small`,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.hakim", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.hakim") },
    startdescription:
      'Early in the morning I find Hakim looking out over the returning fishermen. He smiles at me. "You have blessed me with certainty, which has somehow left me feeling quite unsteady. What should come next for me? Mortimer and me had started our journeys both in pursuit of great things, yet somehow I have been living my life one course at a time. Do I return to that? I can\'t say the life of the vagrant scholar holds the same charm to me it once did. . is this the place to start my great work? not capitalised of course... but perhaps Bizerta can be where I make something of my life." I leave him at the docks, as the sun paints her golden road on the water.',
    effects: {
      "mariner.influences.forge": 4,
      "mariner.influences.moth": 4,
      "mariner.influences.winter": 4,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
