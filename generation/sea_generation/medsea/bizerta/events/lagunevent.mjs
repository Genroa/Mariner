import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateLagunEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "medsea.randomevent.lagun"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<A Stranger in a Strange Land>`,
    grandReqs: {
      ...TIMER_ELAPSED("medsea.randomevent.lagun", 25),
    },
    rootAdd: { ...RESET_TIMER("medsea.randomevent.lagun") },
    startdescription: "Under the dappled shade of the Green Maisons trelliswork,I find a man browsing between the flowers. cloaked as if in deep cold, but he smiles a crooked smile under a crooked eyebrew, and tells us he is looking for a gift to decorate a grave. The man does not appear to be impressed by the wares of the maison, but perhaps we could supply?",
    tablereqs: { "mariner.ship.grammarie": 1, },
    slots: [
      {
        id: "offer",
        label: "on offer?",
        description: "The stranger is seeking some flowers for a special funerary arrangement",
        required: "mariner.trappingtypes.natural",
      },],
    linked: [{ id: "medsea.randomevent.lagun.continued", requirements: { "mariner.vaults.eddangerforest.flower": 1 }, warmup: 30, label: "Tomb-setting", startdescription: "Thank you, traveler. this is just what i was looking for. When I am ready, I will make sure you recieve an invitation.", effects: { "mariner.trappingtypes.natural": -1, "mariner.scrapofinformation": 1 } }]
  }); //todo set a flag that you have completed this event, so we can bring it back better.
  setWanderingEventChance(EVENT_START_ID, 20);
}
