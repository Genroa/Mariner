import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateKarakouzEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.karakouz"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "medsea", "bizerta"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Karakouz and the Nameday Riddle`,
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.karakouz", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.karakouz") },
    startdescription:
      'There is illicit, there is indecent and there is occult, each of these the authoritities try to keep from the public, each has there own appeal. But by all three being underground, finding one often leads to finding the other. On the place de Sol I find a hasteily erected shadow theater of Karakouz, who is as sly as he is virile. Madama is throwing a grand féte to celebrate her birthday, but Karakouz claims she stole that date from him. So he intends to ruin her grand fete, by force or guile. He will have to sneak past the Mores in the coffee house, but he as always, they cannot find Karakouz, nor harm him.',
    effects: {
      "mariner.influences.grail": 4,
      "mariner.influences.moth": 4,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
