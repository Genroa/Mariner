import {
  LOCAL_TO,
  specialWanderingEventScaffhold,
} from "../../../generation_helpers.mjs";

export function generateCopenhagenMermaidEvent() {
  let { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold(
    "northsea.copenhagen.randomevent.mermaid"
  );

  mod.initializeRecipeFile(RECIPE_FILE, [
    "locations",
    "northsea",
    "copenhagen",
  ]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Familiar Story, and a Familiar Name`,
    tablereqs: { ...LOCAL_TO("northsea", "copenhagen", "harald") },
    description:
      "As my ramblings took me past the statuesque Royal Danish Theater, I noticed an old weatherworn bill. It depicts a woman, arms outstretched to the sky, head cast back in agony or exstacy. It is a striking image, but with it I also notice the name of the composer... Harald Blomkwist.",
    effects: { "mariner.stories.mermaid.source": 1 },
    maxexecutions: 1,
  });
}
