import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateStatueEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.statue"
  );
  mod.initializeRecipeFile(RECIPE_FILE, [
    "locations",
    "northsea",
    "copenhagen",
  ]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `An Encounter with a Statue`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.statue", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.statue") },
    startdescription:
      "Last night I encountered a young woman, staring out diligently over the Oresund. It is dedicated to one storyteller, but it was only commissioned after a certain Opera hit the stage...",
    effects: { contentment: 1 },
    tablereqs: { "mariner.stories.mermaid": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
