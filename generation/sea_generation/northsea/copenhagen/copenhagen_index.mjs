import { generateCopenhagenMermaidEvent } from "./mermaidevent.mjs";
import { generateDivisionEvent } from "./divisionevent.mjs";
import { generateHaraldEvent } from "./haraldevent.mjs";
import { generateResinEvent } from "./ibnyahya.mjs";
import { generateStatueEvent } from "./statueevent.mjs";
import { generateWolfEvent } from "./wolfevent.mjs";

export function generateCopenhagenStreetsEvents() {
    generateCopenhagenMermaidEvent();
    generateDivisionEvent();
    generateHaraldEvent();
    //generateResinEvent();
    generateStatueEvent();
    generateWolfEvent();
}
