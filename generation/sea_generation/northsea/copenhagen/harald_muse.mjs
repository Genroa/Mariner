import { LOCAL_TO } from "../../../generation_helpers.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";

export function generateHaraldMuseQuest() {
  const RECIPE_FILE = mod.initializeRecipeFile(
    "recipes.northsea.copenhagen.quest.haraldmuse",
    ["locations", "northsea", "copenhagen"]
  );
  const ELEMENT_FILE = mod.initializeElementFile(
    "northsea.copenhagen.quest.haraldmuse",
    ["locations", "northsea", "copenhagen"]
  );
  const QUEST_PREFIX = "mariner.northsea.copenhagen.haraldmuse";
  const MYSTERY_PREFIX = `${QUEST_PREFIX}.mystery`;

  // Beginning
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.first`,
    label: "An Invitation from the Maestro",
    actionId: "talk",
    startdescription:
      "I arrive at a house as joyous as a mausoleum. Inside awaits Harald Blomkwist, a once renowned musical virtuoso. Now, however, he sits in eerie silence. But he is appearantly willing to break that silence to offer me a job.",
    description:
      '"She was taken from me, and now I cannot find her." Harald limps through the sun room towards piano. "She is a dancer, my muse. Knew as much about the Suns\' House as I did. I think she tried to tell me where she went... But I do not understand it. Can you find her for me?" He collects scribbled musical notes strewn on the piano. "I need her."',
    maxexecutions: 1,
    craftable: true,
    effects: { "mariner.mysteries.dancer": 1 },
    requirements: {
      ...LOCAL_TO("northsea", "copenhagen", "harald"),
    },
    unlockCodexEntries: ["codex.harald"],
    warmup: 60,
  });

  // MYSTERY
  mod.setElement(ELEMENT_FILE, {
    id: "mariner.mysteries.dancer",
    overlays: ["locations/northsea/london/unmarkedgrave", "mystery_overlay"],
    label: "Harald's Missing Muse",
    description:
      "Once, Harald spent a long summer with a Dancer-Adept in Hamburg. When they got seperated during the socialist riots, she yelled at him that they should reconvene at the Shedders' Club for she planned to follow the Old Way. He never figured out what she meant.",
    aspects: {
      "mariner.mystery": 1,
    },
    slots: [
      {
        id: "missing.place",
        actionId: "mariner.navigate",
        label: "Missing Place",
        description: "this mystery requires a location",
        required: {
          "mariner.location": 1,
          "mariner.port": 1,
        },
      },
      {
        id: "Missing.song",
        actionId: "mariner.navigate",
        label: "Missing Song",
        description: "this mystery requires a song",
        required: {
          "mariner.song": 1,
        },
      },
    ],
  });
  // RESOLUTION & HINTS
  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.success`,
    label: "The Old Way Out",
    actionId: "mariner.navigate",
    warmup: 60,
    startdescription: "I consider the Ecdysiasts riddle.",
    description:
      "I understand now that she was crawling in her skin. I understand that Sulochana helps artists such as her, sometimes. I need to find out how far she went down the Old Way.",
    craftable: true,
    effects: {
      "mariner.locations.northsea.london.unmarkedgrave": 1,
      "mariner.mysteries.dancer": -1,
    },
    requirements: {
      "mariner.locations.northsea.london.ecdysisclub": 1,
      "mariner.song.moth": 1,
      "mariner.mysteries.dancer": 1,
    },
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint.missingsong`,
    label: "A Missing Muse... Missing Song",
    actionId: "mariner.navigate",
    startdescription:
      "I am close to finding Harald's muse. I know she went to the Ecdysis Club. But no one there is forthcoming with information. They won't share the secret unless I understand what desires drove her.",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisclub": 1,
      "mariner.song.moth": -1,
      "mariner.mysteries.dancer": 1,
    },
  });
  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint.missingplace`,
    label: "A Missing Muse... Missing Place",
    actionId: "mariner.navigate",
    startdescription:
      "I am close to finding Harald's Muse. I understand what impulses drove her to find the Old Way. I need to find where she started her search for Change. Where is The Shedder's Club?",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisclub": -1,
      "mariner.song.moth": 1,
      "mariner.mysteries.dancer": 1,
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint.missingsongandaway`,
    label: "A Missing Muse...Missing Song ",
    actionId: "mariner.navigate",
    startdescription:
      "Harald has asked me to find his Muse... I suspect she may have passed through the Ecdysis Club, but I must be there to investigate further.",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisclub.away": 1,
      "mariner.song.moth": -1,
      "mariner.mysteries.dancer": 1,
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint..missingsongawayfake`,
    label: "A Missing Muse... Too Far",
    actionId: "mariner.navigate",
    startdescription:
      "Harald has asked me to find his Muse... I suspect she may has visited the Ecdysis Club, but I must be there to investigate further.",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisfake.away": 1,
      "mariner.song.moth": -1,
      "mariner.mysteries.dancer": 1,
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint.awayfake`,
    label: "A Missing Muse... Too Far",
    actionId: "mariner.navigate",
    startdescription:
      "I am close to finding Harald's Muse. I understand what impulses drove her to find the Old Way. I suspect she may have passed through the Ecdysis Club, but I must be there to investigate further.",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisfake.away": 1,
      "mariner.song.moth": 1,
      "mariner.mysteries.dancer": 1,
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint.missingsongfake`,
    label: "A Missing Muse... Unwilling to Talk",
    actionId: "mariner.navigate",
    startdescription:
      "Harald has asked me to find his Muse... I suspect she may has visited the Ecdysis Club, but they are unwilling to talk to an outsider. Perhaps I should take up Agdistis offer and Perform.",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisfake": 1,
      "mariner.song.moth": -1,
      "mariner.mysteries.dancer": 1,
    },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint.fake`,
    label: "A Missing Muse... Unwilling to Talk",
    actionId: "mariner.navigate",
    startdescription:
      "I am close to finding Harald's Muse. I understand what impulses drove her to find the Old Way. I suspect she may has visited the Ecdysis Club, but they are unwilling to talk to an outsider. Perhaps I should take up Agdistis offer and Perform.",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisfake": 1,
      "mariner.song.moth": 1,
      "mariner.mysteries.dancer": 1,
    },
  });

  //  mod.setRecipe(RECIPE_FILE, {
  //    id: `${MYSTERY_PREFIX}.hint.missingsongandfakehostile`,
  //    label: "A Missing Muse... Missing Song",
  //    actionId: "mariner.navigate",
  //    startdescription:
  //      "Harald has asked me to find his Muse... I suspect she may has visited the Ecdysis Club, but they are unwilling to talk to an outsider. Perhaps I should take up Agdistis offer and Perform. But with the Club observed like this, I cannot easily visit to pursue this further.",
  //    hintonly: "true",
  //    requirements: {
  //      "mariner.locations.northsea.london.ecdysisfake.hostile": 1,
  //      "mariner.song.moth": -1,
  //      "mariner.mysteries.dancer": 1,
  //    },
  //  });

  //  mod.setRecipe(RECIPE_FILE, {
  //    id: `${MYSTERY_PREFIX}.hint.fakehostile`,
  //    label: "A Missing Muse... Missing Place",
  //    actionId: "mariner.navigate",
  //    startdescription: "",
  //    hintonly: "true",
  //    requirements: {
  //      "mariner.locations.northsea.london.ecdysisfake.hostile": 1,
  //      "mariner.song.moth": 1,
  //      "mariner.mysteries.dancer": 1,
  //    },
  //  });

  //  mod.setRecipe(RECIPE_FILE, {
  //    id: `${MYSTERY_PREFIX}.hint.missingsongandhostile`,
  //    label: "A Missing Muse... Missing Song",
  //    actionId: "mariner.navigate",
  //    startdescription: "",
  //    hintonly: "true",
  //    requirements: {
  //      "mariner.locations.northsea.london.ecdysisclub.hostile": 1,
  //      "mariner.song.moth": -1,
  //      "mariner.mysteries.dancer": 1,
  //    },
  //  });

  //  mod.setRecipe(RECIPE_FILE, {
  //    id: `${MYSTERY_PREFIX}.hint.hostile`,
  //    label: "A Missing Muse... Missing Place",
  //    actionId: "mariner.navigate",
  //    startdescription:
  //      "I am close to finding Harald's muse. I know she went to the Ecdysis Club, and I think I know her motives. But with the Club observed like this, I cannot easily visit to pursue this further.",
  //    hintonly: "true",
  //    requirements: {
  //      "mariner.locations.northsea.london.ecdysisclub.hostile": 1,
  //      "mariner.song.moth": 1,
  //      "mariner.mysteries.dancer": 1,
  //    },
  //  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${MYSTERY_PREFIX}.hint`,
    label: "Haralds Missing Muse",
    actionId: "mariner.navigate",
    startdescription:
      "Harald has asked me to find his Muse... how to begin? Where would she go? What drove her?",
    hintonly: "true",
    requirements: {
      "mariner.locations.northsea.london.ecdysisclub": -1,
      "mariner.song.moth": -1,
      "mariner.mysteries.dancer": 1,
    },
  });

  // Ending
  mod.setRecipe(RECIPE_FILE, {
    id: `${QUEST_PREFIX}.unmarkedgrave`,
    label: "The Worst News",
    actionId: "talk",
    startdescription:
      '"What aren\'t you telling me? I can see in your face that you have found her. Spit it out!" his face melts from anger to tears. "I need to know."',
    description:
      '"She\'s gone?" He falls with his face in his hands. "What\'s the point then..." He sobs once, twice, three times, and then returns to silence for a while. "The music. Take it, please. I cannot bare to write another note."',
    maxexecutions: 1,
    craftable: true,
    effects: {
      "mariner.inspirations.melody.winter": 1,
      "mariner.locations.northsea.london.unmarkedgrave.away": -1,
    },
    requirements: {
      "mariner.locations.northsea.copenhagen.harald": 1,
      "mariner.locations.northsea.london.unmarkedgrave.away": 1,
    },
    warmup: 60,
    rootAdd: {
      [QUEST_FLAG_ID("unmarkedgrave", "completed")]: 1,
    },
    achievements: ["mariner.achievements.haraldsmuse"],
    updateCodexEntries: {
      "codex.harald": {
        add: ["codex.harald.solvedmystery"],
      },
    },
  });
}
