import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generatehumoursEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.humours"
  );
  mod.initializeRecipeFile(RECIPE_FILE, [
    "locations",
    "northsea",
    "copenhagen",
  ]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `An Enlightening Performance at the New Scandinavian`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.humours", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.humours") },
    startdescription:
      "I caught a performance at the New Scandinavian last night. Four actors played out a four act play, each speaking a different language in each act. But their hands swayed and wove as they spoke and in the second act I realised their motions where a language of their own... and listening to the reactions of the audience, I learned who understood that language.",
    effects: {
      "mariner.influences.knock": 2,
      "mariner.influences.lantern": 2,
      "mariner.experiences.unsettling": 1,
    },
    tablereqs: { "mariner.locations.northsea.copenhagen.newscandinavian": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
