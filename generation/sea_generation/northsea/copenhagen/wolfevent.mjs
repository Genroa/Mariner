import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateWolfEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.wolf"
  );
  mod.initializeRecipeFile(RECIPE_FILE, [
    "locations",
    "northsea",
    "copenhagen",
  ]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Battle of One`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.wolf", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.wolf") },
    startdescription:
      "A single performer is booked for tonight, and he starts at the back of the hallway, smashing each lamp as he passes to the stage. Once there, he cuts at his costume with a knife. Wolf fur and the smell of metal fills the air. When the stage is sprayed red and his motions trembling, he finally throws his dogtooth knife at the final lamp, and casts us all in glass-shard darkness.",
    effects: {
      "mariner.influences.winter": 2,
      "mariner.influences.edge": 2,
      "mariner.experiences.dreadful": 1,
    },
    tablereqs: { "mariner.locations.northsea.copenhagen.newscandinavian": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
