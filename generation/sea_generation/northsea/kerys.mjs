import {
  LOCAL_TO,
  specialWanderingEventScaffhold,
} from "../../generation_helpers.mjs";

const { RECIPE_FILE, EVENT_START_ID } = specialWanderingEventScaffhold(
  "northsea.quest.kerys"
);

export function generateKerysQuest() {
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea"]);

  // Beginning

  // Get the "ticket to kerys" plot item
  mod.setRecipe(RECIPE_FILE, {
    id: `${EVENT_START_ID}.findticket`,
    tablereqs: {
      ...LOCAL_TO("northsea", "london", "ecdysisclub"),
      "mariner.mysteries.kerys": 1,
    },
    label: `A Serendipidous Discovery`,
    description:
      'Even when I am not performing, the Ecdysis Club pulls me in on many a night, just like its motto alludes. This night I am led backstage, to await mr Agdistis, who is otherwise obtained. I sit in a cramped waiting room, allowing the bustle of the performers to pass me by, when I spot a crumpled slip of paper on the floor. It seems to detail an insignificant journey through a dreary part of the english coast, until I see the final stop. "Kerisham". The word beats within my brain.',
    effects: { "mariner.ticket.kerys": 1 },
    maxexecutions: 1,
  });

  // End: This quest is finished at the end of the Sunken City vault.
}
