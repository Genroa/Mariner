import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateBirdsEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.birds"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "amsterdam"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Small Flitting Things`,
    grandReqs: {
      [READ_QUEST_FLAG("amsterdamsewers", "completed")]: 1,
      ...TIMER_ELAPSED("northsea.randomevent.birds", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.birds") },
    startdescription:
      "Today I have acquired another shadow. At the edge of my vision, notice the bird-twitch motions and empty eyes. I think i keep them at a distance, but in the evening, I find my pockets light of coin, but filled with magpie-down.",
    effects: {
      "mariner.influences.moth": 4,
    },
    furthermore: [{ target: "~/exterior", effects: { funds: -2 } }],
    tablereqs: { "mariner.locations.northsea.amsterdam": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
