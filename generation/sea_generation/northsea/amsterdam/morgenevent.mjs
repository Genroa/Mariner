import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";

export function generateMorgenEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.morgen"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "amsterdam"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Saphire Conjuration`,
    startdescription:
      "Last night I witnessed something peculiar. A lethal looking vagabond stood on the edge of the harbor, and brought a bone-white flute to their lips. An eerie gull-cry followed, and the flute cracked end to end. Shortly after, the white Yaght glides into the harbor.",
    effects: {
      //mystery: 1,
    }, //add morgan mystery
    maxexecutions: 1,
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
