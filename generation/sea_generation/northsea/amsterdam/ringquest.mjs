import { generateQuest } from "../../../quest_generation.mjs";

export function generateRingQuest() {
  generateQuest({
    id: "amsterdamsewers",
    // Quest card informations
    label: "A Misplaced Ring",
    description:
      "I met the Veiled Lady. She made me offer I shouldn't refuse: get back a ring stolen by Amsterdam's underworld.",
    // Quest stages
    updates: [
      // Order of stages here sets the visual order, as well as the icon used ([1] for the first, [2] for the second) unless specified otherwise.
      // Labels and descriptions are set on the aspect.

      // This stage doesn't exist yet in the current version of the game. The Veiled Lady gives you the sewers and takes the letter,
      // and then the quest flag marking the stage "completed" is set when you give the ring back. We could split that into three stages
      // for good measure.
      {
        id: "started",
        label: "A Demand",
        description:
          '<i>"While you are here... I need something back that I have lost. A trinket from a former life has be been pilfered by some sewer rats. You seem like a person of action. Retrieve it for me, please?"</i><br><br>Easier said than done. The sewers aren\'t the safest place to look, especially under Amsterdam.',
        effects: {
          updateCodexEntries: {
            "codex.veiledlady": {
              add: ["codex.veiledlady.1"],
            },
          },
          unlockCodexEntries: ["codex.auntieekster"],
        },
      },
      {
        id: "ringcollected",
        icon: 2,
        label: "Negociated With A Magpie",
        description:
          "Deep into the Nest under the Bazaar, I managed to trade trinkets for the ring the Veiled Lady wanted back from the lady of this place. I should bring it back to her.",
        effects: {
          updateCodexEntries: {
            "codex.auntieekster": {
              add: ["codex.auntieekster.negotiatedring"],
            },
          },
        },
        slots: [
          {
            id: "ring",
            label: "Ring",
            description: "<A small thing from a long time ago.>",
            required: {
              "mariner.vaults.ekstersnest.ring": 1,
            },
          },
        ],
      },
      {
        id: "ringstolen",
        icon: 2,
        label: "Pilfered A Magpie",
        description:
          "Deep into the Nest under the Bazaar, I managed to obtain the ring the Veiled Lady wanted back from the lady of this place. I should bring it back to her.",
        effects: {
          updateCodexEntries: {
            "codex.auntieekster": {
              add: ["codex.auntieekster.stolering"],
            },
          },
        },
        slots: [
          {
            id: "ring",
            label: "Ring",
            description: "<A small thing from a long time ago.>",
            required: {
              "mariner.vaults.ekstersnest.ring": 1,
            },
          },
        ],
      },
      // Label and description don't matter, because this stage will always be coupled with the destruction of the quest card.
      // We still define it, so that we can remember (set on the root) that this quest happened, even if the card (tracking current active quests) isn't there anymore.
      {
        id: "completed",
        effects: {
          updateCodexEntries: {
            "codex.veiledlady": {
              add: ["codex.veiledlady.2"],
            },
          },
        },
      },
    ],
  });
}
