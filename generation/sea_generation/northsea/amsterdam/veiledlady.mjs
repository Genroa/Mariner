import { repeatableConversation, convertSourceConversation } from "../../../port_generation/helpers.mjs";
import { QUEST_ID, UPDATE_QUEST } from "../../../quest_generation.mjs";
import { generateRingQuest } from "./ringquest.mjs";

export const VEILED_LADY = {
  type: "npc",
  elementId: "veiledlady",
  label: "The Veiled Lady",
  description:
    "An elegant creature of the Dutch underground subculture. She performs only on private parties now, but the rumours of her veiled dances have spread throughout the town. She does not meet many for free, but she is willing to meet with me.",
  localAspects: { notyetmuse: 1 },
  storyTrading: {
    story: {
      label: "The Clipped-Wing Bride of Joko Tarub",
      icon: "birdsong",
      description:
        "Once, a young man saw seven Bidadari bathing in the woods. They left the water one by one, and each gathered their discarded sjawls, which turned to wings. Quickly, he snatched one of the sjawls, Nawang Wulan, the last of the nymphs could not transform. Joko approached her, and as she was bound without her garments, took her as his wife. Joko prospered, for she could feed the whole family with a single grain of rice from her magic pot. But one day, curious, foolish Joko opened her cooking pot and broke the spell. Then his rice stores again started dwindling, and Nawang Mulan discovered her wings, hidden beneath the rice. She donned her guise again, and left her husband, children, and earthly life behind.",
      aspects: {
        moth: 2,
        grail: 2,
        edge: 2,
        "mariner.storyaspect.cthonic": 1,
        "mariner.storyaspect.whimsical": 1,
        "mariner.storyaspect.rousing": 1,
        "mariner.storyaspect.grim": 1,
      },
    },
    label: "Trade Stories with The Veiled Lady",
    startdescription: '"Trade stories? Always. It is the best way to travel without leaving any comfort behind."',
    description:
      'She walks over to a shrine, and lights an insence stick amids cuttings of jasmine, lilies and lotusses. " I will tell you a story I learned in my second home. Maybe the secret contained within it will help you as much as it did me."',
  },
  deliveryOptions: {
    previousPackageLocations: ["London", "Copenhagen"],
    knownLanguages: ["Dutch", "French", "Jawa"],
    usedNames: ["VEILED LADY", "LADY"],
    usedInitials: ["V.L.", "V.L.M.H."],
    gender: "female",
    whatToDoWithIt: [
      "Send her my love, and tell her I remember her still.",
      "Kiss her hand, if you can.",
      "Remind her of the Podon Joho we met at in Java. She will know who the package is from than.",
    ],
  },
  deliveryRiddles: [
    "Ecdysiast who does not shed her shrouds.",
    "Known but Unknown, a occluded gem sparkles even brighter.",
    "Not the one in Alexandria, though she wears the name well.",
    "Parisienne in exile, queen of my heart.",
    "Sad-Eyed Lady of the Low Lands",
    "The petit bijou of the Venice of the North.",
  ],
};

export const MATA_HARI = {
  type: "npc",
  elementId: "matahari",
  overlayId: "veiledlady",
  localAspects: {
    "mariner.inspiration.muse": 1,
    "mariner.inspiration.grail": 1,
  },
  notDiscoverable: true,
  label: "The Veiled Lady",
  description:
    "An elegant creature of the Dutch Demi-monde. She performs only on private parties now, but the rumours of her veiled dances have spread throughout the town. She does not meet many for free, but she is willing to meet with me.",
  deliveryOptions: {
    sameRecipientAs: "veiledlady",
  },
  conversations: [
    repeatableConversation({
      id: "sunkencity",
      element: "mariner.mysteries.kerys",
      useRecipientId: true,
      label: "Asking After The Sunken City",
      startdescription: '"A city at the bottom of the sea?"',
      description:
        '"There are plenty. The sea is ravenous, and this coast has always been nibbled on. But I don\'t think you seek the soggy townsships lost to these low lands... I think I heard the story you are referring to, closer to my true home in Paris."',
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.lackheart",
      useRecipientId: true,
      label: "Asking after a Broken Heart",
      startdescription: ' "Your heart has been broken? Oh mais non!"',
      description:
        '"I do so sympathize, bijou, I am very familiar with the sentiment. Not from personal experience of course, but many in my immediate surroundings keep getting afflicted!"',
    }),
    repeatableConversation({
      id: "corpseflowers",
      element: "mariner.vaults.eddangerforest.flower",
      useRecipientId: true,
      label: "Talk about a Rare Bloom?",
      startdescription: '"You brought a bouquet? You shouldn\'t have"',
      description:
        '"These are not really my color, bijou. Besides, I have head a rumor that these flowers can be used to raise a corpse from the grave." Her veils russle as she smiles. " I have no intention of becoming a corpse, so I have no need for these."',
    }),
    repeatableConversation({
      id: "merryberries",
      element: "mariner.vaults.eddangerforest.fruit",
      useRecipientId: true,
      label: "Talk about a Rare Fruit",
      startdescription: '"What a délicatesse did you bring me?"',
      description:
        '"I was given these twice, once to celebrate me, once to kill me. I believe you can find a market for wares such as these in the bazaars of Tripoli"',
    }),
    repeatableConversation({
      id: "experimental",
      element: "mariner.locations.northsea.copenhagen.newscandinavian.away",
      useRecipientId: true,
      label: "Talk about the New Scandinavian Theater",
      startdescription: ' "You wish to discuss the Scandinavian Avant Garde?"',
      description:
        '"I knew the Countess and her husband long before they founded the first Experimental Scandinavian. They came to see me perform, a lifetime ago."',
    }),
    repeatableConversation({
      id: "ecdysis",
      element: "mariner.locations.northsea.london.ecdysisclub.away",
      useRecipientId: true,
      label: "Discuss the Ecdysis Club",
      startdescription: '"Amavasya\'s place?"',
      description:
        '"I have never been, but I\'ve heard things about it. I have a collegial curiosity, in a way, though these days I prefer my skin shedding to be a more intimate affair."',
    }),
    repeatableConversation({
      id: "musicmoth",
      element: "mariner.song.moth",
      useRecipientId: true,
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss the Arts Unregarded?"',
      description:
        '"Moth guides what hunts and stalks us. Our doubts, our fears, our shadow, our past. Any negativity still clinging to your clothes or hair. With the Intent of Lantern and the Direction of Moth, you can reveal the nature of a curse that has latched on to you. To Quell the curse, you will need to change your Intention, but keep the Direction the same."',
      additionalProperties: {
        unlockCodexEntries: ["codex.performance.conversation.veiledlady.moth"],
      },
    }),
    repeatableConversation({
      id: "musicwrongwinter",
      element: "mariner.song.winter",
      useRecipientId: true,
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss the Arts Unregarded?"',
      description:
        '"I do not like to deal with Winter. I have come quite close enough to death, thank you. The Winter Direction is a dangerous road... one for the desperate or the desolate. One who believes they have lost all may seek to pursue it further..."',
      additionalProperties: {
        unlockCodexEntries: ["codex.performance.conversation.veiledlady.winter"],
      },
    }),
    repeatableConversation({
      id: "musicwronggrail",
      element: "mariner.song.grail",
      useRecipientId: true,
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss the Arts Unregarded?"',
      description:
        '"The acts of Grail? And here I picked up that your Heart belonged to antother? Oh I tease mon chéri. The Intent of Grail is to charm, subsume the will of another with their desire to please you. If you wish to set of in that Direction though... well... You allready are, in a way. In the Direction of Grail lies both the Sea, and it\' drowning waters, as well as the depths of your Desire itself."',
      additionalProperties: {
        unlockCodexEntries: ["codex.performance.conversation.veiledlady.grail"],
      },
    }),
    repeatableConversation({
      id: "musicwronglantern",
      element: "mariner.song.lantern",
      useRecipientId: true,
      label: "Discuss the Power of my Songs",
      startdescription: '"You wish to discuss the Arts Unregarded?"',
      description:
        '"The Light of Lantern was never mine to persue, I have much preferred the kindness of shadows. But just like my dances, the Direction of lantern is also one of Revelation. "',
      additionalProperties: {
        unlockCodexEntries: ["codex.performance.conversation.veiledlady.lantern"],
      },
    }),
    repeatableConversation({
      id: "musicwrong",
      element: "mariner.song",
      label: "Discuss the Power of my Songs",
      useRecipientId: true,
      startdescription: '"You wish to discuss the Arts Unregarded?"',
      description:
        '"It has been to long since I did any of the sort. I am not interested to dredge up memories from a lifetime ago, not for this Direction or Intent"',
    }),
    repeatableConversation({
      id: "character",
      element: "mariner.mysteries.howtomendcharacter",
      useRecipientId: true,
      label: "The Mending of the Heart",
      startdescription: '"You are seeking to mend your Broken Heart?"',
      description:
        '"If there were an easy path, I think everyone would take it, non?" She takes my hands and places them over my chest, cupped in her own. "Keep save what remains to you... Protecting is much easier than stitching the pieces back together."',
    }),
    repeatableConversation({
      id: "dancer",
      element: "mariner.mysteries.dancer",
      useRecipientId: true,
      label: "Ask After A Dancer",
      startdescription: '"A dancer friend of Harald?"',
      description:
        '"Non, désolée. That poor man has been pining after that girl for far too long... I would take his his mind of of her, but he does not travel much, and I am much too comfortable for an over sea trip. " she stretches languid as a cat',
    }),
    convertSourceConversation({
      id: "kerys",
      element: "mariner.stories.kerys",
      useRecipientId: true,
      label: "Folk Tales and French Histories",
      startdescription: '"What do you have there?"',
      description:
        'She riffles the pages "There is an opera based on this tale as well, and more versions than you can count strewn through the countryside. Though I am a Parisienne at heart, I cannot deny the romance found in Bretagne Great or Small. Listen, I\'ll tell you the tragedy of the Princess of Ker Ys"',
    }),
  ],
};

mod.initializeRecipeFile("recipes.talkto.northsea", []);
mod.setRecipes(
  "recipes.talkto.northsea",
  {
    id: "mariner.talkto.northsea.amsterdam.veiledlady.deliverletter",
    actionId: "talk",
    craftable: true,
    maxexecutions: 1,
    requirements: {
      "mariner.locations.northsea.amsterdam.veiledlady": 1,
      "mariner.letter.veiledlady": 1,
    },
    label: "An Audience With The Veiled Lady",
    startdescription:
      'Through clubs, through drawn curtains, through cigarette smoke, I pass. Then I finally spot her. "Enchantée" she says, although her accent is not french.',
    warmup: 30,
    description:
      '"A letter from miss Morland.?" She opens it and scans the words. Her eyes flick back up to my face, once. "Yes, I think this has been quite helpful."',
    effects: {
      "mariner.letter.veiledlady": -1,
    },
    linked: [
      {
        id: "mariner.talkto.northsea.amsterdam.veiledlady.giverequest",
      },
    ],
  },
  {
    id: "mariner.talkto.northsea.amsterdam.veiledlady.giverequest",
    label: "A Demand Wrapped In Honey",
    actionId: "talk",
    startdescription:
      '"While you are here... I need something back that I have lost. A trinket from a former life has be been pilfered by some sewer rats. You seem like a person of action. Retrieve it for me, please?"',
    warmup: 30,
    furthermore: [
      {
        effects: {
          [QUEST_ID("amsterdamsewers")]: 1,
          "mariner.locations.northsea.ekstersnest.away": 1,
        },
      },
      UPDATE_QUEST("amsterdamsewers", "started"),
    ],
  },
  {
    id: "mariner.talkto.northsea.amsterdam.veiledlady.completerequest",
    actionId: "talk",
    craftable: true,
    maxexecutions: 1,
    requirements: {
      "mariner.locations.northsea.amsterdam.veiledlady": 1,
      [QUEST_ID("amsterdamsewers")]: 1,
      "mariner.vaults.ekstersnest.ring": 1,
    },
    label: "A Ring Returned",
    startdescription:
      '"Thank you. I appreciate this favor." The ring disappears into a jewelry box, locked and stowed. "Lets get to know eachother a little closer now..."',
    warmup: 30,
    description:
      '<i>She turns her head slightly.</i><br>"Ask, and I will dance. Just for you." [speak to a Muse about your Temptation or Dedication to use them as a dedication.',

    furthermore: [
      UPDATE_QUEST("asmterdamsewers", "completed"),
      { target: "~/extant", effects: { [QUEST_ID("amsterdamsewers")]: -1 } },
      {
        effects: {
          "mariner.locations.northsea.amsterdam.veiledlady": -1,
          "mariner.locations.northsea.amsterdam.matahari": 1,
          "mariner.vaults.ekstersnest.ring": -1,
        },
      },
    ],
  }
);

generateRingQuest();
