import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateDiamondEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.diamond"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "amsterdam"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `An Opportunity at Vermaet`,
    grandReqs: { ...TIMER_ELAPSED("northsea.randomevent.diamond", 25) },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.diamond") },
    startdescription:
      'Amsterdams canal-district is filled with a heady mix of classist snobbism and down to earth materialism. This means I am allowed to enter Vermaet through the front door, but I do have to wait until they handle their customers, even though they called me. When Rogier Vermaet finally turns to me, he pulls a little box from under the table. "This seemed perhaps too interesting for our regualr clientell, we thought to offer it to you first."',
    effects: {
      "mariner.trappings.lantern.1": 1,
    }, // todo add oportunity to a tool
    tablereqs: {
      "mariner.locations.northsea.amsterdam.vermaetdiamantenhuis": 1,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
