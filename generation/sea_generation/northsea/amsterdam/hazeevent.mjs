import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateHazeEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.haze"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "amsterdam"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Fragrance Remembered`,
    grandReqs: {
      [READ_QUEST_FLAG("amsterdamsewers", "completed")]: 1,
      ...TIMER_ELAPSED("northsea.randomevent.haze", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.haze") },
    startdescription:
      "Yesterday, during a walk through the grachten, a whiff of something sweet and powerfull came up from a sewer grate. immedeately my head rushed with cawing laughter and glinting darkness. My desires have grown wings, and are stalking me.",
    effects: {
      "mariner.experiences.sophorific": 1,
    },
    tablereqs: {
      "mariner.weathermeta.windy": -1,
      "mariner.locations.northsea.amsterdam": 1,
    },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
