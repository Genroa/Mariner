import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateRaidEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.raid"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Raid at the Club`,
    grandReqs: { ...TIMER_ELAPSED("raid", 25) },
    rootAdd: { ...RESET_TIMER("raid") },
    startdescription:
      "As the performance was getting underway, a flurry of whispers spread through the club. A police raid was on it's way. We were ushered through the backrooms and out into the alleyway, and dispersed, like moths to the night.",
    effects: {
      "mariner.notoriety": 1,
    },
    tablereqs: { "mariner.locations.northsea.london.ecdysisclub": 1 }, //turn distant
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
