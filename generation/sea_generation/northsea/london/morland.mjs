import {
  repeatableConversation,
  convertSourceConversation,
  uniqueConversation,
} from "../../../port_generation/helpers.mjs";

export const MORLAND = {
  notDiscoverable: true,
  type: "npc",
  elementId: "morland",
  label: "Miss Morland",
  description: "A merchant in rare and valuable books.",
  deliveryOptions: {
    previousPackageLocations: ["Amsterdam", "Copenhagen"],
    knownLanguages: ["latin"],
    usedNames: ["MISS MORLAND"],
    usedInitials: ["M.M."],
    gender: "female",
    whatToDoWithIt: ["No names, under any circumstances"],
  },
  deliveryRiddles: [
    "Enabler of many an Aspirant.",
    "Her hearth is warm, her shelves are stocked.",
    "Her rule is “no names” and I will keep to that.",
    "Look for the yellow windows, a haven from Albion’s rains",
    "A merchant of the memory that does not die.",
    "The shop is found in an ill-favored bend of a lesser river.",
  ],
  storyTrading: {
    story: {
      label: "The Nine Muses",
      description:
        "Once, the Arts were not only visible, but lauded, glorified. Not just limited to Song and Dance, but the higher studies. nine is their number, as nine so often is. While the Muses seem more distant now, it is their nature to be pursued.",
      aspects: {
        lantern: 2,
        "mariner.storyaspect.apollonian": 1,
        "mariner.storyaspect.mortal": 1,
        "mariner.storyaspect.occult": 1,
      },
      icon: "onwhatiscontainedbysilver",
    },
    label: "Trade Stories with Morland",
    startdescription:
      '"Trade stories? I was rather hoping you would collect them for me for free..."',
    description:
      '"there is an old story that has been a particular favorite of mine. Come, I\'ll put the cattle on."',
  },
  conversations: [
    uniqueConversation({
      id: "vabagondtempatation",
      element: "mariner.ascension.vagabond.temptation",
      label: "The First Step",
      startdescription: '"Oh, yes, the first step."',
      description:
        '"The route changes over the years, but I like to keep in the know of the first stop, to send any aspirants I meet in the right direction. Seek a place that isn\'t, where the sea has conquered the land."',
      effects: { "mariner.mysteries.kerys": 1 },
      unlockCodexEntries: ["codex.ascensions.exemplum.hiraezh"],
    }),
    repeatableConversation({
      id: "vagabondtemptationreminder",
      element: "mariner.ascension.twins.temptation",
      label: "Ask again after the Pilgrimage",
      startdescription: '"Again?"',
      description:
        '"The way changes, now and again. But I always pay mind to stay informed on the first step. I told you where it is, now go take it."',
    }),
    repeatableConversation({
      id: "vagabonddedication",
      element: "mariner.ascension.vagabond.dedication",
      label: "Tell of my Exploits",
      startdescription: '"On the way, are we?"',
      description:
        '"Now we are in uncharted waters! I have no idea where she will have you travel next. But I cannot wait to find out. Now tell me everything of your first step..."',
    }),
    repeatableConversation({
      id: "twinstemptation",
      element: "mariner.ascension.twins.temptation",
      label: "A Different Path",
      startdescription: '"You want to abandon the pilgrimage?"',
      description:
        'She frowns. "You would leave it all behind? The mysteries beyond the skin of the world, the Pilgrimage, the Brightness behind the stories? And for what... Companionship?"',
    }),
    repeatableConversation({
      id: "twinsdedication",
      element: "mariner.ascension.twins.dedication",
      label: "A Different Path",
      startdescription: '"So you went your own way..."',
      description:
        '"Oh well, there goes my shot at hearing the greatest gossips right from the source." she sighs. "I shall have to sate my curiosity with snippets a little longer."',
    }),
    repeatableConversation({
      id: "sunkencity",
      element: "mariner.mysteries.kerys",
      label: "Asking after the Sunken City",
      startdescription: '"Trying to find a place that isn\'t?"',
      description:
        "\"The Sunken City... must lie somewhere near Kerisham, I gather, but I don't know exactly where. That's an issue for you to navigate. You Half-Hearts are more sensitive for special places like that.\"",
    }),
    repeatableConversation({
      id: "paintedwaterfall",
      element: "mariner.mysteries.paintedwaterfall",
      label: "Asking after the Painted Waterfall",
      startdescription: '"Trying to skip to end of the tale, are we?"',
      description:
        '"The Painted Waterfall is where pilgrimages such as yours can end. If you stick your nose in the right wrong places, if you strum up some stories to share, and if you don\'t get hindered by something as inconvenient as death."',
    }),
    repeatableConversation({
      id: "dancer",
      element: "mariner.mysteries.dancer",
      label: "Asking After a Dancer",
      startdescription: '"A foreign dancer girl?"',
      description:
        "\"Those aren't scarce. Try the Gaiety... She's an Adept you say? There was one of those, recently... I think Sulochana took her under her wings.\"",
    }),
    repeatableConversation({
      id: "halfheart",
      element: "mariner.halfheart",
      label: "Ask after my Half-Heart",
      startdescription: '"You want to talk to me about Heartbreak?"',
      description:
        "\"Don't fuss. Everyone has something. If we'd make a big deal over every unhappy artist, the empire would grind to a halt. Just make the best of your sensitivity. Let it guide you to places that others might miss.\"",
    }),
    repeatableConversation({
      id: "lackheart",
      element: "mariner.lackheart",
      label: "Ask after my Lack-Heart",
      startdescription: '"You think a part of your soul is missing?"',
      description:
        '"Hmm... You seem functional enough, so I doubt you are really missing one of the Nine. But knowing what you are... Perhaps some fraction of each part is seperated from you. I\'m not sure. If you wish to learn more about this, perhaps visit a library."',
    }),
    repeatableConversation({
      id: "music",
      element: "mariner.song",
      label: "Discuss my songs with Miss Morland",
      startdescription: '"You wish to talk to me about the Orphean Arts?"',
      description:
        '"I am not a specialist, But I have seen a Performance or two in my days. I know to start a ritual that way it must occur in a Dionysian Temple: A Theater. The Ritual itself is guided by two princples... Intent First, followed by Direction. With Intent is declared how you wish to affect the world, where Direction declares the sphere of influence, or the target."',
    }),
    repeatableConversation({
      id: "character",
      element: "mariner.mysteries.howtomendcharacter",
      label: "Ask after the Mending of my Soul",
      startdescription:
        '"You are trying to find out how to heal your fractured Soul?"',
      description:
        '"I find the answers I seek in books, most of the time. Those I can\'t find there I find in the world."',
    }),
    repeatableConversation({
      id: "slip",
      element: "mariner.ticket.kerys",
      label: "Ask after the Train Time",
      startdescription: '"What do you have there?"',
      description:
        '"Kerisham? I haven\'t been, but I have heard much about it. "Nothing ever changed in Kerisham Town" they say."',
    }),
    convertSourceConversation({
      id: "debellismurorum",
      element: "mariner.stories.debellismurorum",
      label: "Tea and Translation",
      startdescription:
        '"Did you find something interesting for me? Come in then."',
      description:
        '"Ah, yes, a classic. The translated work is easier to sell, but I think I can find a buyer for this one too." She pours me another cup of steaming Earl Grey. "But you did not bring me this just for the stipend, did you... let me alleviate your curiosity. It\'s a good story... relevant to you in more ways than one."',
    }),
    convertSourceConversation({
      id: "pineknight",
      element: "mariner.stories.pineknight",
      label: "Tea and Translation",
      startdescription:
        '"Did you find something interesting for me? Come in then."',
      description:
        '"Quite the collectable piece, for the right buyers. And I think I have just the one browsing my shop of late..." She mixes three sugars into my tea. "But this is not a story to tell without on an empty stomach, or in the gloomy dark. Stoke up the hearth for me, and I\'ll begin."',
      additionalProperties: {
        updateCodexEntries: {
          "codex.hours.ringyew": {
            add: ["codex.hours.ringyew.1"],
            unlock: true,
          },
        },
      },
    }),
  ],
};
