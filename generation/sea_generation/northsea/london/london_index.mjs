import { generateAgdistisEvent } from "./agdistisevent.mjs";
import { generateEtherialEvent } from "./etherialevent.mjs";
import { generateMorlandEvent } from "./morlandevent.mjs";
import { generateRaidEvent } from "./raidevent.mjs";
import { generateStrathcoyneEvent } from "./strathcoyneevent.mjs";
import { generateUnnervingEvent } from "./unnervingevent.mjs";

export function generateLondonStreetsEvents() {
    generateAgdistisEvent();
    generateEtherialEvent();
    generateMorlandEvent();
    generateRaidEvent();
    generateStrathcoyneEvent();
    generateUnnervingEvent();
}
