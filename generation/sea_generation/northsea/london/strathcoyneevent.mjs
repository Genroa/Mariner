import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateStrathcoyneEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.strathcoyne"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `An Encounter at Strathcoynes`,
    grandReqs: { ...TIMER_ELAPSED("strathcoyne", 25) },
    rootAdd: { ...RESET_TIMER("strathcoyne") },
    startdescription:
      "As I wander one of the nicer neighborhoods of the queens city, I can see a pillow of smoke billowing out from a nearby house. Not long after, several entities leap throw the broken front door, and disperse into the night beyond. One of them drops a package, I am the first to pick it up.",
    effects: {
      "mariner.notoriety": 2,
    },
    linked: [{ id: "mariner.drawreward.precious" }],
    tablereqs: { "mariner.locations.northsea.london": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
