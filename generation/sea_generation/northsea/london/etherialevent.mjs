import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateEtherialEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.etherial"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `An Ephemeral Performance at the Ecdysis Club`,
    startdescription:
      "Tonight I visited the Club again. They dimmed all the lights, and in the crimson gloom the dancers swayed slowly in tattered rags. With each turn, moths would release from the folds of their clothing, shining brightly in the gloom.",
    grandReqs: {
      ...TIMER_ELAPSED("northsea.randomevent.etherial", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.etherial") },
    effects: {
      "mariner.influences.moth": 2,
      "mariner.influences.lantern": 2,
    },
    tablereqs: { "mariner.locations.northsea.london.ecdysisclub": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
