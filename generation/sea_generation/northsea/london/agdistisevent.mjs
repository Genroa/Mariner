import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { READ_QUEST_FLAG } from "../../../global_flags_generation.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateAgdistisEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "northsea.randomevent.agdistis"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "northsea", "london"]);

  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Conductor at Work`,
    grandReqs: {
      [READ_QUEST_FLAG("marinettedoll", "completed")]: 1,
      ...TIMER_ELAPSED("northsea.randomevent.agdistis", 25),
    },
    rootAdd: { ...RESET_TIMER("northsea.randomevent.agdistis") },
    startdescription:
      "I hoped to visit Agdistis and share a drink, but I find him rehearsing with his troupe. Instead of turning me away, the theater director gestures me to sit down at the back of the darkened stage. What I saw was nymphal, pliable, but full of potential. The dancers move like broken dolls, the music swells like waves. After it was over, I surrepticiously collected the props that where left discarded on the stage.",
    effects: {
      "mariner.trappings.heart.1": 1, //should be a higher level trapping
    },
    tablereqs: { "mariner.locations.northsea.london.ecdysisclub": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
