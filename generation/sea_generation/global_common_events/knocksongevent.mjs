import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateConnectionEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.connection"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Chance encounter with a Raconteur`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.connection", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.connection") },
    startdescription:
      "I encounter another, who knows the stories and whistles the tunes of the raconteurs. As we walk the streets, we teach eachother the greetings, the walks and the gestures we have learned on our travels.",
    effects: { "mariner.influences.knock": 2 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
