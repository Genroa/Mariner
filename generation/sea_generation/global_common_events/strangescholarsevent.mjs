import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeScholarsEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.scholars");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.scholars", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.scholars") },
    startdescription:
      "Under a Scholars Moon, the blank page beckons the pen, and the moon glint shines in the eyes of those inspired.",
    effects: { "mariner.influences.lantern": 4 },
    tablereqs: { "mariner.moon.basic.6": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
