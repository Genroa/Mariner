import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateScrapEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.scrap"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Chance encounter with a Raconteur`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.scrap", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.scrap") },
    startdescription:
      "I encounter another, who knows the stories and whistles the tunes of the raconteurs. We speak, guardedly, of the deeper mysteries. Some of the worlds veil is lifted for me, and I glimpse at the secret light beneath.",
    effects: { "mariner.scrapofinformation": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
