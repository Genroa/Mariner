import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateChillyEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.chilly"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<Chilly Weather>`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.chilly", 10) },
    rootAdd: { ...RESET_TIMER("sallseas.randomevent.chilly") },
    startdescription:
      "When breath frosts and fingers turn stiff even within your overcoat, it might make a sailor think twice before sailing out again.",
    tablereqs: { "mariner.weathermeta.cold": 1 },
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.wantstoleave",
            level: 1,
            limit: 1,
            additive: false
          }
        ]
      }
    ]
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
