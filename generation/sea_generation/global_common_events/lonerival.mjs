import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateRivalLeavingEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.rivalleaving"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Leaving`,
    grandReqs: {
      ...TIMER_ELAPSED("allseas.randomevent.rivalleaving", 25),
      "[~/exterior : mariner.crew.traits.rival] ==1": 1,
    },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.rivalleaving") },
    startdescription:
      "\"This ship isn't big enough for the two of us. I am leaving.\"",
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter: "mariner.crew.traits.rival",
            mutate: "mariner.crew.wantstoleave",
            limit: 1,
            level: 1,
            additive: false,
          },
        ],
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
