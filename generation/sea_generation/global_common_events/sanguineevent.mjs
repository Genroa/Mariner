import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateSanguineEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.sanguine"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<A Heat from the Blood>`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.sanguine", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.sanguine") },
    startdescription:
      "<>",
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
