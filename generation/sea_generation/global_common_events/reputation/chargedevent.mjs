import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateChargedEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.charges"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Trumpeting Charge>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.charges", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.charges") },
        startdescription:
            "<With much fanfare and evidential glee. These trumped up charges are delivered by the constabulary The charges hold water like a net, but that matters litter when judge, jury and executioner are all shore. If I do not leave in time, I will be arrested, and my story ends here ignobaly.>",
        inductions: [
            { id: "mariner.trouble.consequence.highestrep" },
            { id: "mariner.trouble.consequence.highrep" },],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 3`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
