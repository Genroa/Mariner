import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateLeakingCaptainEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.leakingcaptain"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Leaking Captain>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.leakingcaptain", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.leakingcaptain") },
        startdescription:
            "<It was only a little stabbing. if I where full of bluster i might call it a graze. The spirited youth knew the town doesn't want me here, and I knew to get out of the way. Almost out of the way enough. This injury will affect my movement, at least for a while.>",
        effects: { "mariner.experiences.injury": 3 },
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 2`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
