import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generatePaperworkEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.paperwork"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<A Wall of Paper>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.paperwork", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.paperwork") },
        startdescription:
            "<In polite societes bureaucracy is the way to get rid of those one does not approve off. in the civilised manner of making their industry, dealings and life impopssible, This port has set up an impenetrable amount of paperwork for me, and they hope to bore me back out onto the sea.>",
        effects: { "mariner.wanderlust": 2 },
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 2`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
