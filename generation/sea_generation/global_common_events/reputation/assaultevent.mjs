import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateAssaultEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.assault"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Harassed Crew>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.assault", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.assault") },
        startdescription:
            "<Doors close, youths jeer and people turn away. My crew has been made aware of the costs of being in my employ.>",
        linked: [
            { id: "mariner.genericevents.injurecrewmember" },
        ],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 2`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
