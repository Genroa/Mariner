import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateHarriedCaptainEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.harriedcaptain"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Harried Captain>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.harriedcaptain", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.harriedcaptain") },
        startdescription:
            "<As I walk through town, eyes flock to my like gulls trailing a whaler's death-barge. All I see are the eyes of predators, and the sounds of the city has turned to shrill shrieks or gull-cry mockery.>",
        effects: { "mariner.experiences.unsettling": 2 },
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 2`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
