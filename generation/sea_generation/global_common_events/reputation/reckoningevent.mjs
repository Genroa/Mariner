import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateReckoningEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.reckoning"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<A Decimation>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.reckoning", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.reckoning") },
        startdescription:
            "<Reckoners have found my crew, and in this instance, they gave no quarter and took no prisoners.  We are lucky some have survived. >",
        furthermore: [
            {
                target: "~/exterior",
                effects: { "mariner.crew": -3 },
            }
        ],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 4`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
