import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateGossipRepEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.gossiprep"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Words like buzzing flies>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.gossiprep", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.gossiprep") },
        startdescription:
            "<Wagging tongues are inevitable, but they mean a bad reputation can multiply like vermin above rotting meat.>",
        effects: { "mariner.notoriety": 2 },
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 1`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
