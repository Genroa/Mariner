import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateSqueezedCaptainEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.squeezed"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Captain in a Squeeze>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.squeezed", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.squeezed") },
        startdescription:
            "<I find my self in an unfortunate alleyway, with a small crowd explaining with visceral delight how unwelcome we are. While they bradished more then knuckles alone. >",
        effects: { "mariner.experiences.rattling": 1 },
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 2`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
