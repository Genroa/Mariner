import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateHarassmentEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.harassment"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Harassed Crew>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.harassment", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.harassment") },
        startdescription:
            "<Doors close, youths jeer and people turn away. My crew has been made aware of the costs of being in my employ.>",
        furthermore: [
            {
                target: "~/exterior",
                mutations: [
                    {
                        filter: "mariner.crew",
                        mutate: "mariner.crew.longing",
                        level: 2,
                        limit: 3
                    },
                ],
            },
        ],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 1`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}