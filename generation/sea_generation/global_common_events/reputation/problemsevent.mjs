import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateProblemsEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.problems"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Problems Multiply>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.problems", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.problems") },
        startdescription:
            "<Once again, doors once open to me are now closed, faces that once delighted in my visits now have to turn away.>",
        linked: [
            {
                id: "mariner.trouble.start"
            }
        ],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 2`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
