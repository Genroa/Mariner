import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateSabotageEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.sabotage"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Sabotage>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.sabotage", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.sabotage") },
        startdescription:
            "<Crates pried open, and barrels smashed, the contents spilled over the docks like the rotten insides of an overripe gourd.>",
        furthermore: [
            {
                target: "~/exterior",
                effects: { "mariner.cargo": -2 },
            }
        ],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 3`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
