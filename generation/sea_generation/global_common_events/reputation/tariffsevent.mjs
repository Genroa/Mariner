import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateTariffsEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.tariffs"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Inventive Tarrifs>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.tariffs", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.tariffs") },
        startdescription:
            "An artist must always be willing to recognise the creativity in another, and what the dockmaster comes to me with is actually a piece of art. I see taxes multiply in manners never seen before. But it is pay or scram, of course.",
        furthermore: [
            {
                target: "~/exterior",
                effects: { "funds": -2 },
            }
        ],
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 3`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
