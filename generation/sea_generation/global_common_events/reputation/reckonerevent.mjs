import {
    setWanderingEventChance,
    wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";
import { LOCAL_REPUTATION } from "../../../generation_helpers.mjs";

export function generateReckonerEvent() {
    const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
        "allsea.randomevent.reckoner"
    );
    mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
    mod.setRecipe(RECIPE_FILE, {
        id: EVENT_START_ID,
        label: `<Savaged by a Reckoner>`,
        grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.reckoner", 10) },
        rootAdd: { ...RESET_TIMER("allsea.randomevent.reckoner") },
        startdescription:
            "<A reckoner Caught me, but i managed to get away. Well some of me me did.>",
        effects: { "mariner.experience.pain": 1, "mariner.experience.impairment": 1, "mariner.experience.dreadful": 1 },
        grandReqs: {
            [`${LOCAL_REPUTATION} >= 4`]: 1,
        },
    });
    setWanderingEventChance(EVENT_START_ID, 20);
}
