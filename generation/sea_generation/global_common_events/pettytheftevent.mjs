import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generatePettyTheftEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allsea.randomevent.pettytheft"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<Petty Theft>`,
    grandReqs: { ...TIMER_ELAPSED("somevent", 25) },
    rootAdd: { ...RESET_TIMER("somevent") },
    startdescription:
      "<The harbors are not always the safest area to be, not even for those native to the waterline. When I arrive back to the kite, I find my purse a little lighter then it was before.>",
    tablereqs: { "funds": 1 },
    furthermore: [
      {
        target: "~/exterior",
        effects: { "funds": -2 },
      }
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
