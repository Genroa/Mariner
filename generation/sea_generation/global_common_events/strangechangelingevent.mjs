import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeChangelingEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.strangechangeling");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.strangechangeling", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.strangechangeling") },
    startdescription:
      "Under a Changeling Moon, peoples \"are\" may bleed into their \"may be\".",
    effects: { "mariner.influences.moth": 4 },
    tablereqs: { "mariner.moon.basic.2": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
