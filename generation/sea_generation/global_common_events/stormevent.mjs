import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStormEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.storm"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Nature Invades`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.storm", 10) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.storm") },
    startdescription:
      "In civil confines, it is easy to forget the the raw strength of nature, but tonight she insists on showing us her majesty. The night is carved to bright shards by lightning, windowpanes clatter in their rebates. My Soul soaks in the wild glory.",
    tablereqs: {
      "mariner.weathermeta.windy": 1,
      "mariner.weathermeta.rainy": 1,
    },
    effects: { "mariner.thrill": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
