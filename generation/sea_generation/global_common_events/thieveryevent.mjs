import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateThieveryEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.thievery"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<Lost in Transport>`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.thievery", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.thievery") },
    startdescription:
      "<Some cargo did not reach my ship after chartering it. it has vanished on the docks. Perhaps this was an accident, perhaps this was negligence. but it likely was malice.>",
    tablereqs: { "mariner.cargo": 1 },
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.cargo": -1 },
      }
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
