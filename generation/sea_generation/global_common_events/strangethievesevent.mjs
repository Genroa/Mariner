import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeThievesEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.strangethievess");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.strangethievess", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.strangethievess") },
    startdescription:
      "Under the Thieves Moon, a soft pipe can be heard on certain corners of certain cities. The Raconteurs are out tonight.",
    effects: { "mariner.influences.knock": 4, },
    tablereqs: { "mariner.moon.basic.8": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
