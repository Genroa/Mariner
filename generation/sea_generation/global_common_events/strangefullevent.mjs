import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeFullEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.strangefull");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.strangefull", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.strangefull") },
    startdescription:
      "Lunacy, as it is defined is ones behavior being influenced by the moon. But how can we not be? when her full, ripe face looks down at us so lovingly, haughtily, fiercly, challengingly. ",
    effects: { "mariner.influences.lantern": 2, "mariner.influences.grail": 2, "mariner.influences.forge": 2, "mariner.influences.edge": 2 },
    tablereqs: { "mariner.moon.basic.5": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
