import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateExampleEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allsea.randomevent.timedexample"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<Some event>`,
    grandReqs: { ...TIMER_ELAPSED("allsea.randomevent.timedexample", 10) },
    rootAdd: { ...RESET_TIMER("allsea.randomevent.timedexample") },
    startdescription:
      "<If you can see this, it means we forgot to remove it from the final build. This is an example recipe that isn't meant to be actually present in the game.>",
    maxexecution: 1,
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
