import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateSmogEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.smog"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Rotting Air`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.smog", 10) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.smog") },
    startdescription:
      "The effluvium of civilisations chokes the streets of this town. Some of my sailors, used to the purifying tonic of sea air, has fallen sick.",
    tablereqs: { "mariner.weathermeta.sickly": 1 },
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew] && [mariner.crew.exhausted]<1": {
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 3,
          },
        },
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
