import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";


export function generateWindyEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.windy"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<Head Blown Empty>`,
    grandReqs: { ...TIMER_ELAPSED("sallseas.randomevent.windy", 10) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.windy") },
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.experience": -1 },
      }
    ],
    startdescription:
      "<A strong wind blows from sea, salty and fresh. As I wonder through the town, it sometimes pushes me forward, and sometimes challenges my steps. The great rushing of air overwhelms my thoughts, until all that is left is me and the wind, and any negative experiences are carried with it.>",
    tablereqs: { "mariner.weathermeta.windy": 1, "mariner.experience": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
