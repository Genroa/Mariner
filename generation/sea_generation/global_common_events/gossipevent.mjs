import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateGossipEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.gossip"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Chance encounter with a Raconteur`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.gossip", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.gossip") },
    startdescription:
      "I encounter another, who knows the stories and whistles the tunes of the raconteurs. After we are sure of our connection, we exchange gossip as if old friends. They let me know something interesting that is going on in the North Sea.",
    effects: { "mariner.rumour.northsea": 1 },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
