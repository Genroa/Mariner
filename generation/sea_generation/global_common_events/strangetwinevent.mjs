import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeTwinEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.strangetwin");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.strangetwin", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.strangetwin") },
    startdescription:
      "with half a mind I wander streets half empty under the other half of the moon. This is not enough. I will soon find myself back at the shoreline, where the dancing reflections can complete neither me nor the moon. I must soon depart, unless this half life becomes my whole existance.",
    effects: { "mariner.experiences.sophorific": 1 },
    tablereqs: { "mariner.moon.basic.3": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
