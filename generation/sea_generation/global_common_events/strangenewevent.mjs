import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeNewEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.strangenew");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonless Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.strangenew", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.strangenew") },
    startdescription:
      "Tonight the sky is dominated by a lack. The moon shows her blackest face, and under that unligth the clandestine prosper, and mortals can mingle and live our own lives in the brief respite that there is no grand light above us, and no terrible purpose before me.",
    effects: { "mariner.influences.moth": 2, "mariner.influences.knock": 2, "mariner.influences.winter": 2, "mariner.influences.heart": 2 },
    tablereqs: { "mariner.moon.basic.1": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
