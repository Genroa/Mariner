import { generateStormEvent } from "./stormevent.mjs";
import { generateSmogEvent } from "./smogevent.mjs";
import { generateScrapEvent } from "./scrapevent.mjs";
import { generateRainEvent } from "./rainevent.mjs";
import { generatePleasantEvent } from "./pleasantevent.mjs";
import { generateNamelessEvent } from "./namelessevent.mjs";
import { generateConnectionEvent } from "./knocksongevent.mjs";
import { generateGossipEvent } from "./gossipevent.mjs";
import { generateChillyEvent } from "./chillyevent.mjs";
import { generatePettyTheftEvent } from "./pettytheftevent.mjs";
import { generateRivalLeavingEvent } from "./lonerival.mjs";
import { generateThieveryEvent } from "./thieveryevent.mjs";
import { generateWindyEvent } from "./windyevent.mjs";

import { generateStrangeEvent } from "./strangeevent.mjs";
import { generateStrangeFullEvent } from "./strangefullevent.mjs";
import { generateStrangeNewEvent } from "./strangenewevent.mjs";
import { generateStrangeTwinEvent } from "./strangetwinevent.mjs";

import { generateAssaultEvent } from "./reputation/assaultevent.mjs";
import { generateChargedEvent } from "./reputation/chargedevent.mjs";
import { generateGossipRepEvent } from "./reputation/gossiprepevent.mjs";
import { generateHarassmentEvent } from "./reputation/harassmentevent.mjs";
import { generateHarriedCaptainEvent } from "./reputation/harriedcaptain.mjs";
import { generateLeakingCaptainEvent } from "./reputation/leakingcaptain.mjs";
import { generatePaperworkEvent } from "./reputation/paperworkevent.mjs";
import { generateProblemsEvent } from "./reputation/problemsevent.mjs";
import { generateSabotageEvent } from "./reputation/sabotageevent.mjs";
import { generateSqueezedCaptainEvent } from "./reputation/squeezedcaptain.mjs";
import { generateTariffsEvent } from "./reputation/tariffsevent.mjs";

import { generateWantedEvent } from "./trait_events/wantedevent.mjs";
import { generateFightEvent } from "./trait_events/cholericevent.mjs";
import { generateCholericPositiveEvent } from "./trait_events/cholericpositive.mjs";
import { generateContemplativeEvent } from "./trait_events/dreamyencountersevent.mjs";
import { generateGoodIntentionsEvent } from "./trait_events/goodintention_event.mjs";
import { generateKeenEyedEvent } from "./trait_events/keeneyedevent.mjs";
import { generateMelancholyEvent } from "./trait_events/melancholicevent.mjs";
import { generateMelancholyPositiveEvent } from "./trait_events/melancholicpositiveevent.mjs";
import { generateNegativeSocialEvent } from "./trait_events/negativesocialevent.mjs";
import { generateOutlanderEvent } from "./trait_events/outlander.mjs";
import { generatePhlegmaticEvent } from "./trait_events/phlegmaticevent.mjs";
import { generatePhlegmaticPositiveEvent } from "./trait_events/phlegmaticpositiveevent.mjs";
import { generateSanguineEvent } from "./trait_events/sanguineevent.mjs";
import { generateSanguinePositivetEvent } from "./trait_events/sanguinepositiveevent.mjs";

export function generateGlobalCommonEvents() {

  // trait events
  generateWantedEvent();
  generateFightEvent();
  generateCholericPositiveEvent();
  generateContemplativeEvent();
  generateGoodIntentionsEvent();
  generateKeenEyedEvent();
  generateMelancholyEvent();
  generateMelancholyPositiveEvent();
  generateNegativeSocialEvent();
  generateOutlanderEvent();
  generatePhlegmaticEvent();
  generatePhlegmaticPositiveEvent();
  generateSanguineEvent();
  generateSanguinePositivetEvent();

  //weather events
  generateWindyEvent();
  generateChillyEvent();
  generateRainEvent();
  generatePleasantEvent();
  generateNamelessEvent();
  generateStormEvent();
  generateSmogEvent();

  //moon events
  generateStrangeEvent();
  generateStrangeFullEvent();
  generateStrangeNewEvent();
  generateStrangeTwinEvent();

  // no requirements
  generateScrapEvent();
  generateConnectionEvent();
  generateGossipEvent();


  generatePettyTheftEvent();
  generateRivalLeavingEvent();
  generateThieveryEvent();

  generateAssaultEvent();
  generateChargedEvent();
  generateGossipRepEvent();
  generateHarassmentEvent();
  generateHarriedCaptainEvent();
  generateLeakingCaptainEvent();
  generatePaperworkEvent();
  generateProblemsEvent
  generateSabotageEvent();
  generateSqueezedCaptainEvent();
  generateTariffsEvent();

}
