import { setWanderingEventChance, wanderingEventScaffhold } from "../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../global_timers_generation.mjs";

export function generateStrangeGluttonsEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold("allseas.randomevent.gluttons");
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Moonlit Ramblings`,
    grandReqs: { ...TIMER_ELAPSED("allseas.randomevent.gluttons", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.gluttons") },
    startdescription:
      "Under a Gluttons moon, savor follows temptation easily, but satisfaction is rare to capture.",
    effects: { "mariner.influences.grail": 4 },
    tablereqs: { "mariner.moon.basic.4": 1, "mariner.weathermeta.clear": 1, },
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
