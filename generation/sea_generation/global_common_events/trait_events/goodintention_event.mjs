import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateGoodIntentionsEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.goodintentions"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Harmony rebounding`,
    grandReqs: { "[~/exterior:mariner.crew.traits.cultured] || [~/exterior:mariner.crew.traits.fashionable] || [~/exterior:mariner.crew.traits.helpful] || [~/exterior:mariner.crew.traits.productive] ||[~/exterior:mariner.crew.traits.humourous] || [~/exterior:mariner.crew.traits.kind] || [~/exterior:mariner.crew.traits.kindhearted] || [~/exterior:mariner.crew.traits.kind] || [~/exterior:mariner.crew.traits.well-intentioned] || [~/exterior:mariner.crew.traits.nice] || [~/exterior:mariner.crew.traits.principled] || [~/exterior:mariner.crew.traits.likable] || [~/exterior:mariner.crew.traits.compassionate] ||[~/exterior:mariner.crew.traits.considerate] ||[~/exterior:mariner.crew.traits.diligent]": 1, ...TIMER_ELAPSED("allseas.randomevent.goodintentions", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.goodintentions") },
    startdescription:
      "A little act of goodness spread among the crew can turn the life of a sailor from a job into a calling and a crew from a collection into a band.",
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          filter: "[mariner.crew]",
          mutate: "mariner.crew.longing",
          level: -1,
          additative: true,
          limit: 4,
        },
      },
      {
        target: "~/exterior",
        mutations: [{
          filter: "[mariner.crew.traits.cultured] || [mariner.crew.traits.fashionable] || [mariner.crew.traits.helpful] || [mariner.crew.traits.productive] ||[mariner.crew.traits.humourous] || [mariner.crew.traits.kind] || [mariner.crew.traits.kindhearted] || [mariner.crew.traits.kind] || [mariner.crew.traits.well-intentioned] || [mariner.crew.traits.nice] || [mariner.crew.traits.principled] || [mariner.crew.traits.likable] || [mariner.crew.traits.compassionate] ||[mariner.crew.traits.considerate] ||[mariner.crew.traits.diligent] && [mariner.crew.exhausted] < 1",
          mutate: "mariner.crew.exhausted",
          level: 1,
          limit: 1,
        }],
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
