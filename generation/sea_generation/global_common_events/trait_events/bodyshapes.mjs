import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";

export function generateWantedEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.bodyshapes"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Two Stooges`,
    grandReqs: { "([~/exterior:mariner.crew.traits.skeletal] || [~/exterior:mariner.crew.traits.skinny] || [~/exterior:mariner.crew.traits.tiny] ||[~/exterior:mariner.crew.traits.slim] || [~/exterior:mariner.crew.traits.small] ||[~/exterior:mariner.crew.traits.petite] || [~/exterior:mariner.crew.traits.frail] ||[~/exterior:mariner.crew.traits.short]) && ([~/exterior:mariner.crew.traits.lanky] ||[~/exterior:mariner.crew.traits.tall] ||[~/exterior:mariner.crew.traits.towering] ||[~/exterior:mariner.crew.traits.brutish] ||[~/exterior:mariner.crew.traits.portly] ||[~/exterior:mariner.crew.traits.stout]||[~/exterior:mariner.crew.traits.wellbuilt]||[~/exterior:mariner.crew.traits.exhausted] ||[~/exterior:mariner.crew.traits.stocky]||[~/exterior:mariner.crew.traits.pudgy] ||[~/exterior:mariner.crew.traits.thick])": 1, ...TIMER_ELAPSED("allseas.randomevent.bodyshapes", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.bodyshapes") },
    startdescription:
      "Unlikely pairings among my crew can offer a welcome bit of levity amids the drudgery of life.",
    furthermore: [
      { effects: { "mariner.thrill": 1 } },
      { target: "~/exterior", effects: { "mariner.wanderlust": -2 } }, {
        target: "~/exterior",
        mutations: {
          filter: "([mariner.crew.traits.skeletal] || [mariner.crew.traits.skinny] || [mariner.crew.traits.tiny] ||[mariner.crew.traits.slim] || [mariner.crew.traits.small] ||[mariner.crew.traits.petite] || [mariner.crew.traits.frail] ||[mariner.crew.traits.short]) && [mariner.crew.exhausted] < 1",
          mutate: "mariner.crew.exhausted",
          level: 1,
          limit: 1,
        },
      },
      {
        target: "~/exterior",
        mutations: {
          filter: "([mariner.crew.traits.lanky] ||[mariner.crew.traits.tall] ||[mariner.crew.traits.towering] ||[mariner.crew.traits.brutish] ||[mariner.crew.traits.portly] ||[mariner.crew.traits.stout]||[mariner.crew.traits.wellbuilt]||[mariner.crew.traits.exhausted] ||[mariner.crew.traits.stocky]||[mariner.crew.traits.pudgy] ||[mariner.crew.traits.thick]) && [mariner.crew.exhausted] < 1",
          mutate: "mariner.crew.exhausted",
          level: 1,
          limit: 1,
        }
      }]
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
