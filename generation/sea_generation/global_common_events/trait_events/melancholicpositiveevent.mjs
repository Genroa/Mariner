import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateMelancholyPositiveEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.melancholypositive"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Understanding spreads like 
    +Ripples`,
    grandReqs: {
      "([~/exterior:mariner.crew.traits.sad] || [~/exterior:mariner.crew.traits.dark] || [~/exterior:mariner.crew.traits.dour] || [~/exterior:mariner.crew.traits.grave] || [~/exterior:mariner.crew.traits.heavyhearted] || [~/exterior:mariner.crew.traits.melancholic] || [~/exterior:mariner.crew.traits.somber]|| [~/exterior:mariner.crew.traits.sorrowful]) &&  [~/exterior:mariner.weathermeta.rainy]": 1,
      ...TIMER_ELAPSED("allseas.randomevent.melancholypositive", 25),
    },
    tablereqs: { "mariner.weathermeta.rain": 1 },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.melancholypositive") },
    startdescription: "",
    furthermore: [
      {
        target: "~/exterior",
        mutations: [
          {
            filter:
              "[mariner.crew.traits.sad] || [mariner.crew.traits.dark] || [mariner.crew.traits.dour] || [mariner.crew.traits.grave] || [mariner.crew.traits.heavyhearted] || [mariner.crew.traits.melancholic] || [mariner.crew.traits.somber]|| [mariner.crew.traits.sorrowful]",
            mutate: "mariner.crew.exhausted",
            level: 5,
            limit: 1,
          },
        ],
      },
      {
        target: "~/exterior",
        effects: { "mariner.experience": -1 },
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
