import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateMelancholyEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.melancholy"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<A Day cast in Sombre Tones>`,
    grandReqs: { "[~/exterior:mariner.crew.traits.sad] || [~/exterior:mariner.crew.traits.dark] || [~/exterior:mariner.crew.traits.dour] || [~/exterior:mariner.crew.traits.grave] || [~/exterior:mariner.crew.traits.heavyhearted] || [~/exterior:mariner.crew.traits.melancholic] || [~/exterior:mariner.crew.traits.somber]|| [~/exterior:mariner.crew.traits.sorrowful]": 1, ...TIMER_ELAPSED("allseas.randomevent.melancholy", 10) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.melancholy") },
    startdescription:
      "<From some people a unpleasant thoughts radiate like spreading frost. And having one such on the crew, it clings to us even when we have entered the ports. Today their moods turned bleak like a winters dawn. We will not get much out of them this.>",
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.thrill": -2 },
      },
      {
        target: "~/exterior",
        mutations: [{
          filter: "[mariner.crew.traits.sad] || [mariner.crew.traits.dark] || [mariner.crew.traits.dour] || [mariner.crew.traits.grave] || [mariner.crew.traits.heavyhearted] || [mariner.crew.traits.melancholic] || [mariner.crew.traits.somber]|| [mariner.crew.traits.sorrowful]",
          mutate: "mariner.crew.exhausted",
          level: 5,
          additative: true,
          limit: 1,

        }],
      },
    ],
  });

  setWanderingEventChance(EVENT_START_ID, 20);
}
