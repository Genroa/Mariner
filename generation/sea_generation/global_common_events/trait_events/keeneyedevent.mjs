import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateKeenEyedEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.keeneyed"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `A Lucky Find>`,
    grandReqs: { "mariner.crew.traits.keeneyed": 1, ...TIMER_ELAPSED("allseas.randomevent.keeneyed", 10) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.keeneyed") },
    startdescription:
      "I find a my keen-eyed crewmember on deck, triumphant. treasured layed out before him. something glinted in the dark, and they stumbled upon it where no one else would have spotted it. Or so they claim. We have our doubts, but they decide to share with the crew.",
    effects: { funds: 2 },
    furthermore: [{ effects: { funds: 2 } }, {
      target: "~/exterior",
      mutations: [{
        filter: "[mariner.crew.traits.keeneyed] && [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 1,
        limit: 1,
      }],
    }]
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
