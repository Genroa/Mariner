import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";

export function generatePhlegmaticEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.phlegmatic"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: 'Placidity Compounding',
    grandReqs: { "[~/exterior:mariner.crew.traits.calm] || [~/exterior:mariner.crew.traits.careful] || [~/exterior:mariner.crew.traits.focussed] || [~/exterior:mariner.crew.traits.devout] || [~/exterior:mariner.crew.traits.fearful]|| [~/exterior:mariner.crew.traits.goodnatured] || [~/exterior:mariner.crew.traits.harmonious] || [~/exterior:mariner.crew.traits.pensive] || [~/exterior:mariner.crew.traits.mildmannered] || [~/exterior:mariner.crew.traits.steadfast] || [~/exterior:mariner.crew.traits.taciturn]": 1, ...TIMER_ELAPSED("allseas.randomevent.phlegmatic", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.phlegmatic") },
    startdescription:
      "<>",
    furthermore: [{
      target: "~/exterior",
      mutations: [{
        filter: "[mariner.crew.traits.calm] || [mariner.crew.traits.careful] || [mariner.crew.traits.focussed] || [mariner.crew.traits.devout] || [mariner.crew.traits.fearful]|| [mariner.crew.traits.goodnatured] || [mariner.crew.traits.harmonious] || [mariner.crew.traits.pensive] || [mariner.crew.traits.mildmannered] || [mariner.crew.traits.steadfast] || [mariner.crew.traits.taciturn]&& [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 6,
        limit: 1,
      }],
    }]
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
