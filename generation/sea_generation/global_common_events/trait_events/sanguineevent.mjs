import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateSanguineEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.sanguine"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Ill-Considered Words`,
    grandReqs: { "[~/exterior:mariner.crew.traits.arrogant] || [~/exterior:mariner.crew.traits.bold]|| [~/exterior:mariner.crew.traits.brave]|| [~/exterior:mariner.crew.traits.brazen]|| [~/exterior:mariner.crew.traits.persistent]|| [~/exterior:mariner.crew.traits.candid]|| [~/exterior:mariner.crew.traits.commanding]|| [~/exterior:mariner.crew.traits.eager]|| [~/exterior:mariner.crew.traits.easygoing]||  [~/exterior:mariner.crew.traits.jolly]||  [~/exterior:mariner.crew.traits.overeager]|| [~/exterior:mariner.crew.traits.loquatious]|| [~/exterior:mariner.crew.traits.proud]|| [~/exterior:mariner.crew.traits.talkative]|| [~/exterior:mariner.crew.traits.energetic]|| [~/exterior:mariner.crew.traits.exuberant] ||[~/exterior:mariner.crew.trait.soutgoing] ||[~/exterior:mariner.crew.traits.passionate]": 1, ...TIMER_ELAPSED("allseas.randomevent.sanguine", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.sanguine") },
    startdescription:
      "The wrong mix of chipper, vain, brazen or persistant can mean the beginning of the end of good will.",
    effects: { "mariner.notoriety": 1 },
    furthermore: [
      {
        target: "~/exterior",
        mutations: [{
          filter: "[mariner.crew.traits.arrogant] || [mariner.crew.traits.bold]|| [mariner.crew.traits.brave]|| [mariner.crew.traits.brazen]|| [mariner.crew.traits.persistent]|| [mariner.crew.traits.candid]|| [mariner.crew.traits.commanding]|| [mariner.crew.traits.eager]|| [mariner.crew.traits.easygoing]|| [mariner.crew.traits.jolly]|| [mariner.crew.traits.overeager]|| [mariner.crew.traits.loquatious]|| [mariner.crew.traits.proud]|| [mariner.crew.traits.talkative]|| [mariner.crew.traits.energetic]|| [mariner.crew.traits.exuberant] ||[mariner.crew.trait.soutgoing] ||[mariner.crew.traits.passionate] && [mariner.crew.exhausted] < 1",
          mutate: "mariner.crew.exhausted",
          level: 1,
          limit: 1
        }],
      }
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
