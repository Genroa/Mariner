import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateCholericPositiveEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.cholericpositive"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Talent Encouragement`,
    grandReqs: { "([~/exterior:mariner.crew.traits.choleric] || [~/exterior:mariner.crew.traits.crass] || [~/exterior:mariner.crew.traits.cruel] || [~/exterior:mariner.crew.traits.careless]||[~/exterior:mariner.crew.traits.vengefull] ||[~/exterior:mariner.crew.traits.vile] ||[~/exterior:mariner.crew.traits.boisterous] ||[~/exterior:mariner.crew.traits.callous]|| [~/exterior:mariner.crew.traits.flighty] || [~/exterior:mariner.crew.traits.hotheaded] || [~/exterior:mariner.crew.traits.nervous]) && [~/exterior:mariner.weathermeta.sunny]": 1, ...TIMER_ELAPSED("allseas.randomevent.cholericpositive", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.cholericpositive") },
    startdescription:
      '<Some of my most commited sailors are not the nicest. But even those talents have their uses. This day, one of my sailors brings with him another, one he insists would be perfect on our ship, an who "has been given plenty of reasons to stay.">',
    furthermore: [{
      target: "~/exterior",
      mutations: [{
        filter: "([mariner.crew.traits.choleric] || [mariner.crew.traits.crass] || [mariner.crew.traits.cruel] || [mariner.crew.traits.careless]||[mariner.crew.traits.vengefull] ||[mariner.crew.traits.vile] ||[mariner.crew.traits.boisterous] ||[mariner.crew.traits.callous]|| [mariner.crew.traits.flighty] || [mariner.crew.traits.hotheaded] || [mariner.crew.traits.nervous])  && [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 1,
        limit: 1,
      }],
    }],
    linked: [
      { id: "mariner.randomevent.cholericpositive.outcome.intellectual" },
      { id: "mariner.randomevent.cholericpositive.outcome.manual" },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.randomevent.cholericpositive.outcome.intellectual",
    deckeffects: {
      "mariner.decks.specialists.opportunities.intellectual.free": 1,
    },
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.randomevent.cholericpositive.outcome.manual",
    deckeffects: {
      "mariner.decks.specialists.opportunities.manual.free": 1,
    },
  });
}
