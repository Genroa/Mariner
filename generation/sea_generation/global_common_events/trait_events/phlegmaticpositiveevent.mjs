import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generatePhlegmaticPositiveEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.phlegmaticpositive"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Something from Nothing`,
    grandReqs: { "[~/exterior:mariner.crew.traits.calm] || [~/exterior:mariner.crew.traits.careful] || [~/exterior:mariner.crew.traits.focussed] || [~/exterior:mariner.crew.traits.devout] || [~/exterior:mariner.crew.traits.fearful]|| [~/exterior:mariner.crew.traits.goodnatured] || [~/exterior:mariner.crew.traits.harmonious] || [~/exterior:mariner.crew.traits.pensive] || [~/exterior:mariner.crew.traits.mildmannered] || [~/exterior:mariner.crew.traits.steadfast] || [~/exterior:mariner.crew.traits.taciturn] && [~/exterior:mariner.weathermeta.misty]": 1, ...TIMER_ELAPSED("allseas.randomevent.phlegmaticpositive", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.phlegmaticpositive") },
    startdescription:
      "Leave a driven person nothing to do, and he will find something to excell at. The mists have driven anything else to do from the world, so this crew member has found something to occupy his time.",
    furthermore: [{ effects: { funds: 2 } }, {
      target: "~/exterior",
      mutations: [{

        filter: "[mariner.crew.traits.calm] || [mariner.crew.traits.careful] || [mariner.crew.traits.focussed] || [mariner.crew.traits.devout] || [mariner.crew.traits.fearful]|| [mariner.crew.traits.goodnatured] || [mariner.crew.traits.harmonious] || [mariner.crew.traits.pensive] || [mariner.crew.traits.mildmannered] || [mariner.crew.traits.steadfast] || [mariner.crew.traits.taciturn]&& [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 1,
        limit: 1,

      }],
    }
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
