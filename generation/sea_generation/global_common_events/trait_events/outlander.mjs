import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateOutlanderEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.outlander"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `The Mores and More`,
    grandReqs: { "[~/exterior:mariner.crew.traits.westislander] || [~/exterior:mariner.crew.traits.channelborn] || [~/exterior:mariner.crew.traits.mainlander] || [~/exterior:mariner.crew.traits.northerner] || [~/exterior:mariner.crew.traits.cityslicker] || [~/exterior:mariner.crew.traits.east-coast] || [~/exterior:mariner.crew.traits.provincial]|| [~/exterior:mariner.crew.traits.heavilyaccented]": 1, ...TIMER_ELAPSED("allseas.randomevent.outlander", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.outlander") },
    startdescription:
      "As a notion, people sometimes enjoy what is exotic, but as a rule they do not like to find it at their doorstep. One of my crew members did not understand the customs of this port so far from their home, and our reputation has suffered for it.",
    effects: { "mariner.notoriety": 1 },
    furthermore: [{
      target: "~/exterior",
      mutations: [{
        filter: "[mariner.crew.traits.westislander] || [mariner.crew.traits.channelborn] || [mariner.crew.traits.mainlander] || [mariner.crew.traits.northerner] || [mariner.crew.traits.cityslicker] || [mariner.crew.traits.east-coast] || [mariner.crew.traits.provincial]|| [mariner.crew.traits.heavilyaccented] && [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 1,
        limit: 1,
      }],
    }]
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
