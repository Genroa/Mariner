import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";

export function generateWantedEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.wanted"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `On Our Trail`,
    grandReqs: { "[~/exterior:mariner.crew.traits.wanted]": 1, ...TIMER_ELAPSED("allseas.randomevent.wanted", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.wanted") },
    startdescription:
      "One of my crew had a past they would have preferred to leave behind, but it has caught up to them now. Their bad name is spreading to the Kite, and it might be wise to leave here soon.",
    effects: { "mariner.notoriety": 2 },
    furthermore: [{
      target: "~/exterior",
      mutations: [{
        filter: "[mariner.crew.traits.wanted] && [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 1,
        limit: 1,
      }],
    }]
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
