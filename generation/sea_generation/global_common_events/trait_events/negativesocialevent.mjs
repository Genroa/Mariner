import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateNegativeSocialEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.negativesocial"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Discord Swells`,
    grandReqs: { "[~/exterior:mariner.crew.traits.humorless] || [~/exterior:mariner.crew.traits.unlikable] || [~/exterior:mariner.crew.traits.wild] || [~/exterior:mariner.crew.traits.foulmouthed] || [~/exterior:mariner.crew.traits.ominous] || [~/exterior:mariner.crew.traits.superstitious] || [~/exterior:mariner.crew.traits.pious]": 1, ...TIMER_ELAPSED("allseas.randomevent.negativesocial", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.negativesocial") },
    startdescription:
      "What is merely irksome at small doses becomes a strain after a longer length of time. And few people have such constraint of a social life as sailors.",
    furthermore: [
      {
        target: "~/exterior",
        mutations: {
          "[mariner.crew]": {
            mutate: "mariner.crew.longing",
            level: 1,
            additative: true,
            limit: 4,
          },
        },
      },
      {
        target: "~/exterior",
        mutations: [{
          filter: "[mariner.crew.traits.humorless] || [mariner.crew.traits.unlikable] || [mariner.crew.traits.wild] || [mariner.crew.traits.foulmouthed] || [mariner.crew.traits.ominous] || [mariner.crew.traits.superstitious] || [mariner.crew.traits.pious]&& [mariner.crew.exhausted] < 1",
          mutate: "mariner.crew.exhausted",
          level: 1,
          limit: 1,
        }],
      }
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
