import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import {
  RESET_TIMER,
  TIMER_ELAPSED,
} from "../../../global_timers_generation.mjs";

export function generateContemplativeEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.contemplative"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `<Ideas Ripple>`,
    grandReqs: { "[~/exterior:mariner.crew.traits.contemplative]": 1, ...TIMER_ELAPSED("allseas.randomevent.contemplative", 10) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.contemplative") },
    startdescription:
      "<Unexpectedly, I meet one of my crewmembers in town, book in hand, but staring out to sea. They speak to me without looking, of the great questions of the life and of the sea. For this moment, we speak without me being captain, and them being crew. Today, They lead, and I follow and insights rise like waves.>",
    effects: { "mariner.influences.lantern": 4 },
    furthermore: [{
      target: "~/exterior",
      mutations: [{
        filter: "[mariner.crew.traits.contemplative] && [mariner.crew.exhausted] < 1",
        mutate: "mariner.crew.exhausted",
        level: 1,
        limit: 1,

      }],
    }],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
