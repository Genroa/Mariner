import {
  setWanderingEventChance,
  wanderingEventScaffhold,
} from "../../../generation_helpers.mjs";
import { RESET_TIMER, TIMER_ELAPSED } from "../../../global_timers_generation.mjs";

export function generateFightEvent() {
  const { RECIPE_FILE, EVENT_START_ID } = wanderingEventScaffhold(
    "allseas.randomevent.fight"
  );
  mod.initializeRecipeFile(RECIPE_FILE, ["locations", "global_common_events"]);
  mod.setRecipe(RECIPE_FILE, {
    id: EVENT_START_ID,
    label: `Fisticuffs on the Docks`,
    grandReqs: { "([~/exterior:mariner.crew.traits.choleric] || [~/exterior:mariner.crew.traits.crass] || [~/exterior:mariner.crew.traits.cruel] || [~/exterior:mariner.crew.traits.careless]||[~/exterior:mariner.crew.traits.vengefull] ||[~/exterior:mariner.crew.traits.vile] ||[~/exterior:mariner.crew.traits.boisterous] ||[~/exterior:mariner.crew.traits.callous]|| [~/exterior:mariner.crew.traits.flighty] || [~/exterior:mariner.crew.traits.hotheaded] || [~/exterior:mariner.crew.traits.nervous])": 1, ...TIMER_ELAPSED("allseas.randomevent.fight", 25) },
    rootAdd: { ...RESET_TIMER("allseas.randomevent.fight") },
    startdescription:
      "One of my crew has a high-strung temperament, and recently it has snapped. Blows where traded, and this has not come to the good of my good name, or the health of the crew.",
    furthermore: [
      { effects: { "mariner.notoriety": 1 } },
      {
        target: "~/exterior",
        mutations: [{
          filter: "([mariner.crew.traits.choleric] || [mariner.crew.traits.crass] || [mariner.crew.traits.cruel] || [mariner.crew.traits.careless]||[mariner.crew.traits.vengefull] ||[mariner.crew.traits.vile] ||[mariner.crew.traits.boisterous] ||[mariner.crew.traits.callous]|| [mariner.crew.traits.flighty] || [mariner.crew.traits.hotheaded] || [mariner.crew.traits.nervous])  && [mariner.crew.exhausted] < 1",
          mutate: "mariner.crew.exhausted",
          level: 5,
          limit: 1,
        }],
      },
    ],
  });
  setWanderingEventChance(EVENT_START_ID, 20);
}
