import { SIGNALS } from "../generate_signal_aspects.mjs";
import { ME_TRANSFORM } from "../generation_helpers.mjs";

const CARGO_TYPES = {
  local: {
    label: (portRegion) => `Cargo [local, destination: ${portRegion}]`,
    description: (portRegion) =>
      "Goods and wares that reliably sell on the local market. They will fetch more then they cost, but not much more.",
    price: 2,
    upgrade: "close",
  },
  close: {
    label: (portRegion) => `Cargo [close, destination: ${portRegion}]`,
    description: (portRegion) =>
      `Goods and wares with a destination of ${portRegion}. They will fetch a pretty penny, when they make it there.`,
    price: 3,
    upgrade: "distant",
  },
  distant: {
    label: (portRegion) => `Cargo [long haul, destination: ${portRegion}]`,
    description: (portRegion) =>
      `What is plentyful in one region, is exotic far away. If I bring this haul to ${portRegion}, I will quadruple my investment.`,
    price: 4,
  },
};

export function generateCargo(portRegion, distance) {
  mod.setElements(
    "cargo",
    {
      id: `mariner.cargoprecursor.for.${portRegion}.${distance}`,
      decayTo: `mariner.cargo.for.${portRegion}.${distance}`,
      xtriggers: {
        [SIGNALS.UPGRADE_CARGO]:
          CARGO_TYPES[distance].upgrade !== undefined
            ? ME_TRANSFORM(`mariner.cargoprecursor.for.${portRegion}.${CARGO_TYPES[distance].upgrade}`)
            : undefined,
      },
      aspects: {
        "mariner.cargoprecursor": 1,
        ...(CARGO_TYPES[distance].upgrade !== undefined ? { "mariner.cargo.canbeupgraded": 1 } : {}),
      },
    },
    {
      id: `mariner.cargo.for.${portRegion}.${distance}`,
      label: CARGO_TYPES[distance].label(portRegion),
      description: CARGO_TYPES[distance].description(portRegion),
      icon: "cargo",
      aspects: {
        "mariner.cargo": 1,
        "mariner.holduse": 1,
        modded_explore_allowed: 1,
      },
      xtriggers: {
        [SIGNALS.SELL_CARGO]: [
          {
            morpheffect: "break",
            chance: `100 - ([~/extant : mariner.locations.${portRegion}.sailingthere] * 100)`,
          },
          {
            morpheffect: "transform",
            id: "funds",
            level: CARGO_TYPES[distance].price,
          },
        ],
      },
    }
  );
  return `mariner.cargo.for.${portRegion}.${distance}`;
}
