import { generateLocationsDiscovery } from "./generate_locations_discovery.mjs";
import { generateVaultsContent } from "../vault_generation/vault_generation.mjs";

import { generateNorthSea } from "./northsea/generate_north_sea.mjs";
import { generateMedSea } from "./medsea/generate_mediterranean_sea.mjs";
import { generateGlobalCommonEvents } from "./global_common_events/index.mjs";
import { generateComposableArchetypes } from "./generate_composable_archetypes.mjs";
import { generateSailingSystem } from "./generate_sailing_system.mjs";
import { generatePortActions } from "../port_generation/port_actions/generate_port_actions.mjs";
import { generateUpgradeCargo } from "../generate_wharf_actions.mjs";

export function generateSeas() {
  generateComposableArchetypes();
  mod.initializeAspectFile("aspects.agents.locations", ["ais"]);
  mod.initializeRecipeFile("recipes.agents.locations", ["ais"]);

  mod.initializeElementFile("cargo", ["resources"]);
  mod.initializeRecipeFile("recipes.buycargo", ["resources"]);
  mod.initializeRecipeFile("recipes.upgradecargo", ["resources"]);
  generateUpgradeCargo();
  mod.initializeDeckFile("decks.resources", ["resources"]);
  mod.setDeck("decks.resources", {
    id: "mariner.decks.cargo",
    spec: [],
    shuffleAfterDraw: true,
  });

  mod.initializeAspectFile("aspects.seas", ["locations"]);
  mod.initializeAspectFile("aspects.destinations", ["locations"]);
  mod.initializeAspectFile("aspects.deliveryrecipients", ["packages"]);
  mod.initializeAspectFile("aspects.packagerecipients", ["packages"]);
  mod.initializeDeckFile("decks.seas", ["locations"]);
  mod.initializeRecipeFile("recipes.sailing.arriving.unlockavailableports", ["sailing"]);
  mod.initializeRecipeFile("recipes.dockto.genericvaults", ["sailing", "dockto"]);
  mod.initializeAspectFile("aspects.seas.uniquenessgroups", ["locations"]);

  mod.initializeElementFile("rumours", ["locations"]);
  mod.initializeAspectFile("aspects.rumours", ["locations"]);
  mod.setAspect("aspects.rumours", {
    id: "mariner.rumour",
    label: "A Rumour",
    description: "This is the edge between fact and fiction. It a thread to follow to find the unlikely or the extraordinary.",
  });

  mod.readRecipesFile("recipes.sailing.leaving", ["sailing"]);

  mod.setAspect("aspects.destinations", {
    id: "mariner.waypoint",
    label: "Waypoint",
  });
  mod.initializeElementFile("waypoints", ["sailing", "waypoints"]);
  mod.initializeAspectFile("aspects.waypoints", ["sailing", "waypoints"]);
  mod.setAspects(
    "aspects.waypoints",
    { id: "mariner.waypoint.north", label: "North Waypoint" },
    { id: "mariner.waypoint.east", label: "East Waypoint" },
    { id: "mariner.waypoint.south", label: "South Waypoint" },
    { id: "mariner.waypoint.west", label: "West Waypoint" }
  );
  mod.setElements(
    "waypoints",
    {
      id: "mariner.waypoint.north.none",
      useBigPicture: true,
      useAlternativeTextStyle: true,
      aspects: { "mariner.waypoint": 1, "mariner.waypoint.north": 1 },
      xtriggers: {},
    },
    {
      id: "mariner.waypoint.east.none",
      useBigPicture: true,
      useAlternativeTextStyle: true,
      aspects: { "mariner.waypoint": 1, "mariner.waypoint.east": 1 },
      xtriggers: {},
    },
    {
      id: "mariner.waypoint.south.none",
      useBigPicture: true,
      useAlternativeTextStyle: true,
      aspects: { "mariner.waypoint": 1, "mariner.waypoint.south": 1 },
      xtriggers: {},
    },
    {
      id: "mariner.waypoint.west.none",
      useBigPicture: true,
      useAlternativeTextStyle: true,
      aspects: { "mariner.waypoint": 1, "mariner.waypoint.west": 1 },
      xtriggers: {},
    }
  );

  mod.deckIdsToShuffleOnLeave = {};
  generateVaultsContent();

  generateNorthSea();
  generateMedSea();
  generateLocationsDiscovery();
  generateGlobalCommonEvents();
  //console.log("\x1b[36m[Package Generation]\x1b[0m Description Sentences use stats:", mod.packageGenerationDescriptionStats);
  //console.log("\x1b[36m[Package Generation]\x1b[0m Inspection Sentences use stats:", mod.packageGenerationInspectionEndStats);
  generateSailingSystem();

  generatePortActions();
}
