const DEFAULT_HELP_SLOT = (i) => ({
  id: `help_${i}`,
  label: "Help",
  required: {
    tool: 1,
    "mariner.influence": 1,
    "mariner.crew": 1,
  },
  forbidden: {
    "mariner.crew.exhausted": 1,
    "mariner.tool.broken": 1,
  },
});

export function generateDistanceMatchingMinigame({
  id,
  distanceElement,
  startRecipe = {},
  tooLowRecipe,
  tooHighRecipe,
  failTooLowRecipe = {},
  failTooHighRecipe = {},
  startingDistance = 5,
  min = 0,
  max = 10,
  tooLow = 2,
  tooHigh = 8,
  states,
}) {
  const RECIPE_FILE = `recipes.minigames.distancematching.${id}`;
  const ELEMENT_FILE = `minigames.distancematching.${id}`;
  const ASPECT_FILE = `aspects.minigames.distancematching.${id}`;
  const PREFIX = `mariner.minigames.distancematching.${id}`;

  mod.initializeRecipeFile(RECIPE_FILE, ["minigames", "distancematching", id]);
  mod.initializeElementFile(ELEMENT_FILE, ["minigames", "distancematching", id]);
  mod.initializeAspectFile(ASPECT_FILE, ["minigames", "distancematching", id]);

  if (!mod.aspects["aspects.minigames.distancematching"]) {
    mod.initializeAspectFile("aspects.minigames.distancematching", ["minigames", "distancematching"]);
    mod.setHiddenAspect("aspects.minigames.distancematching", {
      id: "mariner.minigames.distance",
    });
    mod.setHiddenAspect("aspects.minigames.distancematching", {
      id: "mariner.minigames.steps",
    });
  }

  if (!mod.verbs[`verbs.minigames.distancematching`])
    mod.initializeVerbFile("verbs.minigames.distancematching", ["minigames", "distancematching"]);

  mod.setVerb("verbs.minigames.distancematching", {
    id: `mariner.minigames.distancematching.holdhelp`,
    multiple: true,
  });

  if (!mod.recipes["recipes.minigames.distancematching"]) {
    mod.initializeRecipeFile("recipes.minigames.distancematching", ["minigames", "distancematching"]);
    mod.setRecipe("recipes.minigames.distancematching", {
      id: "mariner.minigames.distancematching.holdhelp.hold",
      actionId: `mariner.minigames.distancematching.holdhelp`,
      warmup: 40,
    });
  }

  mod.setElement(ELEMENT_FILE, {
    id: `${PREFIX}.distance`,
    ...distanceElement,
    aspects: { "mariner.minigames.distance": 1 },
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${PREFIX}.start`,
    effects: { [`${PREFIX}.distance`]: startingDistance },
    linked: [{ id: `${PREFIX}.pickstate` }],
    ...startRecipe,
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${PREFIX}.pickstate`,
    linked: states.map((state) => ({ id: `${PREFIX}.states.${state.id}.warmup` })),
  });

  if (tooLowRecipe) {
    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.warmup.toolow`,
      requirements: { "mariner.minigames.distance": -(tooLow + 1) },
      // grandReqs: { [`[mariner.minigames.distance] <= ${tooLow}`]: 1 },
      ...tooLowRecipe,
      linked: [{ id: `${PREFIX}.effect` }],
    });
  }

  if (tooHighRecipe) {
    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.warmup.toohigh`,
      requirements: { "mariner.minigames.distance": tooHigh },
      // grandReqs: { [`[mariner.minigames.distance] >= ${tooHigh}`]: 1 },
      ...tooHighRecipe,
      linked: [{ id: `${PREFIX}.effect` }],
    });
  }

  mod.setRecipe(RECIPE_FILE, {
    id: `${PREFIX}.fail.toolow`,
    grandReqs: { [`[${PREFIX}.distance] <= ${min}`]: 1 },
    ...failTooLowRecipe,
  });

  mod.setRecipe(RECIPE_FILE, {
    id: `${PREFIX}.fail.toohigh`,
    grandReqs: { [`[${PREFIX}.distance] >= ${max}`]: 1 },
    ...failTooHighRecipe,
  });

  for (const state of states) {
    const helpEffects = state.helpEffects ?? [];

    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.states.${state.id}.warmup`,
      warmup: 10,
      slots: [DEFAULT_HELP_SLOT(1), DEFAULT_HELP_SLOT(2), DEFAULT_HELP_SLOT(3)],
      ...state.warmupRecipe,
      alt: [
        ...helpEffects.map((help) => ({
          id: `${PREFIX}.states.${state.id}.warmup.usinghelp.${help.id ?? help.aspect}`,
          requirements: { [help.aspect]: state.quantity ?? 1 },
          label: help.label ?? "",
          startdescription: help.startdescription ?? "",
          linked: [{ id: `${PREFIX}.states.${state.id}.effect` }],
        })),
        ...(state.tooLowRecipe ? [{ id: `${PREFIX}.states.${state.id}.warmup.toolow` }] : []),
        ...(state.tooHighRecipe ? [{ id: `${PREFIX}.states.${state.id}.warmup.toohigh` }] : []),
      ],
      linked: [{ id: `${PREFIX}.states.${state.id}.effect` }],
    });

    if (state.tooLowRecipe) {
      mod.setRecipe(RECIPE_FILE, {
        id: `${PREFIX}.states.${state.id}.warmup.toolow`,
        grandReqs: { [`[${PREFIX}.distance] <= ${tooLow}`]: 1 },
        ...state.tooLowRecipe,
        linked: [{ id: `${PREFIX}.states.${state.id}.effect` }],
      });
    }

    if (state.tooHighRecipe) {
      mod.setRecipe(RECIPE_FILE, {
        id: `${PREFIX}.states.${state.id}.warmup.toohigh`,
        grandReqs: { [`[${PREFIX}.distance] >= ${tooHigh}`]: 1 },
        ...state.tooHighRecipe,
        linked: [{ id: `${PREFIX}.states.${state.id}.effect` }],
      });
    }

    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.states.${state.id}.effect`,
      linked: [
        ...helpEffects.map((help) => ({ id: `${PREFIX}.states.${state.id}.usehelp.${help.aspect}` })),
        { id: `${PREFIX}.states.${state.id}.nohelpused` },
      ],
    });

    for (const helpDefinition of helpEffects) {
      const aspect = helpDefinition.aspect;
      let effects = helpDefinition.recipe;
      if (!Array.isArray(effects)) effects = [effects];
      mod.setRecipe(RECIPE_FILE, {
        id: `${PREFIX}.states.${state.id}.usehelp.${helpDefinition.id ?? aspect}`,
        requirements: { [aspect]: 1 },
        furthermore: [
          ...effects,
          ...(helpDefinition.noInduction
            ? []
            : [
                {
                  inductions: [
                    { id: `mariner.minigames.distancematching.holdhelp.hold`, expulsion: { filter: { [aspect]: 1 } } },
                  ],
                },
              ]),
        ],
        linked: [{ id: `${PREFIX}.states.${state.id}.postwarmup` }],
      });
    }

    // Generate the default consequence effect
    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.states.${state.id}.nohelpused`,
      linked: [{ id: `${PREFIX}.states.${state.id}.postwarmup` }],
      ...state.noHelpUsed,
    });

    mod.setRecipe(RECIPE_FILE, {
      id: `${PREFIX}.states.${state.id}.postwarmup`,
      linked: [{ id: `${PREFIX}.fail.toolow` }, { id: `${PREFIX}.fail.toohigh` }, { id: `${PREFIX}.pickstate` }],
      ...state.postWarmupRecipe,
    });
  }
}
