import { INJURE_CREWMEMBER, requireAndDestroy } from "../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { RECIPES_FILE, vaultPrefix } from "./rescue_mission.mjs";

// EVENTS
// TASKS
export function generateIslandLayer(vb) {
  vb.defineLayer("3");

  vb.definePassiveHelpElement({
    id: "survivor",
    label: "Survivor",
    description: "Shipwrecked, starving, scrappy, sick.", //should maybe be a base game thing. supplies, medicine, rations, herbs. solves different events, changes othert ones, causes other ones.
  });

  vb.defineExplorationTask({
    taskId: "shore",
    locationLabel: "The Unknown Interior",
    icon: "mariner.vaults.rescuemission.island",
    locationDescription:
      "Rocky shores, dense forests and sheer cliffs. While this island might be habitable, it does not seem hospitable. Now to hope the helpless shipwrecked crew has survived until now.",
    tasks: ["trails", "hut", "treasureshore"],
  });

  vb.defineTask({
    taskId: "trails",
    label: "The Island Game Trails",
    icon: "tasks/mariner.tasks.steps.green",
    description:
      "Paths wind through the forest and over the plains, hewn out by the rotations of the grazers finding their way through the seasons. Anyone traversing this island would follow these paths.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "The Game of Trails",
            startdescription:
              "These paths are not random. They are carved by the beasts following the logic of the land. If we crouch low, reach deep, and follow those instincts buried beneath our bones, we will surely find the survivor's camp. ",
          },
          success: {
            label: "That Common Ground",
            description:
              "There are a few commonalities that all life seeks to survive. Shelter, clean water, resources. As we follow the hoofprints of the island deer, we were funneled inevitably to the peak of those small comforts. And inevitably, that is where we found the survivor's lair.",
            effects: vb.vp({ encampment: 1, trails: -1 }),
          },
          failure: {
            label: "Led Around The Horn",
            description:
              "We follow the goats' trails up through the hills and around the rocky cliffs. They lead us nowhere except a particular patch of salt and minerals, where one of our own slips and trips. In the end, all we find is a tragedy between slate and sea and sky, we have no option but to return home to try another tactic.",
            inductions: [INJURE_CREWMEMBER],
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "hut",
    label: "Woodsman's Hut",
    icon: "tasks/mariner.tasks.door.pink",
    description:
      "Clinging to the slopes of the rocky interior, we find a little cottage of timber, with a padlocked door and boarded window. We can see scratch and batter marks on the exterior, but it does not seem like the threshold has been breached.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Conquer the Cottage",
            startdescription:
              "While the survivors have not found this refuge, something in there might be of use to us. The padded lock has long since rusted through, so all that would get us in is raw, brute strength.",
          },
          success: {
            label: "Force our Entry",
            description:
              "With a Gilgameshian effort, the padlock is snapped and the wood splintered. Inside the abode, we find a leather case with the tools of the trade of its one-time inhabitant. It's edges gleam still, not a spot on it.",
            effects: { [vb.vp("hut")]: -1, "mariner.tool.edge.1": 1 },
            linked: [{ id: "mariner.drawreward.jumble" }],
          },
          failure: {
            label: "Barriers Unyielding",
            description:
              "The solid wood is unresponsive to our tools, our guile, our fingernails. We are left dismayed, hurt and dejected, and still unclear on what might be contained inside. It was probably not worth it, we decide. ",
            effects: { "mariner.experiences.injury.standby": 1 },
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "treasureshore",
    label: "The Treasure-Sown Shore",
    icon: "tasks/mariner.tasks.shoreline.yellow",
    description:
      "These islands are known for shipwrecks. These shores are known for their salvage, and the locals are known for combing the beaches for whatever they might find.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Search guided by Intellect",
            startdescription:
              "If I check my charts of currents and my maps of sea and land, I might find the place where the shipwrecked ship's cargo hits its rest upon the shore.",
          },
          success: {
            label: "Sheltered Stash",
            description:
              "In a little inlet, where driftwood sticks out like straw from within a semicircle of sea-shorn boulders. Nestled within lies treasures deposited by the currents, nipped at by wind and weather, but not devoured.",
            effects: { [vb.vp("treasureshore")]: -1 },
            deckeffects: { "mariner.decks.cargo": 1 },
            linked: [{ id: "mariner.drawreward.jumble" }],
          },
          failure: {
            label: "Opportunity Lost",
            description:
              "We searched as long as we thought wise, and then a little longer still. As we ventured back to our ship, the waves were nipping at our heels like wild dogs. As we reached the high ground, all the sand was gone, and we could see flotsam of cargo, shattered against the shore. ",
            effects: vb.vp({ treasureshore: -1 }),
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      "mariner.sing": {
        challenge: {
          task: {
            label: "Search Guided by Instinct",
            startdescription:
              "All shorelines are sacred, this is true. All shorelines are a battlefield, this is said.  All shorelines are in crucem, for so many lay a claim to them, and to so many, they are sanctified. Let me follow this beach deeper rhythm, and see what I find. ",
          },
          success: {
            label: "A Gift-in-Wrapping",
            description:
              "I trail across the beach, following the wake of the crashing waves, harmonizing with their choir of thundering roars and hissing whispers. I come across a patch of the beach decorated with garlands of seaweed and thorny dune brush. In the center of that stage, I bend down and collect a treasure in its protective chest.",
            effects: {
              [vb.vp("treasureshore")]: -1,
              "mariner.trappings.heart.1": 1, //should be a higher level trapping.
            },
          },
          failure: {
            label: "Passion Lost",
            description:
              "I wander… I stare at the horizon. I hum and study the beach. But the moment has passed. Today I shall find nothing exceptional here. Nothing the sky and the sea and the horizon in between.",
            effects: {
              "mariner.experiences.sobering.standby": 1,
              "mariner.wanderlust": 1,
            },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
    },
  });

  vb.defineTask({
    taskId: "encampment",
    label: "The Pitiful Encampment",
    icon: "tasks/mariner.tasks.tents.vertdegris",
    description:
      "Ramshackle huts between windshorn trees. The folks inside are hardly in any better condition than the shelter they fashioned. A sorry lot, hardly able to stand from malnourishment and exhaustion. \n But Alive. ",
    actions: {
      explore: {
        challenge: {
          bypass: requireAndDestroy(vb.vp("supplies")),
          task: {
            label: "Get Them Back On Their Feet",
            startdescription:
              "They need medical care, they require energy, warmth, and comfort before they can make the track back to the ship. ",
          },
          success: {
            label: "Nourished and Sustained",
            description: "It is not a recovery yet, but it is on the path towards it.",
            effects: {
              [vb.vp("encampment")]: -1,
              [vb.vp("grim")]: 1,
              [vb.vp("survivor")]: 4,
            },
          },
          failure: {
            label: "Cold Comfort",
            description:
              "We do what we can, but we are insufficiently knowledgeable, or insufficiently prepared. One of the sailors we meet closes their eyes for the final time, right after we have laid eyes on them for the first time.",
            effects: {
              [vb.vp("encampment")]: -1,
              [vb.vp("grim")]: 1,
              [vb.vp("survivor")]: 3,
            },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "grim",
    label: "The Grim and Grizzled",
    icon: "tasks/mariner.tasks.watchers.vertdegris",
    description:
      "While a shore dweller might think this lot ill-capable or unlucky, any sailor knows a different truth. They survived. They were spared by the sea and scraped together a life on these unforgiving shore.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "The Long Track Back",
            startdescription:
              "Finding the men fulfilled our curiosity, bringing them back alive will fill our coffer. For in polite society, the value of life is still estimated higher than the value of flesh. We will bring them to the Kite and then home.",
          },
          success: {
            label: "Reached the Kite",
            description: "Back to the comfort of our swaying, wooden box.",
            effects: vb.vp({ setoff: 1 }),
          },
          failure: {
            label: "Another Last Breath",
            description:
              "They could not survive the harsh hike. While the body reaches the kite, the soul will linger on these shores.",
            effects: vb.vp({ setoff: 1 }),
            furthermore: [
              {
                target: "~/exterior",
                effects: vb.vp({ survivor: -1 }),
              },
            ],
          },
          template: probabilityChallengeTemplates.ENDURE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
      talk: {
        challenge: {
          task: {
            label: "Win Their Hearts",
            startdescription: "These sailors are perfect for my crew. Always better to have a lucky sailor than a brave one.",
          },
          success: {
            label: "New Blood on the Kite",
            description: "A grin, a handshake, and a clinging purse. These sailors are ready for another adventure.",
            furthermore: [
              {
                target: "~/exterior",
                effects: vb.vp({ survivor: -1 }),
              },
            ],
            deckeffects: {
              "mariner.decks.specialists.opportunities.manual.free": 1,
            },
          },
          failure: {
            label: "Seasick and Homesick",
            description:
              "They shake their heads and turn away from me. All these men long for now is the shore. They are no use to me.",
            effects: {
              "mariner.experiences.divisive.standby": 1,
              "mariner.wanderlust": 1,
            },
          },
          template: probabilityChallengeTemplates.HONEYED_TONGUE,
        },
        slots: CHALLENGE_SLOTS(3, "talk"),
      },
    },
  });

  vb.defineTask({
    taskId: "setoff",
    label: "Set Off for Home",
    icon: "tasks/mariner.tasks.boat.blue",
    description:
      "The final lap of our journey, is going all the way back. Luckily the path always feels shorter when the way is known.",
    actions: {
      "mariner.sail": {
        challenge: {
          task: {
            label: "The Final Lapping Waves",
            startdescription:
              "Oncemore, we brace the waves. The Kites is carrying more life then she usually is, but death is following us like a wolfish shadow. ",
          },
          success: {
            label: "Into Home Port",
            description: "The trip goes smoothly, and time flies by as if we travel for only a fraction of the time. ",
            effects: vb.vp({ setoff: -1 }),
            linked: [{ id: vb.vp("payout") }],
          },
          failure: {
            label: "One Soul Lost to the Howl",
            description:
              "Their frail constitutions did not survive the rigors of the rough voyage home. With the harbor in sight, one's breath is snatched by the fierce north winds, and then, cruelty on cruelty, we reach the harbor safely anyway.",
            effects: vb.vp({ setoff: -1 }),
            furthermore: [
              {
                target: "~/exterior",
                effects: vb.vp({ survivor: -1 }),
              },
            ],
            linked: [{ id: vb.vp("payout") }],
          },
          template: probabilityChallengeTemplates.OVERTAKE,
        },
        slots: SHIP_MANOEUVERING_SLOTS(),
      },
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("payout"),
    actionId: "payout",
    label: "Rewards Due",
    startdescription: "The value of life, a prosaic weight in my pockets.",
    furthermore: [
      {
        effects: {
          funds: `[~/exterior:${vb.vp("survivor")}]*2`,
        },
      },
      {
        target: "~/extant",
        effects: {
          [vb.vp("survivor")]: -10,
        },
      },
    ],
  });
}
