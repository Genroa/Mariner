import { requireAndDestroy } from "../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { SHIP_NAME } from "../ship_wrecks_names.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "./rescue_mission.mjs";

// EVENTS
export function generateHarborEvents(vb) {
  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.heart",
    task: {
      id: vaultPrefix("events.boredom"),
      craftable: false,
      label: "Small Town Jitters",
      startdescription:
        "I am not made to sit still long, and this dreary backwater harbor has little to offer to keep my Lack-Heart content. I wish to leave... I should calm myself with Winter or fortify myself with Heart",
    },
    chance: quantityDuoToChance("heart", "winter", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Inner fire",
      description:
        "I call upon an inner store of mental fortitude, and push myself on a little longer. I still have more to do in this town.",
    },
    failure: {
      label: "Hole in my Chest",
      description:
        "Wherever I wish to go, I find the streets turning back to the waters' edge. The Horizon calls to me... and my Lack-Heart eats at my joys until I answer that call.",
      effects: { "mariner.wanderlust": 1 },
    },
  });
}

// TASKS
export function generateHarborLayer(vb) {
  vb.defineLayer("1");

  vb.defineHelpElement({
    id: "trail",
    label: "A Trail...",
    icon: "mariner.vaults.salvagemission.trail",
    description: "Notes on the cargo, speculations on the route, indication on how the ship was stowed.",
    aspects: { lantern: 2, winter: 2 },
  });

  vb.defineHelpElement({
    id: "resonance",
    label: "A Resonance...",
    description: "I can feel it from under the horizon. Like a low hum, lower than breathing, lower than the ocean's roll.",
    aspects: { lantern: 2, secrethistories: 2 },
  });

  vb.defineHelpElement({
    id: "notion",
    label: "A Notion...",
    icon: "mariner.vaults.salvagemission.notion",
    description: `Locally this is as solid as fact… Venture further and it would be dismissed as biased speculation. [This can be used to pass the Rapids or help find where the ${SHIP_NAME} went down.]`,
    aspects: { lantern: 2, moth: 2 },
  });

  vb.defineHelpElement({
    id: "supplies",
    label: "Supplies",
    icon: "cargo",
    description: "Those things the mortal engine requires for fuel and maintenance.", //should maybe be a base game thing. supplies, medicine, rations, herbs. solves different events, changes othert ones, causes other ones.
  });

  vb.defineExplorationTask({
    taskId: "harbor",
    locationLabel: "Backwater Harbor",
    icon: "mariner.vaults.rescuemission.harbor",
    locationDescription: `Where a near-nameless tributary flows into a mist-clogged sea. This is where the ${SHIP_NAME} has last been seen. So this is where our search begins.`,
    tasks: ["generalstore", "detailsofthewreck", "radiotower", "divebar", "sailout"], //icon?
    events: ["events.boredom"],
    next: [{ id: "mariner.applyshipnameid" }],
  });

  vb.defineTask({
    taskId: "generalstore",
    label: "General store",
    icon: "tasks/mariner.tasks.market.darkpurple",
    description: "Sometimes a good store is just a good store. But it holds stores of goods which are good to store.",
    actions: {
      explore: {
        hint: {
          label: "The Things Life Needs",
          startdescription: `If I find the survivors of the ${SHIP_NAME} wreck. I must feed them and clothe them and treat their wounds. I could buy the goods I need here. Or… I could try to get them another way. [You can either Buy the supplies with Funds or steal them by attempting the challenge with Moth or Edge]`,
        },
        challenge: {
          bypass: requireAndDestroy("funds"),
          task: {
            label: "The Things Life Needs",
            startdescription: `If I find the survivors of the ${SHIP_NAME} wreck. I must feed them and clothe them and treat their wounds. I could buy the goods I need here. Or… I could try to get them another way.`,
          },
          success: {
            label: "Movements Unregarded",
            description: "Practiced fingers place the items in a box. No one pays the exchange any mind. I whistle as I leave.",
            effects: { [vb.vp("supplies")]: 1, funds: -1 },
          },
          failure: {
            label: "The Embarrassing Light of Day",
            description:
              "We are found with our hands in places they do not belong. We are searched and stripped and fined for our troubles too. We are not allowed entry into the store again.",
            effects: {
              "mariner.experiences.divisive.standby": 1,
              funds: -1,
              [vb.vp("generalstore")]: -1,
            },
            purge: { funds: 2 },
          },
          template: probabilityChallengeTemplates.SNEAK_IN,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "detailsofthewreck",
    label: "Details of the Wreck",
    icon: "tasks/mariner.tasks.papers.yellow",
    description: `We have some details about the ${SHIP_NAME}'s last journey. Some scraps of paper, some lists of names and goods. Somewhere here, we could find the hints to the location of its crew.`,
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Delving Down the Paper Trail",
            startdescription:
              "Every ship, commercial or private, leaves documents and figures in its wake. The harbormaster will not do my work for me however, and the archive is as cold and dusty. These will be long, harrowing hours. ",
            requirements: {},
          },
          success: {
            label: "The Glint of Reason",
            description:
              "Between the words, a pattern. Within the pattern, a clue. And from that a direction to sail, and an area to comb.",
            effects: vb.vp({ trail: 1 }),
          },
          failure: {
            label: "The Death of Passion",
            description:
              "My mind has become as dusty and disorganized as the archives I dwell in. The world seems frayed and yellowed and dull and disappointing. I have not found what I sought.",
            effects: { "mariner.experiences.sobering.standby": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "radiotower",
    label: "Radio Tower",
    icon: "tasks/mariner.tasks.ear.vertdegris",
    description: `A spindly tower of latticed steel. Here the S.O.S. was received that informed us that the ${SHIP_NAME}'s wreck had survivors.  The received message is more static than sound, but perhaps I can discover some hidden depth within the tones.`,
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "From the Aether",
            startdescription: `A spindly tower of latticed steel.  Here the S.O.S. was received that informed us that the ${SHIP_NAME}'s wreck had survivors.  The received message is more static than sound, but perhaps I can discover some hidden depth within the tones.`,
          },
          success: {
            label: "Hear What Sounds Beneath",
            description:
              "There is a source to the sound, twisted and distorted as it came through the medium. If I strip it of the chaos of the travel, the flaws of men and machines, I find the signature tone of their location.",
            effects: vb.vp({ resonance: 1 }),
          },
          failure: {
            label: "Strain For What Lies Above",
            description:
              "I start to hear many things. The patterns radiate from the static, pulling my attention out and beyond. I find terrible things. I find glorious things. I find nothing.",
            effects: { "mariner.experiences.sophorific.standby": 1 },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "divebar",
    label: "Downtrodden Dive Bar",
    icon: "tasks/mariner.tasks.crowd.pink",
    description:
      "Everyone who is anyone in this town,either passes through here, or is passed as a topic over the tongues of patrons.",
    actions: {
      talk: {
        hint: {
          label: "Mingle with the Locals",
          startdescription:
            "With the regularity of the tides, the patrons will stream into this bar and stumble home. If I come with enough cash, I ",
          requirements: { funds: -1 },
        },
        challenge: {
          task: {
            label: "Mingle with the Locals",
            startdescription:
              "With the regularity of the tides, the patrons will stream into this bar and stumble home. No one will know these waters better, and no place is more prone to gossip. If I can get these locals to open up, they could tell more than books ever could.",
            requirements: { funds: 1 },
          },
          success: {
            label: "Facts and Friendship",
            description: `People tend to be hesitant to talk to outsiders… but the captain of the ${SHIP_NAME} had not been local, and people love to gossip about outsiders, especially those who met a tragic end. At last call, I have the pervasive theories on where the ship went down.`,
            furthermore: [
              {
                effects: vb.vp({ notion: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("notion"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
            ],
            purge: { funds: 1 },
          },
          failure: {
            label: "Fraught Fiction",
            description:
              "For a while it seems we are on the right track, but I can notice the snickers that follow any direction we are given. I know for sure I have been played when at the end of the night, the bar tap has been put in my name.",
            effects: { funds: -1 },
            purge: { funds: 1 },
          },
          template: probabilityChallengeTemplates.HONEYED_TONGUE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "talk"),
          {
            id: "cash",
            label: "Cash",
            description: "funds for the drinks that will lubricate any social occassion.",
            required: { funds: 1 },
          },
        ],
      },
    },
  });

  vb.defineTask({
    taskId: "sailout",
    label: "Sail Out",
    icon: "tasks/mariner.tasks.boat.blue",
    description: `At some point, I just have to head out. With clues or without, I could follow the ${SHIP_NAME}'s wake, and triangulate the place she went down.`,
    actions: {
      "mariner.sail": {
        hint: {
          label: "Leave the Harbor",
          startdescription:
            "Where the brackish river meets the briny sea. Unfortunate currents have kept this harbor from growing beyond its humble size. To leave this harbor for the next step in this adventure, I will have to sail the Kite.",
          requirements: { "mariner.ship": -1 },
        },
        challenge: {
          task: {
            label: "Leave the Harbor",
            startdescription: `Where the brackish river meets the briny sea. Unfortunate currents have kept this harbor from growing beyond its humble size. Now those currents threaten to make the Kite follow the same fate as the ${SHIP_NAME}.`,
            requirements: { "mariner.ship": 1 },
          },
          success: {
            label: "Tight Sailing",
            description:
              "The sea is never merciful, but today she is forgiving. I am guided through the hidden straits as by a kindly mother. I know I will not return here, but at least my exit was dignified. ",
            furthermore: [
              {
                effects: vb.vp({ mistedreefs: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("mistedreefs"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
              {
                effects: vb.vp({ sailout: -1 }),
              },
            ],
            purge: { [vb.currentLayerAspectId()]: 5 },
          },
          failure: {
            label: "A Scrape and a Bruise",
            description:
              "It makes an ungodly noise, when the Kite scrapes the sandbar hidden just below the surf. But worse than that is the laughter I can hear from the fishermen wives and retired mariners upon the boardwalks and the piers.",
            effects: { "mariner.experiences.divisive.standby": 1 },
            mutations: [
              {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            ],
            linked: [{ id: "mariner.sailing.sinking" }],
          },
          template: probabilityChallengeTemplates.OVERTAKE,
        },
        slots: [
          ...SHIP_MANOEUVERING_SLOTS(),
          {
            id: "shipslot",
            label: "My Ship",
            description: "To travel the sea, I require my vessel",
            required: { "mariner.ship": 1 },
          },
        ],
      },
    },
  });
}
