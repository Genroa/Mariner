import { SECONDS } from "../../../routine_generation.mjs";
import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateMorgueLayer } from "./morgue.mjs";
import { generateStreetLayer } from "./street.mjs";

export const vaultPrefix = (end) => `mariner.vaults.aleppo.${end}`;

export function generateBizertaMorgueVault() {
  const vb = new VaultBuilder("bizerta_morgue", "medsea");

  mod.initializeVerbFile(`verbs.vaults.${vb.vaultId}`, ["vaults", vb.vaultId]);
  mod.setHiddenAspect(vb.ASPECTS_FILE, { id: vb.vp("reducetime") });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: vb.vp("threatofdiscovery"),
    label: "Threat of Discovery",
    description:
      "We have managed to avoid alerting the local authorities for now, but it is only a matter of time before someone realizes our intent... We better be gone by the time that happens.<br><br>[Any suspicious activity will reduce this countdown.]",
    lifetime: 1600,
    aspects: { "mariner.local": 1 },
    xtriggers: {
      [vb.vp("reducetime")]: {
        morpheffect: "timespend",
        useCatalystQuantity: true,
        level: 60,
      },
    },
    decayTo: vb.vp("alert"),
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: vb.vp("alert"),
    label: "General Alert",
    description:
      "They definitely unmasked us. The dogs are barking and very soon, armed men will rush in and round us up like sheep.",
    aspects: { "mariner.local": 1 },
    timers: {
      spawnauthorities: {
        frequency: 5 * SECONDS,
        morpheffects: [{ morpheffect: "induce", id: vb.vp("roundcrew") }],
      },
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("roundcrew"),
    actionId: "mariner.roundcrew",
    linked: [{ id: vb.vp("roundcrew.grabcrew") }, { id: vb.vp("roundcrew.grabplayer") }],
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("roundcrew.grabcrew"),
    warmup: 10,
    extantreqs: { [vb.vp("selected")]: 1, [vb.vp("alert")]: 1 },
    label: "Rounding Us Up",
    startdescription:
      "This building is full of niches and small rooms, but they're quickly rounding up the ones inside with me. As long as they encounter people of my crew to arrest, and propagate the confusion, they'll be too busy to get to me. But the moment they got all of them, I'm done. I really need to leave <b>now</b>. There's nothing left to do.",
    slots: [{ id: "crew", label: "Victim", description: "<>", required: { "mariner.crew": 1 }, greedy: true }],
    effects: { "mariner.crew": -1 },
  });

  mod.setRecipe(vb.RECIPES_FILE, { id: vb.vp("roundcrew.grabplayer"), warmup: 0, ending: "mariner.endings.arrested" });

  generateStreetLayer(vb);
  generateMorgueLayer(vb);
}
