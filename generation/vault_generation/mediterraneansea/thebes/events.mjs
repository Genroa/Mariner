import { INJURE_CREWMEMBER, LOCAL_REPUTATION } from "../../../generation_helpers.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../../vault_generation.mjs";
import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "./thebes.mjs";

export function generateReedEvents(vb) {
  mod.setRecipe(RECIPES_FILE, {
    id: vaultPrefix("events.others"),
    craftable: false,
    warmup: 1,
    linked: [{ id: vaultPrefix("events.ships") }, { id: vaultPrefix("events.party") }],
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.nile",
    task: {
      id: vaultPrefix("events.nile"),
      craftable: false,
      label: "<Up the Nile>",
      startdescription:
        "<I must travel to the Egyptian Thebes, that is to say the city of Luxor. The river has been well traveled for millenia with ships from all over the great green. If I mess up here, news will spread fast, and any pursuer will find my scent. [Approach this challenge with Heart or Forge]>",
    },
    chance: quantityDuoToChance("heart", "forge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Needle and Thread",
      description:
        "<We weave our way between ships powered by muscle, wind and steam, while traveling up the long green lint that has been draped off the North African coast.>",
    },
    failure: {
      label: "<An Unpleasant Business>",
      description: "<We make our way up the Nile, but it does not go without its issues.>",
      effects: { "mariner.notoriety": 1 },
    },
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.scales",
    task: {
      id: vaultPrefix("events.scales"),
      craftable: false,
      label: "<Scales In The Water>",
      startdescription:
        "<The Egyptians have lived on the Nile for millenia, but its oldest inhabitants have bobbed on that river for millenia more. The siblings of scale: watersnakes, crocodiles and their brethren have come to investigate our investigation. We must fend them off or else call upon the proper authorities to ward them off. [Approach this challenge with Edge or Knock]>",
    },
    chance: quantityDuoToChance("edge", "knock", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Dissolving Back into the Dark",
      description: "<The shapes again merge with the water and the night. We are clear of the reptilians ire for now.>",
    },
    failure: {
      label: "<Snapping Maws>",
      description:
        "<A rush in the water, a viselike jaw snaps shut. The crocodile retreats with its prey and we are now with one less than we were before. >",
      inductions: [INJURE_CREWMEMBER],
    },
  });
  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.colossus",
    task: {
      id: vaultPrefix("events.colossus"),
      craftable: false,
      label: "<The Colossus>",
      startdescription:
        "<Our small dinghy or our splashing selves have attracted the ire of the king of the river. We must flee, or defend ourselves… Once a Hippo has been enraged, it does not lose interest of its own accord. [Approach this challenge with Moth or Edge]>",
    },
    chance: quantityDuoToChance("moth", "edge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Pounding Heart, Quiet Waters",
      description:
        "<One does not defeat a hippopotamus, not without specialist weaponry. But we have discouraged it to further attack our investigation. It disappears into the night.>",
    },
    failure: {
      label: "<The Rampage Continues>",
      description:
        "<It changes, it rocks the boat, it rams those in the water. It will exact its toll on us until we drive it away, or leave the area all together. >",
      inductions: [INJURE_CREWMEMBER],
      linked: [{ id: vb.vp("events.colossus"), chance: 50 }],
    },
  });
  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.ships",
    task: {
      id: vaultPrefix("events.ships"),
      grandReqs: { [LOCAL_REPUTATION]: -3 },
      craftable: false,
      label: "<Ships in the Night>",
      startdescription:
        "<Late night cargo, or lantern lit river cruises. The Nile in Luxor is rarely quiet, even in the depths of night. We must hide our operations here, or else the authorities will be called. [Approach this challenge with Moth or Winter]>",
    },
    chance: quantityDuoToChance("winter", "moth", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Passing By",
      description: "<Lanterns hooded, voices down. The ships pass without taking note of our activities.>",
    },
    failure: {
      label: "<Spreading News >",
      description: "<Those on the boats will spread the news to those on shore, and soon the authorities will be alerted.>",
      effects: { "mariner.notoriety": 1 },
    },
  });
  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.party",
    task: {
      id: vaultPrefix("events.party"),
      grandReqs: { [LOCAL_REPUTATION]: 3 },
      craftable: false,
      label: "<Search Party>",
      startdescription:
        "<Police ships have been sent out onto the river to investigate untowards activities. The authorities do not look too kindly to looters of ancient tombs, not even those that have been lost to the river. We will have to hide, or talk our way out of the situation. [Approach this challenge with Moth or grail]>",
    },
    chance: quantityDuoToChance("moth", "grail", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Diverted Attention",
      description: "<We have diverted the attention away from us, perhaps the river will be a little more quiet now. >",
      effects: { "mariner.notoriety": -1 },
    },
    failure: {
      label: "<Caught in the Lights>",
      description:
        "<We are caught in a white circle, trapped in visibility. Even diving down into the river soon loses its attractiveness as an exit. Consequences will follow. >",
      effects: { "mariner.notoriety": 2 },
      linked: [
        {
          id: "mariner.thebes.booted",
          label: "<Kicked out Thebes>",
          description:
            "In Egypt they do not deal lightly with looters and graverobbers. But they cannot proove much more then that we went for a swim. Therefor we are just, pointedly, removed entry from town. We will have to return later, when things cooled down.",
          grandReqs: { [LOCAL_REPUTATION]: 4 },
          purge: { "mariner.aspects.vaults.thebes.layer.1": 10 },
        },
      ],
    },
  });
}
