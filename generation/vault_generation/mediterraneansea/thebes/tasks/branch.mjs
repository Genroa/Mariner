import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "../thebes.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { SPAWN_MISFORTUNE_CURSE } from "../../../../curses_generation/curses/misfortune.mjs";
import { QUEST_FLAG_ID, FLAG_ID, READ_FLAG } from "../../../../global_flags_generation.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";


export function generateBranch(vb) {
  vb.defineTask({
    taskId: "branch",
    label: "The Theban Branch",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Scoured by the sands and the river, the smoothed out walls are all that remains of the southernmost branch of the House of Lethe. With the Lethians expertise in the wisdom of Preservation, some treasures may have survived.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Break Into the Theban Branch",
            startdescription:
              "Most wards have washed away, but the door is still shut with a strength beyond the mundane. This is compounded by our muscles slowed in the water, and our longues pressed down by the weight. ",
          },
          success: {
            label: "Rewarded",
            description:
              "The door yields, and we flow into the room with the waters of the rivers. Now a rapid exploration ensues, all scramble against the increasingly pressing feeling against our lungs.",
            effects: { "mariner.vaults.thebes.chest": 1, [vb.vp("branch")]: -1 },
            rootAdd: {
              [QUEST_FLAG_ID("luxor", "cleared")]: 1,
            },
          },
          failure: {
            label: "Warded Off",
            description:
              "We bust in, but slow and sluggish, and triggering some ward long held on this place. We take what we can and leave, but even as we dry off the damp of the water, a chill remains. ",
            effects: { "mariner.vaults.thebes.chest": 1, [vb.vp("branch")]: -1 },
            alt: [SPAWN_MISFORTUNE_CURSE],
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });
}
