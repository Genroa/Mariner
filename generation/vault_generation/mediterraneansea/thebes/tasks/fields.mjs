import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../../../vault_generation.mjs";
import { UPDATE_QUEST } from "../../../../quest_generation.mjs";

export function generateGauntletLayer(vb) {
  vb.defineLayer("gauntlet");
  vb.disableTideForThisLayer();

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.start"),
    actionId: "gauntlet",
    label: "A Journey",
    description: "We have ventured beyond the cities bustle, and now traverse a haze of gold and sepia.",
    linked: [{ id: vb.vp("gauntlet.path") }],
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    task: {
      id: vb.vp("gauntlet.path"),
      craftable: false,
      label: "The Flaxen Fields ",
      startdescription:
        "Three paces into the reed fields, the river noises quiet, and the city sounds are muffled into a hush. Waist deep in water, with stalks reaching a span above your head, the true extent of the fields becomes hard to gauge. From outside, it seemed no wider than a city block, but inside, it seemed to stretch forever and into eternity. [approach this challenge with Knock or Lantern]",
    },
    chance: quantityDuoToChance("moth", "forge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Into the Flaxen Fields",
      description:
        "We push through, and walk for an hour or more. Each step, the reeds bow before us, and at each pace, they rise again behind us. Unclear how far we travel, as time and direction too seem to have been swallowed by the mud. But then we can see a tower, tilted, rise above the waving reeds.",
      linked: [{ id: vb.vp("gauntlet.innermost") }],
    },
    failure: {
      label: "A Mire and a Maze",
      description:
        "<We Tramble, muddle, fumble and fall. we leave big holes that fill up with water behind us where we tread, but we do not leave a good impression where we go.>",
      effects: { "mariner.notoriety": 1 },
    },
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    task: {
      id: vb.vp("gauntlet.innermost"),
      craftable: false,
      label: "Into the Innermos",
      startdescription:
        "Only the spire still sticks out of the mud, its Axumite Cross the last vestiges still portraying what was once held in reference here. Somewhere, here, there once was an ally, a courtyard, and a woman on the run. Somewhere here she hid a secret, now buried by mud and root and river. Now we dig. [Approach this challenge with Forge or Heart]",
    },
    chance: quantityDuoToChance("moth", "knock", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Uproot a Secret",
      description:
        "Not much of the square can still be recognized, its features spackled by decades of sediment deposited by the river floods. All we have to go on is the church, so we excavate, and excavate again. We might dig a 100 holes before we find the right spot, unless we can intuit what we need, or use something to ease our trouble.",
      linked: [{ id: vb.vp("gauntlet.end") }],
    },
    failure: {
      label: "Starving, Drowning, Thirsting, Feast",
      description:
        "Above, the celestial lights rotate and dance, as closer, the mosquito’s orbit our toiling forms. We find what we are looking for, eventually. But we rise broken and unhappy, but fulfilled.",
      effects: { "mariner.notoriety": 1 },
      linked: [{ id: vb.vp("gauntlet.end") }],
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.end"),
    label: "On the Edge",
    startdescription: "We return back at the rivers edge, clutching our precious cargo to our chest.",
    furthermore: [UPDATE_QUEST("buriedsecrets", "secret")],
  });
}
