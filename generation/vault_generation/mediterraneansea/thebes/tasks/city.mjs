import { vaultPrefix, ELEMENTS_FILE, RECIPES_FILE } from "../thebes.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { UPDATE_QUEST } from "../../../../quest_generation.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";
import { FLAG_ID, READ_FLAG } from "../../../../global_flags_generation.mjs";

export function generateCity(vb) {
  vb.defineTask({
    taskId: "city",
    label: "The Streets of Luxor",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "The city carried a different name before Alexander invaded, and now again carries a different name since the Arabs rule. But cobblestones and washer-wives hold long memories.",
    actions: {
      talk: {
        challenge: {
          hint: {
            label: "Trade Gossip At the Streets?",
            startdescription:
              "The river is teeming with news, as it always is. To get anything of real interest, I need to sweeten offering something of interest of my own to the mix, or else lubricating the wheels of social interaction with sterling silver.",
            requirements: { "mariner.rumour": -1 },
          },
          task: {
            label: "Trade Gossip At the Streets",
            startdescription:
              "The river is teeming with news, as it always is. To get anything of real interest, I need to sweeten offering something of interest of my own to the mix, or else lubricating the wheels of social interaction with sterling silver.",
            requirements: { "mariner.rumour": 1 },
          },
          success: {
            label: "Notable News",
            description: "Between tall tales, urban legends and petty squabbling, something of interest has come up...",
            effects: { "mariner.rumour": -1 },
            linked: [{ id: vb.vp("rumour.router") }],
          },
          failure: {
            label: "Barrage of the Banal",
            description:
              "This day, we learn many, many things, all bound to be of interest  to someone: a nosy neighbor, a cuckold husband, a worried mother. But only when we drudge through the riff-raff of the human experience until we are to drown in it’s mundanity.",
            effects: { "mariner.wanderlust": 1, "mariner.rumour": -1 },
            linked: [{ id: vb.vp("rumour.router") }],
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "talk"),
          {
            id: "rumour",
            actionid: "talk",
            label: "Rumour",
            description: "To gain, to give",
            required: { "mariner.rumour": 1 },
          },
        ],
      },
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Listen for Rumors at the Harbor",
            startdescription:
              "Empires have risen and fallen since Luxor was the center of the world, but the Nile is still the slow pulsing artery connecting the African Interior to the Great Green Mediterranian, carrying wealth, people and information. If I make myself innocuous and listen in on hushed conversations, some drops of news from throughout the sea may find my ears.",
          },
          success: {
            label: "Reeling in a Catch",
            description:
              "A rumour reached my ears, fresh as the Alexandrian catch. But information like this keeps as well as fish, so I best decide on my course soon. ",
            effects: { "mariner.rumour.medsea": 1 },
          },
          failure: {
            label: "No Bites",
            description:
              "The seamen’s murmur holds as much information as the river’s babble. Wherever information of note might be, it is shielded from my ears, and the other seafolk view my interest with distrust.",
            effects: { notoriety: 1 },
          },
          template: probabilityChallengeTemplates.SILVER_TONGUE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("rumour.router"),
    effects: { "mariner.rumour.medsea": -1, funds: -1 },
    linked: [
      {
        id: vb.vp("thread"),
      },
      {
        id: vb.vp("rumour_city"),
        chance: 20,
      },
      {
        id: vb.vp("rumour_branch"),
        chance: 25,
      },
      {
        id: vb.vp("rumour_story"),
        chance: 33,
      },
      {
        id: vb.vp("rumour_serapeum"),
        chance: 50,
      },
      {
        id: vb.vp("thread"),
      },
      {
        id: vb.vp("nill"),
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("thread"),
    label: "Follow the Thread of Euphresme Recollections",
    description:
      "Euphresme’s directions are vague, based on the memory of a city she visited once 40 years ago. They are surprisingly not much more use than directions to an Ancient site might be. But still, I can venture forward into the city, and see if anyone can point me to a promising path.",
    grandReqs: { "[root/mariner.globalflags.quest.buriedsecrets.beginnings]": 1, [READ_FLAG("luxor.thread.draw")]: -1 },
    furthermore: [
      {
        rootAdd: {
          [FLAG_ID("luxor.thread.draw")]: 1,
        },
      },
    ],
    effects: { [vb.vp("rumour_flood")]: 1, "mariner.rumour": -1 },
    furthermore: [UPDATE_QUEST("buriedsecrets", "flood")],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("rumour_city"),
    label: "<A Rumour about Luxor>",
    description:
      "<A story reaches me from the walled compounds, about a city magistrate, an unlocked bedroom window, and clandestine meetings under starlight.>",
    effects: { [vb.vp("rumour_city")]: 1, "mariner.rumour": -1 },
    grandReqs: { [READ_FLAG("luxor.city.draw")]: -1 },
    furthermore: [
      {
        rootSet: {
          [FLAG_ID("luxor.city.draw")]: 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("rumour_branch"),
    label: "<A Rumour about the River>",
    description: "<A story has reached my ears from the pearl divers, that might help me find the hidden branch.>",
    effects: { [vb.vp("rumour_branch")]: 1, "mariner.rumour": -1 },
    grandReqs: { [READ_FLAG("luxor.branch.draw")]: -1 },
    furthermore: [
      {
        rootSet: {
          [FLAG_ID("luxor.branch.draw")]: 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("rumour_story"),
    label: "<A Rumour about an Hyena>",
    description: "<A story has reached me from the desert edge.>",
    effects: { "mariner.stories.something": 1, "mariner.rumour": -1 },
    grandReqs: { [READ_FLAG("luxor.story.draw")]: -1 },
    furthermore: [
      {
        rootSet: {
          [FLAG_ID("luxor.story.draw")]: 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("rumour_serapeum"),
    label: "<A Rumour from Alexandria>",
    description: "<A story has reached me from the river mouth.>",
    effects: { "mariner.stories.serapeum": 1, "mariner.rumour": -1 },
    furthermore: [
      {
        rootSet: {
          [FLAG_ID("luxor.serapeum.draw")]: 1,
        },
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("nill"),
    label: "Mill ran dry",
    description: "I have exhausted the offerings of rumours from Luxor.",
  });

  mod.initializeElementFile(ELEMENTS_FILE, ["thebes"]);
  vb.defineHelpElement({
    id: "rumour_city",
    label: "A rumour about the City",
    description: "I heard a rumour about an person of authority, and their peculiar predeliction",
    aspects: {
      "mariner.rumour": 1,
      "mariner.vaults.help": 1,
    },
  });
  vb.defineHelpElement({
    id: "rumour_branch",
    label: "<A Rumour about the River>",
    description: "I heard a rumour from the pearl divers about the stretches of river where marble still graces the ground.",
    aspects: {
      "mariner.rumour": 1,
      "mariner.vaults.help": 1,
    },
  });
  vb.defineHelpElement({
    id: "rumour_flood",
    label: "<A Rumour about a Flood>",
    description:
      "I heard a rumour about a Coptic Church, that togehter with its whole neighborhood, got covered by first the waves, then the mud and then the roots of the ",
    aspects: {
      "mariner.rumour": 1,
      "mariner.vaults.help": 1,
    },
  });
}
