import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "../thebes.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { UPGRADE_REPUTATION } from "../../../../port_generation/notoriety_generation.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";
import { FLAG_ID, READ_FLAG } from "../../../../global_flags_generation.mjs";

export function generateReeds(vb) {
  vb.defineTask({
    taskId: "reeds",
    label: "The River Reeds",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "The Archeologist had given me directions to the house, but the city has swelled and the river has shifted. A curtain of swaying gold now covers the former riverside. There I will have to root for clues of the former lay of the land.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Root around in the Slick and Roots",
            startdescription:
              "With the water to our knee or to our navel or up to our chest, we have to seek what still remains in the silt. The reeds are surprisingly sharp, and the roots fight our digging fingers.  We have to sink our arms into the  silt to the elbow, as feet threaten to slip into the muck.",
          },
          success: {
            label: "A Clue",
            description: "My hand brought something to the surface. I wash it off clay and muck and slick.",
            linked: [
              {
                id: "camp.draw.stone",
                chance: 50,
                grandReqs: {
                  [READ_FLAG("luxor.stone.draw")]: -1,
                },
                effects: { [vb.vp("stone")]: 1 },
                furthermore: [
                  {
                    rootSet: {
                      [FLAG_ID("luxor.stone.draw")]: 1,
                    },
                  },
                ],
                label: "A Prize, Coarse and Heavy",
                description: "<Our hands bring up a prize.>",
              },
              {
                id: "camp.draw.wood",
                effects: { [vb.vp("wood")]: 1 },
                grandReqs: {
                  [READ_FLAG("luxor.wood.draw")]: -1,
                },
                furthermore: [
                  {
                    rootSet: {
                      [FLAG_ID("luxor.wood.draw")]: 1,
                    },
                  },
                ],
                label: "A Prize, Gnarled and Twisted",
                description: "Our hands bring up a prize.",
              },
              {
                id: "camp.draw.last",
                effects: { "mariner.trappings.heart": 2 },
                maxexecutions: 1,
                grandReqs: {
                  [READ_FLAG("luxor.stone.draw")]: 1,
                  [READ_FLAG("luxor.wood.draw")]: 1,
                },
                label: "A Final Prize",
                description: "All we bring up is a mass of root, grown around a string of discarded pearls.",
              },
              {
                id: "camp.draw.nothing",
                grandReqs: {
                  [READ_FLAG("luxor.stone.draw")]: 1,
                  [READ_FLAG("luxor.wood.draw")]: 1,
                },
                label: "Nothing",
                description:
                  "There is no more to be found, now we are just standing in the water for the perverse enjoyment of it.",
              },
            ],
          },
          failure: {
            label: "Frustration and Exhaustion",
            description:
              "The work is hard, the conditions unpleasant, and musquitos make every moment worse than the one before. While we come up with a prize, it has left my crew exhausted, and having much more work then this will drive them away from my employment.",
            linked: [
              {
                id: "camp.draw.stone",
                chance: 50,
                grandReqs: {
                  [READ_FLAG("luxor.stone.draw")]: -1,
                },
                effects: { [vb.vp("stone")]: 1 },
                furthermore: [
                  {
                    rootSet: {
                      [FLAG_ID("luxor.stone.draw")]: 1,
                    },
                  },
                ],
                label: "A Prize, Coarse and Heavy",
                description: "<Our hands bring up a prize.>",
              },
              {
                id: "camp.draw.wood",
                effects: { [vb.vp("wood")]: 1 },
                grandReqs: {
                  [READ_FLAG("luxor.wood.draw")]: -1,
                },
                furthermore: [
                  {
                    rootSet: {
                      [FLAG_ID("luxor.wood.draw")]: 1,
                    },
                  },
                ],
                label: "A Prize, Gnarled and Twisted",
                description: "Our hands bring up a prize.",
              },
              {
                id: "camp.draw.last",
                effects: { "mariner.trappings.heart": 2 },
                maxexecutions: 1,
                grandReqs: {
                  [READ_FLAG("luxor.stone.draw")]: 1,
                  [READ_FLAG("luxor.wood.draw")]: 1,

                },
                label: "A Final Prize.",
                description: "All we bring up is a mass of root, grown around a string of discarded pearls.",
              },
              {
                id: "camp.draw.nothing",
                grandReqs: {
                  [READ_FLAG("luxor.stone.draw")]: 1,
                  [READ_FLAG("luxor.wood.draw")]: 1,
                },
                label: "Nothing",
                description:
                  "There is no more to be found, now we are just standing in the water for the perverse enjoyment of it.",
              },
            ],
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
      "mariner.navigate": {
        challenge: {
          hint: {
            label: "Offer Something to the Reeds",
            startdescription:
              "There is a power of Root and Earth and swallowed secrets. One of her Names is known to frequent riverbanks, and it may pay well to do tribute to her.",
            requirements: { "mariner.rumour": -1 },
          },
          task: {
            label: "Whisper to the Reeds",
            startdescription:
              "The reeds swallow sound like dirt swallows water, and while the mud below its roots may be sated, the reeds are not. If I offer it a secret, I may please its hidden patron below.",
            requirements: { "mariner.rumour": 1 },
          },
          success: {
            label: "An Audience Enrapt",
            description:
              "The reeds still as I speak, as if even the wind is listening. Mud tugs at my heels, silently dragging me down. When the last words leave my lips, the reeds receive them...",
            linked: [{ id: vb.vp("reeds.router") }],
          },
          failure: {
            label: "A Whisper Chorus",
            description:
              "The words flood from my tongue like a rising river, and the reed's roots drink them in just as hungrily. I leave feeling empty, and unsure what else spilled out of me now locked in root and stem.",
            effects: { notoriety: 1 },
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "mariner.navigate"),
          {
            id: "secret",
            label: "A Secret",
            actionId: "mariner.navigate",
            description: "What do I offer the Black-Flax",
            required: { "mariner.rumour": 1, "mariner.quests.buriedsecrets": 1 },
          },
        ],
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("reeds.router"),
    effects: { "mariner.rumour.medsea": -1 },
    linked: [
      {
        id: vb.vp("ouroboros"),
      },
      {
        id: vb.vp("forward"),
      },
      {
        id: vb.vp("whisper"),
      },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("ouroboros"),
    label: "Ouroborean Rejection",
    description:
      "I try to offer the reeds the secrets that the nun confided in me. The reeds shudder, shiver, whistle, thrum, offering no opening for me to speak. This secret is not theirs to take, like a snake cannot consume its own egg-yolk.",
    requirements: { "mariner.quests.buriedsecrets": 1 },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("forward"),
    label: "<Whisper to the Reeds of the Lost Church>",
    description:
      "<The reeds tremble, and then bow. Their flat backs will form a path into the center of the field, to the first secret they were summoned to swallow.>",
    linked: [{ id: vb.vp("gauntlet.path") }],
    requirements: { [vb.vp("rumour_flood")]: 1 },
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("whisper"),
    label: "Steps Occluded",
    description:
      "The power of that  and their rustling carries on, and it will follow me, casting me as grey as the river vapors",
    furthermore: UPGRADE_REPUTATION("medsea", "thebes", -1),
  });
}
