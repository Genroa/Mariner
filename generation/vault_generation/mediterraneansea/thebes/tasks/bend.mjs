import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "../thebes.mjs";
import { probabilityChallengeTemplates } from "../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../slots_helpers.mjs";
import { FLAG_ID, READ_FLAG } from "../../../../global_flags_generation.mjs";

export function generateBend(vb) {
  vb.defineTask({
    taskId: "bend",
    label: "The Deeper Bend",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Here the river is too deep to stand, and too deep for the reeds and lotuses to find purchase. Somewhere here the Theban branch of the House of Lethe has sunk off to. ",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Dive Down The Deeper Bend",
            startdescription:
              "The river is deep here, and fast flowing. You cannot tell what the bottom hides, and somewhere, it hides the slice of thebes, lost to the histories. ",
          },
          success: {
            label: "A Glimmer of the City",
            description:
              "Down and down, fighting the current and keeping an eye out for slinking predators. We go until we worry our longs will give out, and trail the river bottom like tilapia. There! The glint of marble. The curve of stone, sunken below water and silt and sand.",
            linked: [
              {
                id: vb.vp("further"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                requirements: {
                  [vb.vp("wood")]: 1,
                  [vb.vp("stone")]: 1,
                  [vb.vp("rumour_branch")]: 1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("further_nostone"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                chance: 70,
                requirements: {
                  [vb.vp("wood")]: 1,
                  [vb.vp("stone")]: -1,
                  [vb.vp("rumour_branch")]: 1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("further_nowood"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                chance: 70,
                requirements: {
                  [vb.vp("wood")]: -1,
                  [vb.vp("stone")]: 1,
                  [vb.vp("rumour_branch")]: 1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("further_wood"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                chance: 50,
                requirements: {
                  [vb.vp("wood")]: 1,
                  [vb.vp("stone")]: -1,
                  [vb.vp("rumour_branch")]: -1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("further_stone"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                chance: 50,
                requirements: {
                  [vb.vp("wood")]: -1,
                  [vb.vp("stone")]: 1,
                  [vb.vp("rumour_branch")]: -1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("further_rumour"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                chance: 50,
                requirements: {
                  [vb.vp("wood")]: -1,
                  [vb.vp("stone")]: -1,
                  [vb.vp("rumour_branch")]: 1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("further_lowchance"),
                label: "The Lost House",
                warmup: 20,
                description:
                  "I can almost see the curve of the street again, picturing the city as it might exist underneath the silt and sand. With the groundplan of old thebes in my mind, we move with purpose. And then there, another sunken shape, unremarkable in look from the others, but we know this place holds history.",
                chance: 10,
                requirements: {
                  [vb.vp("wood")]: -1,
                  [vb.vp("stone")]: -1,
                  [vb.vp("rumour_branch")]: -1,
                },
                effects: { [vb.vp("branch")]: 1, [vb.vp("bend")]: -1 },
              },
              {
                id: vb.vp("dive_nill"),
                label: "Fruitless",
                description:
                  "Between the decaying columns and the remains of streets, we swim until we must recede back to air and light. We did not find what we where looking for this time. Perhaps there is another clue we could use to triangulate our position.",
              },
            ],
          },
          failure: {
            label: "Swayed by the Current",
            description:
              "We dive down, but the currents are strong. We lose both are nerve and our way, and we have to abort and dive back up, but we arise in troubled waters...",
            linked: [
              {
                id: vb.vp("events.colossus"),
                chance: 33,
              },
              {
                id: vb.vp("events.scales"),
                chance: 50,
              },
              {
                id: vb.vp("events.ships"),
              },
            ],
          },
          template: probabilityChallengeTemplates.DIVE,
        },
        slots: [...CHALLENGE_SLOTS(6, "explore")],
      },
    },
  });
}
