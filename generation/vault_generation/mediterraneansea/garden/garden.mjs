import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateHospitalLayer } from "./hospital/hospital.mjs";

export const ELEMENTS_FILE = "vaults.garden";
export const RECIPES_FILE = "recipes.vaults.garden";
export const ASPECTS_FILE = "aspects.vaults.garden";

export const vaultPrefix = (end) => `mariner.vaults.garden.${end}`;

export function generateGardenVault() {
  const vb = new VaultBuilder("garden", "medsea");
  generateHospitalLayer(vb);
  //generateHospitalEvents(vb);
  vb.computeCustomTideIfRequired();
}
