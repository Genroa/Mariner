import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";

export function generateBureaucracy(vb) {
  vb.defineTask({
    taskId: "bureaucracy",
    label: "The Room of Records",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "Despite the smooth running, glossy exterior lies a tangled labyrinth of halogen-lit hallways, bureaucracy and paperwork. Here the less then shining cogs of the machine are kept, the morgue the boilerrooms and lastly, the Hall of Records.",
    actions: {
      talk: {
        challenge: {
          task: {
            label: "Gain Entry to the Stacks",
            startdescription:
              "To gain entry to the stacks, one needs the right keys to unlock the door. Once inside, we will still have to rely on our statement.",
          },
          success: {
            label: "Passage Granted",
            description:
              "The key turns, the locks squecks and the stacks are open... as are the eyes of the questioning attendants. Even with our writ of passage, only those of a certain station within the hospital can enter.",
            mutations: [
              {
                filter: [vb.vp("bureaucracy")],
                mutate: [vb.vp("bureaucracy.accessed")],
                level: 1,
              },
            ],
          },
          failure: {
            label: "Perilous Entry",
            description:
              "We have been permitted entry, but not someone a message was send to verify. It will take a while until it makes it's way to through the maze of bureaucracy.",
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
        slots: CHALLENGE_SLOTS(3, "talk"),
      },
      "mariner.investigate": {
        hint: {
          label: "Investigate the Record Rooms",
          startdescription:
            "There is a navigable channel, but it is narrow... I know the Kite can traverse it, if my sailors follow my commands.",
          requirements: { [vb.vp("bureaucracy.accessed")]: -1 },
        },
        challenge: {
          task: {
            label: "Investigate the Stacks",
            requirements: { [vb.vp("bureaucracy.accessed")]: 1 },
            startdescription:
              "There is a daunting task before us. It is is not just a mountain of paperwork, but one stored away in many cabinets, boxes and stores, with inpenetrable indexes and barely legible ledgers. and one may be well versed in ancient language, but no one is easily prepared for docters script.",
          },
          success: {
            label: "The Joys of reading",
            description:
              "When a pattern is appearant, a thousand pages are as easy to parse as one. There is a rhythm appearant with some patients: Deteriorate, get moved to the Annex D building, and get released as healthy 7 days later... and then never appear in the books again.",
            effects: { [vb.vp("insight.paperworks")]: 1 },
          },
          failure: {
            label: "Slow, Tedious hours",
            description:
              'We get so tangled up in what to read and what we have read, that we are sure we must have seen the same document five times before we find what we need. people disappear from the records, and when they do, they are always called "perfectly whole and perfectly well" and they have always been sequencestered at Annex D for a week.',
            effects: { [vb.vp("insight.paperworks")]: 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: [
          {
            id: "firsthelp",
            actionId: "mariner.navigate",
            label: "Help in the Records",
            description: "Not everyone has access to the record rooms, only doctors are allowed access.",
            required: {
              "mariner.song": 1,
              "mariner.disguise.doctor": 1,
              "mariner.heart": 1,
            },
          },
          {
            id: "secondhelp",
            actionId: "mariner.navigate",
            label: "Help in the Records",
            description: "Not everyone has access to the record rooms, only doctors are allowed access.",
            required: {
              "mariner.song": 1,
              "mariner.disguise.doctor": 1,
              "mariner.heart": 1,
            },
          },
          {
            id: "thirdhelp",
            actionId: "mariner.navigate",
            label: "Help in the Records",
            description: "Not everyone has access to the record rooms, only doctors are allowed access.",
            required: {
              "mariner.song": 1,
              "mariner.disguise.doctor": 1,
              "mariner.heart": 1,
            },
          },
        ],
      },
    },
  });
}
