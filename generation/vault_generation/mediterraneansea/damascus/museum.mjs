import { generateWalls } from "./tasks/walls.mjs";
import { generateStatue } from "./tasks/statue.mjs";
import { generateCrowds } from "./tasks/crowds.mjs";
import { generateKickOut } from "./kicked_out.mjs";
import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "./damascus.mjs";
import { trappingAspects } from "../../../generate_trappings.mjs";

// TASKS
export function generateMuseumLayer(vb) {
  vb.defineLayer("1");

  //items you take with you



  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.bird",
    label: "<bird>",
    unique: true,
    description:
      "<The Lethians, who seperated themselves from the affairs of the House, hold sacred a certain bird who holds vigil at gravesights and laments the dead. Why this breach in their own sturdy rules? Because the Elegiast allowed them something they desired even more?>",
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.page1_totake",
    label: "<First Part of Matthias and the Imago: Loss>",
    aspects: { "mariner.navigate_allowed": 1 },
    unique: true,
    description:
      "<Copied scribbled notes, in hopefully recognisable aramaic. I should see if any of my contacts can assist me in translating the story.>",
    slots: [
      {
        id: "pages2",
        label: "Other Pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page2_totake": 1,
        },
      },
      {
        id: "pages3",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page3_totake": 1,
        },
      },
      {
        id: "pages4",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page4_totake": 1,
        },
      },
    ],
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.page2_totake",
    label: "<Second Part of Matthias and the Imago: Loss>",
    aspects: { "mariner.navigate_allowed": 1 },
    unique: true,
    description:
      "<Copied scribbled notes, in hopefully recognisable aramaic. I should see if any of my contacts can assist me in translating the story.>",
    slots: [
      {
        id: "pages2",
        label: "Other Pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page1_totake": 1,
        },
      },
      {
        id: "pages3",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page3_totake": 1,
        },
      },
      {
        id: "pages4",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page4_totake": 1,
        },
      },
    ],
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.page3_totake",
    label: "<Third Part of Matthias and the Imago: Loss>",
    aspects: { "mariner.navigate_allowed": 1 },
    unique: true,
    description:
      "<Copied scribbled notes, in hopefully recognisable aramaic. I should see if any of my contacts can assist me in translating the story.>",
    slots: [
      {
        id: "pages2",
        label: "Other Pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page2_totake": 1,
        },
      },
      {
        id: "pages3",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page1_totake": 1,
        },
      },
      {
        id: "pages4",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page4_totake": 1,
        },
      },
    ],
  });
  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.page4_totake",
    label: "<Final Part of Matthias and the Imago: Loss>",
    aspects: { "mariner.navigate_allowed": 1 },
    unique: true,
    description:
      "<The final section of the book has been deemed too dangerous for public viewing, even when they are written in a language dead to the vulgar masses.>",
    slots: [
      {
        id: "pages2",
        label: "Other Pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page2_totake": 1,
        },
      },
      {
        id: "pages3",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page3_totake": 1,
        },
      },
      {
        id: "pages4",
        label: "Other pages",
        description: "The other pages that together make up Loss.",
        actionId: "mariner.navigate",
        required: {
          "mariner.vaults.damascus.page1_totake": 1,
        },
      },
    ],
  });

  mod.setHiddenAspect(vb.ASPECTS_FILE, {
    id: "mariner.flubbed",
    label: "<Flub the meeting with the curator>",
    description: "<used to decrease the timer on the curator>",
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.glyphs.hidden",
    label: "Rubbings of Markings in an Unfamiliar Script",
    description:
      "A dark imitation of the glyphs found at the walls of the Damascene branch of the house of Lethe. They hold a strange appeal.",
    aspects: {
      ...trappingAspects({
        value: 1,
        types: ["artifact", "readable"],
        aspects: { edge: 1 },
      }),
      "mariner.topic": 1,
    },
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.damascus.glyphs",
    label: "Hyksian Glyphs",
    description:
      "Even these mere shadows of a Hyksian ward carry with them a dangerous allure. I know not what they mean, but still I trace the lines with my fingers on moments lost in reveries.",
    aspects: {
      ...trappingAspects({
        value: 3,
        types: ["artifact", "readable"],
        aspects: { winter: 2, edge: 2, grail: 1 },
      }),
      "mariner.topic": 1,
    },
  });

  //help items
  vb.defineHelpElement({
    id: "empty",
    label: "<Empty>",
    unique: true,
    description:
      "<The room is cleared, and as so often in empty spaces that once bustled, the air is laced with a sense of awkwardnes, like a conversation interrupted.>",
    aspects: { op: 1 },
  });
  vb.defineHelpElement({
    id: "distraction",
    label: "Distraction",
    unique: true,
    description: "<No Eyes on me, for a moment, I could take a clandestine action, if I act quickly.>",
    aspects: { op: 1 },
  });
  vb.defineHelpElement({
    id: "guidance",
    label: "<Statues Guidance>",
    unique: true,
    description:
      "<Directions, hidden in plain sight. Some-one was very clever to hide a secret in such a central structure, or very arrogant.",
  });

  vb.defineHelpElement({
    id: "curator",
    label: "<Curator>",
    lifetime: "180",
    description:
      "<All experts love to hear themselves talk about their specialty. But all academics also see their own time as incomparably valuable, so their patience is limited.>",
    xtriggers: {
      "mariner.flubbed": [
        {
          morpheffect: "timespend",
          level: 60000,
        },
      ],
    },
  });

  mod.setHiddenAspect(vb.ASPECTS_FILE, {
    id: vb.vp("progress"),
    description: "this is a hidden element to track progress",
  });

  vb.defineExplorationTask({
    taskId: "museum",
    locationLabel: "Imperial Museum of the Near East",
    locationDescription:
      "While the content is curated, the display feels more focussed on available space than history or context. The walls are mounted with trophies from times and places hunted throughout history. But in this chaos, stands the White Room. walls, ceiling and contents of the Damascene Branch of the House of Lethe  all reassembled in a feat of architectural taxidermy.",
    tasks: ["crowds", "statue", "walls"],
    events: ["events.guards", "events.throngs"],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.merge.book`,
    requirements: {
      "mariner.vaults.damascus.page1_totake": 1,
      "mariner.vaults.damascus.page2_totake": 1,
      "mariner.vaults.damascus.page3_totake": 1,
      "mariner.vaults.damascus.page4_totake": 1,
    },
    warmup: 30,
    actionid: "mariner.navigate",
    craftable: true,
    label: "Stitch together the Book",
    description: "<Four sheets become one. A story now whole, even if the language barrier is still in surmountable>",
    effects: {
      "mariner.vaults.damascus.page1_totake": -1,
      "mariner.vaults.damascus.page2_totake": -1,
      "mariner.vaults.damascus.page3_totake": -1,
      "mariner.vaults.damascus.page4_totake": -1,
      "mariner.matthiasamethystlossaramaic": 1,
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.consider.imago.mystery`,
    requirements: {
      "mariner.mysteries.imago.solved": 1,
    },
    warmup: 5,
    actionid: "mariner.navigate",
    craftable: true,
    label: "<Remember the Details>",
    startdescription: "<I think back on the three accounts I read, and the three houses I visited. What details stand out...>",
    description:
      '<From the account of ..., the other members of the House suspected Matthias may have hidden something inside the walls of the Damascene Branch, but they feared him too much too investigate. All they knew, is that Matthias disappeared to hunt. "On nights wreathed in mist, when the Damascene Halls were deserted...". There may be more to investigate there.>',
  });

  generateWalls(vb);
  generateStatue(vb);
  generateCrowds(vb);
  generateKickOut(vb);
}
