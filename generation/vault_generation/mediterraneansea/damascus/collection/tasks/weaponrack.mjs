import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";

export function generateWeaponRack(vb) {
  vb.defineTask({
    taskId: "weaponrack",
    label: "Mattias' Implements",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "The sharps and the straps, traps and binds. Implements to pry and poke and sever and disect. There were the tools of the trade of Matthias the Hunter.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Collect the Implements",
            startdescription:
              "The tools still glint in the light, even though they carry strange stains. Some of these may be useful to me, others might be valuable. But we should be careful while handling them, and sometimes have cutting edges in unexpected places.",
          },
          success: {
            label: "A Grim Haul",
            description: "With care and linnen cloths, we fold items of interest into our inventory.",
            effects: { "mariner.trappings.implements": 1 },
          },
          failure: {
            label: "Four Tendons Cut",
            description:
              "A fresh smell of copper fills the air, and the ancient bronze tools are painted crimson once again. I hold my shaking hand, as blood drips from between my fingers.",
            effects: { "mariner.trappings.implements": 1, "mariner.experiences.injury.standby": 1 },
          },
          template: probabilityChallengeTemplates.OPERATE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
