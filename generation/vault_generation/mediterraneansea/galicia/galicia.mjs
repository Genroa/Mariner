import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateLimiaLayer } from "./limia.mjs";
//import { generateLimiaEvents } from "./events.mjs";

export const ELEMENTS_FILE = "vaults.galicia";
export const RECIPES_FILE = "recipes.vaults.galicia";
export const ASPECTS_FILE = "aspects.vaults.galicia";

export const vaultPrefix = (end) => `mariner.vaults.galiicia.${end}`;

export function generateGaliciaVault() {
    const vb = new VaultBuilder("galicia", "medsea");
    generateLimiaLayer(vb);
    //generateLimiaEvents(vb);
    vb.computeCustomTideIfRequired();
}
