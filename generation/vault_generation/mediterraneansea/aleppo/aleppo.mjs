import { LORE_ASPECTS } from "../../../generation_helpers.mjs";
import { VaultBuilder } from "../../vaultbuilder.mjs";
import { generateMazeGroup } from "./tasks/group.mjs";
import { generateMazeShelf } from "./tasks/shelf.mjs";

export const vaultPrefix = (end) => `mariner.vaults.aleppo.${end}`;

export function generateAleppoVault() {
  const vb = new VaultBuilder("aleppo", "medsea");
  defineCafeLayer(vb);
}

/**
 * Generates the Cafe layer of the Aleppo vault.
 * @param {VaultBuilder} vb
 */
function defineCafeLayer(vb) {
  vb.defineLayer("cafe");

  // This is a weird one. It doesn't actually define any task to draw from and immediately routes to a different logic instead.
  vb.defineExplorationTask({
    taskId: "maze",
    locationLabel: "Maze of Stairs and Hallways",
    locationDescription:
      "<The library doesn't have a floor plan. It couldn't have one. Stairs and windowless hallways spring in all directions, seemingly without any logic. Yet, its visitors seem to always reach what they want, assuming they know what to look for. All around us, always at the periphery of our vision, groups of people are sitting, studying books, devices, debating in a river of whispers. And everywhere, the everpresent speckled light, flowing from the central well's network of mirrors.>",
    label: "Wandering the Stairs and Hallways",
    description:
      'We walk down a flight of stairs, turn in some direction, walk up another flight, lose our way and end up "here". If it wasn\'t for the stone under our boots, I would swear some of these hallways weren\'t there a moment ago. The groups around us seem to traverse the place without any kind of disorientation. Maybe "here" isn\'t as tangible - or important as it seems in this place.',
    shuffleAfterDraw: true,
    slots: [
      {
        id: "intent",
        label: "Intent",
        description: "What am I looking for?",
        required: {
          "mariner.lackheart": 1,
          "mariner.halfheart": 1,
        },
      },
    ],
    preRecipe: {
      purge: {
        [vb.currentLayerAspectId()]: 3,
      },
    },
    additionalPreLinked: [
      { id: vb.vp("maze.find.group") },
      { id: vb.vp("maze.find.shelf") },
    ],
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("maze.find.group"),
    warmup: 30,
    requirements: {
      "mariner.halfheart": 1,
    },
    description:
      "<We approach a small group of people, attracted by their discussion; as we slowly integrate the ever changing circle of debaters, we notice the dancing lightspots in their eyes, reflecting the mirrors.\nTime seems to flow differently here; with all the mirrors, and the light source ever present, be it the Sunlight or the myriads of oil lamps and candles, the brightness never really changes. Debatters come and go, but the discussion circle never disappears. It is like a conversation subject was puppetteering people into endlessly talking about it.>",
    effects: {
      [vb.vp("group")]: 1,
      [vb.currentActiveLayerAspectId()]: -1,
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("maze.find.shelf"),
    warmup: 30,
    description: "<>",
    linked: [
      {
        id: vb.vp("maze.find.shelf.*"),
        randomPick: true,
        chances: Object.fromEntries(
          LORE_ASPECTS.map((aspect) => [
            vb.vp(`maze.find.shelf.${aspect}`),
            `10*[mariner.influences.${aspect}]`,
          ])
        ),
      },
      {
        id: vb.vp("maze.find.defaultshelf"),
      },
    ],
  });

  generateMazeGroup(vb);
  generateMazeShelf(vb);
}
