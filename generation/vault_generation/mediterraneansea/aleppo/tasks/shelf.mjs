import { LORE_ASPECTS, SPAWN_SUSPICION } from "../../../../generation_helpers.mjs";

const SHELF_REWARDS = {
  default: ["funds"],
  lantern: ["funds"],
  forge: ["funds"],
  edge: ["funds"],
  winter: ["funds"],
  heart: ["funds"],
  grail: ["funds"],
  moth: ["funds"],
  knock: ["funds"],
};

export function generateMazeShelf(vb) {
  mod.setHiddenAspect(vb.ASPECTS_FILE, { id: vb.vp("shelf") });
  mod.setHiddenAspect(vb.ASPECTS_FILE, { id: vb.vp("shelftrapping") });
  const SPAWN_REWARD_SIGNAL = `mariner.signal.alepposhelf.spawnreward`;
  mod.setHiddenAspect("aspects.signals", {
    id: SPAWN_REWARD_SIGNAL,
  });

  mod.setDeck(vb.DECKS_FILE, {
    id: vb.vp(`shelfdeck.default`),
    spec: SHELF_REWARDS.default.map((_, i) => vb.vp(`shelf.default.${i + 1}`)),
  });
  for (let i = 0; i < SHELF_REWARDS.default.length; i++) {
    mod.setElement(vb.ELEMENTS_FILE, {
      id: vb.vp(`shelf.default.${i + 1}`),
      label: `<Default Shelf ${i + 1}>`,
      aspects: {
        "mariner.local": 1,
        [vb.currentLayerAspectId()]: 1,
        "mariner.navigate_allowed": 1,
        [vb.vp("shelf")]: 1,
      },
      xtriggers: {
        [SPAWN_REWARD_SIGNAL]: {
          morpheffect: "spawn",
          id: SHELF_REWARDS.default[i],
        },
      },
    });
  }

  for (const aspect of LORE_ASPECTS) {
    mod.setDeck(vb.DECKS_FILE, {
      id: vb.vp(`shelfdeck.${aspect}`),
      spec: SHELF_REWARDS[aspect].map((_, i) => vb.vp(`shelf.${aspect}.${i + 1}`)),
    });

    for (let i = 0; i < SHELF_REWARDS[aspect].length; i++) {
      mod.setElement(vb.ELEMENTS_FILE, {
        id: vb.vp(`shelf.${aspect}.${i + 1}`),
        label: `<Shelf ${aspect} (${i + 1})>`,
        aspects: {
          "mariner.local": 1,
          [vb.currentLayerAspectId()]: 1,
          "mariner.navigate_allowed": 1,
          [vb.vp("shelf")]: 1,
        },
        xtriggers: {
          [SPAWN_REWARD_SIGNAL]: {
            morpheffect: "spawn",
            id: SHELF_REWARDS[aspect][i],
          },
        },
      });
    }
  }

  // Find shelf
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("maze.find.defaultshelf"),
    description: "<>",
    effects: {
      [vb.currentActiveLayerAspectId()]: -1,
    },
    deckeffects: {
      [vb.vp(`shelfdeck.default`)]: 1,
    },
  });

  mod.setRecipes(
    vb.RECIPES_FILE,
    ...LORE_ASPECTS.map((aspect) => ({
      id: vb.vp(`maze.find.shelf.${aspect}`),
      effects: {
        [vb.currentActiveLayerAspectId()]: -1,
      },
      deckeffects: {
        [vb.vp(`shelfdeck.${aspect}`)]: 1,
      },
    }))
  );

  // Use shelf
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp(`useshelf.default.pre`),
    craftable: true,
    actionId: "mariner.navigate",
    effects: {
      [vb.currentActiveLayerAspectId()]: 1,
    },
    extantreqs: {
      [vb.currentActiveExclusiveLayerAspectId()]: -1,
    },
    requirements: {
      [vb.vp("shelf")]: 1,
    },
    linked: [{ id: vb.vp("useshelf.special.*") }, { id: vb.vp(`useshelf.default.drawtrapping`) }],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp(`useshelf.default.drawtrapping`),
    furthermore: [
      {
        aspects: {
          [SPAWN_REWARD_SIGNAL]: 1,
        },
      },
      {
        mutations: [
          {
            filter: "mariner.trapping",
            mutate: vb.vp("shelftrapping"),
            level: 1,
          },
        ],
      },
    ],
    linked: [{ id: vb.vp(`useshelf.default.considertrapping`) }],
  });

  // Swapping/stealing logic
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("useshelf.default.considertrapping"),
    // Draw a trapping, display a warmup slot. Accept any trapping.
    // On execution, link to generic recipes doing:
    // - halfheart found: delete the shelf, spawn notoriety and spawn suspicion.
    // - 2 trappings found: delete the non-marked one, remove the mark on the other, delete the shelf.
    slots: [
      {
        id: "Offering or Impulse",
        required: { "mariner.trapping": 1, "mariner.halfheart": 1 },
      },
    ],
    warmup: 30,
    linked: [{ id: vb.vp("stealtrapping") }, { id: vb.vp("swaptrapping") }],
    description: "<Didn't take the offering, in the end.>",
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("swaptrapping"),
    requirements: {
      "mariner.trapping": 2,
    },
    furthermore: [
      // delete non-marked trapping
      {
        decays: [
          {
            filter: `[{[${vb.vp("shelftrapping")}]} : mariner.trapping]`,
            limit: 1,
          },
        ],
      },
      // remove mark on marked-trapping
      {
        mutations: [
          {
            filter: "mariner.trapping",
            mutate: vb.vp("shelftrapping"),
            level: 0,
            additive: false,
          },
        ],
      },
      {
        effects: {
          [vb.currentActiveLayerAspectId()]: -1,
          [vb.vp("shelf")]: -1,
        },
      },
    ],
  });
  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("stealtrapping"),
    requirements: {
      "mariner.halfheart": 1,
    },
    effects: {
      [vb.currentActiveLayerAspectId()]: -1,
      [vb.vp("shelf")]: -1,
      "mariner.notoriety": 1,
    },
    inductions: [SPAWN_SUSPICION],
  });
}
