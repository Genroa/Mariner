import { INJURE_CREWMEMBER, requireAndDestroy, SINK_SHIP } from "../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { SHIP_NAME } from "../ship_wrecks_names.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "./salvage_mission.mjs";

// EVENTS
export function generateMistedReefsEvents(vb) {
  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.forge",
    task: {
      id: vaultPrefix("events.windrises"),
      craftable: false,
      label: "The Wind Rises",
      startdescription:
        "The flag stands straight horizontal, and a constant wind tempts the kite to turn its way. We need to tame this weather with Forge, or survive it with Heart.",
    },
    chance: quantityDuoToChance("heart", "forge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Strong Tailwind",
      description:
        "We leash the wind, and let it serve us instead. With this we will arrive at our destination sooner than expected.",
    },
    failure: {
      label: "Blown off Course",
      startdescription: "We fight against the wind, but at some point we have to bend, and give ourselves.",
      warmup: 30,
      linked: [
        {
          id: vb.vp("events.fairweather"),
          chance: 33,
        },
        {
          id: vb.vp("events.windrises"),
          chance: 50,
        },
        {
          id: vb.vp("events.marinemists"),
        },
      ],
    },
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.heart",
    task: {
      id: vaultPrefix("events.fairweather"),
      craftable: false,
      label: "Fair Weather",
      startdescription:
        "The sea is a mercurial mistress, but rarely, she grants opportunity. The weathervane turns in our favor today. If we seize this opportunity, we may reap the benefits. Forge for the will to act or let our Heart beat in tune with the world.",
    },
    chance: quantityDuoToChance("heart", "forge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Flawless Sailing",
      description:
        "The weather is crisp, the wind swells our sails. We travel twice as fast this day, and enjoy the journey fourfold.",
      effects: { "mariner.thrill": 1 },
    },
    failure: {
      label: "Merely Good",
      description:
        "Opportunity slips away, but the weather stays true. We travel at the expected rate, and whistle while we work. A journey to enjoy.",
    },
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.knock",
    task: {
      id: vaultPrefix("events.marinemists"),
      craftable: false,
      label: "Marine Mists",
      startdescription:
        "Today the marine layer is thick as pea soup, and the feeble, ailing sun is not enough to cut through it. We can pierce it with Lantern or intuit the route with Knock.",
    },
    chance: quantityDuoToChance("lantern", "knock", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Navigate Through",
      description:
        "Guided by maps, and tools and what we can see with our eyes and beyond, we have found our way out of the mistbank without harm.",
    },
    failure: {
      label: "A Scrape on the Bow",
      description:
        "A horrid groan, and our worries were proven true. We had guessed our location incorrectly, or the sandbanks shifted further than we thought. Now we are oriented again, though our ship's hull bleeds.",
      mutations: [
        {
          filter: "mariner.ship",
          mutate: "mariner.ship.damage",
          level: 1,
          additive: true,
        },
      ],
      alt: [SINK_SHIP],
    },
  });
}

// TASKS
export function generateMistedReefsLayer(vb) {
  vb.defineLayer("2");

  vb.definePassiveHelpElement({
    id: "air",
    label: "Air",
    description: "My breaths are numbered. When there are gone, I must go.",
  });

  vb.defineHelpElement({
    id: "distance1",
    icon: "mariner.vaults.rescuemission.distance",
    label: "Past the Uncharted Waters",
    description: "A distance travelled, a leg traversed. [I have passed the Uncharted Waters.]",
  });

  vb.defineHelpElement({
    id: "distance2",
    icon: "mariner.vaults.rescuemission.distance",
    label: "Past the Hidden Currents",
    description: "A distance travelled, a leg traversed. [I have passed the Hidden Currents.]",
  });

  vb.defineExplorationTask({
    taskId: "mistedreefs",
    locationLabel: "Misted Reefs",
    icon: "mariner.vaults.salvagemission.mistedreefs",
    locationDescription: `It is no wonder the ${SHIP_NAME} went down here. The fractured rocks peek from the water like broken teeth, but only when they are not covered by a suffocating sea fog.`,
    tasks: ["unchartedwaters", "hiddencurrents", "traveltime"],
    events: ["events.fairweather", "events.windrises", "events.marinemists"],
    next: [{ id: "mariner.applyshipnameid" }],
  });

  vb.defineTask({
    taskId: "unchartedwaters",
    label: "Uncharted Waters",
    icon: "tasks/mariner.tasks.research.blue",
    description:
      "These are the waters that no company cared to map, where the sun doesn’t care to shine and where the shallows often shift. ",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Chart the Uncharted",
            startdescription: `Waters that no company cared to map, where the sun doesn't care to shine and shallows shift often. And here I have to find the unhallowed brine where the ${SHIP_NAME} found its demise.`,
          },
          success: {
            label: "We Know The Way",
            description:
              "By our knowledge of the hidden stars above or what we learned from the locals back in town, we do not lose our way.",
            effects: vb.vp({ distance1: 1 }),
          },
          failure: {
            label: "Lose our Way",
            startdescription:
              "What I thought was a lighthouse turns out to be a parked automobile. We are not where we should go, and must try again. But first, we drift into eventful waters",
            warmup: 30,
            linked: [
              {
                id: vb.vp("events.fairweather"),
                chance: 33,
              },
              {
                id: vb.vp("events.windrises"),
                chance: 50,
              },
              {
                id: vb.vp("events.marinemists"),
              },
            ],
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "hiddencurrents",
    label: "Hidden Currents",
    icon: "tasks/mariner.tasks.waves.pink",
    description:
      "Among these many fractured islands in this fractal coast, the currents are unpredictable and strange. Our path leads us to a narrow passage way filled with churning water and gleaming rocks.",
    actions: {
      "mariner.sail": {
        hint: {
          label: "Avoid the Rapids",
          startdescription:
            "There is a navigable channel, but it is narrow... I know the Kite can traverse it, if my sailors follow my commands.",
          requirements: { "mariner.ship": -1 },
        },
        challenge: {
          task: {
            label: "Avoid the Rapids",
            startdescription:
              "There is a navigable channel, but it is narrow, and like Scylla and Charybdis, there are dangers on either side. My crew needs to act precise, and ordered, and disciplined. We can make it through, if they follow my command to the inch.",
            requirements: { "mariner.ship": 1 },
          },
          success: {
            label: "With Calculated Precision",
            description:
              "With movements precise as a marshall's measurements, we navigate the channel. Orders flow down the chain of command, and together we are pulled out of the danger. ",
            effects: vb.vp({ distance2: 1 }),
          },
          failure: {
            label: "Mayhem and Misfortune ",
            description:
              "It did not happen right away, but order slips to chaos. Before I know it, orders are opposed, a sail flaps aimlessly in the wind, and my ship crashes upon the rocks.",
            mutations: [
              {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            ],
          },
          template: probabilityChallengeTemplates.COMMAND,
        },
        slots: [
          ...SHIP_MANOEUVERING_SLOTS(),
          {
            id: "shipslot",
            label: "My Ship",
            description: "To travel the sea, I require my vessel",
            required: { "mariner.ship": 1 },
          },
        ],
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hintneither"),
    label: "The Final Leg?",
    startdescription:
      "There are steps on a journey that one must take, before a destination is reached. I know I must pass through uncharted waters, and brave the rapid torrents between the isles. If I haven't yet, I am not yet at my destination.",
    requirements: vb.vp({ distance1: -1, distance2: -1, traveltime: 1 }),
    actionId: "mariner.sail",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hintuncharted"),
    label: "The Final Leg?",
    startdescription:
      "There are steps on a journey that one must take, before a destination is reached. I have passed throug the uncharted waters, but not yet braved the rapids.",
    requirements: vb.vp({ distance1: -1, distance2: 1, traveltime: 1 }),
    actionId: "mariner.sail",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hintrapids"),
    label: "The Final Leg?",
    startdescription:
      "There are steps on a journey that one must take, before a destination is reached. I have braved the rapids, but not yet passed the uncharted waters.",
    requirements: vb.vp({ distance2: -1, distance1: 1, traveltime: 1 }),
    actionId: "mariner.sail",
    hintonly: true,
  });

  vb.defineTask({
    taskId: "traveltime",
    label: "The Distance",
    icon: "tasks/mariner.tasks.path.vertdegris",
    description: "There are steps on a journey that one must take, before a destination is reached.",
    actions: {
      "mariner.sail": {
        challenge: {
          task: {
            label: "The Final Leg",
            startdescription:
              "I have found the path, I am past the challenges. Now just to sail to the supposed site of the wreck, and choose a spot to dive.",
            requirements: vb.vp({ distance1: 1, distance2: 1 }),
          },
          success: {
            label: "The Vicinity",
            description:
              "We have arrived, as much as one can when the precise destination is only an estimation. Now follows the gamble which all our preparation has lead towards.",
            effects: vb.vp({ unassumingsea: 1, distance1: -1, distance2: -1 }),
          },
          failure: {
            label: "Twisted 'Round",
            description:
              "The winds blow unfavorably, and my crew sails unresponsibly, and before we know, we are further behind than we are ahead. I will have to take a part of the journey again. ",
            effects: vb.vp({ distance1: -1, distance2: -1 }),
          },
          template: probabilityChallengeTemplates.OVERTAKE,
        },
        slots: [...CHALLENGE_SLOTS(5, "mariner.sail")],
      },
    },
  });

  vb.defineTask({
    taskId: "unassumingsea",
    label: "An Unassuming Sea",
    icon: "tasks/mariner.tasks.pathquestion.yellow",
    description:
      "Somewhere here it must have happened, so somewhere here we must drop into the abyss. Where precisely is something we must now decide. Now we either gather the hints and clues we have gathered so far, or we must act on our own intellect, and attempt to pierce the depths by the brightness of our mind alone.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Decide The Spot",
            startdescription:
              "Now we either gather the hints and clues we have gathered so far, or we must act on our own intellect, and attempt to pierce the depths by the brightness of our mind alone. ",
          },
          success: {
            label: "A Decision Made",
            description:
              "We have tallied the facts, and discarded what we view as fiction. We expect the ship's wreck to have ended up right here. But we must descend into darkness to find out if we are right.",
            effects: vb.vp({
              locationright: 1,
              trail: -1,
              inkling: -1,
              notion: -1,
            }), //help items used should be purged
          },
          failure: {
            label: "A Decision Made",
            description:
              "We have tallied the facts, and discarded what we view as fiction. We expect the ship's wreck to have ended up right here. But we must descend into darkness to find out if we are right.",
            effects: vb.vp({
              locationwrong: 1,
              trail: -1,
              inkling: -1,
              notion: -1,
            }), // help item used should be purged
          },
          template: probabilityChallengeTemplates.DESIGN,
        },
        slots: CHALLENGE_SLOTS(6, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "locationright",
    label: "A Location",
    icon: "tasks/mariner.tasks.pathcross.darkpurple",
    description: "This is the spot we have decided must be right above our quarry.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Let Down the Rig",
            startdescription:
              "The chain rattles, and we prepare our rig for the voyage below. We gather whatever could give us help or hope down in the darkness, and we descend.",
          },
          success: {
            label: "Next to the Ship",
            description: `As we lower down, we see it arise below us. The torn and wounded shape of the ${SHIP_NAME}. Our rig reaches right besides it, and we are free to explore this quiet grave.`,
            furthermore: [
              {
                effects: vb.vp({ shipwreck: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("shipwreck"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
              {
                effects: vb.vp({ locationright: -1 }),
              },
            ],
            purge: { [vb.currentLayerAspectId()]: 10 },
            linked: [{ id: vb.vp("generateair") }],
          },
          failure: {
            label: "Unable To Succeed",
            description:
              "We come up gasping for air, for light, for warmth. But the sun is distant, and dim. It cannot warm me from the chill that has already settled around my Lack-Heart. ",
            effects: { "mariner.experiences.rattling.standby": 1 },
          },
          template: probabilityChallengeTemplates.DIVE,
        },
        slots: [
          ...CHALLENGE_SLOTS(5, "explore"),
          {
            id: "divingbellslot",
            label: "Diving Equipment",
            description: "Assistance to bring my own and keep my blood pumping",
            required: vb.vp({ divingbell: 1 }),
          },
        ],
      },
    },
  });

  vb.defineTask({
    taskId: "locationwrong",
    label: "A Location",
    icon: "tasks/mariner.tasks.pathcross.darkpurple",
    description: "This is the spot we have decided must be right above our quarry.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Let Down the Rig",
            startdescription:
              "The chain rattles, and we prepare our rig for the voyage below. We gather whatever could give us help or hope down in the darkness, and we descend. ",
          },
          success: {
            label: "Down in the Depths",
            description:
              "We reach the bottom of the ocean, and explore carefully that frigid darkness. But there is nothing but sand and ink besides the rig. Then a glimmer is caught in our light. The wreck in the ship. Far in the distance.",
            furthermore: [
              {
                effects: vb.vp({ abyssalplains: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("abyssalplains"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
              {
                effects: vb.vp({ locationwrong: -1 }),
              },
            ],
            purge: { [vb.currentLayerAspectId()]: 10 },
            linked: [{ id: vb.vp("generateair") }],
          },
          failure: {
            label: "Unable To Succeed",
            description:
              "We come up gasping for air, for light, for warmth. But the sun is distant, and dim. It cannot warm me from the chill that has already settled around my Lack-Heart. ",
            effects: { "mariner.experiences.rattling.standby": 1 },
          },
          template: probabilityChallengeTemplates.DIVE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "explore"),
          {
            id: "divingbellslot",
            label: "Diving Equipment",
            description: "Assistance to bring my own and keep my blood pumping",
            required: vb.vp({ divingbell: 1 }),
          },
        ],
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("generateair"),
    linked: [{ id: vb.vp("airwithbell") }, { id: vb.vp("airwithoutbell") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("airwithbell"),
    extantreqs: vb.vp({ divingbell: 1 }),
    effects: vb.vp({ air: 12 }),
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("airwithoutbell"),
    extantreqs: vb.vp({ divingbell: -1 }),
    effects: vb.vp({ air: 4 }),
  });
}
