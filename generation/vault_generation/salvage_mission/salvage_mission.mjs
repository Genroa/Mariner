import { generateHarborEvents, generateHarborLayer } from "./harbor.mjs";
import {
  generateMistedReefsEvents,
  generateMistedReefsLayer,
} from "./misted_reefs.mjs";
import {
  generateShipwreckEvents,
  generateShipwreckLayer,
} from "./shipwreck.mjs";
import { VaultBuilder } from "../vaultbuilder.mjs";
import { CYCLE_IDS, SHIP_WRECKS } from "../ship_wrecks_names.mjs";

export const ELEMENTS_FILE = "vaults.salvagemission";
export const RECIPES_FILE = "recipes.vaults.salvagemission";
export const ASPECTS_FILE = "aspects.vaults.salvagemission";

export const vaultPrefix = (end) => `mariner.vaults.salvagemission.${end}`;

export function generateSalvageMissionVault() {
  const vb = new VaultBuilder("salvagemission", null, true);

  generateHarborEvents(vb);
  generateHarborLayer(vb);

  generateMistedReefsEvents(vb);
  generateMistedReefsLayer(vb);

  generateShipwreckEvents(vb);
  generateShipwreckLayer(vb);

  mod.setRecipe("recipes.vaults.salvagemission", {
    id: "mariner.applyactionpostfindlocation.start.applyshipname",
    requirements: {
      "mariner.applyshipname": 1,
    },
    furthermore: [
      {
        rootAdd: {
          "mariner.shipnameid": CYCLE_IDS(
            "mariner.shipnameid",
            SHIP_WRECKS.length
          ),
        },
      },
      {
        mutations: [
          {
            filter: "mariner.destination",
            mutate: "mariner.shipnameid",
            level: "root/mariner.shipnameid",
          },
        ],
      },
    ],
  });

  mod.setRecipe("recipes.vaults.salvagemission", {
    id: "mariner.applyshipnameid",
    mutations: [
      {
        filter: "mariner.local",
        mutate: "mariner.shipnameid",
        level:
          "[~/exterior : {[mariner.port] && [mariner.docked]} : mariner.shipnameid]",
      },
    ],
  });
}
