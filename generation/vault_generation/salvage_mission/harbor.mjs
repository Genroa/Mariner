import { INJURE_CREWMEMBER, requireAndDestroy } from "../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS, SHIP_MANOEUVERING_SLOTS } from "../../slots_helpers.mjs";
import { SHIP_NAME } from "../ship_wrecks_names.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "./salvage_mission.mjs";

// EVENTS
export function generateHarborEvents(vb) {
  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.heart",
    task: {
      id: vaultPrefix("events.boredom"),
      craftable: false,
      label: "Small Town Jitters",
      startdescription:
        "I am not made to sit still long, and this dreary backwater harbor has little to offer to keep my Lack-Heart content. I wish to leave... I should calm myself with Winter or fortify myself with Heart",
    },
    chance: quantityDuoToChance("heart", "winter", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Inner Fire",
      description:
        "I call upon an inner store of mental fortitude, and push myself on a little longer. I still have more to do in this town.",
    },
    failure: {
      label: "Hole in my Chest",
      description:
        "Wherever I wish to go, I find the streets turning back to the waters' edge. The Horizon calls to me... and my Lack-Heart eats at my joys until I answer that call.",
      effects: { "mariner.wanderlust": 1 },
    },
  });
}

// TASKS
export function generateHarborLayer(vb) {
  vb.defineLayer("1");

  vb.defineHelpElement({
    id: "trail",
    label: "A Trail...",
    description: "Notes on the cargo, speculations on the route, indication on how the ship was stowed.",
    aspects: { lantern: 2, winter: 2 },
  });

  vb.defineHelpElement({
    id: "inkling",
    label: "An Inkling...",
    description:
      "Some things sailors know by the hair on the back of their neck. Mediums use the same sense, as do charlatans. My hairs all point towards where the ship went down, and shiver with the screams of the drowned.",
    aspects: { lantern: 2, knock: 2 },
  });

  vb.defineHelpElement({
    id: "notion",
    label: "A Notion...",
    description: `Locally this is as solid as fact... Venture further and it would be dismissed as biased speculation. [This can be used to pass the Rapids or help find where the ${SHIP_NAME} went down.]`,
    aspects: { lantern: 2, moth: 2 },
  });

  vb.defineHelpElement({
    id: "divingbell",
    label: "Diving Bell",
    description: "A heavy lead box that will allow us to catch a breath deep below the surf.",
  });

  vb.defineExplorationTask({
    taskId: "harbor",
    locationLabel: "Backwater Harbor",
    icon: "mariner.vaults.salvagemission.harbor",
    locationDescription: `Where a near-nameless tributary flows into a mist-clogged sea. This is where the ${SHIP_NAME} has last been seen. So this is where our search begins.`,
    tasks: ["mottleymechanic", "detailsofthewreck", "divebar", "sailout"],
    events: ["events.boredom"],
    next: [{ id: "mariner.applyshipnameid" }],
  });

  vb.defineTask({
    taskId: "mottleymechanic",
    label: "Mottley Mechanic",
    icon: "tasks/mariner.tasks.tinker.darkpurple",
    description:
      "This place is wharf and junkyard and pawnshop and also the only repair shop in town. Here they carry the tools that the Kite lacks to explore underwater.",
    actions: {
      explore: {
        hint: {
          label: "Commerce or Subterfuge?",
          startdescription:
            "Buried below molting ropes and mottled buoys, I see the glint of brass. A diving bell. I could buy it for a pretty Penny if I bring in the Funds...  Or I could bring my guile and my accomplices and try to spirit it away for free.",
          requirements: { funds: -1 },
        },
        challenge: {
          bypass: requireAndDestroy("funds"),
          task: {
            label: "Heavyweight Heist",
            startdescription:
              "Buried below molting ropes and mottled buoys, I see the glint of brass. A diving bell. I could buy it for a pretty Penny if I bring in the Funds...  Or I could bring my guile and my accomplices and try to spirit it away for free.",
          },
          success: {
            label: "A Labored Walk",
            description:
              "Security is rather lax, since the only thing of value weighs a figurative metric tonne. Yet its weight is no match for us. We carry it to the ship unbothered and unconcerned",
            effects: { [vb.vp("divingbell")]: 1, funds: -3 },
          },
          failure: {
            label: "The Embarrassing Light of Day",
            description:
              "After straining ourselves for hours, sunrise finds us only inches away from the front door. The owners come up to us, perplexed. We must suffer a fine and the embarrassment.",
            effects: { "mariner.experiences.divisive.standby": 1, funds: -1 },
            purge: { funds: 2 },
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "explore"),
          {
            id: "cash",
            label: "Cash",
            description: "Funds",
            required: { funds: 1 },
          },
        ],
      },
    },
  });

  vb.defineTask({
    taskId: "detailsofthewreck",
    label: "Details of the Wreck",
    icon: "tasks/mariner.tasks.papers.yellow",
    description: `We have some details about the ${SHIP_NAME}'s last journey. Some scraps of paper, some lists of names and goods. Somewhere here, we could find the hints to the location of their wreck.`,
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Delving Down the Paper Trail",
            startdescription:
              "Every ship, commercial or private, leaves documents and figures in its wake. The harbormaster will not do my work for me however, and the archive is as cold and dusty. These will be long, harrowing hours. ",
          },
          success: {
            label: "The Glint of Reason",
            description:
              "Between the words, a pattern. Within the pattern, a clue. And from that a direction to sail, and an area to comb.",
            effects: vb.vp({ trail: 1 }),
          },
          failure: {
            label: "The Death of Passion",
            description:
              "My mind has become as dusty and disorganized as the archives I dwell in. The world seems frayed and yellowed and dull and disappointing. I have not found what I sought.",
            effects: { "mariner.experiences.sobering.standby": 1 },
          },
          template: probabilityChallengeTemplates.STUDY,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      "mariner.sing": {
        challenge: {
          task: {
            label: "Alternative Sources",
            startdescription:
              "I could seek my information elsewhere. Memory is left in the wake of waves, carved into the shore. The sea remembers... at least in some histories. If I tune my instrument where the water meets the land, I might find new meanings in the words I sing.",
            requirements: {},
          },
          success: {
            label: "Unspoken Answers",
            description:
              "I am left not with certainty, but with a persistent nagging. Such passions as from the doomed and the drowning resonate deeply in rich, cold ocean waters.",
            effects: vb.vp({ inkling: 1 }),
          },
          failure: {
            label: "Unheard promises",
            description:
              "The tide whispers... But its whispers are unreliable. I lose myself in its rhythm and its tones and when next I open my mouth, not even I understand my speech. But it sounds so, so compelling to my ears.",
            effects: { "mariner.experiences.unsettling.standby": 1 },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
    },
  });

  vb.defineTask({
    taskId: "divebar",
    label: "Downtrodden Dive Bar",
    icon: "tasks/mariner.tasks.crowd.pink",
    description:
      "Everyone who is anyone in this town, either passes through here, or is passed through as a topic over the tongues of patrons.",
    actions: {
      talk: {
        hint: {
          label: "Mingle with the Locals",
          startdescription:
            "With the regularity of the tides, the patrons will stream into this bar and stumble home. If I come with enough cash, I can maybe grease some throats and loosen some lips.",
          requirements: { funds: -1 },
        },
        challenge: {
          task: {
            label: "Mingle with the Locals",
            startdescription:
              "With the regularity of the tides, the patrons will stream into this bar and stumble home. No one will know these waters better, and no place is more prone to gossip. If I can get these locals to open up, they could tell more than books ever could.",
            requirements: { funds: 1 },
          },
          success: {
            label: "Facts and Friendship",
            description: `People tend to be hesitant to talk to outsiders... but the captain of the ${SHIP_NAME} had not been local, and people love to gossip about outsiders, especially those who met a tragic end. At last call, I have the pervasive theories on where the ship went down.`,
            furthermore: [
              {
                effects: vb.vp({ notion: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("notion"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
            ],
            purge: { funds: 1 },
          },
          failure: {
            label: "Fraught Fiction",
            description:
              "For a while it seems we are on the right track, but I can notice the snickers that follow any direction we are given. I know for sure I have been played when at the end of the night, the bar tap has been put in my name.",
            effects: { funds: -1 },
            purge: { funds: 1 },
          },
          template: probabilityChallengeTemplates.HONEYED_TONGUE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "talk"),
          {
            id: "cash",
            label: "Cash",
            description: "Funds for the drinks that will lubricate any social occassion.",
            required: { funds: 1 },
          },
        ],
      },
    },
  });

  vb.defineTask({
    taskId: "sailout",
    label: "Sail Out",
    icon: "tasks/mariner.tasks.boat.blue",
    description: `At some point, I just have to head out. With clues or without, I could follow the ${SHIP_NAME}'s wake, and triangulate the place she went down.`,
    actions: {
      "mariner.sail": {
        hint: {
          label: "Leave The Harbor",
          startdescription:
            "Where the brackish river meets the briny sea. Unfortunate currents have kept this harbor from growing beyond its humble size. To leave this harbor for the next step in this adventure, I will have to sail the Kite.",
          requirements: { funds: -1 },
        },
        challenge: {
          task: {
            label: "Leave The Harbor",
            startdescription: `Where the brackish river meets the briny sea. Unfortunate currents have kept this harbor from growing beyond its humble size. Now those currents threaten to make me follow the same fate as the ${SHIP_NAME}.`,
            requirements: { "mariner.ship": 1 },
          },
          success: {
            label: "Tight Sailing",
            description:
              "The sea is never merciful, but today she is forgiving. I am guided through the hidden straits as by a kindly mother. I know I will not return here, but at least my exit was dignified. ",
            furthermore: [
              {
                effects: vb.vp({ mistedreefs: 1 }),
              },
              {
                mutations: [
                  {
                    filter: vb.vp("mistedreefs"),
                    mutate: "mariner.shipnameid",
                    level: "mariner.shipnameid",
                  },
                ],
              },
              {
                effects: vb.vp({ sailout: -1 }),
              },
            ],
            purge: { [vb.currentLayerAspectId()]: 10 },
          },
          failure: {
            label: "A Scrape and a Bruise",
            description:
              "It makes an ungodly noise, when the Kite scrapes the sandbar hidden just below the surf. But worse than that is the laughter I can hear from the fishermen wives and retired mariners upon the boardwalks and the piers. It will not do my standing with the crew any good.",
            effects: { "mariner.experiences.divisive.standby": 1 },
            mutations: [
              {
                filter: "mariner.ship",
                mutate: "mariner.ship.damage",
                level: 1,
                additive: true,
              },
            ],
            linked: [{ id: "mariner.sailing.sinking" }],
          },
          template: probabilityChallengeTemplates.OVERTAKE,
        },
        slots: [
          ...SHIP_MANOEUVERING_SLOTS(),
          {
            id: "shipslot",
            label: "My Ship",
            description: "To travel the sea, I require my vessel",
            required: { "mariner.ship": 1 },
          },
        ],
      },
    },
  });
}
