import { buildAdvancedRefinementBlock } from "../generation_helpers.mjs";

export const SHIP_WRECKS = ["Bella Sonata", "Vengeful Spirit", "Cerberus", "Olympic", "Ventus Excitor", "USS Abraham Lincoln", "Chimaera", "Dazzler", "Hispaniola"  ];

export function CYCLE_IDS(id, max) {
  return `1 - [root/${id}] * ([root/${id}] >= ${max})`;
}

export const SHIP_NAME = buildAdvancedRefinementBlock(
  SHIP_WRECKS.map((name, index) => ({
    aspect: "mariner.shipnameid",
    value: SHIP_WRECKS.length - index,
    text: name,
  }))
);
