import { CYCLE_IDS, SHIP_NAME, SHIP_WRECKS } from "./ship_wrecks_names.mjs";

export const DOCKSIDE_WAREHOUSE = {
  portId: "docksidewarehouse",
  label: "Dockside Warehouse",
  notorietyLabel: "Alarm in the Warehouse",
  description:
    "Where criminals have access to the sea, strange and valuable things accrue.",
  id: "vaults.docksidewarehouse",
  generic: true,
  arrivalEffects: [
    {
      effects: {
        "mariner.vaults.docksidewarehouse.crookedalley": 1,
      },
    },
  ],
  notorietyLabels: [
    {
      label: "Mild Alarm",
      description:
        "Some whispers have reached them. They don’t know what they don’t know, but they know there is something to know.",
    },
    {
      label: "Fierce alarm",
      description:
        "People who despise getting attention know someone is paying them attention. They will start paying attention back.",
    },
    {
      label: "Gathered Forces",
      description: "They have called in reinforcements. They are ready for us.",
    },
    {
      label: "Full Lock-Down",
      description: "All doors are locked, all eyes are open.",
    },
  ],
};

export const SALVAGE_MISSION = {
  portId: "salvagemission",
  label: "Salvage Mission",
  notorietyLabel: "Air Availability",
  description: `A shipwreck in some ill-fortuned waters have left sailors abuzz with stories of its cargo. If we can salvage something from the ${SHIP_NAME}, it might be very worth our while.`,
  id: "vaults.salvagemission",
  generic: true,
  awayAspects: { "mariner.applyshipname": 1 },
  arrivalEffects: [
    {
      effects: {
        "mariner.vaults.salvagemission.harbor": 1,
      },
    },
    {
      mutations: [
        {
          filter: "mariner.aspects.vaults.salvagemission.layer.1",
          mutate: "mariner.shipnameid",
          level: "mariner.shipnameid",
        },
      ],
    },
  ],
  notorietyLabels: [
    {
      label: "Depleting Levels",
      description: "The clock is beginning to tick.",
    },
    {
      label: "Shallow Breaths",
      description:
        "I must savor what air I hold in my longs, before I can hold no more.",
    },
    {
      label: "Empty tanks",
      description:
        "My body is aching for air and open space. The corners of my vision are beginning to shiver.",
    },
    {
      label: "Final Vapors",
      description:
        "Stars are beginning to dance in the darkness. Not long now. Not much longer at all.",
    },
  ],
};

export const RESCUE_MISSION = {
  portId: "rescuemission",
  label: "Rescue Mission",
  notorietyLabel: "Time Elapsed",
  description:
    "A shipwreck has been reported recently, and it the company has placed a reward out on who ever can bring back the missing crewmembers, who are stranded, somewhere on an island.",
  id: "vaults.rescuemission",
  generic: true,
  awayAspects: { "mariner.applyshipname": 1 },
  arrivalEffects: [
    {
      effects: {
        "mariner.vaults.rescuemission.harbor": 1,
      },
    },
    {
      mutations: [
        {
          filter: "mariner.aspects.vaults.salvagemission.layer.1",
          mutate: "mariner.shipnameid",
          level: "mariner.shipnameid",
        },
      ],
    },
  ],
  notorietyLabels: [
    {
      label: "Minutes Ticking",
      description: "The clock is beginning to tick.",
    },
    {
      label: "Hours Wasting",
      description: "Weather and ware may have taken their toll.",
    },
    { label: "Days Withering", description: "few could survive so long." },
    {
      label: "Too Long",
      description:
        "The Last destination of many a sailor are the shores of No Hope.",
    },
  ],
};

export const SUNKEN_CITY = {
  portId: "sunkencity",
  label: "The Sunken City",
  description:
    "Sometimes, when the moon is right and the tides are swayed by nostalgia, the sea is opened like a treshold and a swallowed city is laid bare.",
  authorizeRetreat: true,
  notDiscoverable: true,
  arrivalEffects: [
    {
      effects: {
        "mariner.vaults.sunkencity.edge": 1,
      },
    },
  ],
  arrivalAlt: [
    { id: "mariner.vaults.sunkencity.token.terror.appears", additional: true },
  ],
  recipeContentOnLeave: {
    furthermore: [
      {
        rootAdd: {
          "mariner.moon.fixed": "-[root/mariner.moon.fixed]",
        },
      },
    ],
    updateCodexEntries: {
      "codex.ascensions.exemplum.hiraezh": {
        add: ["codex.ascensions.exemplum.hiraezh.solvedvault"],
        remove: ["codex.ascensions.exemplum.hiraezh.solvedmystery"],
      },
    },
  },
  notorietyLabels: [
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
    { label: "", description: "" },
  ],
};
