import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateHiddenStash(vb) {
  vb.defineTask({
    taskId: "hiddenstash",
    label: "Smugglers Stash",
    icon: "tasks/mariner.tasks.treasure.goldenyellow",
    description:
      "As we search the tunnel walls for the tell-tale graffiti showing us the way, we instead notice an area, suspiciously free of dust and grime. Upon examination we find a hidden shelf, a locked smugglers cache.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Pilfer the Loot",
            startdescription:
              "We enact force or guile upon this vault, we can gain access to the loot stashed inside. It will be noisy, and the rightful owners will investigate. Pursuing this opportunity may hinder my reputation once I reach the Bazaar.",
          },
          success: {
            label: "Folial Rewards",
            description:
              "We crack the hidden space open along it seams. inside, we find a bundle of papers and a leatherbound folio, marked with the a RESTRICTED stamped across it's face.",
            effects: {
              [vb.vp("hiddenstash")]: -1,
              "mariner.notoriety": 1,
              "mariner.trappings.knock.1": 1,
            },
            linked: [{ id: "mariner.drawreward.scholarly.1" }],
            grandReqs: {
              "root/mariner.flag.smuggler.draw": -1,
            },
            furthermore: [
              {
                rootAdd: {
                  "mariner.flag.smuggler.draw": 1,
                },
              },
            ],
          },
          failure: {
            label: "Shouts in the Distance",
            description:
              "As we enact our works of opening, suddenly voices can be heard, getting closer through the tunnels. We flee the scene, before who ever this stance was intended for, catches us in the act.",
            effects: {
              [vb.vp("hiddenstash")]: -1,
              dread: 1,
              "mariner.notoriety": 2,
            },
          },
          template: probabilityChallengeTemplates.BREAK_IN,
        },
      },
    },
  });
}
