import { INJURE_CREWMEMBER, SPAWN_SUSPICION } from "../../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../../../slots_helpers.mjs";
import { RECIPES_FILE } from "../../eksters_nest.mjs";

export function generateOthersUnderground(vb) {
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.coins"),
    label: "An Easy Alternative",
    startdescription:
      "If I make myself agreeable to these people, they might lend me the token of entry into the Bazaar. I’ll probably have to make it worth their while. All here are merchants or criminals, fluent in the language of coin.",
    requirements: { [vb.vp("othersunderground")]: 1, funds: -3 },
    actionId: "talk",
    hintonly: true,
  });

  vb.defineTask({
    taskId: "othersunderground",
    label: "Others Underground",
    icon: "tasks/mariner.tasks.watchers.blue",
    description:
      "Voices just around the corner. Other travelers to the Bazaar? I could approach this issue in different ways, and each carries a risk...",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Overpower Them?",
            startdescription:
              "They hold a silver token, which grants them entry into the bazaar. I don’t think they will part with it painlessly...",
          },
          success: {
            label: "To the Victor...",
            description:
              "We leave with our token of entry, and we leave them with the certainty that they should never speak of this encounter. Yet whispers of it will spread, like an oil-slick",
            effects: {
              [vb.vp("silverflorin")]: 1,
              [vb.vp("othersunderground")]: -1,
              "mariner.notoriety": 1,
            },
            inductions: [SPAWN_SUSPICION],
            linked: [{ id: "mariner.drawreward.precious" }],
          },
          failure: {
            label: "Foiled!",
            description:
              "Knives flashed, men grunted, the lantern fell to the ground. When we got the light back on one of ours was lost, and the others had scattered.\n\nAs a consolation price, maybe the direction they went to could help us locate the hidden entrance...",
            effects: {
              [vb.vp("clue")]: 1,
              [vb.vp("othersunderground")]: -1,
              "mariner.notoriety": 1,
            },
            inductions: [INJURE_CREWMEMBER, SPAWN_SUSPICION],
          },
          template: probabilityChallengeTemplates.OVERCOME,
        },
      },
      talk: {
        challenge: {
          task: {
            label: "Approach Them?",
            startdescription:
              "If I make myself agreeable to these people, they might escort me into the Bazaar. I'll probably have to make it worth their while.",
            requirements: { funds: 3 },
          },
          success: {
            label: "A Deal is Struck",
            description:
              "They leave me with a token of entry, but wish not to associate with us further. they disappear into the tunnels.",
            effects: {
              [vb.vp("othersunderground")]: -1,
              [vb.vp("silverflorin")]: 1,
              funds: -3,
            },
          },
          failure: {
            label: "Bamboozled",
            description:
              "Played by the one I intended to play. I am left with nothing but seething rage, thinking of the stories they will tell of this encounter.\n\nAs a consolation price, maybe the direction they went to could help us locate the hidden entrance...",
            effects: {
              funds: -3,
              [vb.vp("othersunderground")]: -1,
              [vb.vp("clue")]: 1,
              "mariner.notoriety": 1,
              "mariner.experiences.divisive.standby": 1,
            },
            inductions: [SPAWN_SUSPICION],
          },
          template: probabilityChallengeTemplates.HONEYED_TONGUE,
        },
        slots: [
          ...CHALLENGE_SLOTS(3, "talk"),
          {
            actionId: "talk",
            id: "cashslot1",
            label: "Funds",
            description: "Financial encouragement",
            required: { funds: 1 },
          },
          {
            actionId: "talk",
            id: "cashslot2",
            label: "Funds",
            description: "Financial encouragement",
            required: { funds: 1 },
          },
          {
            actionId: "talk",
            id: "cashslot3",
            label: "Funds",
            description: "Financial encouragement",
            required: { funds: 1 },
          },
        ],
      },
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Follow them?",
            startdescription: "If we step carefully, and stick to the shadows, we can follow them to the entrance of the Bazaar.",
          },
          success: {
            label: "Like a Shadow",
            description:
              "The quickest way to a hidden place is with a guide. And their torchlight guided us as much as them, this time. They disappeared right around...here. And around there we find a small slit in the wall, indicatin the entrance.",
            effects: {
              [vb.vp("clue")]: 4,
              [vb.vp("othersunderground")]: -1,
              "mariner.rumour.northsea": 1,
            },
          },
          failure: {
            label: "Exposed!",
            description:
              "They were on to us. They gave us the slip in the darkest corridors, and left us lightless, lost and alone.\n\nAs a consolation price, maybe the direction they went to could help us locate the hidden entrance...",
            effects: {
              [vb.vp("clue")]: 1,
              [vb.vp("othersunderground")]: -1,
              "mariner.notoriety": 1,
              "mariner.experiences.sobering.standby": 1,
            },
            inductions: [SPAWN_SUSPICION],
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });
}
