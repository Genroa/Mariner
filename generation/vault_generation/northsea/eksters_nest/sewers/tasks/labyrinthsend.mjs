import { SPAWN_SUSPICION, requireAndDestroy } from "../../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateLabyrinthsEnd(vb) {
  vb.defineTask({
    isExclusive: true,
    taskId: "labyrinthsend",
    icon: "tasks/mariner.tasks.hiddenentrance.goldenyellow",
    label: "Labyrinth's End",
    description:
      "The place where travel ends. The entrance is here, barely an indent in the stone. But behind the door is a guardian, who expects to be paid only in Silver Florins.",
    actions: {
      talk: {
        hint: {
          label: "A Door and its Guard",
          startdescription:
            "A small slit opens, and a pair of eyes the color of the canals stare at me. They expect to see a particular coin. They expect to see someone they know. But maybe if I apply my charms or my tricks, I can talk my way in.",
          requirements: { [vb.vp("silverflorin")]: -1, moth: -1, grail: -1 },
        },
        challenge: {
          bypass: requireAndDestroy(vb.vp("silverflorin")),
          task: {
            label: "Pass the Bouncer",
            startdescription:
              "A small slit opens, and a pair of eyes the color of the canals stare at me. They expect to see a particular coin. They expect to see someone they know. But maybe... maybe we can talk our way in. If I had the silver coin I saw on the others here, they would let me in without question.",
          },
          success: {
            label: "Passage granted",
            description:
              "The slit closes and the eyes disappear. Gears shift, the walls rumble. Succes! I can hear the din of commerce from beyond.",
            effects: vb.vp({ labyrinthsend: -1, sunlessbazaar: 1 }),
            purge: {
              [vb.currentLayerAspectId()]: 5,
              [vb.vp("clue")]: 30,
              [vb.vp("silverflorin")]: 1,
            },
          },
          failure: {
            label: "Disdain of the Doorkeeper",
            description:
              "The slit closes and the eyes disappear. They do not return for a long time. I might do better if I acquire a Silver Florin from other travelers down here in the dark.",
          },
          template: probabilityChallengeTemplates.SILVER_TONGUE,
        },
      },
      explore: {
        challenge: {
          task: {
            label: "Force Our Way In",
            startdescription:
              "While the door is reinforced and the entrance guarded, we could enter with a display of force. It would not do good for my standing in the bazaar, and alert the eksternest beyond.",
          },
          success: {
            label: "Knocked Off Its Hinges",
            description:
              "We unmoor the door, which lands almost softly on the ground behind, as if cushioned by the air it displayced. The guard of the door is to stunned to stop our passing, but doubtless word will spread of our uncooth and uninvited entrance.",
            effects: {
              [vb.vp("labyrinthsend")]: -1,
              [vb.vp("sunlessbazaar")]: 1,
              "mariner.notoriety": 1,
            },
            purge: {
              [vb.currentLayerAspectId()]: 5,
              [vb.vp("clue")]: 30,
              [vb.vp("silverflorin")]: 1,
            },
            inductions: [SPAWN_SUSPICION],
          },
          failure: {
            label: "A Hatched Job",
            description:
              "We tear the door, strip for strip, splinter for splinter, until we get a hole large enough for us to get through. The Doorkeeper has allready fled. no doubt to ready others for our coming.",
            effects: {
              [vb.vp("labyrinthsend")]: -1,
              [vb.vp("sunlessbazaar")]: 1,
              "mariner.notoriety": 2,
            },
            purge: {
              [vb.currentLayerAspectId()]: 5,
              [vb.vp("clue")]: 30,
              [vb.vp("silverflorin")]: 1,
            },
            inductions: [SPAWN_SUSPICION],
          },
          template: probabilityChallengeTemplates.BREAK_IN,
        },
      },
    },
  });
}
