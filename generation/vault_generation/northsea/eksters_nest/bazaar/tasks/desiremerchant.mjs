import { ENUMS, ENUM_LABELS } from "../../../../../generate_enums.mjs";
import { LORE_ASPECTS, requireAndDestroy } from "../../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { ASPECTS_FILE, RECIPES_FILE } from "../../eksters_nest.mjs";

export function generateDesireMerchant(vb) {
  vb.defineTask({
    taskId: "desiremerchant",
    label: "A Merchant of Want",
    icon: "tasks/mariner.tasks.ear.darkred",
    description:
      "In between great oak casks and fine-necked glass distillers sits a merchant who specialises in desires. They keep it bottled up in many vintages for many tastes, With the right incentives, they can perhaps be persuaded to reveal the needs of a certain corvine client.",
    slots: [
      {
        actionId: "talk",
        id: "intel",
        required: { "mariner.scrapofinformation": 1 },
        label: "Intelligence to Trade",
        description: "If I part with these ritches in potentia, this merchant can enlighten me on the needs of Auntie Ekster.",
      },
    ],
    actions: {
      talk: {
        challenge: {
          bypass: requireAndDestroy("mariner.scrapofinformation"),
          task: {
            label: "Offer a Taste",
            startdescription:
              "If I entice the trader, they could be persuaded to share what thrill or trinket is desired by auntie ekster today.",
          },
          success: {
            label: "A Tasting and a Telling",
            startdescription:
              "Small glasses are set out, and I lean close as if to listen him explain the drink. In truth he explains to me the dealings of the Eksternest, and their devotion to Beachcomber, and their reverence for the Lily King. In between nips of the sweet and the savory, he tells me what the Auntie's Avarice demands next.",
            effects: {
              [vb.vp("desiremerchant")]: -1,
              "mariner.scrapofinformation": -1,
            },
            linked: [{ id: `mariner.giveawayauntieekstersdesire.*` }],
          },
          failure: {
            label: "Discretion",
            description:
              "The Merchant decides to leave my want unfulfilled. I could not convince them to part with even a drop of knowledge.",
            effects: {
              [vb.vp("desiremerchant")]: -1,
            },
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
      },
    },
  });

  mod.setAspect(ASPECTS_FILE, { id: `mariner.auntieekstersdesire` });
  mod.setRecipe(RECIPES_FILE, {
    id: `mariner.updateauntieesktersdesire`,
    rootSet: {
      "mariner.auntieekstersdesire": `Random(1, ${LORE_ASPECTS.length + 1})`,
    },
  });
  for (const lore of LORE_ASPECTS) {
    mod.setRecipe(RECIPES_FILE, {
      id: `mariner.giveawayauntieekstersdesire.${lore}`,
      grandReqs: {
        [`[root/mariner.auntieekstersdesire]=[root/${ENUM_LABELS.principles[lore]}]`]: 1,
      },
      label: `Auntie Desires: ${mod.helpers.capitalize(lore)}`,
      startdescription: `The Beachcomber has demanded that his devotee aquire a hoard of ${mod.helpers.capitalize(lore)}`,
      updateCodexEntries: {
        "codex.auntieekster": {
          add: [`codex.auntieekster.founddesire.${lore}`],
          remove: LORE_ASPECTS.filter((a) => a !== lore).map((a) => `codex.auntieekster.founddesire.${a}`),
        },
      },
    });
  }
}
