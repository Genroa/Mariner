import { SIGNALS } from "../../../../../generate_signal_aspects.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateIntelMerchant(vb) {
  vb.defineTask({
    taskId: "intelmerchant",
    label: "Seller of Songbirds",
    icon: "tasks/mariner.tasks.merchant.vertdegris",
    description:
      "In the eves of a large vaulted room, a salesman with an immaculate reputation can be found. She is called a seller of songbirds, but she rather be known as a seller of Birdsong. Between the cages and the chirping larks, she can be convinced to exchange stories for.",
    slots: [
      {
        actionId: "mariner.sing",
        id: "story1",
        required: { "mariner.story": 1 },
        forbidden: { "mariner.story.exhausted": 1 },
        label: "A Tale to Trade",
        description: "Multiplied when traded, exhausted when sold.",
      },
      {
        actionId: "mariner.sing",
        id: "story2",
        required: { "mariner.story": 1 },
        forbidden: { "mariner.story.exhausted": 1 },
        label: "A Tale to Trade",
        description: "Multiplied when traded, exhausted when sold.",
      },
    ],
    actions: {
      "mariner.sing": {
        hint: {
          label: "A Trade of Secrets",
          startdescription:
            "The Seller knows snippets of the secrets of all the birds... Especially local ones. If I sing for her full rendition of my stories, she would return to me a shred of the Birdsong she has overheard.",
          requirements: { "mariner.story": -2 },
        },
        challenge: {
          task: {
            label: "Sing for Supper",
            startdescription:
              "The Seller knows snippets of the secrets of all the birds... Especially local ones. Between the tweeting, chirping and chirring cacophony, i ready my talents to perform what stories I can offer.",
            requirements: { "mariner.story": 2 },
          },
          success: {
            label: "Symurghical Affair",
            startdescription:
              "She rewards me with a smile, and with a useful scrap she picked up recently. The North sea holds no secrets for an ear as trained as hers.",
            effects: {
              [vb.vp("intelmerchant")]: -1,
              "mariner.scrapofinformation": 1,
            },
            aspects: {
              [SIGNALS.EXHAUST_STORY]: 1,
            },
          },
          failure: {
            label: "Uninspired, Uninspiring",
            description:
              "Today, I had nothing to offer that delighted the Seller. But that just as well might be my performance as the stories. Try meaning is found in the telling, as any collector of birdsong knows.",
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
      },
    },
  });
}
