import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateLittleBird(vb) {
  vb.defineTask({
    taskId: "littlebird",
    label: "A Little Bird",
    icon: "tasks/mariner.tasks.crowd.vertdegris",
    description:
      "Between the lavish stalls with banners of silk and brochade, a dull-eyed child sits behind a vegatable box, displaying trickets and scrap for ware. The doodats do not interest me, but if the child is one of Aunties Little Birds, it could lead me to the entrance of the Nest.",
    decayTo: vb.vp("shapedisappearing"),
    lifetime: 60,
    slots: [
      {
        actionId: "talk",
        id: "trapping",
        required: { "mariner.trapping": 1 },
        label: "Bargaining Chip",
        description: "What trinket or treasure do I offer to the Little Bird?",
      },
    ],
    actions: {
      talk: {
        hint: {
          label: "Try to Bargain with the Little Bird",
          startdescription:
            "The little birds are Aunties eyes and ears, but mostly they are her greedy talons. They all wish to bring treasure back to the nest. If I offer them something appealing, they might lead me back to the nest.",
          requirements: { "mariner.trapping": -1 },
        },
        challenge: {
          task: {
            label: "A Bargain",
            startdescription:
              "I lay out my offer and i lay out my price. I do my best to sugarcoat, but it is hard to guage if my words find purchase. Wether that is because of the language barrier, or the poppy-smoke still wafting from their cloths, I cannot tell.",
            requirements: { "mariner.trapping": 1 },
          },
          success: {
            label: "Tip for Tattle",
            description:
              "their new treasure clutched in rightly in their hand, they begin to weave through the bazaar, barely waiting long enough for me to us to keep up. Once we have arrived in front of a curtained portcullis, they turn away and vanish into the crowds.",
            effects: {
              [vb.vp("corridorsbehind")]: 1,
              [vb.vp("littlebird")]: -1,
              [vb.vp("shapedisappearing")]: 1,
              "mariner.trapping": -1,
            },
          },
          failure: {
            label: "Swiped!",
            description:
              "I display my bargaining chips, but the little bird does not move, simply stares ahead, dim-eyed, dull-faced. But when my attention strays for a moment, suddenly the offering is swiped from my hand. I can still see the tiny figure of the child, shop abandoned, dispappearing into the crowds.",
            effects: {
              [vb.vp("littlebird")]: -1,
              [vb.vp("shapedisappearing")]: 1,
              "mariner.trapping": -1,
            },
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
      },
    },
  });
}
