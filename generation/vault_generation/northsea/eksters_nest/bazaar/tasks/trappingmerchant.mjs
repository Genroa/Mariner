import { LORE_ASPECTS } from "../../../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";
import { RECIPES_FILE } from "../../eksters_nest.mjs";

export function generateTrappingMerchant(vb) {
  vb.defineTask({
    taskId: "trappingmerchant",
    label: "Merchants at the Fringe",
    icon: "tasks/mariner.tasks.merchant.goldenyellow",
    description:
      "In canvas-clad corners of this cavernous compound, exists a commerce of curiosities. Here, power is traded for power.",
    aspects: { modded_talk_allowed: 1 },
    slots: [
      {
        actionId: "talk",
        id: "trapping",
        required: { "mariner.trapping": 1 },
        label: "My Offer to Trade",
        description: "A Trapping to trade for another.",
      },
      {
        actionId: "talk",
        id: "funds",
        required: { funds: 1 },
        label: "Funds",
        description: "An Incentive to sweeten the deal.",
      },
      {
        actionId: "talk",
        id: "song",
        required: { "mariner.song": 1 },
        label: "Intent",
        description: "What do I seek to acquire?",
      },
    ],
    actions: {},
  });

  mod.setRecipe(RECIPES_FILE, {
    id: `${vb.vp(`trappingmerchant.talk.hint`)}`,
    hintOnly: true,
    actionId: "talk",
    grandReqs: {
      [vb.vp("trappingmerchant")]: 1,
      "[funds] + [mariner.trapping.worth]": -1,
    },
    label: "Not Enough Value",
    startdescription: "They'll need a rarer trapping, or a little something to entice them into making the deal.",
  });

  for (const lore of LORE_ASPECTS) {
    mod.setRecipe(RECIPES_FILE, {
      id: `${vb.vp(`trappingmerchant.talk.tradefor.${lore}`)}`,
      actionId: "talk",
      craftable: true,
      grandReqs: {
        [vb.vp("trappingmerchant")]: 1,
        "[funds] + [mariner.trapping.worth]": 1,
        "mariner.song": 1,
        [`mariner.song.${lore}`]: 1,
      },
      label: `Trade for a ${lore} Trapping`,
      startdescription: `A Deal has been struck, and I am giving a curiosity strong with the power of ${lore}.`,
      warmup: 20,
      furthermore: [
        {
          effects: { funds: -1 },
        },
        {
          effects: { funds: "max(0, [mariner.trapping.worth]-1)" },
        },
        {
          effects: { "mariner.trapping": -1 },
        },
        {
          effects: {
            [`mariner.trappings.${lore}.1`]: 1,
            [vb.vp("trappingmerchant")]: -1,
          },
        },
      ],
    });
  }
}
