import { probabilityChallengeTemplates } from "../../../../../probability_challenge_templates.mjs";

export function generateShapeDisappearing(vb) {
  vb.defineTask({
    taskId: "shapedisappearing",
    label: "A Shape Disappearing Between the Canvas...",
    icon: "tasks/mariner.tasks.crowd.vertdegris.quartertransparency",
    description: "The little bird is flitting away. Perhaps, if I am quick enough, I can follow them to their Nest",
    lifetime: 10,
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Birdwatching",
            startdescription:
              "We set off in pursuit. But the Bazaar is unfamiliar terrain for us, where for the Birds, it's home. We try to keep our smaller quary in sight, as they duck into tunnels, over bridges and behind tents.",
          },
          success: {
            label: "The Entrance",
            description:
              "A Final corner, and then the child arrives. A single unmarked corridor covered by crimson curtains. allready we can smell the first wafts of the sweet smokes that suffice the insides of the Eksternest.",
            effects: {
              [vb.vp("corridorsbehind")]: 1,
              [vb.vp("shapedisappearing")]: -1,
            },
          },
          failure: {
            label: "Lost in the Crowds",
            description: "Somewhere in the crowded and confusing layout of the bazaar, we lose sight of our prey.",
            effects: {
              [vb.vp("shapedisappearing")]: -1,
            },
          },
          template: probabilityChallengeTemplates.SNEAK_IN,
        },
      },
    },
  });
}
