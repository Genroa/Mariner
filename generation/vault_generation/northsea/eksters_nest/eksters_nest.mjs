import { VaultBuilder } from "../../vaultbuilder.mjs";

import { generateSewersEvents } from "./sewers/events.mjs";
import { generateSewersLayer } from "./sewers/tasks.mjs";

import { generateSunlessBazaarEvents } from "./bazaar/events.mjs";
import { generateSunlessBazaarLayer } from "./bazaar/tasks.mjs";

import { generateEksternestEvents, generateEksternestLayer } from "./nest.mjs";
import {
  buildSequentialCodexEntry,
  generateCodexEntry,
} from "../../../codex_generation/codex_generation.mjs";
import { LORE_ASPECTS } from "../../../generation_helpers.mjs";

export const ELEMENTS_FILE = "vaults.ekstersnest";
export const RECIPES_FILE = "recipes.vaults.ekstersnest";
export const ASPECTS_FILE = "aspects.vaults.ekstersnest";

export const vaultPrefix = (end) => `mariner.vaults.ekstersnest.${end}`;

export function generateEkstersNestVault() {
  const vb = new VaultBuilder("ekstersnest", "northsea");

  generateSewersEvents(vb);
  generateSewersLayer(vb);

  generateSunlessBazaarEvents(vb);
  generateSunlessBazaarLayer(vb);

  generateEksternestEvents(vb);
  generateEksternestLayer(vb);

  const learnedDesire = (aspect) => ({
    [`codex.auntieekster.founddesire.${aspect}`]: `While passing through the Bazaar, I met merchants who told me  her latest known desire was a ${aspect} Trapping.\n\n\n`,
  });

  generateCodexEntry(
    "codex.auntieekster",
    "[N. Sea] Auntie Ekster",
    buildSequentialCodexEntry([
      // Given by the veiled lady when the ring quest starts
      "Below the city of amsterdam lies a labyrinth of waterways and walkways, built upon the water management system of this undrowned city. To that lightless place sinks down what cannot be tolerated by light of day, and among the thieves, renegades and smugglers, operates a gang of adepts lead by Auntie Ekster. She has pilfered a trinket from the Veiled Lady.\n\n\n",
      // Optional bit, it's the bit appearing if you learned about her last known desire
      ...LORE_ASPECTS.map((aspect) => learnedDesire(aspect)),
      // Appears if you stole the ring
      {
        "codex.auntieekster.stolering":
          "I stole back the ring that was stolen from the Veiled Lady",
      },
      // Appears if instead you negociated the ring
      {
        "codex.auntieekster.negotiatedring":
          "I dangled what Auntie desired before her, and got her to part with the Ring.",
      },
    ])
  );
}
