import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { CHALLENGE_SLOTS } from "../../../slots_helpers.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../../vault_generation.mjs";
import { RECIPES_FILE, vaultPrefix } from "./sunken_city.mjs";

// EVENTS
export function generateCityEvents(vb) {
  mod.setDeck("decks.vaults.sunkencity", {
    id: vaultPrefix("artifacts.deck"),
    spec: [vb.vp("gapingcup"), vb.vp("pair"), vb.vp("links")],
    defaultcard: vb.vp("dust"),
  });
  mod.deckIdsToShuffleOnLeave["sunkencity"].push(vaultPrefix("artifacts.deck"));

  // horror TOKEN
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.horror",
    id: vb.vp("token.horror.appears"),
    label: "Churning Darkness",
    startdescription:
      "The walls of water encircle the city. The sight of an endless expense of water is familiar to me, as is the feeling of being drawn towards it.",
    warmup: 180,
    linked: [{ id: vb.vp("token.horror.windup") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.horror",
    id: vb.vp("token.horror.windup"),
    label: "Sink-silt Horror",
    startdescription:
      "One would think the fear of a wall of water is it coming crashing down. More insidious, however is my urge to walk to its edge. To stare into its ripples of black on black.",
    warmup: 60,
    linked: [{ id: vb.vp("token.horror.wardedoff") }, { id: vb.vp("token.horror.attack") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.horror",
    id: vb.vp("token.horror.wardedoff"),
    label: "Protected",
    startdescription: "Memories of joy are my shield now, and the expectation of brighter times my lantern.",
    extantreqs: {
      [vb.vp("homebound")]: 1,
    },
    warmup: 30,
    linked: [{ id: vb.vp("token.horror.windup") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    actionId: "mariner.haltable.horror",
    id: vb.vp("token.horror.attack"),
    label: "Depths of Despair",
    startdescription: "Horror is the fear of that which you can't tear your eyes away from.",
    description: "The gloom has slid through my pupils and settled in my stomach like a restless sea.",
    furthermore: [
      {
        target: "~/tabletop",
        effects: {
          "mariner.experiences.rattling.standby": 1,
        },
      },
    ],
    warmup: 30,
    linked: [{ id: vb.vp("token.horror.windup") }],
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.moth",
    task: {
      id: vaultPrefix("events.seagull"),
      craftable: false,
      label: "Seagull Choir",
      startdescription:
        "The churning walls of water get masked by the throngs of birds, coalescing on our position. We stand at the nadir of their spiral and here they plunge down onto the soggy seafloor. Their beaks break open the earth, but they just as easily could break into us too. [Approach this challenge with Moth or Winter.]",
    },
    chance: quantityDuoToChance("moth", "winter", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Settle the Flock",
      description:
        "A great quiet falls over the scene, the featherstorm lessening till the last remaining seastern drifts gently down, settling on the white-blanketed sea floor. We move gently, stepping only where there is sand. The birds watch. ",
    },
    failure: {
      label: "Rapt at the Earth",
      description:
        "All around us, the earth is raked by beaks, as more and more land to open the earth. It’s unnervingly hard to resist the urge to bring my own face to the salt, and more unnerving still that I know what I’ll find there would be delectable.",
      effects: { "mariner.experiences.pain.standby": 1 },
    },
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.winter",
    task: {
      id: vaultPrefix("events.specters"),
      craftable: false,
      label: "Specters of Ker Ys",
      startdescription:
        "The city is empty. No person could have survived its fall. But as our movements leave an impression on the salt-caked stone, so too have the citizens of ker ys left hollow outline in the air where they once walked. [Approach this challenge with winter of Lantern.]",
    },
    chance: quantityDuoToChance("lantern", "winter", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "A Protective Nimbus",
      description:
        "The pain of the past might burn still, but it is distant. I will keep the Unmourned at a distance with the sneer of cold command on my furrowed brow and the brightness of my torch. ",
    },
    failure: {
      label: "Crumbling Will",
      description: "They wish they were. Interrupted, then. Now, stuck. No life left to go about.",
      effects: { "mariner.experiences.dreadful.standby": 1 },
    },
  });

  generateEventChallenge({
    recipesFile: RECIPES_FILE,
    actionId: "mariner.event.grail",
    task: {
      id: vaultPrefix("events.tidepools"),
      craftable: false,
      label: "Stygian Tide Pools",
      startdescription:
        "Around a weathered black stone, seawater is regurgitated from many C’thonic openings to a deeper place. We must survive the dark, whipping waters from these weeping wells. [Approach this with Grail or Heart.]",
    },
    chance: quantityDuoToChance("grail", "heart", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Ageless Sailor-rites",
      description:
        "The channel has been sailed for millennia and old rites mouthed on the waters above are remembered below. We murmur the Bretonic-Latin to placate the Linfern: the Lares and Lemures who have been neglected since before the Bretons landed these shores.",
    },
    failure: {
      label: "Tribute to the Dei Manes",
      description:
        "The tide comes in, strong enough to draw water and blood beneath the skin. Even the cold brine is not enough to numb us to its flaying flow. When the tide reverses, the tidepool swallows it all, taking its tithe with it.",
      inductions: [INJURE_CREWMEMBER],
    },
  });
}

// TASKS
export function generateCityLayer(vb) {
  vb.defineLayer("2");

  vb.defineHelpElement({
    id: "gapingcup",
    icon: "toolgrailf",
    label: "The Gaping Cup",
    unique: true,
    description:
      "The city was guided to venerate the power that births and consumes. These objects show their widespread worship. Each face is of the same man, but his expression changes from haughty to ravenous. [A Piece of the Story of Ker Ys.]",
  });

  vb.defineHelpElement({
    id: "links",
    label: "Love-long Links",
    icon: "toolwinterd",
    unique: true,
    description:
      "A metallic knot, made of two triskelions entwining. Tin, Silver, Gold and some metals that seem more exotic still. A symbol of an upcoming union, or tribute paid to a rising power? [A Piece of the Story of Ker Ys.]",
  });

  vb.defineHelpElement({
    id: "pair",
    icon: "toolheartb",
    label: "Icons of a Royal Pair",
    unique: true,
    description:
      "A stone statue, showing a crowned man behind a robed woman, her hair entangled in his beard. Is he sheltering her or keeping her trapped, or could her fate have been direr still? [A Piece of the Story of Ker Ys.]",
  });

  vb.defineHelpElement({
    id: "dust",
    label: "Only Dust",
    icon: "jumble",
    unique: true,
    description: "our only reward is the dust of crumpled memories.",
  });

  vb.defineHelpElement({
    id: "homebound",
    label: "The Homebound Song",
    icon: "contentment",
    description:
      "Remember home, remember birdsong. Remember the smell of  grass, remember sand scrapping your heels. Remember that small thing can be enough, even when big things are not. [This card protects me and my sailors from Horror as long as it is on the board.]",
  });

  vb.defineExplorationTask({
    taskId: "city",
    locationLabel: "Waterlogged Ruins",
    locationDescription:
      "The city is surrounded by grinding walls of water, black as ink. The only light is from the spotlight-moon above. Mother-moon casts the cobbled streets in her bleak light: pale colors, deep shade. The newly revealed doors still weep. Each of us feels in our heart, that we are trespassing on grounds hallowed, or profaned.",
    tasks: ["cityremains", "inkwall", "storiesend"],
    events: ["events.seagull", "events.specters", "events.tidepools"],
  });

  vb.defineTask({
    taskId: "cityremains",
    label: "What Remains Of The City",
    icon: "tasks/mariner.tasks.hiddenentrance.vertdegris",
    description:
      "This city is like a skeleton, salt-crusted upon the abyssal silt. Here we pass beneath the palace gaping stare, and there lies the dike-wall like a snapped spine. If we scamper like beetles, we might still find scraps preserved below the salt. ",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Scavenge The Remains",
            startdescription:
              "This will not be a matter of speed. It is a challenge to traverse the salt-sharpened streets, each step a scar. Most portcullises will yield no rewards, yet, again and again we must venture into darkness and sift for treasure with our bleeding hands.",
          },
          success: {
            label: "Something of Use",
            description:
              "We have plucked something from between the sands that has not been digested by the oceans yet. Some remnants of this town's history... There may be others.",
            deckeffects: { "mariner.vaults.sunkencity.artifacts.deck": 1 },
          },
          failure: {
            label: "Creeping Darkness",
            startdescription:
              "The work is exhausting, stressful, and in the end, we bring nothing back from the darkness beside the dark and creeping fears that wormed their way into our hearts. ",
            warmup: 30,
            linked: [
              {
                id: vb.vp("events.seagull"),
                chance: 33,
              },
              {
                id: vb.vp("events.specters"),
                chance: 50,
              },
              {
                id: vb.vp("events.tidepools"),
              },
            ],
          },
          template: probabilityChallengeTemplates.ENDURE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "inkwall",
    label: "The Ink-Wall Lure",
    icon: "tasks/mariner.tasks.haze.petroleblue",
    description:
      "The city is unnerving, like walking through a tomb, where each step leaves a crisp outline recording our trespasses. But more ominous still are the black walls of water beyond, their low roar pervasive in the back of our mind.",
    actions: {
      "mariner.sing": {
        challenge: {
          task: {
            label: "Ward of the Influence of a Drowning Power",
            startdescription:
              "I know that nameless emptiness that calls from the abyss. On my worst days, it would lead me to the ship's edge, to stare into the darkness. But through my Half-Hearts pulse I can wrench myself away from that, so too can I protect my crew from this.",
          },
          success: {
            label: "Echoes Of Home",
            description:
              "My song reverberates through the city, speaking of the lands above and the lives we left behind. The grinding waters were pressed to the background until we could hardly hear it at all.",
            effects: vb.vp({ homebound: 1 }),
          },
          failure: {
            label: "A Memory Surfaces",
            description:
              "As I try to sing, I see a shadow move in the water, black on black. My heart, that wound within my chest, turns as cold and empty as in my darkest days. A shiver travels through my body, and my voice dies in my throat.",
            effects: { "mariner.experiences.rattling.standby": 2 },
          },
          template: probabilityChallengeTemplates.WARD,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
    },
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.all"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. But I need more, the memory made history of made manifest.",
    requirements: {
      [vb.vp("gapingcup")]: -1,
      [vb.vp("links")]: -1,
      [vb.vp("pair")]: -1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.cup"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. I have found one of the memories I need.",
    requirements: {
      [vb.vp("gapingcup")]: 1,
      [vb.vp("links")]: -1,
      [vb.vp("pair")]: -1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.links"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. I have found one of the memories I need.",
    requirements: {
      [vb.vp("gapingcup")]: -1,
      [vb.vp("links")]: 1,
      [vb.vp("pair")]: -1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.pair"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. I have found one of the memories I need.",
    requirements: {
      [vb.vp("gapingcup")]: -1,
      [vb.vp("links")]: -1,
      [vb.vp("pair")]: 1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.nopair"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. I have found two pentiments of Ker Ys Narrative. One more awaits me in the darkness.",
    requirements: {
      [vb.vp("gapingcup")]: 1,
      [vb.vp("links")]: 1,
      [vb.vp("pair")]: -1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.nocup"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. I have found two pentiments of Ker Ys Narrative. One more awaits me in the darkness.",
    requirements: {
      [vb.vp("gapingcup")]: -1,
      [vb.vp("links")]: 1,
      [vb.vp("pair")]: 1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hint.nolinks"),
    label: "Find The Place Where The Story Ended?",
    startdescription:
      "Through the tales I've heard I know what happened, through the songs I learn what was felt. I have found two pentiments of Ker Ys Narrative. One more awaits me in the darkness.",
    requirements: {
      [vb.vp("gapingcup")]: 1,
      [vb.vp("links")]: -1,
      [vb.vp("pair")]: 1,
      [vb.vp("storiesend")]: 1,
    },
    actionId: "mariner.navigate",
    hintonly: true,
  });

  vb.defineTask({
    taskId: "storiesend",
    label: "Stories End",
    icon: "tasks/mariner.tasks.book.darkpurple",
    description:
      "There is an itch at the back of my skull. I can feel a hundred feet tap me just above the skin of my neck, urging me forward. I am here to sight-see, yes, but only if I see this story to its end.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Find The Place Where The Story Ended",
            startdescription:
              "Through the tales I've heard I know what happened, through the songs I learn what was felt. Now I must find where the Sorceress-Princess doomed her city, and see what awaits me there.",
            requirements: {
              [vb.vp("gapingcup")]: 1,
              [vb.vp("links")]: 1,
              [vb.vp("pair")]: 1,
            },
          },
          success: {
            label: "Theft of Sights",
            description:
              "All that I see is mine thereafter for eternity. Soon I have pilfered enough sights to reconstruct a map of the city of old. I cannot be exhausted, and I lead my crew to our next encounter.",
            effects: vb.vp({ storiesend: -1, seawall: 1 }),
            purge: { [vb.currentLayerAspectId()]: 5 },
            haltverb: {
              "mariner.haltable.horror": 1,
            },
          },
          failure: {
            label: "Wrong Turns",
            description:
              "Confidently I lead my people on, but every turn seemed to lead back on itself, and we found our own footsteps pressed on salt-crusted cobblestones leading ahead of us.",
            effects: { "mariner.experiences.unsettling.standby": 1 },
          },
          template: probabilityChallengeTemplates.PERCEIVE,
        },
        slots: CHALLENGE_SLOTS(6, "mariner.navigate"),
      },
    },
  });
}
