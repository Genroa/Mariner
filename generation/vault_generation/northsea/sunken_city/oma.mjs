export function generateOMA(vb) {
  // Quick functions to avoid copy pasting a lot of boring stuff
  function generateSongConversation(
    loreAspect,
    { previewLabel, previewDescription, label, description, effects }
  ) {
    mod.setRecipe(vb.RECIPES_FILE, {
      id: vb.vp(`oma.secondtopic.${loreAspect}.pre`),
      requirements: {
        [vb.vp("oma")]: 1,
        [`mariner.song.${loreAspect}`]: 1,
      },
      label: previewLabel,
      startdescription: previewDescription,
      linked: [{ id: vb.vp(`oma.secondtopic.${loreAspect}`) }],
    });

    mod.setRecipe(vb.RECIPES_FILE, {
      id: vb.vp(`oma.secondtopic.${loreAspect}`),
      label,
      description,
      effects: { ...(effects ?? {}), [vb.vp("oma")]: -1 },
      // spawn the gauntlet token here
      inductions: [{ id: vb.vp("gauntlet.start") }],
    });
  }
  function generateItemConversation(
    itemId,
    { previewLabel, previewDescription, label, description, effects }
  ) {
    mod.setRecipe(vb.RECIPES_FILE, {
      id: vb.vp(`oma.firsttopic.${itemId}.pre`),
      requirements: {
        [vb.vp("oma")]: 1,
        [vb.vp(itemId)]: 1,
      },
      label: previewLabel,
      startdescription: previewDescription,
      linked: [{ id: vb.vp(`oma.firsttopic.${itemId}`) }],
    });

    mod.setRecipe(vb.RECIPES_FILE, {
      id: vb.vp(`oma.firsttopic.${itemId}`),
      label,
      description,
      effects: { ...(effects ?? {}) },
      mutations: [
        {
          filter: vb.vp("oma"),
          mutate: vb.vp("oma.firsttopicdiscussed"),
          level: 1,
        },
      ],
    });
  }

  vb.defineHelpElement({
    id: "hiraezh",
    label: "Hiraezh",
    description: "The longing for that which is no longer.",
  });

  // Generation
  vb.defineTask({
    taskId: "oma",
    label: "Old Mother Anguish",
    icon: "mariner.vaults.sunkencity.oma",
    description:
      "Nations rose and empires sank, still Old Mother Anguish returned to the home she condemned. Stricken with a trifold grief for a trifold death, she wears all the scars that time and sadness can leave a woman with. Her eyes know cruelty, but her hands know mercy.",
    actions: {},
  });
  mod.getElement(vb.ELEMENTS_FILE, vb.vp("oma")).aspects[
    "modded_talk_allowed"
  ] = 1;

  mod.setHiddenAspect(vb.ASPECTS_FILE, {
    id: vb.vp("oma.firsttopicdiscussed"),
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("oma.wasted"),
    label: "Wasted Words",
    description:
      "She looks past me, back at the city and the cetacean shape which languishes at the broken gate. I should more carefully consider my words.",
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("oma.firsttopic.pre"),
    actionId: "talk",
    craftable: true,
    requirements: { [vb.vp("oma")]: 1, [vb.vp("oma.firsttopicdiscussed")]: -1 },
    warmup: 30,
    slots: [
      {
        id: "Topic",
        actionId: "talk",
        label: "First Topic",
        description:
          "I can ask the Old Mother Anguish after one of the artifacts i found here. ",
        required: {
          "mariner.vaults.help": 1,
        },
      },
    ],
    alt: [
      { id: vb.vp(`oma.firsttopic.pair.pre`) },
      { id: vb.vp(`oma.firsttopic.gapingcup.pre`) },
      { id: vb.vp(`oma.firsttopic.links.pre`) },
    ],
    label: "Parlay with a Name of the Mother",
    startdescription:
      "Do I wish to ask the Old Mother after a treasure I found?",
    linked: [{ id: vb.vp(`oma.wasted`) }],
  });
  generateItemConversation("pair", {
    previewLabel: "Parlay with a Name of the Mother",
    previewDescription: "Do I wish to ask The Old Mother after the statue?",
    label: "The One, the Two",
    description:
      'She holds the statue between two fingers but stares over our shoulder, back at the gate and what lies beyond. "We were a pair, him and me. We rose together in the Albian House. I should have known what was coming... He cared not for Glory, only Power. The Mansus is not a place for love, not even filial love."',
  });
  generateItemConversation("gapingcup", {
    previewLabel: "Parlay with a Name of the Mother",
    previewDescription: "Do I wish to ask The Old Mother after the Cup?",
    label: "The Providence, The Essurience",
    description:
      'She stares at the bowl, but does not touch it. "The Sea is always queen in towns like these. In those days that meant Her, and Her many gifts. Oh but what a gift they were! The more we worshipped, the more she gave. The more we got, the more we craved. A violent conclusion was inevitable."',
  });
  generateItemConversation("links", {
    previewLabel: "Parlay with a Name of the Mother",
    previewDescription: "Do I wish to ask the old Mother after the loveknots?",
    label: "The Love, The Lie",
    effects: { [vb.vp("etchedbands")]: 1, [vb.vp("links")]: -1 },
    description:
      'Her skin is like folded paper, but her fingers are strong. "This once meant the world to me. My love... My love, my love, my love from beyond the sea. In ages past, I\'ve wondered if it was always them, and not just that final night. It hardly matters now..." She hands the token back, acid-etched fingerprints mar the wound bands.',
  });

  // Song topics
  generateSongConversation("edge", {
    previewLabel: "Ask After Justice?",
    previewDescription: '"You wish to know about righteous retribution?"',
    label: "The Day of Reckoning",
    description:
      '"There was a break between the triple goddesses of the sea. The Two-One opposed the Tide-Queen. They wished her Vitulation to be a stillbirth, to repay past crimes and prevent worse harm. I was merely the needle; who cares if your tool bleeds? Only the Mother of Mercy."',
    effects: { "mariner.experiences.rattling.standby": 1 },
  });
  generateSongConversation("knock", {
    previewLabel: "Ask After Mercy?",
    previewDescription:
      '"You wish to know about the lessons that arise from wounds?"',
    label: "The Path Forward",
    description:
      '"She opens the path to grant access for this vigil, though it is I who decided not to pass the gates into the city again. I have no need. I travel not to remember what is lost, but what arises after. Compassion, understanding, mercy. This is not a painless path, but it is mine."',
    effects: { "mariner.experiences.unsettling.standby": 1 },
  });
  generateSongConversation("moth", {
    previewLabel: "Ask After Loss?",
    previewDescription:
      '"You wish to know about the pain caused by a loss that can never be recovered?"',
    label: "Trapped Behind Glass",
    description:
      '"Something is not truly lost until it is forgotten, but nothing that is not experienced is ever alive. Suns may set and Moons may rise, but I will never hear the songs of my hometown again. That <i>Hiraezh</i> is mine forever. I commemorate what cannot be undone."',
    effects: vb.vp({ hiraezh: 1 }),
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp(`oma.secondtopic.pre`),
    actionId: "talk",
    craftable: true,
    requirements: { [vb.vp("oma")]: 1, [vb.vp("oma.firsttopicdiscussed")]: 1 },
    warmup: 30,
    slots: [
      {
        id: "Topic",
        actionId: "talk",
        label: "Second Topic",
        description: "A Topic to discuss, represented by one of my songs.",
        required: {
          "mariner.song": 1,
        },
      },
    ],
    alt: [
      { id: vb.vp(`oma.secondtopic.edge.pre`) },
      { id: vb.vp(`oma.secondtopic.knock.pre`) },
      { id: vb.vp(`oma.secondtopic.moth.pre`) },
    ],
    label: "Talk about one Last Subject?",
    startdescription: "One more question I can ask...",
    linked: [{ id: vb.vp(`oma.wasted`) }],
  });
}
