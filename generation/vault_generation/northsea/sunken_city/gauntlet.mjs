import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../generation_helpers.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../../vault_generation.mjs";
import { RESET_TIMER } from "../../../global_timers_generation.mjs";
import { QUEST_FLAG_ID } from "../../../global_flags_generation.mjs";

export function generateGauntletLayer(vb) {
  vb.defineLayer("gauntlet");

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.start"),
    actionId: "gauntlet",
    label: "A Dismissal",
    startdescription:
      '"This tale is done now. Take it for your mistress. But know that the town may not appreciate the theft. They have so little left." She points a finger crooked like viper\'s fang. Before my head is turned, she dissolves into starlight.',
    linked: [{ id: vb.vp("gauntlet.tidesturn") }],
    purge: { "mariner.aspects.vaults.sunkencity.layer.3": 10 },
    warmup: 15,
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.tidesturn"),
    actionId: "gauntlet",
    label: "Tide Turns",
    startdescription:
      "Without Anguish, memory holds no power over reality. I can see the walls start slipping, The moon is poised to move. This intermezzo is close to ending. I must retreat to the world above, soon...",
    linked: [{ id: vb.vp("gauntlet.beast") }],
    warmup: 15,
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    actionId: "gauntlet",
    bypass: requireAndDestroy(vb.vp("pair")),
    task: {
      id: vb.vp("gauntlet.beast"),
      craftable: false,
      label: "The Beast Enraged",
      startdescription:
        "As this world is slipping away again, its king roars in challenge. It cannot abide losing everything, not again. The Blubberthing trashes and turns and threatens our way forward with its mass. [Approach this challenge with Moth or Grail.]",
    },
    chance: quantityDuoToChance("moth", "grail", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Catharsis",
      description:
        "Undulating and ululating, threshing its body in a horrific dance of grief. Its swollen form shrivels as tears and spittle fly in all directions. In the end, nothing is left besides seafoam, nothing besides its echoing wail.",
      linked: [{ id: vb.vp("gauntlet.streets") }],
      warmup: 15,
    },
    failure: {
      label: "The Press of Flesh",
      description:
        "Its beady eyes show recognition, and it honks and slobbers with great exuberance. Its lumbering form attempts to consume the statue, and our hand with it. We manage to pull away, but we will not forget it's eyes, it's touch, it's fluids.",
      effects: { "mariner.experiences.divisive.standby": 1 },
      linked: [{ id: vb.vp("gauntlet.streets") }],
      warmup: 15,
    },
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    actionId: "gauntlet",
    bypass: requireAndDestroy(vb.vp("hiraezh")),
    task: {
      id: vb.vp("gauntlet.streets"),
      craftable: false,
      label: "Spiteful Streets",
      startdescription:
        "The white-dusted cobblestones are marred with our ramblings, and our way back is not hard to find. Our footsteps have attracted the spirits of the city, which flank our passage as the grim audience to an illicit parade. [Approach this challenge with Heart or Winter.]",
    },
    chance: quantityDuoToChance("heart", "winter", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Words Granted",
      description:
        "My song has the tune of a childhood lullaby, but the words are in a tongue I do not speak, taken from memory that is not mine, from a power I do not serve.",
      linked: [{ id: vb.vp("gauntlet.ascent") }],
      warmup: 15,
    },
    failure: {
      label: "Stolen Voice",
      description:
        "It is not a dirge, nor a march, I cannot find the words, but the dead sing with rotted voices. We are tear-blinded and finger-chilled, as we reach the line back up to the ship. Our company is less than it was. The city has claimed a new voice for its choir.",
      inductions: [INJURE_CREWMEMBER],
      linked: [{ id: vb.vp("gauntlet.ascent") }],
      warmup: 15,
    },
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    actionId: "gauntlet",
    task: {
      id: vb.vp("gauntlet.ascent"),
      craftable: false,
      label: "The Ascent",
      startdescription:
        "Above us, the light is shrinking. The tunnel collapses, and the moon flees our sight. Even now the water is lapping at our feet, as the city is smothered again by a blanket of waves. [Approach this challenge with Knock or Edge.]",
    },
    chance: quantityDuoToChance("knock", "edge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "The Sea Settles",
      description:
        "As we rise, the water chases. The tunnel becomes a waterfall, and crushing weight threatens us from all sides, until the light above is needlethin and the roar is all-consuming. And then… a sudden stillness, as we emerge on the moon kissed midnight sea. ",
      linked: [{ id: vb.vp("gauntlet.talesend") }],
      warmup: 15,
    },
    failure: {
      label: "Crashing Weight",
      description:
        "The thundering waters from above slow our ascent, and the slower we travel, the more the waters cover us. The light is lost when we are still far from the surface, and we hold our breath as we hold on to line. Most of us emerge, gasping and red-eyed and bone-drenched. Most of us do. ",
      inductions: [INJURE_CREWMEMBER],
      linked: [{ id: vb.vp("gauntlet.talesend") }],
      warmup: 15,
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.talesend"),
    actionId: "gauntlet",
    label: "Tide Turns",
    startdescription:
      "The Crew re-emerged, far-eyed and exhausted. I stand at the helm, as I feel a hundred-legged shiver travel up my spine.",
    linked: [{ id: vb.vp("gauntlet.moralfound") }],
    purge: {
      "mariner.vaults.help": 10,
      "mariner.ascension.vagabond.temptation": 1,
      "mariner.ascension.twins.temptation": 1,
    },
    effects: {
      "mariner.ascension.vagabond.dedication": 1,
      "mariner.locations.northsea.sunkencity": -1,
    },
    achievements: ["mariner.achievements.hiraezh"],
    warmup: 15,
    // Set everything in place to start the med sea-wide narrative
    rootAdd: {
      [QUEST_FLAG_ID("medseacults", "started")]: 1,
      ...RESET_TIMER("medsea.studentdeath"),
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.moralfound"),
    actionId: "gauntlet",
    label: "Moral Found",
    startdescription: "As the pinpricks tap at the inside of my skull, a voice congratulates me.",
    description:
      "I now carry details of a story with me only known by the secret gods. There are more stories to find. I shall not be stayed.",
    warmup: 15,
    haltverb: {
      "mariner.haltable.ship": 1,
    },
  });
}
