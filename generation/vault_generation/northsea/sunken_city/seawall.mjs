import { INJURE_CREWMEMBER } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { SPAWN_BLEACHED_HEART_CURSE } from "../../../curses_generation/curses/bleached_heart.mjs";
import { SPAWN_PARCHED_THROAT_CURSE } from "../../../curses_generation/curses/parched_throat.mjs";
import { generateOMA } from "./oma.mjs";
import { trappingAspects } from "../../../generate_trappings.mjs";
import { CHALLENGE_SLOTS } from "../../../slots_helpers.mjs";

// EVENTS

// TASKS
export function generateDikeLayer(vb) {
  vb.defineLayer("3");

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.sunkencity.doorsplinters",
    label: "Door Slivers",
    description: "Splinters of a great gate, broken in betrayal, sanctified in mercy.",
    aspects: trappingAspects({
      value: 1,
      types: ["artifact"],
      aspects: { knock: 4 },
    }),
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.sunkencity.etchedbands",
    label: "Etched Lovelinks",
    icon: "toolwinterd",
    description:
      "A metallic knot, made of two triskelions entwining. The bands are now marred by five finger prints, left by its owner letting go.",
    aspects: trappingAspects({
      value: 3,
      types: ["artifact"],
      aspects: { moth: 2, grail: 2 },
    }),
  });

  vb.defineExplorationTask({
    taskId: "seawall",
    locationLabel: "The Broken Lock",
    locationDescription:
      "The dike encircles the city, but the barrier has been broken.\n\nsea-gate stands fractured- \ndoor-splinters white in moonlight -\nits salt-coated scars",
    tasks: ["blubberthing", "slivers", "wallsedge"],
  });

  vb.defineTask({
    taskId: "blubberthing",
    label: "The Blubberthing",
    icon: "tasks/mariner.tasks.blubberthing.vertdegris",
    description:
      "A mound lies at the edge of town. It resembles a whale, if not for the crying. It resembles a stillborn, if not for the mottled beard. It resembles a caterpillar, but it will never take its final form.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Sneak Past the Blubberthing",
            startdescription:
              "It is just lying there... writhing and moaning. Perhaps we do not have to engage at all. Perhaps we can just sneak past it, sneak out of the seagate, through the lock.",
          },
          success: {
            label: "Through the Sea Gate.",
            description:
              "While the being is large as an orca, its head is as a pea upon its body. We can easily traverse its blind corners, while  its mournful weepings cover our steps. ",
            effects: vb.vp({ oma: 1, blubberthing: -1 }),
          },
          failure: {
            label: "An Unfortunate End",
            description:
              "Just before we got halfway through, it rolled over. The screams of our backmost companion quickly get stifled by the smooth skin. His voice is lost before his arms have been crushed between the weight. The behemoth settles right on top of what remains of our companion. ",
            inductions: [INJURE_CREWMEMBER],
            effects: vb.vp({ oma: 1, blubberthing: -1 }),
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
      "mariner.sing": {
        challenge: {
          task: {
            label: "Sooth the Blubberthing",
            startdescription:
              "I loathe to approach it, but perhaps we can sooth it, just like great beasts are soothed by soft music. We just need to quiet it long enough for my crew to pass.",
          },
          success: {
            label: "Orphean Victory",
            description:
              "The thing closes its lids, but its eyes bulge so far they cannot fully close. Still... it seems contented. It’s wretched face seems more human now: old, lonely, regretful.",
            effects: vb.vp({ oma: 1, blubberthing: -1 }),
          },
          failure: {
            label: "Drowned out",
            description:
              "As soon as we approached it started shouting. Our music could not overcome its cacophony. We did not speak its language, but I fear we understood too much of his words. We are rooted in place, slowly sinking into the muck. Our heart is heavy before we regain our motor function and dash past it blubbering form.",
            effects: {
              ...vb.vp({ oma: 1, blubberthing: -1 }),
              "mariner.experiences.dreadful.standby": 1,
            },
          },
          template: probabilityChallengeTemplates.FORKED_TONGUE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
    },
  });

  vb.defineTask({
    taskId: "slivers",
    label: "The Door Slivers",
    icon: "tasks/mariner.tasks.treasure.darkpurple",
    description:
      "The Broken Lock is consecrated in triplicate: A great Gate, wounded illicitly by moonlight, to cause a cataclysmic purification. Its remnants remember.",
    actions: {
      explore: {
        challenge: {
          task: {
            label: "Gather an Ingredient of Great Power",
            startdescription:
              "If I can collect some of the wood splinters, I could put their powers to use. But to touch them is to know pain. We must endure its admonitions in the laborious process of collecting them",
          },
          success: {
            label: "A Pocket Full of Power",
            description:
              "In an inner pocket, we now carry a collection of ancient, sea-soaked wood. Underneath my clothing, my skin is beginning to ache. I expect to have been opened before sunrise.",
            effects: { [vb.vp("doorsplinters")]: 1, [vb.vp("slivers")]: -1 },
          },
          failure: {
            label: "Thousand Needle Aches",
            description:
              "We could not bear it. We crumpled to the floor like discarded poetry, as our fingers and eyes were opened and our insides flooded forth. We stain the seafloor with our liquids, but it can never be stained worse than it is.",
            alt: [SPAWN_PARCHED_THROAT_CURSE],
          },
          template: probabilityChallengeTemplates.ENDURE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  vb.defineTask({
    taskId: "wallsedge",
    label: "Walls' Edge",
    icon: "tasks/mariner.tasks.bubbles.petroleblue",
    description:
      "Like sheets of onyx, like a diaphanous mirror, like an inviting death. Here, so close to the edge, the sound of the water roars strong enough to cover movement, conversation, thought. Its emptiness calls to mine, and echoes within my Lack-Heart",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Indulge my Worse Nature",
            startdescription:
              "There is no need to go here. My objective is the city, not the sea. But I have felt a presence beyond these walls, one that has haunted me from below for all my life.. If I approach, I do so only for myself, and at great risk.",
          },
          success: {
            label: "A Promise",
            description:
              "The best way to perceive it is to close my eyes. His movements cause a ripple that leaves an ache in my bone. To my query his answer is simple. Yes, the one who isn’t here is.",
            effects: {
              "mariner.mysteries.whisperer": 1,
              "mariner.experiences.unraveling.standby": 1,
            },
          },
          failure: {
            label: "A Reminder",
            description:
              "What moves in the deep did not deign to acknowledge me. Perhaps it knows I will seek his mercy eventually. My pleas have only strengthened its hold over me, and his rejection only deepened the fractures in my soul.",
            alt: [SPAWN_BLEACHED_HEART_CURSE],
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  generateOMA(vb);
}
