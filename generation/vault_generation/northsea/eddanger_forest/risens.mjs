export function generateRisenTokens(vb) {
  const RECIPES_FILE = "recipes.vaults.eddangerforest.risens";
  const END_TEXT =
    "It stalks behind us for a moment, but soon the vinestrings draw tight, and the risen must return to court.";

  mod.initializeRecipeFile(RECIPES_FILE, ["vaults", "eddangerforest"]);

  // Loop/behaviour
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("risen.loop"),
    actionId: "mariner.haltable.risen",
    warmup: 60,
    slots: [
      {
        id: "defender1",
        label: "Defender",
        description: "Someone I commit to repel the Risen.",
        required: { "mariner.crew": 1 },
        forbidden: {
          "mariner.crew.traits.superstitious": 1,
          "mariner.crew.exhausted": 1,
        },
      },
      {
        id: "defender2",
        label: "Defender",
        description: "Someone I commit to repel the Risen.",
        required: { "mariner.crew": 1 },
        forbidden: {
          "mariner.crew.traits.superstitious": 1,
          "mariner.crew.exhausted": 1,
        },
      },
      {
        id: "defender3",
        label: "Defender",
        description: "Someone I commit to repel the Risen.",
        required: { "mariner.crew": 1 },
        forbidden: {
          "mariner.crew.traits.superstitious": 1,
          "mariner.crew.exhausted": 1,
        },
      },
    ],
    label: "The Risen Puppet Approaches",
    startdescription:
      "Its steps are silent, graceful and inhuman. Its broken-doll limbs are steered and strengthened by ivy vines, its skin protrudes with brambles. It neither breathes nor talks, but its wasp-nest chest drones. It readies to strike.",
    description: END_TEXT,
    linked: [
      { id: vb.vp("risen.freeattack") },
      { id: vb.vp("risen.hurtsdefender"), chance: "5 * [mariner.crew]" },
      {
        id: vb.vp("risen.disassembled"),
        chance: `20 + (10 * [mariner.crew] * [~/exterior : ${vb.vp(
          "insight"
        )}])`,
      },
      { id: vb.vp("risen.repelled") },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("risen.freeattack"),
    requirements: { "mariner.crew": -1 },
    warmup: 15,
    label: "It Lunges",
    startdescription:
      "No one could - or wanted to resist the sheer horror that this thing is and risk losing their life. But as we stood there, frozen by fear, it lunged towards one of us.",
    description: END_TEXT,
    linked: [{ id: vb.vp("risen.hurtsdefender") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("risen.hurtsdefender"),
    warmup: 15,
    label: "It Grabbed One",
    startdescription: "Watch out, the vines also come from this direction-",
    description: END_TEXT,
    inductions: [
      {
        id: vb.vp("hurt.crew"),
        expulsion: {
          limit: 1,
          filter: {
            "mariner.crew": 1,
          },
        },
      },
    ],
    linked: [
      {
        id: vb.vp("risen.disassembled"),
        chance: `20 + (10 * [mariner.crew] * [~/exterior : ${vb.vp(
          "insight"
        )}])`,
      },
      { id: vb.vp("risen.postloop") },
    ],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("risen.disassembled"),
    warmup: 60,
    label: "Risen Regenerating",
    startdescription:
      "We have seperated limb from limb, and tore lose the vines that bound it. But here in the Ring Yew Heartlands, they will never want for raw materials. Allready the vines are growing, allready the fallen form is renewed... Must must whatever we came here to do, and quickly. ",
    description: END_TEXT,
    linked: [{ id: vb.vp("risen.postloop") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("risen.repelled"),
    warmup: 20,
    label: "Risen Repelled",
    startdescription:
      "With a tug it is pulled away from us, towards the tree. Again its broken dancer steps circle our group.",
    description: END_TEXT,
    linked: [{ id: vb.vp("risen.postloop") }],
  });
  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("risen.postloop"),
    furthermore: [
      {
        mutations: [
          {
            filter: "mariner.crew",
            mutate: "mariner.crew.exhausted",
            level: 1,
            additive: true,
          },
        ],
      },
      {
        movements: {
          "~/tabletop": ["mariner.crew"],
        },
      },
    ],
    linked: [{ id: vb.vp("risen.loop") }],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("hurt.crew"),
    actionId: "mariner.injurecrewmember",
    linked: [
      { id: vb.vp("makesupersitious"), chance: "50 * [mariner.crew]" },
      { id: "mariner.genericevents.injurecrewmember" },
    ],
  });

  mod.setRecipe(RECIPES_FILE, {
    id: vb.vp("makesupersitious"),
    label: "A Seed of Fear",
    description:
      "A fear has found purchase in this sailor's heart, and soon it will blossom into Terror. They will still be capable, but they will not be capable of standing up to Events tinged by the Mansus or the Woods.",
    mutations: [
      {
        filter: "mariner.crew",
        mutate: "mariner.crew.traits.superstitious",
        level: 1,
      },
    ],
    linked: [{ id: vb.vp("risen.postloop") }],
  });
}
