import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../generation_helpers.mjs";
import { CHALLENGE_DIFFICULTY, generateEventChallenge, quantityDuoToChance } from "../../vault_generation.mjs";
import { SPAWN_MISFORTUNE_CURSE } from "../../../curses_generation/curses/misfortune.mjs";

export function generateGauntletLayer(vb) {
  vb.defineLayer("gauntlet");
  vb.disableTideForThisLayer();

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.start"),
    actionId: "gauntlet",
    label: "A Journey",
    description: "We have ventured beyond the verdant twilight, and now traverse the jade and copper gloom",
    linked: [{ id: vb.vp("gauntlet.path") }],
    inductions: [{ id: vb.vp("eoc.start") }],
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    task: {
      id: vb.vp("gauntlet.path"),
      craftable: false,
      label: "Pathfinding",
      startdescription:
        "This wood is like a web of verdant growth. The trees cling to eachother so closely that suns nor stars can penetrate, and the wind is stiffled to sigh. To find our way to the center we will need to rely on our instinct or our inginuity. [Approach this challenge with Forge or Moth] ",
    },
    chance: quantityDuoToChance("moth", "forge", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Onwards",
      description:
        "Deeper we go, past birches that shed their skins like bandages, past fallen leaves that russle like discarded skin. Perhaps the forest grows so densely, for grew in an attempt to catch the wind?",
      linked: [{ id: vb.vp("gauntlet.limits") }],
    },
    failure: {
      label: "Backwards",
      description: "We have found our way back to the waters' edge. That is to say, we did not find our way at all.",
      effects: { "mariner.notoriety": 1 },
    },
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    bypass: requireAndDestroy(vb.vp("laws")),
    task: {
      id: vb.vp("gauntlet.limits"),
      craftable: false,
      label: "Laws and Limits",
      startdescription:
        "Every inch of this forest is alive. Beneath our feet insect conquer and multiply, above our head birds chatter within the canopy. But folk wisdom also tells us that these parts are prowled by the hidden folk. Some in the Court of the Honey Queen crawled here from between the roots of a more Primal Wood. We better follow the rules few still remember. [Approach this challenge with Knock or Moth.]",
      inductions: [{ id: vb.vp("eoc.start") }],
    },
    chance: quantityDuoToChance("moth", "knock", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Onwards",
      description: "Deeper we go. We do not pass between gates of sloping trees, and give the mushroom circles a wide berth.",
      linked: [{ id: vb.vp("gauntlet.mare") }],
    },
    failure: {
      label: "Unwelcome",
      description:
        "It is hard to tell when, but we can feel the forest chill to our presence. Somewhere we have offended, but I am not sure how it will come to bear.",
      effects: { "mariner.notoriety": 1 },
      inductions: [SPAWN_MISFORTUNE_CURSE, { id: vb.vp("eoc.start") }],
      linked: [{ id: vb.vp("gauntlet.mare") }],
    },
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    bypass: requireAndDestroy(vb.vp("guidance")),
    task: {
      id: vb.vp("gauntlet.mare"),
      craftable: false,
      label: "Nothing is Watching Us",
      startdescription:
        "A new presence has been felt by all our party. It is not a sound, but it has the suggestion of laughter. It is no predator stalking, yet the shadows move to follow us. It is not a physical threat, but the hairs on our arms are rising. We must rid ourselves of this Unthing, before it chases us Nowhere. [Approach this challenge with Heart or Lantern.]",
      inductions: [{ id: vb.vp("eoc.start") }],
    },
    chance: quantityDuoToChance("lantern", "heart", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "Onwards",
      description:
        "Deeper we go, into the heart chambers of this ancient wood. The trees here have never seen a blade, yet they remember the taste of blood. It is not in nature of the Yew to mourn, but her family does remember.",
      linked: [{ id: vb.vp("gauntlet.end") }],
    },
    failure: {
      label: "No-One Behind Use",
      description:
        "We hurry along, watching over our shoulders, unsure what we are fearing besides the fear. Deeper into the gloom we realise there is now one pair of shoulders less, and that the air is suddenly ripe with earth-smells",
      inductions: [INJURE_CREWMEMBER],
      effects: { "mariner.experiences.unraveling.standby": 1 },
      linked: [{ id: vb.vp("gauntlet.end") }],
    },
  });

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.end"),
    label: "Into the Clearing",
    startdescription: "",
    effects: vb.vp({ edge: -1, river: -1, glade: 1 }),
    purge: { "mariner.aspects.vaults.eddangerforest.layer.1": 10 },
    haltverb: {
      "mariner.haltable.eoc": 1,
    },
    unlockCodexEntries: ["codex.hours.ringyew"],
  });

  // River Mini Gauntlet

  mod.setRecipe(vb.RECIPES_FILE, {
    id: vb.vp("gauntlet.mini.start"),
    actionId: "gauntlet",
    label: "A Riverside Journey",
    description: "With the river as both our path and our guide, we find our winding way towards the heart of this forest.",
    linked: [{ id: vb.vp("gauntlet.nokken") }],
  });

  generateEventChallenge({
    recipesFile: vb.RECIPES_FILE,
    actionId: "gauntlet",
    bypass: requireAndDestroy(vb.vp("guidance")),
    task: {
      id: vb.vp("gauntlet.nokken"),
      craftable: false,
      label: "River Secrets",
      startdescription:
        "The river shows the path, but it is just as alive and temperamental as the forest. If the old stories are true, these streams would be just the place for a nokken to lair. And if the old stories weren't true, there would be nothing for us to collect at the river's end. [Protect yourself with Lantern or Heart.]",
    },
    chance: quantityDuoToChance("lantern", "heart", CHALLENGE_DIFFICULTY.EASY),
    success: {
      label: "The Brook Babbles",
      description:
        "Here, at the river's edge, we still walk mostly in daylight. The wind gossips in the trees and the lapping waves babbles nonsense at the shore. Nothing but nature around.",
      linked: [{ id: vb.vp("gauntlet.end") }],
    },
    failure: {
      label: "From Storied depths",
      description:
        "We can never be sure we seen it, though some of us see horse heads among the white capped waves, and others imagine whinnying laughs to come beyond the reeds. What we can be sure of is the crash behind us, and the sailor that slipped into the river and never came back up.",
      linked: [{ id: vb.vp("gauntlet.end") }],
      inductions: [INJURE_CREWMEMBER],
      effects: { "mariner.experiences.dreadful.standby": 1 },
    },
  });
}
