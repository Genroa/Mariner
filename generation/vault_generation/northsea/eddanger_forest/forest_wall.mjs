import { INJURE_CREWMEMBER, requireAndDestroy } from "../../../generation_helpers.mjs";
import { probabilityChallengeTemplates } from "../../../probability_challenge_templates.mjs";
import { vaultPrefix, ASPECTS_FILE, RECIPES_FILE } from "./eddanger_forest.mjs";
import { SPAWN_BLEACHED_HEART_CURSE } from "../../../curses_generation/curses/bleached_heart.mjs";
import { trappingAspects } from "../../../generate_trappings.mjs";
import { CHALLENGE_SLOTS } from "../../../slots_helpers.mjs";

// EVENTS

// TASKS
export function generateWallLayer(vb) {
  vb.defineLayer("1");

  vb.defineHelpElement({
    id: "laws",
    label: "The Doorstep Laws",
    unique: true,
    description:
      "The Saga-tales and Fishwife Wisdoms are in accord on how to treat the Unseen Folk. The Dream-dwelling Hillfolk shall not impede me if I follow these old rules of cordiality and hospitality.",
  });

  vb.defineHelpElement({
    id: "riverkey",
    label: "River Unwound",
    unique: true,
    description: "What was looped is now uncoiled. What was a lock is now a road.",
  });

  vb.defineHelpElement({
    id: "guidance",
    label: "Guidance of Pearls",
    unique: true,
    icon: "tranquillity",
    description: "The seabreeze follows me, laces between my fingers, to remind both me and observing parties who I belong to.",
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.eddanger.resin",
    label: "Spiteful Resin",
    description:
      "Still, within its amber brilliance, a stubborn hatred for machines lies. Do not allow this close to the cogs of any machine you care about",
    aspects: trappingAspects({
      value: 2,
      types: ["natural"],
      aspects: { moth: 2 },
    }),
  });

  mod.setElement(vb.ELEMENTS_FILE, {
    id: "mariner.vaults.eddanger.machineparts",
    label: "Eager Parts",
    description: "Scraps of machines, whose movement has been halted for decades. They ache to act.",
    aspects: trappingAspects({
      value: 1,
      types: ["crafts"],
      aspects: { forge: 1 },
    }),
  });

  vb.defineExplorationTask({
    taskId: "wall",
    locationLabel: "The Forest Wall",
    locationDescription:
      "The coast here has been swallowed by the Forest. The trees weave a thick canopy right up to the waterline, dark and impenetrable.",
    tasks: ["coast", "edge", "camp", "river"],
  });

  vb.defineTask({
    taskId: "coast",
    label: "The Coast",
    icon: "tasks/mariner.tasks.shoreline.blue",
    description:
      "More familiar to me than the dark embrace of the forest, is the open expanse of sea and sky. I could take a moment to align myself to its rhythms and affirm my affiliation to the powers that guide me there.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Consider the Coast",
            startdescription:
              "The open expanse of sea and sky is more familiar to me the nthe dark embrace of the woodlands. I could take a moment, and linger on the shores to align myself to the rhythms of the waters and affirm my affiliation to its powers.",
          },
          success: {
            label: "The Sea Hungers, the Moon Protects",
            description:
              "All coastlines are crossroads, and all waves stitch the edge between land and sea. The seabreeze laces my hair and kisses my eyelashes. It will follow me in, a reminder that I am never alone.",
            effects: { [vb.vp("guidance")]: 1 },
          },
          failure: {
            label: "Unaccompanied",
            description:
              "I am aiming to travel into the realm of another power. Those that walk the waters' edge will not follow me there, not today.",
            effects: {
              "mariner.experiences.rattling.standby": 1,
              [vb.vp("coast")]: -1,
            },
          },
          template: probabilityChallengeTemplates.COMMUNE,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "edge",
    label: "The Twilight Edge",
    icon: "tasks/mariner.tasks.forestpath.green",
    description:
      "I could venture into the borderlands, but not far enough to lose my bearings. Here I would always find my way as I take my first step into those sylvan depths.",
    actions: {
      "mariner.sing": {
        challenge: {
          task: {
            label: "Implore for Passage",
            startdescription:
              "All woods are a reflection of The Wood, though some lie closer to that primordial boskiness. Here creatures hold court usually only found in lightstarved dreams. For safe travels, I should show my deference.",
          },
          success: {
            label: "Unspoken Etiquettes",
            description:
              "If I follow the old rules, if I remain respectful, if I avoid ancient hollows and the fernclad burrows… The court that crawls and climbs shall not bother me.",
            effects: { [vb.vp("laws")]: 1 },
          },
          failure: {
            label: "Audience Interrupted",
            description:
              "The words were heard, but not appreciated. The forest turns cold to me before I have even crossed it's borders proper.",
            effects: { "mariner.notoriety": 1 },
          },
          template: probabilityChallengeTemplates.PLEAD,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.sing"),
      },
      explore: {
        challenge: {
          task: {
            label: "Into the Woods",
            startdescription:
              "If I find my preparation sufficient, we can set off into the realm of the Honey Queen, and of those creatures who are rarely seen but whose presence is often felt.",
          },
          success: {
            label: "Enter the Heartlands",
            description: "We must begin the journey.",
            inductions: [{ id: vb.vp("gauntlet.start") }],
          },
          failure: {
            label: "Backwards",
            description:
              "The way was lost, the light ill-omened. We had to turn back before it got dark. I have no doubt however, that we were noticed.",
            effects: { "mariner.notoriety": 1 },
          },
          template: probabilityChallengeTemplates.ENDURE,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });

  mod.setHiddenAspect(ASPECTS_FILE, { id: `mariner.flag.forge.draw` });
  mod.setHiddenAspect(ASPECTS_FILE, { id: `mariner.flag.moth.draw` });
  mod.setHiddenAspect(ASPECTS_FILE, { id: `mariner.flag.heart.draw` });

  vb.defineTask({
    taskId: "camp",
    label: "The Resin Soaked Camp",
    icon: "tasks/mariner.tasks.tinker.forge",
    description: "Near the waters' edge lies a logging camp, which was discarded when the situation became untenable.",
    actions: {
      "mariner.navigate": {
        challenge: {
          task: {
            label: "Scavenge from battle of Sap and Scrap",
            startdescription:
              "The machines, once roaring, are now still and silent. The tree blood coated their blades and then the cockpits and then the buildings in between. Every surface is covered with muddled amber, with violent machines trapped underneath. Either could be of use to me.",
          },
          success: {
            linked: [
              {
                id: "camp.draw.forge",
                chance: 50,
                grandReqs: {
                  "root/mariner.flag.forge.draw": -1,
                },
                effects: { "mariner.vaults.eddanger.machineparts": 1 },
                furthermore: [
                  {
                    rootAdd: {
                      "mariner.flag.forge.draw": 1,
                    },
                  },
                ],
                label: "Scrap Collected",
                description:
                  "With chisels, with nails, with cunning and determination, we scrape the resin from the machines, and gather what is underneath. Now we hold machine parts, who have been aching to move for years. ",
              },
              {
                id: "camp.draw.moth",
                effects: { "mariner.vaults.eddanger.resin": 1 },
                grandReqs: {
                  "root/mariner.flag.moth.draw": -1,
                },
                furthermore: [
                  {
                    rootAdd: {
                      "mariner.flag.moth.draw": 1,
                    },
                  },
                ],
                label: "Sap Collected",
                description:
                  "Scraping, scrapping, careful and with loving care, we can remove the resin from its coating. Its hatred of machines thrums inside it still, and I better be careful for it not to touch my wristwatch.",
              },
            ],
          },
          failure: {
            label: "Stuck",
            description:
              "One of my helpers is trapped by the resin. As soon they touched it, it started spread around their arms. We managed to  ",
            mutations: [
              {
                filter: "mariner.crew",
                mutate: "mariner.crew.exhausted",
                limit: 1,
                level: 5,
                additive: true,
              },
            ],
            furthermore: [
              {
                target: "~/tabletop",
                mutations: {
                  tool: {
                    mutate: "mariner.tool.broken",
                    level: 1,
                    additive: false,
                  },
                },
              },
            ],
          },
          template: probabilityChallengeTemplates.TAMPER,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
    },
  });

  vb.defineTask({
    taskId: "river",
    label: "The River Mouth",
    icon: "tasks/mariner.tasks.landscape.petroleblue",
    description:
      "At this river mouth, the strangest waters collect from a hundred little streams, flowing from the countless secret places in the depths of the Eddanger forest.",
    actions: {
      "mariner.navigate": {
        hint: {
          label: "Understand the River",
          startdescription:
            "This stream carves shapes into the forest floor, like runes in a forgotten script. It spells out an enchantment, which it could be wise to understand before I travel down its length. I must ensure I know the Laws of this Forest before I can try to understand the script.",
          requirements: { [vb.vp("laws")]: -1 },
        },
        challenge: {
          task: {
            label: "Understand the River",
            startdescription:
              "This stream carves shapes into the forest floor, like runes in a forgotten script. I remember the Old Laws, and with that basis, I can try to parse the River.",
            requirements: { [vb.vp("laws")]: 1 },
          },
          success: {
            label: "Truth on the Tongue",
            description:
              "A single sip has revealed the truth to me. This river winds in knots for those that do not know the words. But if I hold the right ones on my tongue, it cannot bar my entry.",
            effects: { [vb.vp("riverkey")]: 1 },
          },
          failure: {
            label: "Tongue-tripped",
            description: "My attempt is clumsy, and in the end, my own soul is ensnared in the convolutions of the rivulet.",
            alt: [SPAWN_BLEACHED_HEART_CURSE],
          },
          template: probabilityChallengeTemplates.WARD,
        },
        slots: CHALLENGE_SLOTS(3, "mariner.navigate"),
      },
      explore: {
        challenge: {
          bypass: requireAndDestroy(vb.vp("riverkey")),
          task: {
            label: "Follow the River",
            startdescription:
              "This could be the easiest path into the woods. The banks are reasonably bare. This method should save us from the issues of navigating the dark and confusing woods. ",
          },
          success: {
            label: "Into the Depths",
            effects: { [vb.vp("riverkey")]: -1 },
            description:
              "The river is both our road and our guide. It will lead it right towards the deepest, most ancient parts of the woods. ",
            inductions: [{ id: vb.vp("gauntlet.mini.start") }],
          },
          failure: {
            label: "Turned Around",
            description:
              "We keep our eyes forward unwaveringly. Yet somehow, as we travel around one more river bend, we arrive right back where we started. ",
          },
          template: probabilityChallengeTemplates.AVOID,
        },
        slots: CHALLENGE_SLOTS(3, "explore"),
      },
    },
  });
}
