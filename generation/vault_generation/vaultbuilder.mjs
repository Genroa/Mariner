import { CHALLENGE_SLOTS } from "../slots_helpers.mjs";
import { defineCustomTideForVault } from "./generate_custom_tide.mjs";
import {
  generateDynamicRiskExplorationTask,
  generateExplorationTask,
  generateLayerRecipe,
  generateProbabilityChallengeTask,
} from "./vault_generation.mjs";

const _vaultPrefix = function (el) {
  if (typeof el === "string") return `mariner.vaults.${this.vaultId}.${el}`;
  if (typeof el === "object")
    return Object.fromEntries(Object.entries(el).map(([id, val]) => [`mariner.vaults.${this.vaultId}.${id}`, val]));
};

export class VaultBuilder {
  vaultId;
  portRegion;
  generic;
  currentLayer = null;
  ELEMENTS_FILE = null;
  ASPECTS_FILE = null;
  RECIPES_FILE = null;
  DECKS_FILE = null;

  layerTideManuallyDefined = false;
  layerExplorationEvents = [];

  vaultPrefix = _vaultPrefix;
  vp = _vaultPrefix;

  /**
   * Builds a new instance of the VaultBuilder. The VaultBuilder class is designed to generate vaults via a simplified syntax.
   *
   * The main tradeoff is that you must generate it in a forward order: layer 1, layer 2, layer 3. The entirety of a layer must be generated before moving on to the next. Thanks to that, the VaultBuilder remembers what the current layer is, and can handle a bunch of generation busywork for you.
   *
   * After instantiating this, you should very probably start by defining a layer via the defineLayer method.
   * @param {string} vaultId a unique vault id. Must be absolutely unique even among all the seas.
   * @param {string} portRegion the simplified sea id. Example: "northsea"
   * @param {boolean} generic is this vault a generic one? (Might not actually be used anymore by the VaultBuilder or the underlying code, but some functions still require it, so we ask it just in case it is still relevant, or becomes relevant again in the future)
   */
  constructor(vaultId, portRegion, generic) {
    this.vaultId = vaultId;
    this.portRegion = portRegion;
    this.generic = generic;

    this.generateBaseFiles();
  }

  generateBaseFiles() {
    this.ELEMENTS_FILE = `vaults.${this.vaultId}`;
    this.RECIPES_FILE = `recipes.vaults.${this.vaultId}`;
    this.ASPECTS_FILE = `aspects.vaults.${this.vaultId}`;
    this.DECKS_FILE = `decks.vaults.${this.vaultId}`;
    mod.initializeElementFile(this.ELEMENTS_FILE, ["vaults", this.vaultId]);
    mod.initializeAspectFile(this.ASPECTS_FILE, ["vaults", this.vaultId]);
    mod.initializeRecipeFile(this.RECIPES_FILE, ["vaults", this.vaultId]);
    mod.initializeDeckFile(this.DECKS_FILE, ["vaults", this.vaultId]);
  }

  currentLayerAspectId() {
    return `mariner.aspects.vaults.${this.vaultId}.layer.${this.currentLayer}`;
  }
  currentActiveLayerAspectId() {
    return `mariner.aspects.vaults.${this.vaultId}.activelayer.${this.currentLayer}`;
  }
  currentActiveExclusiveLayerAspectId() {
    return `mariner.aspects.vaults.${this.vaultId}.activeexclusivelayer.${this.currentLayer}`;
  }

  /**
   * Moves the generation to a new layer. Any next task generated will be considered part of it when it comes to activity exclusivity.
   * @param {string} layerId a layer id, unique to this vault
   * @param {number} tidePace the tide pace (warmup time of the tide while in this layer)
   */
  defineLayer(layerId, tidePace = 120) {
    this.computeCustomTideIfRequired(tidePace);

    this.currentLayer = layerId;
    this.layerTideManuallyDefined = false;
    this.layerExplorationEvents = [];
    mod.setHiddenAspect(this.ASPECTS_FILE, { id: this.currentLayerAspectId() });
    mod.setHiddenAspect(this.ASPECTS_FILE, {
      id: this.currentActiveLayerAspectId(),
    });
    mod.setHiddenAspect(this.ASPECTS_FILE, {
      id: this.currentActiveExclusiveLayerAspectId(),
    });
  }

  definePassiveHelpElement({ id, icon, label, description, aspects, lifetime, unique }) {
    mod.setElement(this.ELEMENTS_FILE, {
      id: this.vp(id),
      icon: icon,
      label,
      description: `${description}<br><br>[This card will affect some obstacles and events of this Adventure from the board.]`,
      unique,
      lifetime,
      aspects: {
        "mariner.local": 1,
        "mariner.vaults.help": 1,
        "mariner.vaults.help.passive": 1,
        ...aspects,
      },
    });
  }

  defineHelpElement(element) {
    mod.setElement(this.ELEMENTS_FILE, {
      ...element,
      id: this.vp(element.id),
      aspects: {
        ...(element.aspects ?? {}),
        "mariner.local": 1,
        "mariner.vaults.help": 1,
      },
    });
  }

  defineNonLocalElement({ id, icon, label, description, aspects, lifetime, unique }) {
    mod.setElement(this.ELEMENTS_FILE, {
      id: this.vp(id),
      icon: icon ?? "mariner.genericvaultitem",
      label,
      description,
      unique,
      lifetime,
      aspects: { ...aspects },
    });
  }

  /**
   * Generates an exploration task. It will generate the element card, and the task action itself.
   * @param {string} taskId the short id of the task. Must be unique for the vault.
   * @param {string} [icon] the icon of the card. Currently unused, use the automatic id matching feature.
   * @param {string} locationLabel the label of the card
   * @param {string} locationDescription the description of the card
   * @param {string} label the label of the action
   * @param {string} description the description of the action
   * @param {number} [lifetime] the lifetime of the action. No lifetime by default.
   * @param {number} [warmup] the warmup time of the action. 30 by default.
   * @param {string[] | string} [decayTo] the value of the decayTo property of the card. No decay value by default.
   * @param {string} [exhaustedLabel] label displayed when the action ends with no draw because of an exhausted exploration deck. If not defined, a default label will be used.
   * @param {string} [exhaustedDescription] description displayed when the action ends with no draw because of an exhausted exploration deck. if not defined, a default description will be used.
   * @param {string[]} tasks list of short task ids the exploration can find
   * @param {string[]} [events] list of short event ids the exploration can find
   * @param {string} [next] list of links to try to run immediately after running the exploration action draw, if the deck isn't empty.
   * @param {string} [preRecipe] a recipe body injected into the very first recipe of the action, executed BEFORE the warmup. Can be used to add specific requirements to the task, for instance, or purge things as soon as the action is started (this is how "wandering" explorations actually purge the table)
   * @param {string} [recipe] a recipe body injected into the recipe of the action, the one displaying the warmup. Can be used to add specific effects to the task when it completes.
   * @param {array} [additionalPreLinked] an array of links injected into the very first recipe of the action, executed BEFORE the warmup. Can be used to route to something else before even doing the exploration draw itself.
   * @param {array} [slots] an array of slots to display for this task. If not defined, the exploration card won't display any slots by default.
   * @param {boolean} [resetOnExhaustion] reset the exploration deck on exhaustion. False by default.
   * @param {boolean} [resetOnExhaustion] reset the exploration deck after each draw. False by default.
   */
  defineExplorationTask({
    taskId,
    icon,
    locationLabel,
    locationDescription,
    label,
    description,
    lifetime,
    warmup,
    decayTo,
    exhaustedLabel,
    exhaustedDescription,
    tasks,
    events,
    next,
    preRecipe,
    recipe,
    additionalPreLinked,
    slots,
    resetOnExhaustion,
    shuffleAfterDraw,
  }) {
    if (events !== undefined) this.layerExplorationEvents = [...this.layerExplorationEvents, ...events.map((id) => ({ id }))];

    mod.setElement(this.ELEMENTS_FILE, {
      id: this.vp(taskId),
      //icon: icon ?? "mariner.genericvaulttask",
      label: locationLabel,
      description: locationDescription,
      aspects: {
        "mariner.local": 1,
        [this.currentLayerAspectId()]: 1,
        modded_explore_allowed: 1,
      },
      slots,
      lifetime,
      decayTo,
    });

    generateExplorationTask({
      vaultId: this.vaultId,
      taskCompleteId: this.vp(taskId),
      taskId,
      label: label ?? `Explore ${locationLabel}`,
      description: description ?? "What can we accomplish here? And how do we advance?",
      activityAspects: { [this.currentActiveLayerAspectId()]: 1 },
      exclusionAspects: { [this.currentActiveExclusiveLayerAspectId()]: -1 },
      exhaustedLabel: exhaustedLabel ?? `${locationLabel} Explored`,
      exhaustedDescription: exhaustedDescription ?? "We've seen every inch of this place. There is nothing more for us here.",
      warmup,
      tasks: (tasks ?? []).map((id) => this.vp(id)),
      events: (events ?? []).map((id) => this.vp(id)),
      next,
      preRecipe,
      recipe,
      additionalPreLinked,
      slots,
      resetOnExhaustion,
      shuffleAfterDraw,
    });
  }

  defineDynamicRiskExplorationTask({
    taskId,
    icon,
    locationLabel,
    locationDescription,
    label,
    description,
    exhaustedLabel,
    exhaustedDescription,
    tasks,
    events,
    next,
  }) {
    if (events !== undefined) this.layerExplorationEvents = [...this.layerExplorationEvents, ...events.map((id) => ({ id }))];

    mod.setElement(this.ELEMENTS_FILE, {
      id: this.vp(taskId),
      //icon: icon ?? "mariner.genericvaulttask",
      label: locationLabel,
      description: locationDescription,
      aspects: {
        "mariner.vaults.task": 1,
        "mariner.local": 1,
        [this.currentLayerAspectId()]: 1,
        modded_explore_allowed: 1,
      },
    });

    generateDynamicRiskExplorationTask({
      vaultId: this.vaultId,
      portRegion: this.portRegion,
      generic: this.generic,
      taskCompleteId: this.vp(taskId),
      taskId,
      label: label ?? `Explore the ${locationLabel}`,
      description: description ?? "What can we accomplish here? And how do we advance?",
      activityAspects: { [this.currentActiveLayerAspectId()]: 1 },
      exclusionAspects: { [this.currentActiveExclusiveLayerAspectId()]: -1 },
      exhaustedLabel: exhaustedLabel ?? `${locationLabel} Explored`,
      exhaustedDescription: exhaustedDescription ?? "We’ve seen every inch of this place. There is nothing more for us here.",
      tasks: (tasks ?? []).map((id) => this.vp(id)),
      events: (events ?? []).map((id) => this.vp(id)),
      next,
    });
  }

  defineTask({ taskId, icon, label, description, lifetime, aspects, actions, isExclusive, slots }) {
    slots = slots ?? [];
    mod.setElement(this.ELEMENTS_FILE, {
      id: this.vp(taskId),
      icon: icon ?? "mariner.genericvaulttask",
      label,
      description,
      lifetime,
      aspects: {
        "mariner.vaults.task": 1,
        "mariner.local": 1,
        [this.currentLayerAspectId()]: 1,
        // Compute allowed verbs based on given actions
        ...this.computeAllowedVerbs(Object.keys(actions)),
        ...(aspects ?? {}),
      },
      // the array is empty but will be filled when we reach the end of the method. The magic of references, all that.
      slots: slots,
    });

    for (let [actionId, action] of Object.entries(actions)) {
      if (action.hint) {
        let hints = action.hint;
        if (!Array.isArray(action.hint)) hints = [hints];

        for (let i = 0; i < hints.length; i++) {
          const hint = hints[i];
          let hintId = hints.length === 1 ? "hint" : `hint.${hint.id ?? i + 1}`;
          mod.setRecipe(this.RECIPES_FILE, {
            actionId,
            hintonly: true,
            id: this.vp(`${taskId}.${this.toVerb(actionId)}.${hintId}`),
            label: hint.label,
            startdescription: hint.startdescription,
            requirements: { [this.vp(taskId)]: 1, ...hint.requirements },
            grandReqs: hint.grandReqs,
            tablereqs: hint.tablereqs,
            extantreqs: hint.extantreqs,
          });
        }
      }

      if (action.challenge) {
        generateProbabilityChallengeTask({
          recipesFile: this.RECIPES_FILE,
          actionId,
          bypass: action.challenge.bypass,
          activityAspects: {
            [isExclusive ? this.currentActiveExclusiveLayerAspectId() : this.currentActiveLayerAspectId()]: 1,
          },
          exclusionAspects: {
            [isExclusive ? this.currentActiveLayerAspectId() : this.currentActiveExclusiveLayerAspectId()]: -1,
          },
          task: {
            id: this.vp(`${taskId}.${this.toVerb(actionId)}`),
            ...action.challenge.task,
            requirements: {
              [this.vp(taskId)]: 1,
              ...action.challenge.task.requirements,
            },
          },
          success: action.challenge.success,
          failure: action.challenge.failure,
          template: action.challenge.template,
        });
      }

      if (action.recipe) {
        let recipes = action.recipe;
        if (!Array.isArray(action.recipe)) recipes = [recipes];

        for (let i = 0; i < recipes.length; i++) {
          const recipe = recipes[i];
          generateLayerRecipe({
            recipesFile: this.RECIPES_FILE,
            task: {
              ...recipe,
              actionId,
              id: this.vp(recipe.id),
              // TODO: By default, for now, all task actions of type "recipe" will require the task card for all the recipes
              requirements: {
                [this.vp(taskId)]: 1,
                ...recipe.requirements,
              },
            },
            activityAspects: {
              [isExclusive ? this.currentActiveExclusiveLayerAspectId() : this.currentActiveLayerAspectId()]: 1,
            },
            exclusionAspects: {
              [isExclusive ? this.currentActiveLayerAspectId() : this.currentActiveExclusiveLayerAspectId()]: -1,
            },
          });
        }
      }

      Array.prototype.push.apply(slots, this.computeTaskActionSlots(actionId, action));
    }
  }
  /**
   * Returns the slots defined by the provided action, or an array of three default challenge slots based on the provided action id (should match the action)
   * @param {string} actionId the actionId of this action
   * @param {action} action the action
   * @returns {slot[]}the list of slots
   */
  computeTaskActionSlots(actionId, action) {
    if (!action.slots) return CHALLENGE_SLOTS(3, actionId);
    return action.slots;
  }

  computeAllowedVerbs(actionIds) {
    const map = {
      "mariner.navigate": "mariner.navigate_allowed",
      explore: "modded_explore_allowed",
      "mariner.sail": "mariner.sail_allowed",
      "mariner.sing": "mariner.sing_allowed",
      talk: "modded_talk_allowed",
    };
    return Object.fromEntries(actionIds.map((actionId) => [map[actionId], 1]));
  }

  /**
   *
   * @param {string} actionId returns a simpler readable version of a verb's id. For instance: "mariner.navigate" => "navigate". Only used internally by the VaultBuilder.
   * @returns {string} the simplified verb id
   */
  toVerb(actionId) {
    const map = {
      "mariner.navigate": "navigate",
      "mariner.sing": "sing",
    };
    return map[actionId] ?? actionId;
  }

  defineCustomTide(eventsInformations, pace) {
    defineCustomTideForVault(this.vaultId, this.currentLayer, eventsInformations, pace);
    this.layerTideManuallyDefined = true;
  }

  computeCustomTideIfRequired(pace = 120) {
    if (this.layerTideManuallyDefined) return;
    if (this.layerExplorationEvents.length === 0) return;

    this.defineCustomTide(this.layerExplorationEvents, pace);
  }

  disableTideForThisLayer() {
    this.layerTideManuallyDefined = true;
  }
}
