const REWARD_FILE = "functions.drawreward";
const rewardTypes = ["precious", "jumble", "scholarly"];

export function generateSeaRewardFunctionBase() {
  mod.initializeRecipeFile(REWARD_FILE, ["rewarddecks"]);
  mod.initializeElementFile("rewarddecks.utils");
  mod.setElement("rewarddecks.utils", { id: "mariner.rewarddeck.drawhowmany", isHidden: true });
  for (const rewardType of rewardTypes) {
    mod.setRecipe(REWARD_FILE, {
      id: `mariner.drawreward.${rewardType}`,
      effects: { "mariner.rewarddeck.drawhowmany": 1 },
      linked: [{ id: `mariner.drawreward.${rewardType}.draw.*` }],
    });

    for (let i = 1; i <= 10; i++) {
      mod.setRecipe(REWARD_FILE, {
        id: `mariner.drawreward.${rewardType}.${i}`,
        effects: { "mariner.rewarddeck.drawhowmany": i },
        linked: [{ id: `mariner.drawreward.${rewardType}.draw.*` }],
      });
    }

    mod.setRecipe(REWARD_FILE, {
      id: `mariner.drawreward.${rewardType}.end`,
    });
  }
}

export function generateSeaRewards(sea, rewardsByType) {
  generateRewardDecks(sea, rewardsByType);

  for (const rewardType of rewardTypes) {
    mod.setRecipe(REWARD_FILE, {
      id: `mariner.drawreward.${rewardType}.draw.${sea}`,
      extantreqs: {
        [`mariner.locations.${sea}.sailingthere`]: 1,
      },
      furthermore: [
        {
          deckeffects: {
            [`mariner.rewarddecks.${sea}.${rewardType}`]: "mariner.rewarddeck.drawhowmany",
          },
        },
        {
          effects: {
            "mariner.rewarddeck.drawhowmany": "-20",
          },
        },
      ],
      linked: [{ id: `mariner.drawreward.${rewardType}.end` }],
    });
  }
}

function generateRewardDecks(sea, rewardsByType) {
  const DECK_FILE = `mariner.rewarddecks.${sea}`;
  mod.initializeDeckFile(DECK_FILE, ["rewarddecks"]);

  const generateRewardDeck = (type) =>
    mod.setDeck(DECK_FILE, {
      id: `${DECK_FILE}.${type}`,
      spec: rewardsByType[type] ?? ["funds"],
      resetonexhaustion: true,
    });

  generateRewardDeck("precious");
  generateRewardDeck("jumble");
  generateRewardDeck("scholarly");
}
