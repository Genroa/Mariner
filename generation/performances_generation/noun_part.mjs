export const performanceNounPart = {
  grail: {
    moth: {
      label: "Borrowed Meaning",
      description:
        "<1 One impulse from a vernal wood, I wandered lonely as a cloud <br>Upon the breathless starlit air, Hark! to the swelling nuptial song, <br>till a free soul to composedness charmed. tameless, and swift, and proud. <br>One strains with his strength at the fetter, gates of steel so strong. <br><br>Alone in chains! who shall me deliver whole <br>I have promises to keep;  like a corpse within its grave, until <br>Do with me as thou wilt; captain of my soul... <br>O Captain! my Captain, The Mariner hath his will.>",
    },
    lantern: {
      label: "Almost!",
      description:
        "The Intent of Grail holds potential. Try a more Erratic direction",
    },
    forge: {
      label: "The Ballad of the Monstrous",
      description: [
        "<what is small and spurned may spring forth to smite <br> dropped in a well, a kitten-whelp once lost <br> grew up in the darkness, growling in the dank muck <br> flint on claws whetting -iron clanging wells up <br>on shale their skin-scruff is roughed to be scale-tough",
        "through hardship find hardness, hell-scarred and strong-harmed <br>a monster made manifest, moulded in mole-space <br>but never once forgetting, the forged things throwing it nether <br>giving the gift of being beneath him <br>mighty that hand; a knight errant fleeing",
        "to go halberd clashing for holy lands castles <br>soon a retinue legging: the legion returning <br>up in this hilled earldom, upturned in upheavel <br>a dragon-catastrophe, dragged cattle to cave-lairs <br>so he vowed triple-tricemore, the vermin to vanquish",
        "and heaved with his halberd, achieving half-nothing <br>his hammer swings second which shatters to splinters <br>willing to retreat he entreats with a witch <br>she says that the monstrous a monster can slay <br>so his clan-mates then raised him razor clad mail coat",
        "so when that mole-beast molested his breastplate <br>the razors to ribbons would raze the flesh of her ribs<br>rended the tendons,  rendered tender and bleeding <br>when rivals do spill blood, who rightly can split <br>menace from mortal, monster  from men",
      ],
    },
    edge: {
      label: "Almost!",
      description:
        "The Intent of Grail holds potential. Try a colder Direction",
    },
    winter: {
      label: "Two Wade in, Two Wade out",
      description: [
        "<past the roots' edge, two children wade towards the water line <br>To depths that sing, filled with what is eternal and is old <br>Where do they journey, what did they leave behind <br>Blood still remembers, abyss dark and waters freezing cold>",
        "To depths that sing, filled with what is eternal and is old <br>as the currents rise and the waves do sway and course <br>blood still remembers, abyss dark and waters freezing cold <br>swept down below, where waters' soul knows no remorse",
        "As the currents rise and the waves do sway and course <br>air gives out and flesh gives way, we recall our kin <br>swept down below, where waters' soul knows no remorse <br>a remembered mercy, a change wrought from within",
        "Air gives out and flesh gives way, we recall our kin I <br>where do they journey, what did they leave behind <br>a remembered mercy, a change wrought from within <br>past the last roots, two children wade towards the water line",
      ],
    },
    heart: {
      label: "Almost!",
      description:
        "The Intent of Grail holds some potential. Try a bleeding direction",
    },
    knock: {
      label: "Sister-river, riversister",
      description: [
        "<great river, stretched in repose from starting source<br>to your far end. do you coil with power course <br>or swell in sorrow, ripe with spilled tears? <br>of humanity trailing wonders, rage and fears <br>do you thunder past us in a loping gait <br>or do you dawdle, look up at us and wait <br>we throng upon your shorelines, eyes cast down <br>looking for a mirror, looking where to drown?",
        "selfishly, our hearts wish to be with you <br>wild, free, languid and resplendent too <br>what could we shed upon your banks <br>sorrow, heartbreak or restraint, classes and ranks <br>at the plunge down, swaddled only by your stream <br>live as huntress, as solid as a dream <br>as river foam, as a scales silver glints <br>in murky depths, as airs' embrace rescinds <br> <br> powerful as lightning, with a panthers grace <br>not now, but one day, I'll beg for your embrace",
      ],
    },
  },
  moth: {
    grail: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    lantern: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    forge: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    edge: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    winter: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    heart: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    knock: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
  },
  lantern: {
    grail: {
      label: "No magick here!",
      description: "No magick here!",
    },
    moth: {
      label: "The Ballad to Sunset Cealia",
      description: [
        "Caelia, my great mystery<br>Do you enjoy the way you're chased by me<br>Is that why I glimpse your glances<br>At the fêtes and at the dances<br><br>Caelia I know what is said of you<br>You’re a player and a dreamer too<br>In the moonlight you will lead us on<br>But every time by sunrise you are gone",
        "Caelia please enlighten me<br>Only with you am I truly free<br>Lift this veil that clouds my being<br>Only in your eyes I'm truly seeing<br><br>Caelia’s can’t you hear I'm blue<br>I even wrote this song for you",
      ],
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    winter: {
      label: "No magick here!",
      description: "No magick here!",
    },
    heart: {
      label: "No magick here!",
      description: "No magick here!",
    },
    knock: {
      label: "No magick here!",
      description: "No magick here!",
    },
  },
  forge: {
    grail: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    moth: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    lantern: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    edge: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    winter: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    heart: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    knock: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
  },
  edge: {
    grail: {
      label: "Counting The Winds",
      description: [
        "Who runs the Winds, Who runs the Wild Hunt?<br>Who herds the clouds, who whips up the stormfronts?<br><br><br>",
        "Coarser hunts the Seawinds, the Wild Winds<br><br>Föhn!<br>Mistral!<br>Kataburan!<br>",
        "Imago sails the Trade Winds, the Scorch Winds<br><br>Sirocco!<br>No'western!<br>Harmattan!<br>",
        "Knotwing rides the Dream Winds, the Mad Winds<br><br>Doctor!<br>st Anna's!<br>Pampero!<br>",
        "The Howl stalks the Chill Winds, the Death Winds<br><br>Citanoq!<br>Boran!<br>Kamigari!<br>",
      ],
    },
    moth: {
      label: "No magick here! Almost!",
      description:
        "The Intent of Edge holds some potential. Try a Sweeter Direction",
    },
    lantern: {
      label: "No magick here! Almost!",
      description:
        "The Intent of Edge holds some potential. Try a Sweeter Direction",
    },
    forge: {
      label: "No magick here! Almost!",
      description:
        "The Intent of Edge holds some potential. Try a Sweeter Direction",
    },
    winter: {
      label: "No magick here! Almost!",
      description:
        "The Intent of Edge holds some potential. Try a Sweeter Direction",
    },
    heart: {
      label: "The Concord of Queens",
      description: [
        "A knight he marches on he marches on <br>Through the lands he marches on...<br>",
        "The stone queen speaks<br>She speaks the words of Duty<br>Of the ties that bind<br>And the lines that divide <br>And of those borders which never should be crossed at all.<br> <br>But the knight he marches on, he marches on<br>To his doom he marches on<br> ",
        "As the wild queen sings<br>She sings the whimper of desire<br>Of the whims on wings of whimsy<br>To be cherished and discarded<br>But which now hurt more than they should<br> <br> As the knight he marches on, he marches on<br> For his love he marches on<br>",
        "As the red queen speaks<br>She speaks of demands of true devotion<br>Of her aspirations for their future<br>And sacrifices now required,<br>An accord that must be made <br> <br>So the pine knight marches on he marches on <br>To a knotted noose he marches on",
      ],
    },
    knock: {
      label: "No magick here! Almost!",
      description:
        "The Intent of Edge holds some potential. Try a Sweeter Direction",
    },
  },
  winter: {
    grail: {
      label: "The Men of Wicker",
      description: [
        "Hey do you know, The man-of-willow <br>Hey have seen his his tangled hair <br>He responds when the winds do bellow <br>Those cold winds from you-know-where",
        "Hey have you seen the man-of-ivy <br>Have you felt his grasping hands<br>He creeps around with eyes that i see <br>But his eyes only see nowhere-lands",
        "Hey have you felt the man-of-brambles <br>Have you heard his bleeding laugh <br>He walks in rags and leaves in shambles <br>And separates the wheat from chaff",
        "Come won’t you join our growing garden <br>Where the men now walk no more <br>With each step their trunks would harden <br>And now we’ve fruit and bloom galore!",
      ],
    },
    moth: {
      label: "Yuki to Ame",
      description:
        "Snow hides bloodstained earth<br>Or spring showers rinse the soil<br>Both seasons cleanse me",
    },
    lantern: {
      label: "Snuff the Candle",
      description: "No magick here!",
    },
    forge: {
      label: "No magick here!",
      description: "No magick here!",
    },
    edge: {
      label: "In Their Wake",
      description: [
        "nine men, all that was left of a ruined company <br>Are trudging through a field off bodies sown <br> fatigue chackled, but still they moved perfunctory <br> seeking if they could find left any of their own<br> <br> The waning moon above hangs sickle sharp <br> reveals uniforms familiar, as if in  a dream <br> They drag three of their fellows onto a tarp <br>And haul them back 'neath that wicked gleam ",
        '"Get up boys" their captain\'s callous croak <br>The moonblade hangs a low omen in the sky <br>And the fellows stir as if they woke <br>“That dream is over now”; ragged breaths a sorrowed sigh <br>twelve men now, om the battlefields gather and take  <br> Their fallen comrades, leaving no sleepers in their wake.',
      ],
    },
    heart: {
      label: "Today We Mourn",
      description: [
        "Quiet the clocks, stops the gallows swinging<br>Shutter the windows, still the bells a-ringing<br>Today we mourn a king<br><br>Freeze the forests, break the waves to mirrors flat<br>Shroud the sky, veil the sun in pale regret<br>Today we mourn the oldest king<br>",
        "No more shall the forest rustle, no more shall the mountains quake<br> No more shall he set the reeds a shiver, no more plains shall rumble in his wake<br><br>Quell the usurpers rumble<br>Let not a single fall leave tumble<br>Today we mark a loss the world has had<br>For the thunderking is dead",
      ],
    },
    knock: {
      label: "The Dead Still Climb",
      description: [
        'At a sepulchre step, <br>A friend once waited <br>As mourners gathered <br>Who we both evaded <br><br>He asked me then <br> With eyes up high <br> "If heaven Glorious <br>lies behind the sky <br><br>Do you think the dead must climb?"',
        'Stunned for words<br>I thought a spell <br>He sought no comfort <br>I knew him well <br><br>My thoughts were dark <br>But I could not lie<br>"Death is down,<br>And no man can fly"',
        'He shook his head<br>His brow turned grim<br>As distant rose<br>The white doves\' hymn <br><br> "Life means choosing battle<br> And we choose it time and time<br>So if death is down<br>The dead still climb"',
      ],
    },
  },
  heart: {
    grail: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    moth: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    lantern: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    forge: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    edge: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    winter: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
    knock: {
      label: "No magick here!",
      description:
        "No magick has been added to the game starting with the Moth, Forge or Heart Intent. Try Knock, Winter, Edge, Lantern or Grail.",
    },
  },
  knock: {
    grail: {
      label: "Almost!",
      description:
        "The Intent of knock is filled with potential. I should change my direction to a brighter one.",
    },
    moth: {
      label: "Almost!",
      description:
        "The Intent of knock is filled with potential. I should change my direction to a fiercer one.",
    },
    lantern: {
      label: "Muse O, Muse",
      description: [
        'Muse, oh      Oh, Muse<br>will thee        thee will<br>inspire me? Alight        alight me! Inspire<br>great works of chill beauty. A      a beauty chill. Of works great,<br>Terrible and momentous choice.     choice, momentous and terrible,<br>Softly spoken "is it?"     it is spoken softly:<br>Speak not, do!      "Do not speak"',
        'Artist and art: between     Between art and artist<br>take and give: A "forms"-inspiration.   inspiration forms a give-and-take.<br>piece and muse: between what      What between muse and piece?<br>reflection is connection?   connection is reflection.<br>Robbed images.    Images robbed <br>The meaning of   of meaning. The<br>transformation is murder.   murder is transformation <br>Struck, I and,   and  I struck<br>Muse, thee,  thee, Muse.<br>shatter  Shatter,',
      ],
    },
    forge: {
      label: "The Fulcrum of Progres",
      description: [
        "The world is made of paper <br>and men's match-stick fingers <br>'Tsssschk' catch light <br>trailing burn-marks where they graze <br> each act a catastrophy <br> each embrace immolation <br>you might clasp your hands, fold 'em <br>and still scorch your own paper skin",
        "Silver-nitrate, Antimony <br> yellow bile and melancholy <br> bitter tears, red phosphorus <br> mandrake, and a hanged man's noose <br><br> refine it and extract <br>repeat it and dillute whats left <br>repeat it then dillute whats left <br> then discard whats left",
        "Oh we praise the phoenix!<br>do not mind the ash<br>glory to the world-for-us <br>forget the slag that<br>gets<br>us<br>there",
      ],
    },
    edge: {
      label: "Conqueress, Divine",
      description: "",
    },
    winter: {
      label: "Almost!",
      description:
        "The Intent of knock is filled with potential. I should change my direction to a to a more sustaining one.",
    },
    heart: {
      label: "Reverie ",
      description: "",
    },
  },
};
