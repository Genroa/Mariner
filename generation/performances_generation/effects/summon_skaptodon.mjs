import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateSkaptodonPerformance() {
  const RECIPE_FILE = "recipes.performances.skaptodon";
  const ASPECT_FILE = "aspects.performances.skaptodon";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "skaptodon"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "skaptodon"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.skaptodon" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.skaptodon",
    label: "The Call To Arms",
    startdescription:
      "Any performance to call a Skaptodon finishes with a suite of brass, and as the those tones boom, I can feel the bond forming. Somewhere in the countryside, the closest Skaptodon has been called ot my service. Perhaps he was close by allready, living a life quite unfit for war or the hunt.",
    warmup: 20,
    effects: { "mariner.crew.summon.skaptodon": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_SKAPTODON]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.skaptodon";
}
