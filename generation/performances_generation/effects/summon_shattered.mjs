import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateShatteredPerformance() {
  const RECIPE_FILE = "recipes.performances.shatteredrisen";
  const ASPECT_FILE = "aspects.performances.shatteredrisen";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "shatteredrisen"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "shatteredrisen"]);

  mod.setAspect(ASPECT_FILE, {
    id: "mariner.performanceeffects.shatteredrisen",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.shatteredrisen",
    label: "Prop work",
    startdescription:
      "It is a necessity, but undeniably unpleasant, to have the vessel allready prepared and ready at the theater. after all, the power of the Corrivality is allready primed to tear itself apart. It could be a farce, the antics that follow. The newborn risen being secreted out of these busy rooms, right as applause clatters and sycophants clamor for my attention.",
    warmup: 20,
    effects: { "mariner.crew.summon.shatteredrisen": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_SHATTERED]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.shatteredrisen";
}
