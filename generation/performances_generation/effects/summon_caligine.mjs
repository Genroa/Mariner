import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateSummonCaliginePerformance() {
  const RECIPE_FILE = "recipes.performances.summoncaligine";
  const ASPECT_FILE = "aspects.performances.summoncaligine";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "summoncaligine"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "summoncaligine"]);

  mod.setAspect(ASPECT_FILE, {
    id: "mariner.performanceeffects.summoncaligine",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.summoncaligine",
    label: "Birth-rattle",
    startdescription:
      "Music, it could be said, breaks the law of equivalent exchange, for I can ignite so many passions from my audience only expening my own. That excess will allow the Caligine into the world, Ex nihilo, sublimating on my vocal chords and then rising from my throat on the last wheezing, breathless note.",
    warmup: 20,
    effects: { "mariner.crew.summon.caligine": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_CALIGINE]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.summoncaligine";
}
