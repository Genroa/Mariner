import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateMaidPerformance() {
  const RECIPE_FILE = "recipes.performances.maid";
  const ASPECT_FILE = "aspects.performances.maid";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "maid"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "maid"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.maid" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.maid",
    label: "To Summon One's Servants",
    startdescription:
      "When my song ends, the audience leaves the room shivering, but silent, but for the drop of a tear on the ground. Just as softly as that, she will arive, like a knife behind my back, a new weapon in my arsinal.",
    warmup: 20,
    effects: { "mariner.crew.summon.maidinthemiddor": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_MAID]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.maid";
}
