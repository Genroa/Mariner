import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateXXXXPerformance() {
  const RECIPE_FILE = "recipes.performances.xxxx";
  const ASPECT_FILE = "aspects.performances.xxxx";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "xxxx"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "xxxx"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.xxxx" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.xxxx",
    label: "",
    startdescription: "",
    warmup: 20,
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_XXXX]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.xxxx";
}
