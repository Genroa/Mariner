export function generateRevealCursesPerformance() {
  const RECIPE_FILE = "recipes.performances.revealcurses";
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "revealcurses"]);

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.revealcurses",
    label: "Illuminated in the Spotlight",
    startdescription:
      "Like the Watchman, I cast two shadows in the intense theater light. Before my eyes, the second begins to shake and shiver, like a candle in a sigh. I will not forget the dancing of my shadow, nor the consequences which that implied.",
    warmup: 20,
    furthermore: [
      {
        target: "~/exterior",
        effects: { "mariner.curses.concealer": -1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.revealcurses";
}
