import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateInvitedPerformance() {
  const RECIPE_FILE = "recipes.performances.invited";
  const ASPECT_FILE = "aspects.performances.invited";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "invited"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "invited"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.invited" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.invited",
    label: "from the Depths",
    startdescription:
      "The audience is left moving home sluggishly, in a somnambulist silence. The full effects will not be clear immediately, but like shifts below the ocean floor leading soundlessly to a tsunami wave, so too will the Invited soon arrive to my feet. Perhaps also some of the audience members will find their way to the shore again as well, staring into the dark and the deep.",
    warmup: 20,
    effects: { "mariner.crew.summon.invited": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_INVITED]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.invited";
}
