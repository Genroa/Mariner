import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateQuellWeatherPerformance() {
  const RECIPE_FILE = "recipes.performances.quellweather";
  const ASPECT_FILE = "aspects.performances.quellweather";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "quellweather"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "quellweather"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.quellweather" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.quellweather",
    label: "A Languid Sky",
    warmup: 20,
    startdescription:
      "My song is only a suggestion, but I can make it a compelling one. Rest, cease, clear. I lead by example, shedding my passions and ambitions. And mercifully, the sky follows.",
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_CLEAR_WEATHER]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.quellweather";
}
