import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateStrixlingPerformance() {
  const RECIPE_FILE = "recipes.performances.strixling";
  const ASPECT_FILE = "aspects.performances.strixling";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "strixling"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "strixling"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.strixling" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.strixling",
    label: "Polarized Beacon",
    startdescription:
      "At the end of the performance, i scattler needles from my hand. Each lands on it's pin, thrilling as if it found true north beneath my feet. before I am even done, unrest nestles into the crowd. like a crawling on the back of their neck, each can feel that ill omen approaches",
    warmup: 20,
    effects: { "mariner.crew.summon.strixling": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_STRIXLING]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.strixling";
}
