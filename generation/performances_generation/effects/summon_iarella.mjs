import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateIarellaPerformance() {
  const RECIPE_FILE = "recipes.performances.iarella";
  const ASPECT_FILE = "aspects.performances.iarella";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "iarella"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "iarella"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.iarella" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.iarella",
    label: "The Call of the Hunt",
    startdescription:
      "No one will find rest tonight. Not the audience, who will chase their desires or their grievances with ravinity, nor the me, watching the water in breathless anticipation nor for the Iarella, who is lapping her way towards my song from her hunting grounds, compelled to chase my scent.",
    warmup: 20,
    effects: { "mariner.crew.summon.iarella": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_IARELLA]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.iarella";
}
