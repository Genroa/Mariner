import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateBurgeoingPerformance() {
  const RECIPE_FILE = "recipes.performances.burgeoningrisen";
  const ASPECT_FILE = "aspects.performances.burgeoningrisen";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "burgeoningrisen"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "burgeoningrisen"]);

  mod.setAspect(ASPECT_FILE, {
    id: "mariner.performanceeffects.burgeoningrisen",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.burgeoningrisen",
    label: "Transferal and transcorperation",
    startdescription:
      "The power is raised in the performance, but the tune i must carry further, into the gloomy depths of night. All the while it feels like my gums is crawling and throat is buzzing. no moment after the profanity and banality of our midnight excavation is done, do I allow the power to pass into the corpse, softly as a lovers lament.",
    warmup: 20,
    effects: { "mariner.crew.summon.burgeoningrisen": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_BURGEONING]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.burgeoningrisen";
}
