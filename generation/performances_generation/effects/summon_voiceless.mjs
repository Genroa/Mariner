import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateVoicelessRisenPerformance() {
  const RECIPE_FILE = "recipes.performances.voicelessrisen";
  const ASPECT_FILE = "aspects.performances.voicelessrisen";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "voicelessrisen"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "voicelessrisen"]);

  mod.setAspect(ASPECT_FILE, {
    id: "mariner.performanceeffects.voicelessrisen",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.voicelessrisen",
    label: "Call from the void",
    startdescription:
      "To raise a voiceless risen is different from it's counterparts, for the spirit called here is a spirit called back. it remembers life, it recalls physicality, but it has the guile to reach beyond the White Door after death. What compells a spirit to accept a vessel I do not know, but I suspect it goes beyond the power of my voice",
    warmup: 20,
    effects: { "mariner.crew.summon.voicelessrisen": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_VOICELESS_RISEN]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.voicelessrisen";
}
