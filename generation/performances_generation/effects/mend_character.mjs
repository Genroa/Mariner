import { QUEST_FLAG_ID } from "../../global_flags_generation.mjs";
import { RESET_TIMER } from "../../global_timers_generation.mjs";
import { UPGRADE_REPUTATION } from "../../port_generation/notoriety_generation.mjs";

export function generateMendingCharacterPerformance() {
  const RECIPE_FILE = "recipes.performances.mendcharacter";
  const ELEMENTS_FILE = "mendcharacter";
  const ASPECTS_FILE = "aspects.mendcharacter";
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "mendcharacter"]);
  mod.initializeElementFile(ELEMENTS_FILE, ["performances", "mendcharacter"]);
  mod.initializeAspectFile(ASPECTS_FILE, ["performances", "mendcharacter"]);

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.check",
    linked: [{ id: "mariner.performances.mendcharacter" }, { id: "mariner.performances.failure" }],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter",
    extantreqs: {
      "mariner.ascension.twins.character.uncovered": 1,
    },
    label: "The Three Queens",
    startdescription:
      "And suddenly, where before my eyes saw my cast and my rapt audience, I now could see the three Queens who we portrayed in song. The Queens bound in the sacrifice of the Flayed One. As surely as eyes can see something that isn't there, they were here, imbuing the scene, from the bundled green curtains, to the crimson river, to precipice at the edge of the stage.",
    warmup: 30,
    linked: [{ id: "mariner.performances.mendcharacter.ringyew" }],
  });

  // Forest fruit or crewmember
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.ringyew",
    warmup: 30,
    label: "The Honey Queen",
    startdescription: "The Honey Queen, all sweet and encompassing, asked for token of remembrence, to recall her former lover.",
    slots: [
      {
        id: "offering",
        label: "Remembrance",
        description: "A trinket with which two lovers tried to bind their love, though it would not last.",
        required: { "mariner.vaults.eddangerforest.treasure": 1 },
      },
    ],
    effects: { "mariner.vaults.eddangerforest.treasure": -1 },
    linked: [{ id: "mariner.performances.mendcharacter.redgrail" }],
    alt: [
      {
        id: "mariner.performances.mendcharacter.ringyew.failure",
        additional: true,
      },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.ringyew.failure",
    actionId: "mariner.injurecrewmember",
    warmup: 10,
    label: "Someone Will Have To Do",
    startdescription: "If she cannot have what is dear to her, she will content someone who is dear to me.",
    linked: [{ id: "mariner.genericevents.injurecrewmember" }],
  });

  // Grail song or reputation
  mod.setAspect(ASPECTS_FILE, {
    id: "mariner.mendcharacter.dimmedbygrail",
    label: "Dimmed By the Red Grail",
    description:
      "I have offered the very substance of this song to the Red Grail. She accepted it, dimming its power in the process.",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.redgrail",
    warmup: 30,
    label: "The Bloody Queen",
    startdescription:
      "The Bloody Queen, at the center of it all, ever hungry as rivers are, asked for to taste my rendition of her, coveting it for herself alone, to savor ambitions discarded.",
    slots: [
      {
        id: "offering",
        label: "Remembrance",
        description: "She is remembered in art, in fadou, in shanties. She covets to keep this rendition for her own.",
        required: {
          "mariner.song.grail.2": 1,
        },
      },
    ],
    linked: [
      { id: "mariner.performances.mendcharacter.redgrail.success" },
      { id: "mariner.performances.mendcharacter.hornedaxe" },
    ],
    alt: [
      {
        id: "mariner.performances.mendcharacter.redgrail.failure",
        additional: true,
      },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.redgrail.success",
    requirements: { "mariner.song.grail.2": 1 },
    furthermore: [
      {
        mutations: {
          "mariner.song.grail.2": {
            "mariner.mendcharacter.dimmedbygrail": 1,
          },
        },
      },
      {
        aspects: {
          "mariner.inspiration.downgrade": 1,
        },
      },
    ],
    linked: [{ id: "mariner.performances.mendcharacter.hornedaxe" }],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.redgrail.failure",
    actionId: "mariner.redgrailfailure",
    warmup: 10,
    label: "What a Reputation Means",
    startdescription:
      "If my songs of praise won't raise to her, whispers will seep down from her high perch in the mansus. Her many attendents will ensure they reach the right ears.",
    requirements: { "mariner.song.grail.2": -1 },
    linked: [
      {
        id: "mariner.performances.mendcharacter.redgrail.failure.dockedtolondon",
      },
      {
        id: "mariner.performances.mendcharacter.redgrail.failure.dockedtocopenhagen",
      },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.redgrail.failure.dockedtolondon",
    description: "London will be seeped in distrust for me.",
    extantreqs: { "mariner.locations.northsea.london": 1 },
    furthermore: UPGRADE_REPUTATION("northsea", "london", 4, false),
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.redgrail.failure.dockedtocopenhagen",
    description: "Copenhagen shall revile my name.",
    extantreqs: { "mariner.locations.northsea.copenhagen": 1 },
    furthermore: UPGRADE_REPUTATION("northsea", "copenhagen", 4, false),
  });

  // Horned Axe : London
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.hornedaxe",
    warmup: 30,
    label: "The Stone Queen",
    startdescription:
      "The Stone queen, at the threshold of each theater and the overture of every play, didn't ask for anything. She demanded of me an exile from my homestead, barring to me the treshold of London itself.",
    alt: [
      {
        id: "mariner.performances.mendcharacter.hornedaxeprison.start",
        additional: true,
      },
    ],
    linked: [{ id: "mariner.performances.mendcharacter.consequence" }],
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.hornedaxeprison.start",
    actionId: "mariner.hornedaxeprison",
    warmup: 120,
    label: "Exile Looming",
    startdescription:
      "All my sacrifices were binding and irreversable. The Horned Axe will soon demand I follow up with what I promised, or the consequences will be bloody. [We're about to lose access to London. If we're docked there, we need to leave NOW.]",
    linked: [
      { id: "mariner.performances.mendcharacter.hornedaxeprison.gameover" },
      { id: "mariner.performances.mendcharacter.hornedaxeprison.hold" },
    ],
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.hornedaxeprison.gameover",
    extantreqs: { "mariner.locations.northsea.london": 1 },
    warmup: 10,
    label: "Justice Looming",
    startdescription:
      "I made a vow, and in failing to uphold it, my neck was forfit. I topple, and as my vision fades the sky turns to dusk, and the clouds to bloodred smears.",
    ending: "mariner.endings.sank",
    signalendingflavour: "Melancholy",
  });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.hornedaxeprison.hold",
    warmup: 3600,
    label: "Exile",
    startdescription:
      "Even if I did not consider it so often, London was home. I have never missed it more than now that I cannot visit it.",
    slots: [
      {
        id: "offering",
        label: "Home",
        description: "My most treasured threshold.",
        required: { "mariner.locations.northsea.london.uqg": 1 },
        greedy: true,
      },
    ],
    linked: [
      {
        id: "mariner.performances.mendcharacter.hornedaxeprison.release.available",
      },
      {
        id: "mariner.performances.mendcharacter.hornedaxeprison.release.locked",
      },
    ],
  });
  // Don't do anything. It was grabbed while in the north sea, and is released as an available destination.
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.hornedaxeprison.release.available",
    extantreqs: { "mariner.locations.northsea.sailingthere": 1 },
    label: "Path Open Again",
    description: "The Stone Queen Ban has been lifted.",
  });
  // Turn it into a locked destination before releasing it.
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.hornedaxeprison.release.locked",
    label: "Ban Lifted",
    description: "The Stone Queen Ban has been lifted. I can return back to London.",
  });

  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.mendcharacter.consequence",
    warmup: 20,
    label: "An Unthinkable Decision, Remembered",
    startdescription:
      "The city will be the stage, the offerings, the agreement. The fateful audience of it all, the sacrificed Dancers. Such is the price to be paid to mend oneself under the accord of the Queens.",
    description:
      "No one will ever say what it truly was. Did people flay each other's skin, or were they bloody demons wearing human pelts? Nevertheless, they danced. They danced, walking out of the theater and down the streets, in the port, then down the canal. For what felt like an eternity, an unceasing nightmare had taken life, until a moment later, the music stopped. There was no one left to even remember the melody, for all who had heard it disappeared at the bottom of the now burgundy waters.",
    purge: {
      "mariner.ascension.vagabond.temptation": 1,
      "mariner.ascension.twins.temptation": 1,
    },
    effects: {
      "mariner.ascension.twins.dedication": 1,
    },
    // Set everything in place to start the med sea-wide narrative
    rootAdd: {
      ...RESET_TIMER("medsea.studentdeath"),
      [QUEST_FLAG_ID("medseacults", "started")]: 1,
    },
    achievements: ["mariner.achievements.charactermended"],
    updateCodexEntries: {
      "codex.ascensions.constellate.character": {
        add: ["codex.ascensions.constellate.character.mended"],
      },
    },
  });

  return "mariner.performances.mendcharacter.check";
}
