import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateCousinPerformance() {
  const RECIPE_FILE = "recipes.performances.cousin";
  const ASPECT_FILE = "aspects.performances.cousin";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "cousin"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "cousin"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.cousin" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.cousin",
    label: "That Reunion Imminant",
    startdescription:
      "Our call will echo from the concert, resounding in the audience ear and will reverberate between the trees and the hollows, and our distant, distant kin will hear my call and make it way to once again familiaize themselves with our side of the family.",
    warmup: 20,
    effects: { "mariner.crew.summon.cousinscousin": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_COUSIN]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.cousin";
}
