import { SIGNALS } from "../../generate_signal_aspects.mjs";

export function generateDidumosPerformance() {
  const RECIPE_FILE = "recipes.performances.didumos";
  const ASPECT_FILE = "aspects.performances.didumos";
  mod.initializeAspectFile(ASPECT_FILE, ["performances", "didumos"]);
  mod.initializeRecipeFile(RECIPE_FILE, ["performances", "didumos"]);

  mod.setAspect(ASPECT_FILE, { id: "mariner.performanceeffects.didumos" });
  mod.setRecipe(RECIPE_FILE, {
    id: "mariner.performances.didumos",
    label: "A Lullaby and a Lure",
    startdescription:
      "The performance does not end in the theater, though it's continuation is not advertized. Those senstive in the theater find me afterwards, under the open sky, as we open our throats to the sky.In the infinate revolutions of the night sky, something turns our way. ",
    warmup: 20,
    effects: { "mariner.crew.summon.didumos": 1 },
    furthermore: [
      {
        target: "~/exterior",
        aspects: { [SIGNALS.PERFORMANCE_SUMMON_DIDUMOS]: 1 },
      },
    ],
    achievements: ["mariner.achievements.firstsuccessfulperformance"],
  });
  return "mariner.performances.didumos";
}
