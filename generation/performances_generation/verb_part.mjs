export const performanceVerbPart = {
  grail:
    "I reach for a finale with all the passion I can muster. As my cheeks flush, so does the audience.",
  moth: 
    "I slink low, back bent and knuckling the stage. My tonal range seeks to find the edges of humanity, and my melodies will beat within the skull.",
  lantern:
    "My eyes are closed now, but the bright stage lights burn through as if they are diaphenous. I feel as if I am wholy clear, only an image of light and music.",
  forge:
    "The air is shivering under my shouts, my songs, my revolutions. Each audience member can feel the sweat pool on their back and their their fingers itch for action.",
  edge: 
    "The audience is my army, their muscles clenched with summoned passion.",
  winter:
    "These performances have two outcomes, I have been told. Some audience members weep silently, but many feel numb, as if their soul is smothered by the white weight of snow.",
  heart:
    "Halfway through the performance, the first audience member joined in with the chorus. Now, we all sing with one voice, with no edge between one voice and the next.",
  knock:
    "I play until my fingers bleed, my eyes water and my tongue feels close to tearing. I can see the hundred pairs of eyes staring at me; opened wide, open doors.",
};
