export const performanceVerbHints = {
    grail: {
        label: "Hint: A Charming Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Grail."
    },
    moth: {
        label: "Hint: A Confounding Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Moth."
    },
    lantern: {
        label: "Hint: A Revealing Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Lantern."
    },
    forge: {
        label: "Hint: A Revolutionary Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Forge."
    },
    edge: {
        label: "Hint: A Rousing Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Edge."
    },
    winter: {
        label: "Hint: A Quieting Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Winter."
    },
    heart: {
        label: "Hint: A Sustaining Performance: Not Enough!",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Heart."
    },
    knock: {
        label: "Hint: A Beckoning Performance: Not Enough",
        description: "There is something here, but I cannot quite reach it. If I could stage a performance with a stronger tie to Knock."
    }
}