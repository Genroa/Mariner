export const performanceVerbPreview = {
    grail: "If I start with a with a sensual movement, I can enrapture the audience, as well as further targets.",
    moth: "I could started with movements which recall the Old Woods, and the things mortal kin pretend to have forgotten. passion struggling with reason, it confounds my audience, as well as further targets.",
    lantern: "I can start with soaring notes, clear as bells, clear as a summer sky. It would be a reveleation upon my audience, as well as to any further subjects. ",
    forge: "I may open with the unmerciful beat of progress, with notes that spark like tinder. the audience shifts in their seats and seek to change their lives, and that energy I can apply to other projects too.",
    edge: "I could start in the dark, catching my audience in surprise. These furious rhytms will arouse their passions, as well as that of any other target I may seek. ",
    winter: "I could plan a performance where the theater goes completely quiet. if the audience shivers or weeps, they dare not do so out loud.  With this performance I can quiet one of the stains upon the canvas of the world.",
    heart: "This performance will not be complete before the audience joins in. together we can sustain the energy of this show until the stars flicker out again, and we can sustain whatever other things I may desire too ", 
    knock: "With notes that play that pierce the heart, mind and soul as once, I can summon forth emotion from the audience which they had locked in secret places. It can be used to summon the things from the fraying edges of the world to me as well. ",
}