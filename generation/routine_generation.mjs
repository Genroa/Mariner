import { SIGNALS } from "./generate_signal_aspects.mjs";
import { ME_RECIPE } from "./generation_helpers.mjs";

export const SECONDS = 1000;

export function ROUTINE_ID(elementId, shortId) {
  return `mariner.routines.${elementId}.${shortId}`;
}

function generateRoutineLogic(elementId, logicType, routine, routines, recipe) {
  const RECIPE_FILE = `recipes.routines.${elementId}`;
  if (typeof recipe === "function") {
    recipe = recipe(elementId, routine, routines);
  }
  mod.setRecipe(RECIPE_FILE, {
    ...recipe,
    id: `mariner.${logicType}.${routine.id}.${recipe.id}`,
  });
  return `mariner.${logicType}.${routine.id}.${recipe.id}`;
}

export function generateRoutineState(
  elementId,
  {
    id,
    isHidden,
    label,
    description,
    rate = 60,
    morpheffects,
    update,
    startMorphEffects,
    start,
    stopMorphEffects,
    stop,
    unavailable,
    aspects = {},
  },
  routines
) {
  const routine = {
    id,
    isHidden,
    label,
    description,
    rate,
    morpheffects,
    update,
    startMorphEffects,
    start,
    stopMorphEffects,
    stop,
    unavailable,
    aspects,
  };
  const ELEMENT_FILE = `aspects.routines.${elementId}`;
  const RECIPE_FILE = `recipes.routines.${elementId}`;
  // A routine is an aspect, associated with a timer, executing a recipe when it triggers.
  if (!mod.aspects[ELEMENT_FILE]) mod.initializeAspectFile(ELEMENT_FILE, ["routines"]);
  if (!mod.recipes[RECIPE_FILE]) mod.initializeRecipeFile(RECIPE_FILE, ["routines"]);

  const applyAspect = (aspectId, level = 1, additive = false) => ({
    filter: `[~/token : ${elementId}]`,
    mutate: aspectId,
    level,
    additive,
  });

  const removeAspect = (aspectId, level = 0, additive = false) => ({
    filter: `[~/token : ${elementId}]`,
    mutate: aspectId,
    level,
    additive,
  });

  // Generate a default start recipe if unavailable
  const addedAspects = Object.entries(routine.aspects).map(([aspectId, quantity]) => applyAspect(aspectId, quantity, true));
  if (routine.unavailable) addedAspects.push(applyAspect("mariner.localelement.unavailable"));

  if (!start && addedAspects.length > 0) {
    start = (elementId, currentRoutine, routines) => {
      return {
        id: `startroutine`,
        mutations: addedAspects,
      };
    };
  }
  // Inject the unavailable mutation if there is a start recipe but routine has unavailable
  else if (Array.isArray(start) && start.length > 0) {
    const firstRecipe = start[0];
    start[0] = (elementId, currentRoutine, routines) => {
      const recipe = generateRoutineLogic(elementId, "startroutine", currentRoutine, routines, firstRecipe);
      recipe.mutations = [...(recipe.mutations ?? []), ...addedAspects];
    };
  } else if (start) {
    start.mutations = [...(recipe.mutations ?? []), ...addedAspects];
  }

  // Generate a default stop recipe if unavailable
  const removedAspects = Object.entries(routine.aspects).map(([aspectId, quantity]) => removeAspect(aspectId, -quantity, true));
  if (routine.unavailable) removedAspects.push(removeAspect("mariner.localelement.unavailable"));

  if (!stop && removedAspects.length > 0) {
    stop = (elementId, currentRoutine, routines) => {
      return {
        id: `stoproutine`,
        mutations: removedAspects,
      };
    };
  }
  // Inject the unavailable mutation if there is a stop recipe but routine has unavailable
  else if (Array.isArray(stop) && stop.length > 0) {
    const firstRecipe = stop[0];
    stop[0] = (elementId, currentRoutine, routines) => {
      const recipe = generateRoutineLogic(elementId, "stoproutine", currentRoutine, routines, firstRecipe);
      recipe.mutations = [...(recipe.mutations ?? []), ...removedAspects];
    };
  } else if (stop) {
    stop.mutations = [...(recipe.mutations ?? []), ...removedAspects];
  }

  // Start routine
  let defaultStartRecipeId = null;
  if (Array.isArray(start)) {
    for (const recipe of start) {
      const recipeId = generateRoutineLogic(elementId, "start", routine, routines, recipe);
      if (defaultStartRecipeId === null) defaultStartRecipeId = recipeId;
    }
  } else if (start) defaultStartRecipeId = generateRoutineLogic(elementId, "start", routine, routines, start);

  // Stop routine
  let defaultStopRecipeId = null;
  if (Array.isArray(stop)) {
    for (const recipe of stop) {
      const recipeId = generateRoutineLogic(elementId, "stop", routine, routines, recipe);
      if (defaultStopRecipeId === null) defaultStopRecipeId = recipeId;
    }
  } else if (stop) defaultStopRecipeId = generateRoutineLogic(elementId, "stop", routine, routines, stop);

  // Update routine

  // If no logic provided, generate a dummy update recipe
  if (!update) {
    update = { id: `update` };
  }
  let defaultUpdateRecipeId = null;
  if (Array.isArray(update)) {
    for (const recipe of update) {
      const recipeId = generateRoutineLogic(elementId, "update", routine, routines, recipe);
      if (defaultUpdateRecipeId === null) defaultUpdateRecipeId = recipeId;
    }
  } else defaultUpdateRecipeId = generateRoutineLogic(elementId, "update", routine, routines, update);

  mod.setAspect(ELEMENT_FILE, {
    id: ROUTINE_ID(elementId, id),
    label,
    description,
    isHidden,
    timers: {
      updateroutine: {
        frequency: rate * SECONDS,
        morpheffects: morpheffects ?? [ME_RECIPE(defaultUpdateRecipeId)],
      },
    },
    xtriggers: {
      ...(startMorphEffects ?? defaultStartRecipeId
        ? {
            [SIGNALS.START_ROUTINE]: startMorphEffects ?? [ME_RECIPE(defaultStartRecipeId)],
          }
        : {}),
      ...(stopMorphEffects ?? defaultStopRecipeId
        ? {
            [SIGNALS.STOP_ROUTINE]: stopMorphEffects ?? [ME_RECIPE(defaultStopRecipeId)],
          }
        : {}),
    },
  });
}

export function swapToAnotherRoutine(routineId) {
  return (elementId, currentRoutine, routines) => {
    const nextRoutine = routines.find((r) => r.id === routineId);
    return {
      id: `swaptoroutine.${routineId}`,
      furthermore: [
        {
          target: `~/exterior`,
          aspects: { [SIGNALS.STOP_ROUTINE]: 1 },
        },
        {
          target: `~/exterior`,
          mutations: [
            {
              filter: `[~/token : ${elementId}]`,
              mutate: ROUTINE_ID(elementId, currentRoutine.id),
              level: 0,
              additive: false,
            },
            {
              filter: `[~/token : ${elementId}]`,
              mutate: ROUTINE_ID(elementId, nextRoutine.id),
              level: 1,
              additive: false,
            },
          ],
        },
        {
          target: `~/exterior`,
          aspects: { [SIGNALS.START_ROUTINE]: 1 },
        },
      ],
    };
  };
}

export function randomlySwapToAnotherRoutine(routineIdsAndProbability, uniqueId) {
  uniqueId = (uniqueId && `.${uniqueId}`) ?? "";
  return [
    (elementId, currentRoutine, routines) => {
      return {
        id: `swaptorandomroutine${uniqueId}`,
        linked: Object.entries(routineIdsAndProbability).map(([routineId, chance]) => ({
          chance,
          id: `mariner.update.${currentRoutine.id}.swaptorandomroutine.${routineId}`,
        })),
      };
    },
    ...Object.keys(routineIdsAndProbability).map((routineId) => {
      return (elementId, currentRoutine, routines) => {
        const nextRoutine = routines.find((r) => r.id === routineId);
        return {
          id: `swaptorandomroutine.${routineId}`,
          furthermore: [
            {
              target: `~/exterior`,
              aspects: { [SIGNALS.STOP_ROUTINE]: 1 },
            },
            {
              target: `~/exterior`,
              mutations: [
                {
                  filter: `[~/token : ${elementId}]`,
                  mutate: ROUTINE_ID(elementId, currentRoutine.id),
                  level: 0,
                  additive: false,
                },
                {
                  filter: `[~/token : ${elementId}]`,
                  mutate: ROUTINE_ID(elementId, nextRoutine.id),
                  level: 1,
                  additive: false,
                },
              ],
            },
            {
              target: `~/exterior`,
              aspects: { [SIGNALS.START_ROUTINE]: 1 },
            },
          ],
        };
      };
    }),
  ];
}
