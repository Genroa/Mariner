import { LORE_ASPECTS } from "./generation_helpers.mjs";

/**
 *
 * @param mod the mod object
 * @param aspect the aspect of the ritual
 *
 */

function generateRitualRecipesOfAspect(loreAspect, ritualsData) {
  const songLoreAspectId = `mariner.song.${loreAspect}`;
  const loreRitualRecipesFile = `recipes.sing.ritual.${loreAspect}`;

  mod.initializeRecipeFile(loreRitualRecipesFile, ["rituals"]);
  //console.log("Called");
  // hint
  mod.setRecipe(loreRitualRecipesFile, {
    actionId: "mariner.sing",
    id: `mariner.sing.ritual.${loreAspect}.hint.missing.trapping`,
    label: "Rush A Ritual for Quick Power",
    startdescription:
      "If I bring the right trappings to a place with the right ambiance, I could perform a ritual to give myself great power. The actions I'd take might be...peculiar, and raise the suspicion of the general public.",
    hintOnly: true,
    requirements: {
      "mariner.tune": 1,
      [`mariner.tune.${loreAspect}`]: 1,
      "mariner.trapping": -1,
    },
  });

  // 1. start recipe
  mod.setRecipe(loreRitualRecipesFile, {
    actionId: "mariner.sing",
    id: `mariner.sing.ritual.${loreAspect}.start`,
    label: ritualsData.label,
    startdescription:
      "I have brought my implements, and the atmosphere is charged and ready.",
    craftable: true,
    linked: [{ id: `mariner.sing.ritual.success.${loreAspect}` }],
    requirements: {
      "mariner.tune": 1,
      [`mariner.tune.${loreAspect}`]: 1,
      [`mariner.trappings.${loreAspect}`]: 1,
    },
  });

  // 2. Success recipe
  mod.setRecipe(loreRitualRecipesFile, {
    id: `mariner.sing.ritual.success.${loreAspect}`,
    label: ritualsData.label,
    startdescription: ritualsData.description,
    description: ritualsData.descriptionEnd,
    warmup: 30,
    effects: {
      [`mariner.influences.${loreAspect}`]: 5,
      "mariner.notoriety": 1,
      [`mariner.tune.${loreAspect}`]: -1,
      [`mariner.trappings.${loreAspect}`]: -1,
    },
  });
}

export function generateRituals() {
  const ritualsData = {
    knock: {
      label: "Perform a covert Knock Ritual in a Holy Space",
      description:
        "Seven incisions are made in the snake skin. Seven lockpicks are pulled through. Seven drops of blood are dapped on the teeth.",
      descriptionEnd:
        "Music brightens, Influence blossoms. As the ritual finishes, all doors and windows slam open at once. New understanding follows in my wake.",
    },
    moth: {
      label: "Perform a covert Moth Ritual in a Nocturnal Space",
      description:
        "Each of the treasures I have brought with me must be left somewhere unexpected. Flowers behind the bar, shed carapace in an unattended drink, the poppet left in the gangster pocket.",
      descriptionEnd:
        "Music brightens, Influence blossoms. For a moment I lose my language, my sight, my reason. My faculties return, but now tantalizing secrets hide in the fuzzy secrets.",
    },
    lantern: {
      label: "Perform a covert Lantern Ritual in a Lofty Space",
      description:
        "This ritual requires candlelight and mirrors and muttered prayers. I must be careful not to shine the light in anyone's eyes but my own.",
      descriptionEnd:
        "Influence blossoms, music brightens. My soul aligns with the sun and stars in a secret constellation. Power arises from my footsteps.",
    },
    forge: {
      label: "Perform a covert Forge Ritual in an Industrious Space",
      description:
        "What I have brought is only part of the ritual. Below the iron and the ash, I need to bury an object fresh from the forge.",
      descriptionEnd:
        "Music brightens, Influence blossoms. An errant raindrop sizzles on my skin. I take care not to touch strangers, for fear their flesh will blister.",
    },
    edge: {
      label: "Perform a covert Edge Ritual in a Competitive Space",
      description:
        "There are symbols to draw on significant body parts: the forehead, the shoulder blade, the left hand. The knives are only symbolic, but if the ritual takes effect, having a weapon close might prove prudent.",
      descriptionEnd:
        "Music brightens, Influence blossoms. It reverberates from me like unheard echoes, cats hiss and voices rise in anger.",
    },
    winter: {
      label: "Perform a covert Winter Ritual in a Silenced Space",
      description:
        "The tears that anoint the bones need not be my own, but it must be my hand that buries them in the earth, and it must happen in perfect silence. ",
      descriptionEnd:
        "Music brightens, Influence blossoms. As I layer on the final crumbs on earth, they crisp with frost. All birdsong halts, besides the distant cooing lamentation of the doves.",
    },
    heart: {
      label: "Perform a covert Heart ritual in a Sleepless Space",
      description:
        "My dance steps will seem unorthodox to the crowd, but  I must truly beware if they see the organs within my dripping hands.",
      descriptionEnd:
        "Music brightens, Influence blossoms. My feet strain to keep up with my racing heartbeat. All around me, the dancers begin to follow my rhythms too.",
    },
    grail: {
      label: "Perform a covert Grail ritual in a Tempting Space",
      description:
        "What I must consume in this ritual is not considered appetizing by most, so I can expect attention from strangers whether I complete the ritual successfully or am interrupted",
      descriptionEnd:
        "Music brightens, Influence blossoms. As I take the last morsel in my mouth and swallow, a shiver passes through the room, as all eyes strain to catch a glimpse of me.",
    },
  };
  //console.log("aspects", LORE_ASPECTS);
  for (const loreAspect of LORE_ASPECTS)
    generateRitualRecipesOfAspect(loreAspect, ritualsData[loreAspect]);
}
