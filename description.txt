[h1][i]"I am not complete. There is some essence missing from the center of my being. I ache, and therefore I wander. I search for a place where the sky is technicolor, and for my missing soul."[/i][/h1]

[i]"Not all of stories of the Mansus and its hours are hidden in stuffy books and forbidden libraries. Many truths live on in the tales of the marketmongers, the songs of the sailors and the hearts of artists. If one is perceptive enough to find them...[/i]


In The Mariner Legacy you are not a scholar, but a different form of Adept. Yours are the Intuitive Arts, the power of Story and Song. You travel the world on your ship 'The Kite', searching for the wonders that hide beneath its surface, from the truths hidden behind the words. One Hour may whisper secrets in your ear, while others tug at your heartstrings. Which mysteries will you pursue? Which places will you discover? What Songs will you compose?

Which destiny shall you fulfill?

[h1]DISCLAIMER: This is an OPEN BETA for the first part of the game: The North Sea. Turned out the world is very large, and we will be adding more seas to explore piecemeal. (Can you guess how many we will add in the end?) The game is not yet polished, not yet finished, not even fully spellchecked. This limited release is here to collect feedback, and because we felt we finally had to release something, after a year of constant work on it. You will encounter broken things at some point. It is lacking a lot of content, even for the north sea, but about 70% of the game's systems are implemented in some form or another.[/h1]

The Mariner is a Mod for the game Cultist Simulator, introducing a new legacy with unique mechanics, themes, and victory conditions, but still taking place within the (dubious) canon of the Secret Histories.

[h2]The AchieveIntegrations mod is required (subscribed AND enabled in the Serapeum menu) to player Mariner.[/h2]

[h2]Features[/h2]
[list]
[*] [b]A new set of verbs[/b] replacing all the ones from the base game, with a ton of new gameplay mechanics. In fact, most of the base game was removed or reworked in major ways. Mariner will be as different from the base game and Exile as Exile is from the base game. 
[*] [b]No Cult founding, no settling[/b]. You travel between cities with your crew and visit vaults alongside your following.
[*] [b]A more plot-driven experience[/b], which will eventually include seven seas and multiple paths towards ascension.
[*] A gameplay more focused on [b]exploration, management and collecting stories[/b], in accordance with the Vagabond and the Twins. Think of it as an anti-Exile.
[*] [b]A reworked reputation system[/b]: being wanted by the authorities in some port doesn't mean you can't do business somewhere else. Tread carefully and balance when to push your luck within a city and when to turn tail and try your luck elsewhere.
[*] Hundreds of new elements and aspects, specific to this legacy
[*] [b]240+[/b] new visual pieces
[*] [b]New custom achievements![/b] (optional, requires The Roost Machine to be installed and enabled)
[*] [b]A unique main menu and intro quotes[/b] while playing this legacy (optional, requires The Roost Machine and Fevered Imagination to be installed, enabled and in this loading order!)
[/list]

AchieveIntegrations:
https://steamcommunity.com/sharedfiles/filedetails/?id=2363532185

The Roost Machine:
https://steamcommunity.com/sharedfiles/filedetails/?id=2625527332

Fevered Imagination:
https://steamcommunity.com/sharedfiles/filedetails/?id=2700850781

[h2]Credit Where Credit Is Due[/h2]

Developers:
[list]
[*] [b]DMwithoutPCs#0270[/b] (Visscher, Jacob)
[*] [b]Genroa#9042[/b] (Blanchard, Baptiste)
[/list]

Artists: Mariner would not be nearly as pleasant on the eye or as pleasing to play without the contributions of our collaborating artists. 
[list]
[*] [b]Alice[/b]                                   (npc)
[*] [b]dyesha#8043[/b] (Insta: @dyesha.art)        (the painting of the Half-Heart)
[*] [b]EviLLs_Resurgence[/b]                       (places, tools and more)
[*] [b]The Pony™#9021[/b]                          (aspects, weathers, songs and more)
[*] [b]Queen Of Bones#261[/b]                      (npcs)
[/list]

Further Acknowledgements
[list]
[*] [b]Noicest Dungeon#1446[/b]: copy editting on Challenge Texts.
[*] My love and partner:  [b]Stlkng[/b]
[*] Support Ferrets: [b]Hadashi[/b] & [b]Mushu[/b]
[/list]
