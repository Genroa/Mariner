import { buildAdvancedRefinementBlock, buildRefinementBlock } from "./generation/generation_helpers.mjs";

function travelSegment(distance) {
  return buildAdvancedRefinementBlock(
    [
      // Full
      {
        aspect: "sailingtest.traveleddistanceinpercent",
        value: distance + 10,
        text: "<sprite name=progress_filled>",
      },
      // Kite symbol
      {
        aspect: "sailingtest.traveleddistanceinpercent",
        value: distance,
        text: "<sprite name=progress_sparrow>",
      },
    ],
    // Empty
    "<sprite name=progress_empty>"
  );
}

function travelBar() {
  let result = "<sprite name=progress_left_bracket>";
  for (let i = 0; i < 10; i++) {
    result += travelSegment(i * 10);
  }
  result += "<sprite name=progress_right_bracket>";
  return result;
}

export function generateSailingTestData() {
  mod.initializeRecipeFile("recipes.sailingtest", ["_test"]);
  mod.setRecipes(
    "recipes.sailingtest",
    {
      id: "sailingtest.setup",
      actionId: "test.sailing",
      effects: {
        "sailingtest.totaldistance": "30 * 5",
      },
      linked: "sailingtest.loop",
    },
    {
      id: "sailingtest.loop",
      label: "Sailing",
      startdescription: `<size=4px><br></size><size=18px>${travelBar()}</size><br>Hey! ${buildRefinementBlock({
        "sailingtest.traveleddistance": "$value",
      })}/${buildRefinementBlock({ "sailingtest.totaldistance": "$value" })} (${buildRefinementBlock({
        "sailingtest.traveleddistanceinpercent": "$value",
      })}%)`,
      warmup: 4,
      linked: [
        {
          id: "sailingtest.postevent",
        },
      ],
    },
    {
      id: "sailingtest.postevent",
      furthermore: [
        { effects: { "sailingtest.traveleddistanceinpercent": -100, "sailingtest.traveleddistance": 8 } },
        {
          effects: {
            "sailingtest.traveleddistanceinpercent": `([sailingtest.traveleddistance] / [sailingtest.totaldistance]) * 100`,
          },
        },
      ],
      linked: ["sailingtest.finished", "sailingtest.loop"],
    },
    {
      id: "sailingtest.finished",
      grandReqs: { "sailingtest.traveleddistance": "sailingtest.totaldistance" },
      label: "Arrived!",
    }
  );
}
